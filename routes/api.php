<?php

use Illuminate\Http\Request;
use App\Http\Middleware\Cors as cors;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


Route::group(['middleware' => \Barryvdh\Cors\HandleCors::class], function () {
    Route::get('/featured-products', 'APIController@index');
    Route::get('/categories', 'APIController@categories');
    Route::get('/subcategories', 'APIController@subcategories');
    Route::get('/search-autocomplete', 'APIController@searchAutocomplete');
    Route::get('/product-list', 'APIController@productList');
    Route::get('/product/{id}', 'APIController@product');
    Route::get('/product/description/{id}', 'APIController@productDescription');
    Route::get('/product/questions/{id}', 'APIController@productQuestions');
    Route::get('/product/shipment/{id}', 'APIController@productShipment');
    Route::post('/signin', 'APIController@signin');
    Route::get('/validate-token', 'APIController@validateToken');
});


Route::group(['middleware' => \Tymon\JWTAuth\Middleware\GetUserFromToken::class], function () {
    Route::get('/favorite', 'APIController@markOrUnmark');
    Route::get('/product/send-consultation/{id}', 'APIController@sendConsultation');
    Route::get('/product/add-question/{id}', 'APIController@addQuestion');
    Route::get('/purchases', 'APIController@purchases');
    Route::get('/purchase-detail/{id}', 'APIController@purchaseDetail');
    Route::get('/purchase-messages/{id}', 'APIController@purchaseMessages');
    Route::get('/send-message/{id}', 'APIController@sendMessage');
    Route::get('/favorites', 'APIController@favorites');
    Route::get('/buyer-questions', 'APIController@buyerQuestions');
    Route::get('/buyer-questions-list/{id}', 'APIController@buyerQuestionsList');
    Route::get('/publications', 'APIController@publications');
    Route::get('/publications-count', 'APIController@publicationsCount');
    Route::get('/publication-questions', 'APIController@publicationQuestions');
    Route::get('/answer-questions/{id}', 'APIController@answerQuestions');
    Route::get('/delete-question/{id}', 'APIController@deleteQuestion');
    Route::get('/sales', 'APIController@sales');
    Route::get('/sale-detail/{id}', 'APIController@saleDetail');
    Route::get('/sale-messages/{id}', 'APIController@saleMessages');
    Route::get('/update-fcm-token/{token}', 'APIController@updateFCMToken');
    Route::get('/confirm-purchase/', 'APIController@confirmPurchase');
    Route::get('/notifications/', 'APIController@notifications');
    Route::get('/count-notifications/', 'APIController@countNotifications');
    Route::get('/reset-notifications/', 'APIController@resetNotifications');
});


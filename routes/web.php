<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/privacy', 'HomeController@privacy')->name('privacy');
Route::get('/home', 'HomeController@index');
Route::get('/category/{category_id}', 'HomeController@category')->name('category');
Route::get('/list', 'HomeController@productList')->name('list');
Route::get('/product/{id}', 'HomeController@product')->name('product');
Route::get('/update_latest_notification_view', 'HomeController@update_latest_notification_view');

Route::get('/sell', 'SellController@index')->name('sell');
Route::get('/sell/category/{id}', 'SellController@category')->name('sell-category');
Route::post('/sell/save-category', 'SellController@saveCategory')->name('sell-save-category');
Route::get('/sell/description', 'SellController@description')->name('sell-description');
Route::post('/sell/save-description', 'SellController@saveDescription')->name('sell-save-description');
Route::post('/sell/save-image', 'SellController@saveImage')->name('sell-save-image');
Route::post('/sell/remove-image', 'SellController@removeImage')->name('sell-remove-image');
Route::get('/sell/price', 'SellController@price')->name('sell-price');
Route::post('/sell/save-price', 'SellController@savePrice')->name('sell-save-price');
Route::get('/sell/confirm', 'SellController@confirm')->name('sell-confirm');
Route::post('/sell/save-confirm', 'SellController@saveConfirm')->name('sell-save-confirm');
Route::get('/sell/list-images', 'SellController@getListOfImages')->name('sell-list-images');


Route::post('/add_question', 'HomeController@add_question');
Route::post('/send_consultation', 'HomeController@send_consultation');
Route::post('/get_questions', 'HomeController@get_questions');
Route::post('/confirm-purchase', 'HomeController@confirm_purchase');
Route::post('/get_subcategory', 'SellController@getSubcategory');

Route::get('/test/mail/{email}', 'UserController@welcome');

Route::get('/login', function() { return view('login'); });

Route::get('/logout', 'UserController@logout');

Auth::routes();

Route::get('/completereg', 'CompleteRegisterController@index');

Route::post('/completeregister', 'CompleteRegisterController@completeregister');

Route::post('/searchprovinces', 'CompleteRegisterController@searchprovinces');

Route::post('/searchdistricts', 'CompleteRegisterController@searchdistricts');

Route::post('/favorite', 'FavoritesController@mark_or_unmark');

Route::post('/delete_questions', 'AccountController@delete_questions')->name('delete_questions');
Route::post('/delete_question', 'AccountController@delete_question')->name('delete_question');

Route::get('/favorite/{id}', 'FavoritesController@mark_or_unmark_favorite')
    ->middleware('auth');

Route::get('/home', 'HomeController@index');
Route::get('/download', 'AccountController@download')->name('download');
Route::get('/download-payments', 'AccountController@download_payments')->name('download-payments');

Route::get('/summary', 'AccountController@summary');
Route::get('/favorites', 'AccountController@favorites')->name('favorites');
Route::get('/purchase-detail', 'AccountController@purchase_detail')->name('purchase_detail');
Route::get('/purchases', 'AccountController@purchases')->name('purchases');
Route::get('/myquestions', 'AccountController@myquestions')->name('myquestions');
Route::get('/profile', 'AccountController@personal_data')->name('profile');
Route::post('/modusername', 'AccountController@modify_username');
Route::post('/modpassword', 'AccountController@modify_password');
Route::post('/modemail', 'AccountController@modify_email');
Route::post('/modphone', 'AccountController@modify_phone');
Route::get('/buyer-qualification', 'AccountController@buyer_qualification')->name('buyer_qualification');
Route::post('/buyer-qualification-save', 'AccountController@buyer_qualification_save')->name('buyer_qualification_save');
Route::get('/buyer-qualification-edit', 'AccountController@buyer_qualification_edit')->name('buyer_qualification_edit');
Route::post('/buyer-qualification-edit-save', 'AccountController@buyer_qualification_edit_save')->name('buyer_qualification_edit_save');
Route::post('/qualification', 'AccountController@qualification')->name('qualification');
Route::get('/buyer-reply-edit', 'AccountController@buyer_reply_edit')->name('buyer_reply_edit');
Route::post('/buyer-reply-edit-save', 'AccountController@buyer_reply_edit_save')->name('buyer_reply_edit_save');
Route::get('/billing', 'AccountController@billing')->name('billing');
Route::get('/invoice-detail', 'AccountController@invoice_detail')->name('invoice_detail');
Route::get('/payment', 'AccountController@payment')->name('payment');
Route::get('/getInfoPayment', 'AccountController@getInfoPayment');
Route::post('/payment-save', 'AccountController@payment_save')->name('payment_save');

Route::get('/sale-detail', 'AccountController@sale_detail')->name('sale_detail');
Route::get('/sales', 'AccountController@sales')->name('sales');
Route::get('/seller-qualification', 'AccountController@seller_qualification')->name('seller_qualification');
Route::post('/seller-qualification-save', 'AccountController@seller_qualification_save')->name('seller_qualification_save');
Route::get('/seller-qualification-edit', 'AccountController@seller_qualification_edit')->name('seller_qualification_edit');
Route::post('/seller-qualification-edit-save', 'AccountController@seller_qualification_edit_save')->name('seller_qualification_edit_save');
Route::post('/qualification-seller', 'AccountController@qualification_seller')->name('qualification_seller');
Route::get('/seller-reply-edit', 'AccountController@seller_reply_edit')->name('seller_reply_edit');
Route::post('/seller-reply-edit-save', 'AccountController@seller_reply_edit_save')->name('seller_reply_edit_save');
Route::post('/save-note', 'AccountController@save_note')->name('save_note');
Route::post('/update-note', 'AccountController@update_note')->name('update_note');
Route::post('/delete-note', 'AccountController@delete_note')->name('delete_note');
Route::get('/charged', 'AccountController@charged')->name('charged');


Route::get('/publications', 'AccountController@publications')->name('publications');
Route::get('/plan', 'AccountController@plan')->name('plan');
Route::post('/save-plan', 'AccountController@save_plan')->name('save_plan');
Route::get('/pause', 'AccountController@pause')->name('pause');
Route::get('/activate', 'AccountController@activate')->name('activate');
Route::get('/finish', 'AccountController@finish')->name('finish');
Route::get('/shipping-cost', 'AccountController@shipping_cost')->name('shipping_cost');
Route::post('/shipping-cost-save', 'AccountController@shipping_cost_save')->name('shipping_cost_save');
Route::get('/change-category/{id}', 'AccountController@change_category')->name('change-category');
Route::post('/save-change-category', 'AccountController@save_change_category')->name('save-change-category');
Route::get('/modify-publication/{id}', 'AccountController@modify_publication')->name('modify-publication');
Route::post('/modify-publication-save', 'AccountController@modify_publication_save')->name('modify-publication-save');
Route::get('/questions', 'AccountController@questions')->name('questions');
Route::post('/answer-questions', 'AccountController@answer_questions')->name('answer-questions');
Route::get('/republish-publication/{id}', 'AccountController@republish_publication')->name('republish-publication');
Route::post('/republish-publication-save', 'AccountController@republish_publication_save')->name('republish-publication-save');
Route::get('/seller/request-change-qualification', 'AccountController@seller_request_change_qualification')->name('seller_request_change_qualification');
Route::get('/buyer-qualification-confirm', 'AccountController@buyer_qualification_confirm')->name('buyer_qualification_confirm');
Route::get('/buyer/request-change-qualification', 'AccountController@buyer_request_change_qualification')->name('buyer_request_change_qualification');
Route::get('/seller-qualification-confirm', 'AccountController@seller_qualification_confirm')->name('seller_qualification_confirm');
Route::get('/stock', 'AccountController@stock')->name('stock');
Route::post('/stock-save', 'AccountController@stock_save')->name('stock_save');

// Addresses
Route::get('/newaddress', 'AccountController@new_address');
Route::post('/addaddress', 'AccountController@add_address');
Route::get('/edit_address/{id}', 'AccountController@edit_address');
Route::post('/modaddress', 'AccountController@mod_address');
Route::get('/use_address_for_sales/{id}', 'AccountController@use_address_for_sales');
Route::get('/use_address_for_purchases/{id}', 'AccountController@use_address_for_purchases');
Route::get('/remove_address/{id}', 'AccountController@remove_address');
Route::post('/set_send_with_email', 'AccountController@set_send_with_email');
Route::post('/save-message', 'AccountController@save_message')->name('save_message');

// Emails
Route::get('/emails', 'AccountController@email_preferences');
Route::post('/update_emails', 'AccountController@update_email_preferences');

//Reputation
Route::get('/myreputation', 'AccountController@index');
Route::get('/reputationseller', 'ReputationController@reputation_as_seller')->name('reputationseller');
Route::get('/reputationbuyer', 'ReputationController@reputation_as_buyer')->name('reputationbuyer');

Route::get('/execute-cron', 'CronController@execute')->name('execute_cron');

Route::get('/help', 'HelpController@index')->name('help');
Route::get('/contact', 'ContactController@index')->name('contact');
Route::post('/send-contact', 'ContactController@send_contact')->name('send_contact');

Route::get('/photo/{size}', function($size = NULL){
	$name = Illuminate\Support\Facades\Input::get('url');
    if(!is_null($size) && !is_null($name)){
        $size = explode('x', $size);
        $cache_image = Image::cache(function($image) use($size, $name){
           //return $image->make($name)->fit($size[0], $size[1]);
			if(File::exists('uploads/images/products/'.$name)) {
                $output = $image->make('uploads/images/products/'.$name)->fit($size[0], $size[1]);
            } else {
                $output = $image->make('uploads/images/products/nophoto.jpg');
            }
            return $output;
        }, 10); // cache for 10 minutes

        return Response::make($cache_image, 200, ['Content-Type' => 'image']);
    } else {
        abort(404);
    }
});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::resource('bank-account','BankAccountController');
    Route::resource('currency','CurrencyController');
    Route::get('/', 'Auth\LoginController@showLoginForm');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');//->name('admin.password.token');
    //Route::get('register', 'Auth\RegisterController@showRegistrationForm');
    //Route::post('register', 'Auth\RegisterController@register');
    Route::get('home', 'HomeController@index')->name('admin_home');
    Route::post('home', 'HomeController@index');
    Route::get('user-detail/{id}', 'HomeController@user_detail')->name('user_detail');
    Route::get('user-modify/{id}', 'HomeController@user_modify')->name('user_modify');
    Route::post('user-modify_save', 'HomeController@user_modify_save')->name('user_modify_save');
    Route::get('user-invoices/{id}', 'HomeController@user_invoices')->name('user_invoices');
    Route::post('user-invoices/{id}', 'HomeController@user_invoices')->name('user_invoices');
    Route::get('user-invoice-detail/{id}', 'HomeController@user_invoice_detail')->name('user_invoice_detail');
    Route::get('user-invoice-payment/{id}', 'HomeController@user_invoice_payment')->name('user_invoice_payment');
    Route::get('change-payment-status/{id}', 'HomeController@change_payment_status')->name('change_payment_status');
    Route::get('change-leader-status/{id}', 'HomeController@change_leader_status')->name('change_leader_status');
    Route::get('user-purchases/{id}', 'HomeController@user_purchases')->name('user_purchases');
    Route::post('user-purchases/{id}', 'HomeController@user_purchases')->name('user_purchases');
    Route::get('user-sales/{id}', 'HomeController@user_sales')->name('user_sales');
    Route::post('user-sales/{id}', 'HomeController@user_sales')->name('user_sales');
    Route::get('seller-delete-qualification/{id}', 'HomeController@seller_delete_qualification')->name('seller_delete_qualification');
    Route::get('buyer-delete-qualification/{id}', 'HomeController@buyer_delete_qualification')->name('buyer_delete_qualification');
    Route::get('transaction-detail/{id}', 'HomeController@transaction_detail')->name('transaction_detail');
    Route::get('payment-invoices', 'HomeController@payment_invoices')->name('payment_invoices');
    Route::post('payment-invoices', 'HomeController@payment_invoices')->name('payment_invoices');

    Route::get('publications', 'HomeController@publications')->name('admin_publications');
    Route::post('publications', 'HomeController@publications')->name('admin_publications');
    Route::get('status-modify/{id}', 'HomeController@status_modify')->name('status_modify');
    Route::post('status-modify_save', 'HomeController@status_modify_save')->name('status_modify_save');
    Route::get('questions/{id}', 'HomeController@questions')->name('admin_questions');
    Route::post('questions/{id}', 'HomeController@questions')->name('admin_questions');
    Route::get('disable-question/{id}', 'HomeController@disable_question')->name('disable_question');
    Route::get('disable-answer/{id}', 'HomeController@disable_answer')->name('disable_answer');
    Route::get('enable-question/{id}', 'HomeController@enable_question')->name('enable_question');
    Route::get('enable-answer/{id}', 'HomeController@enable_answer')->name('enable_answer');
    Route::get('activate_plans', 'HomeController@activate_plans')->name('activate_plans');
});

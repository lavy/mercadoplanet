<?php

namespace App;

use App\Models\PublicationIndex\Relationships\PublicationIndexRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PublicationIndex
 * @package App
 */
class PublicationIndex extends Model
{
	use PublicationIndexRelationship;

    protected $table = 'publication_indexes';
}

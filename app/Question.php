<?php

namespace App;

use App\Models\Question\Relationships\QuestionRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Question
 * @package App
 */
class Question extends Model
{
    use QuestionRelationship;

	/**
	 * @param $publicationId
	 * @param $askerId
	 * @param $questions
	 * @param $answer
	 */
    public static function saveQuestion($publicationId, $askerId, $questions, $answer)
    {
        $question = new self();
        $question->publication_id = $publicationId;
        $question->asker_id = $askerId;
        $question->question = $questions;
        $question->answer = $answer;
        $question->save();
    }
}

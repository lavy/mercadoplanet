<?php

namespace App;

use App\Models\Parameter\Relationships\ParameterRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Parameter
 * @package App
 */
class Parameter extends Model
{

	use ParameterRelationship;
    /**
     * @param int $publicationId
     * @param string $name
     * @param string $value
     * @param string $class
     */
    public static function saveParameters(int $publicationId, string $name, string $value, string $class)
    {
        $parameter = new self();
        $parameter->publication_id = $publicationId;
        $parameter->name = $name;
        $parameter->value = '-1';
        $parameter->class = 'Exterior';
        $parameter->save();
    }
}

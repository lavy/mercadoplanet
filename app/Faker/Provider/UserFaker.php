<?php



namespace App\Faker\Provider;

class UserFaker extends \Faker\Provider\Base
{
  public function user($notin)
  {
	$user = \Illuminate\Support\Facades\DB::table('users')->whereNotIn('id', $notin)->inRandomOrder()->first();
	return $user->id;
  }
}
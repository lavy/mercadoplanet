<?php



namespace App\Faker\Provider;

class CategoryFaker extends \Faker\Provider\Base
{
  public function category()
  {
	$category = \Illuminate\Support\Facades\DB::table('categories')->whereNotIn('id', function($q){
		$q->select('parent_id')->from('categories')->whereNotNull('parent_id');
	})->inRandomOrder()->first();
	return $category;
  }
}
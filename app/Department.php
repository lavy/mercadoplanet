<?php

namespace App;

use App\Models\Parameter\Relationships\DepartmentRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Department
 * @package App
 */
class Department extends Model
{
    use DepartmentRelationship;

    /**
     * @param $pubs_ids
     * @return null
     */
    public static function getCountLocations($pubs_ids) {
		if(count($pubs_ids) < 1) return null;
        
        return self::query()
            ->select(DB::raw('departments.id, departments.name, count(departments.id) as num'))
            ->join('provinces', 'provinces.department_id', '=', 'departments.id')
            ->join('districts', 'districts.province_id', '=', 'provinces.id')
            ->join('publications', 'publications.address_district_id', '=', 'districts.id')
            ->whereIn('publications.id', $pubs_ids)
            ->groupBy('departments.id', 'departments.name')
            ->orderBy('num', 'desc')
            ->get();
    }

    /**
     * @param $departmentId
     * @return mixed
     */
    public static function getArrayDistrictsIdFromDepartmentId($departmentId)
    {
        return self::query()
            ->select(DB::raw('districts.id'))
            ->join('provinces', 'provinces.department_id', '=', 'departments.id')
            ->join('districts', 'districts.province_id', '=', 'provinces.id')
            ->where('departments.id', $departmentId)->pluck('districts.id')->toArray();
    }
}

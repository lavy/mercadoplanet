<?php

namespace App;

use App\Models\Feature\Relationships\FeatureRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Feature
 * @package App
 */
class Feature extends Model
{
    //
    use FeatureRelationship;
}

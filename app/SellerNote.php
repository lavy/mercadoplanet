<?php

namespace App;

use App\Models\SellerNote\Relationships\SellerNoteRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SellerNote
 * @package App
 */
class SellerNote extends Model
{
    use SellerNoteRelationship;
}

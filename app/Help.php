<?php

namespace App;

use App\Models\Help\Relationships\HelpRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Help
 * @package App
 */
class Help extends Model
{
	use HelpRelationship;

    protected $table = 'help';
}

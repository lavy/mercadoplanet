<?php
namespace App\Models\SellerNote\Relationships;

use App\Transaction;

/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 12/04/20
 * Time: 02:49
 */
trait SellerNoteRelationship
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function transaction()
	{
		return $this->belongsTo(Transaction::class);
	}

}
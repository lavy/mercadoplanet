<?php
namespace App\Models\TicketItem\Relationships;

use App\Ticket;
use App\Publication;

/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 12/04/20
 * Time: 02:50
 */
trait TicketItemRelationship
{

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function ticket()
	{
		return $this->belongsTo(Ticket::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function publication()
	{
		return $this->belongsTo(Publication::class);
	}
}
<?php
namespace App\Models\Category\Relationships;

use App\Publication;
use App\GroupFeature;
use App\Category;

/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 12/04/20
 * Time: 02:44
 */
trait CategoryRelationship
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function publications()
	{
		return $this->hasMany(Publication::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function groupFeatures()
	{
		return $this->hasMany(GroupFeature::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function children()
	{
		return $this->hasMany(Category::class, 'parent_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function parentCategory()
	{
		return $this->belongsTo(Category::class, 'parent_id');
	}
}
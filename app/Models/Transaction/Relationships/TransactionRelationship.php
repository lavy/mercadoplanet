<?php
namespace App\Models\Transaction\Relationships;

use App\User;
use App\SellerNote;
use App\BuyerNote;
use App\Message;
use App\Publication;


/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 12/04/20
 * Time: 02:50
 */
trait TransactionRelationship
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function seller()
	{
		return $this->belongsTo(User::class, 'seller_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function buyer()
	{
		return $this->belongsTo(User::class, 'buyer_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function publication()
	{
		return $this->belongsTo(Publication::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function sellerNotes()
	{
		return $this->hasMany(SellerNote::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function buyerNotes()
	{
		return $this->hasMany(BuyerNote::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function messages()
	{
		return $this->hasMany(Message::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function notes()
	{
		return $this->hasMany(SellerNote::class);
	}
}
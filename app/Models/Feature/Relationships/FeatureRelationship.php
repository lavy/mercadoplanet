<?php
namespace App\Models\Feature\Relationships;

use App\GroupFeature;
use App\Publication;

/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 12/04/20
 * Time: 02:46
 */
trait FeatureRelationship
{
	/**
	 * @return mixed
	 */
	public function groupFeature()
	{
		return $this->belongsTo(GroupFeature::class);
	}

	/**
	 * @return mixed
	 */
	public function publications()
	{
		return $this->belongsToMany(Publication::class);
	}
}
<?php

namespace App\Models\Address\Relationships;

use App\User;
use App\Publication;
use App\District;
/**
 * Created by PhpStorm.
 * User: martingomes
 * Date: 12/04/20
 * Time: 02:42
 */
trait AddressRelationship
{
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function publications()
	{
		return $this->hasMany(Publication::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function district()
	{
		return $this->belongsTo(District::class);
	}
}
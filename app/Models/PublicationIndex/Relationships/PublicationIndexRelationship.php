<?php
namespace App\Models\PublicationIndex\Relationships;

use App\Publication;

/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 12/04/20
 * Time: 02:48
 */
trait PublicationIndexRelationship
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function publication()
	{
		return $this->belongsTo(Publication::class);
	}
}
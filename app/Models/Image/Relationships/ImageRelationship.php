<?php
namespace App\Models\Image\Relationships;

use App\Publication;
/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 12/04/20
 * Time: 02:46
 */
trait ImageRelationship
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function publication()
	{
		return $this->belongsTo(Publication::class);
	}

}
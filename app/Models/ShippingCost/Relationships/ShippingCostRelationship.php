<?php
namespace App\Models\ShippingCost\Relationships;

use App\Publication;

/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 12/04/20
 * Time: 02:49
 */
trait ShippingCostRelationship
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function publication()
	{
		return $this->belongsTo(Publication::class);
	}
}
<?php
namespace App\Models\Province\Relationships;

use App\Department;
use App\District;

/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 12/04/20
 * Time: 02:47
 */
trait ProvinceRelationship
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function department()
	{
		return $this->belongsTo(Department::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function districts()
	{
		return $this->hasMany(District::class);
	}
}
<?php
namespace App\Models\Parameter\Relationships;

use App\Publication;

/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 12/04/20
 * Time: 02:47
 */
trait ParameterRelationship
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function publications()
	{
		return $this->belongsTo(Publication::class);
	}
}
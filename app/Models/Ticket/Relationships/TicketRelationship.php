<?php
namespace App\Models\Ticket\Relationships;

use App\User;
use App\TicketItem;

/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 12/04/20
 * Time: 02:50
 */
trait TicketRelationship
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function ticketItems()
	{
		return $this->hasMany(TicketItem::class);
	}
}
<?php
namespace App\Models\District\Relationships;

use App\Province;
use App\Address;

/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 12/04/20
 * Time: 02:45
 */
trait DistrictRelationship
{
	public function province()
	{
		return $this->belongsTo(Province::class);
	}

	public function addresses()
	{
		return $this->hasMany(Address::class);
	}
}
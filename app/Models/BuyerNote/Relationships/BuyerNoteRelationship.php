<?php
namespace App\Models\BuyerNote\Relationships;

use App\Transaction;

/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 12/04/20
 * Time: 02:43
 */
trait BuyerNoteRelationship
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function transaction()
	{
		return $this->belongsTo(Transaction::class);
	}
}
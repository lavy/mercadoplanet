<?php
namespace App\Models\GroupFeature\Relationships;

use App\Category;
use App\Feature;

/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 12/04/20
 * Time: 02:46
 */
trait GroupFeatureRelationship
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function category()
	{
		return $this->belongsTo(Category::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function features()
	{
		return $this->hasMany(Feature::class);
	}
}
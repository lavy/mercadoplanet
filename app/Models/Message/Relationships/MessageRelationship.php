<?php
namespace App\Models\Message\Relationships;

use App\User;
use App\Publication;
use App\Transaction;

/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 12/04/20
 * Time: 02:46
 */
trait MessageRelationship
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function publication()
	{
		return $this->belongsTo(Publication::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function transaction()
	{
		return $this->belongsTo(Transaction::class);
	}
}
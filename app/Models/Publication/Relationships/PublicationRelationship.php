<?php
namespace App\Models\PublicationIndex\Relationships;

use App\Message;
use App\Question;
use App\Transaction;
use App\User;
use App\Address;
use App\Category;
use App\TicketItem;
use App\Feature;
use App\PublicationIndex;
use App\Parameter;
use App\District;
use App\Image;
use App\ShippingCost;

/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 12/04/20
 * Time: 02:48
 */
trait PublicationRelationship
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function questions()
	{
		return $this->hasMany(Question::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function transactions()
	{
		return $this->hasMany(Transaction::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function address()
	{
		return $this->belongsTo(Address::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function category()
	{
		return $this->belongsTo(Category::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function ticketItems()
	{
		return $this->hasMany(TicketItem::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function features()
	{
		return $this->belongsToMany(Feature::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function followers()
	{
		return $this->belongsToMany(User::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function publicationIndex()
	{
		return $this->hasOne(PublicationIndex::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function index()
	{
		return $this->hasOne(PublicationIndex::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function parameters()
	{
		return $this->hasMany(Parameter::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function addressDistrict()
	{
		return $this->belongsTo(District::class, 'address_district_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function images()
	{
		return $this->hasMany(Image::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function shippingCosts()
	{
		return $this->hasMany(ShippingCost::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function messages()
	{
		return $this->hasMany(Message::class);
	}
}
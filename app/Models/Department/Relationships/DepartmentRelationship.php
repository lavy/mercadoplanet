<?php
namespace App\Models\Parameter\Relationships;

use App\Province;
/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 12/04/20
 * Time: 02:45
 */
trait DepartmentRelationship
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function provinces() {
		return $this->hasMany(Province::class);
	}
}
<?php

namespace App;

use App\Helpers\TimeHelper;
use App\Models\PublicationIndex\Relationships\PublicationRelationship;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

/**
 * Class Publication
 * @package App
 */
class Publication extends Model
{
    use PublicationRelationship;

    /**
     * @return mixed
     */
    public function getTopLevelCategory()
    {
        if ($category = Category::find($this->category->id)) {
            while (1) {
                $parent = $category->parentCategory;
                if ($parent->id == '4') {
                    return $category;
                }
                if ($parent->id == '1' or $parent->id == '2' or $parent->id == '3') {
                    return $parent;
                }
                $category = $parent;
            }
        }
    }

    /**
     * @return mixed
     */
    public function topLevelCategory()
    {
        if ($category = Category::find($this->category->id)) {
            while (1) {
                $parent = $category->parentCategory;
                if ($parent->id == '1' or $parent->id == '2' or $parent->id == '3' or $parent->id == '4') {
                    return $parent;
                }
                $category = $parent;
            }
        }
    }

    /**
     * @return int
     */
    public function getSales()
    {
        $pub = Publication::query()
            ->join('transactions', 'publications.id', '=', 'transactions.publication_id')
            ->select(DB::raw('publications.id, sum(transactions.quantity) as num'))
            ->where('publications.id', $this->id)
            ->where(function ($query) {
                $query->where('transactions.seller_concreted', '1')
                    ->orWhere('transactions.buyer_concreted', '1');
            })
            ->groupBy('publications.id')
            ->first();

        if ($pub) return $pub->num;
        return 0;
    }

    /**
     * @param $category_code
     * @param int $num
     * @return mixed
     */
    public static function getLatest($category_code, $num = 6)
    {
        return self::where('status', 'Active')
            ->where('category_path', 'like', $category_code . '%')
            ->orderBy('publicated_as_active_at', 'desc')
            ->limit($num)
            ->get();
    }

    /**
     * @param $category_codes
     * @return int
     */
    public static function countMobileLatest($category_codes)
    {
        $categories = json_decode($category_codes, true);
        if (!$categories) {
            return 0;
        }

        return self::where('status', 'Active')
            ->where(function ($query) use ($categories) {
                foreach ($categories as $c) {
                    $cat = Category::find($c);
                    $query->orWhere('category_path', 'like', $cat->code . '%');
                }
            })->count();
    }

    /**
     * @param $category_codes
     * @param $offset
     * @param $limit
     * @return array
     */
    public static function getMobileLatest($category_codes, $offset, $limit)
    {
        $categories = json_decode($category_codes, true);
        if (!$categories) {
            return [];
        }

        return self::where('status', 'Active')
            ->where(function ($query) use ($categories) {
                foreach ($categories as $c) {
                    $cat = Category::find($c);
                    $query->orWhere('category_path', 'like', $cat->code . '%');
                }
            })
            ->orderBy('publicated_as_active_at', 'desc')
            ->offset($offset)
            ->limit($limit)
            ->get();
    }

    /**
     * @param $category_codes
     * @param $offset
     * @param $limit
     * @return mixed
     */
    public static function getMobileLatestAll($category_codes, $offset, $limit)
    {
        $categories = json_decode($category_codes, true);
        if (!$categories) {
            $categories = [];
        }

        return self::where('status', 'Active')
            ->where(function ($query) use ($categories) {
                foreach ($categories as $c) {
                    $cat = Category::find($c);
                    //$query->where('category_id', '!=', $c);
                    $query->where('category_path', 'not like', $cat->code . '%');
                }
            })
            ->orderBy('publicated_as_active_at', 'desc')
            ->offset($offset)
            ->limit($limit)
            ->get();
    }

    /**
     * @param $category_code
     * @param int $num
     * @return mixed
     */
    public static function getSuggested($category_code, $num = 6)
    {
        if ($category_code === '01' || $category_code === '02' || $category_code === '03') {
            $pubs = Publication::query()
                ->select(DB::raw('publications.id, publications.title, publications.price, (publications.visits) as num'))
                ->where('publications.status', 'Active')
                ->where('publications.category_path', 'like', $category_code . '%')
                ->groupBy('publications.id')
                ->groupBy('publications.title')
                ->groupBy('publications.price')
                ->groupBy('publications.visits')
                ->orderBy('num', 'desc')
                ->limit($num)
                ->get();
            return $pubs;
        }

        $pubs = Publication::query()
            ->join('transactions', 'publications.id', '=', 'transactions.publication_id')
            ->select(DB::raw('publications.id, publications.title, publications.price, (sum(transactions.quantity)+publications.visits) as num'))
            ->where('publications.status', 'Active')
            ->where('publications.category_path', 'like', $category_code . '%')
            ->where(function ($query) {
                $query->where('transactions.seller_concreted', '1')
                    ->orWhere('transactions.buyer_concreted', '1');
            })
            ->groupBy('publications.id')
            ->groupBy('publications.title')
            ->groupBy('publications.price')
            ->groupBy('publications.visits')
            ->orderBy('num', 'desc')
            ->limit($num)
            ->get();
        return $pubs;
    }


    /**
     * @param $category_code
     * @param int $num
     * @return mixed
     */
    public static function getBestsellers($category_code, $num = 6)
    {
        return self::join('transactions', 'publications.id', '=', 'transactions.publication_id')
            ->select(DB::raw('publications.id, publications.title, publications.price, sum(transactions.quantity) as num'))
            ->where('publications.status', 'Active')
            ->where('publications.category_path', 'like', $category_code . '%')
            ->where(function ($query) {
                $query->where('transactions.seller_concreted', '1')
                    ->orWhere('transactions.buyer_concreted', '1');
            })
            ->groupBy('publications.id')
            ->groupBy('publications.title')
            ->groupBy('publications.price')
            ->orderBy('num', 'desc')
            ->limit($num)
            ->get();
    }

    /**
     * @param int $num
     * @return mixed
     */
    public static function getFeatured(int $num = 6)
    {
        return self::where('status', 'Active')
            ->orderBy('cost_type', 'desc')
            ->orderBy('publicated_as_active_at', 'desc')
            ->limit($num)
            ->get();
    }

    /**
     * @param $category
     * @param $features
     * @param $sort
     * @param $condition
     * @param $locations
     * @param $feature_pubs
     * @param $min
     * @param $max
     * @param $search
     * @return mixed
     */
    public static function getFiltered($category, $features, $sort, $condition, $locations, $feature_pubs, $min, $max, $search)
    {

        $query = self::where('publications.status', 'Active');

        switch ($sort) {
            case 'lowtohigh':
                $query->orderBy('publications.price', 'asc');
                break;
            case 'hightolow':
                $query->orderBy('publications.price', 'desc');
                break;
            default:
                $query->orderBy('publications.cost_type', 'desc');
                $query->orderBy('publications.ends_at', 'asc');
                break;
        }

        if ($condition == 'new') {
            $query->where('publications.condition', 'Nuevo');
        }
        if ($condition == 'used') {
            $query->where('publications.condition', 'Usado');
        }

        if (count($locations) > 0) {
            $query->whereIn('publications.address_district_id', $locations);
        }
        if (count($feature_pubs) > 0) {
            $query->whereIn('publications.id', $feature_pubs);
        }

        //TODO: order by reputation, add a field in user table to be updated
        // in every proper oportunity

        if ($category) {
            $query->where('publications.category_path', 'like', $category->code . '%');
        }

        if (!empty($min)) {
            $query->where('publications.price', '>=', $min);
        }
        if (!empty($max)) {
            $query->where('publications.price', '<=', $max);
        }
        if (!empty($search)) {

            if (PublicationIndex::query()
                ->whereRaw("match(`publication_indexes`.`title`,`publication_indexes`.`category_branch_string`,`publication_indexes`.`description`) against ('$search')")->count()
            ) {

                $query->join('publication_indexes', 'publications.id', '=', 'publication_indexes.publication_id')
                    ->whereRaw("match(`publication_indexes`.`title`,`publication_indexes`.`category_branch_string`,`publication_indexes`.`description`) against ('$search')")
                    ->orderBy(DB::raw("match(`publication_indexes`.`title`,`publication_indexes`.`category_branch_string`,`publication_indexes`.`description`) against ('$search')"), 'desc');
            } else {

                $query->join('publication_indexes', 'publications.id', '=', 'publication_indexes.publication_id')
                    ->where(function ($queryx) use ($search) {
                        $queryx->orWhere('publication_indexes.title', 'like', "%$search%")
                            ->orWhere('publication_indexes.description', 'like', "%$search%")
                            ->orWhere('publication_indexes.category_branch_string', 'like', "%$search%");
                    });
            }

        }

        return $query->select('publications.*');
    }


    /**
     * @param $category
     * @param $features
     * @param $sort
     * @param $condition
     * @param $locations
     * @param $feature_pubs
     * @param $min
     * @param $max
     * @param $search
     * @param int $limit
     * @param int $offset
     * @return mixed
     */
    public static function getFilteredAPI($category, $features, $sort, $condition, $locations, $feature_pubs, $min, $max, $search, $limit = 6, $offset = 0)
    {

        $query = self::where('publications.status', 'Active');

        switch ($sort) {
            case 'lowtohigh':
                $query->orderBy('publications.price', 'asc');
                break;
            case 'hightolow':
                $query->orderBy('publications.price', 'desc');
                break;
            default:
                $query->orderBy('publications.cost_type', 'desc');
                $query->orderBy('publications.ends_at', 'asc');
                break;
        }

        if ($condition == 'new') {
            $query->where('publications.condition', 'Nuevo');
        }
        if ($condition == 'used') {
            $query->where('publications.condition', 'Usado');
        }

        if (count($locations) > 0) {
            $query->whereIn('publications.address_district_id', $locations);
        }
        if (count($feature_pubs) > 0) {
            $query->whereIn('publications.id', $feature_pubs);
        }

        //TODO: order by reputation, add a field in user table to be updated
        // in every proper oportunity

        if ($category) {
            $query->where('publications.category_path', 'like', $category->code . '%');
        }

        if (!empty($min)) {
            $query->where('publications.price', '>=', $min);
        }
        if (!empty($max)) {
            $query->where('publications.price', '<=', $max);
        }
        if (!empty($search)) {

            if (PublicationIndex::query()
                ->whereRaw("match(`publication_indexes`.`title`,`publication_indexes`.`category_branch_string`,`publication_indexes`.`description`) against ('$search')")->count()
            ) {

                $query->join('publication_indexes', 'publications.id', '=', 'publication_indexes.publication_id')
                    ->whereRaw("match(`publication_indexes`.`title`,`publication_indexes`.`category_branch_string`,`publication_indexes`.`description`) against ('$search')")
                    ->orderBy(DB::raw("match(`publication_indexes`.`title`,`publication_indexes`.`category_branch_string`,`publication_indexes`.`description`) against ('$search')"), 'desc');
            } else {

                $query->join('publication_indexes', 'publications.id', '=', 'publication_indexes.publication_id')
                    ->where(function ($queryx) use ($search) {
                        $queryx->orWhere('publication_indexes.title', 'like', "%$search%")
                            ->orWhere('publication_indexes.description', 'like', "%$search%")
                            ->orWhere('publication_indexes.category_branch_string', 'like', "%$search%");
                    });
            }

        }
        if ($limit !== 0) {
            $query->limit($limit)->offset($offset);
        }
        return $query->select('publications.*');
    }

    /**
     * @param $pubs_ids
     * @return array
     */
    public static function countConditions($pubs_ids)
    {
        if (count($pubs_ids) < 1) return ['new' => 0, 'used' => 0];

        $result = ['new' => 0, 'used' => 0];

        $conditions = Publication::query()
            ->select(DB::raw('publications.condition, count(publications.condition) as num'))
            ->whereIn('publications.id', $pubs_ids)
            ->groupBy('publications.condition')->get();

        foreach ($conditions as $condition) {
            if ($condition->condition == 'Nuevo')
                $result['new'] = $condition->num;
            if ($condition->condition == 'Usado')
                $result['used'] = $condition->num;
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function isFavorite()
    {
        $publication = Publication::find($this->id);

        if (!Auth::check() || !$publication) {
            return false;
        }

        $user = Auth::user();

        if ($user->favorites->contains($publication)) {
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function getCategoryBranchString()
    {
        $category = $this->category;
        $category_branch = [];
        $features = $this->features;

        foreach ($features as $f) {
            $category_branch[] = $f->name;
        }

        while ($c = $category->parentCategory) {
            $category_branch[] = $c->name;
            $category = $c;
        }

        return implode(' ', array_reverse($category_branch));
    }

    /**
     * @return string
     */
    public function location()
    {
        $location = ucfirst(strtolower($this->addressDistrict->name)) . ", " .
            ucfirst(strtolower($this->addressDistrict->province->department->name));
        return $location;
    }

    /**
     * @return string
     */
    public function fullLocation()
    {
        $location = ucfirst(strtolower($this->addressDistrict->province->department->name)) . " - " .
            ucfirst(strtolower($this->addressDistrict->province->name)) . " - " .
            ucfirst(strtolower($this->addressDistrict->name));
        return $location;
    }

    /**
     * @return string
     */
    public function vehicleFullLocation()
    {
        $location = ucfirst(strtolower($this->addressDistrict->province->department->name)) . " - " .
            ucfirst(strtolower($this->addressDistrict->province->name)) . " - " .
            ucfirst(strtolower($this->addressDistrict->name));
        return $location;
    }


    /**
     * @return string
     */
    public function getFullLocation()
    {
        $location = ucfirst(strtolower($this->addressDistrict->province->department->name)) . " - " .
            ucfirst(strtolower($this->addressDistrict->province->name)) . " - " .
            ucfirst(strtolower($this->addressDistrict->name));
        return $location;
    }

    /**
     *
     */
    #####Verificar
    public function visited()
    {
        $this->visits = $this->visits + 1;
        $this->save();
    }

    /**
     *
     */
    public function addVisit()
    {
        $this->visits = $this->visits + 1;
        $this->save();
    }

    /**
     * @return string
     */
    public function getProductType()
    {

        if (substr($this->category_path, 0, 4) === '0106')
            return 'vehiculo';
        if (substr($this->category_path, 0, 4) === '0105')
            return 'lancha';
        if (substr($this->category_path, 0, 4) === '0104')
            return 'moto';
        if (substr($this->category_path, 0, 4) === '0102')
            return 'vehículo de colección';
        if (substr($this->category_path, 0, 2) === '01')
            return 'vehículo';
        if (substr($this->category_path, 0, 4) === '0201')
            return 'casa';
        if (substr($this->category_path, 0, 4) === '0202')
            return 'departamento';
        if (substr($this->category_path, 0, 4) === '0203')
            return 'local comercial';
        if (substr($this->category_path, 0, 4) === '0204')
            return 'oficina';
        if (substr($this->category_path, 0, 4) === '0205')
            return 'inmueble';
        if (substr($this->category_path, 0, 4) === '0206')
            return 'terreno';
        if (substr($this->category_path, 0, 2) === '03')
            return 'servicio';
        if (substr($this->category_path, 0, 2) === '04')
            return 'producto';
    }

    /**
     * @return string
     */
    public function getTimeToEnd()
    {
        $time = strtotime($this->ends_at) - time();
        return TimeHelper::humanizeTime($time);
    }
}

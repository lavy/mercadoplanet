<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class SellerRequestChangeQualification
 * @package App\Mail
 */
class SellerRequestChangeQualification extends Mailable
{
    use Queueable, SerializesModels;

    protected $purchase;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($purchase)
    {
        $this->purchase = $purchase;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.seller.request_change_qualification')
            ->subject('Te han solicitado un cambio de calificación.')
            ->with([
                'purchase' => $this->purchase
            ]);
    }
}

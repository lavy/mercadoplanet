<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class BuyerRequestChangeQualification
 * @package App\Mail
 */
class BuyerRequestChangeQualification extends Mailable
{
    use Queueable, SerializesModels;

    protected $sale;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sale)
    {
        $this->sale = $sale;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.buyer.request_change_qualification')
            ->subject('Te han solicitado un cambio de calificación.')
            ->with([
                'sale' => $this->sale
            ]);
    }
}

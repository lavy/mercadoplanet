<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class Contact
 * @package App\Mail
 */
class Contact extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $data)
    {
        $this->user = $user;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->view('emails.contact.index')
            ->subject('[Soporte] ' . $this->data['subject'])
            ->from($this->user->email)
            ->with([
                'user' => $this->user,
                'data' => $this->data
            ]);

        foreach ($this->data['tmp_files'] as $name => $path) {
            $view->attach($path, [
                'as' => $name
            ]);
        }

        return $view;
    }
}

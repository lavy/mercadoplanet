<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class ContactCopy
 * @package App\Mail
 */
class ContactCopy extends Mailable
{
    use Queueable, SerializesModels;
    
    protected $user;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $data)
    {
        $this->user = $user;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->view('emails.contact.copy')
            ->subject('Tu solicitud de soporte ha sido recibida.')
            ->from(config('app.global.mail_address_support'))
            ->with([
                'user' => $this->user,
                'data' => $this->data
            ]);

        foreach ($this->data['tmp_files'] as $name => $path) {
            $view->attach($path, [
                'as' => $name
            ]);
        }
        
        return $view;
    }
}

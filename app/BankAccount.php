<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BankAccount
 * @package App
 */
class BankAccount extends Model
{
    public $timestamps = false;
}

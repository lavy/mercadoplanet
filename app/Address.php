<?php

namespace App;

use App\Models\Address\Relationships\AddressRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * Class Address
 * @package App
 */
class Address extends Model
{
	use AddressRelationship;

    protected $fillable = ['user_id', 'address_type', 'address_text', 'number',
        'other_data', 'reference', 'district_id', 'default_for_sales'];

    /**
     * @param Request $request
     * @param $user
     */
    public static function createAddressforUser(Request $request, $user)
    {
        self::create([
            'address_type' => $request->address_type,
            'address_text' => $request->address_text,
            'number' => $request->number,
            'other_data' => $request->other_data,
            'reference' => $request->reference,
            'district_id' => $request->district,
            'user_id' => $user->id,
            'default_for_sales' => true,
        ]);
    }
}

<?php

namespace App;

use App\Models\District\Relationships\DistrictRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class District
 * @package App
 */
class District extends Model
{

    use DistrictRelationship;
}

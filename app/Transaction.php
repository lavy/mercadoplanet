<?php

namespace App;

use App\Helpers\TimeHelper;
use App\Models\Transaction\Relationships\TransactionRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Transaction
 * @package App
 */
class Transaction extends Model
{
    use TransactionRelationship;

    /**
     * @return string
     */
    function getTimeToQualify()
    {
        $left = config('app.global.days_to_qualify') * (24 * 3600) - (time() - strtotime($this->created_at));
        $time = ($left > 0 ? TimeHelper::humanizeTime($left) : '0');
        return $time;
    }
}

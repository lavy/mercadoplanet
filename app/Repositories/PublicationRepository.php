<?php

namespace App\Repositories;

use Illuminate\Contracts\Container\Container;
use Rinvex\Repository\Repositories\EloquentRepository;
use \Illuminate\Support\Facades\DB;

class PublicationRepository extends EloquentRepository
{
    // Instantiate repository object with required data
    public function __construct(Container $container)
    {
        $this->setContainer($container)
             ->setModel(\App\Publication::class)
             ->setRepositoryId('rinvex.repository.publication');

    }
	
	public function getSuggestionsFromCategory($category_id) {
		$pubs = DB::table('publications')
            ->join('transactions', 'publications.id', '=', 'transactions.publication_id')
            ->select(DB::raw('publications.*, count(*) as num'))
			->where('pulications.status', 'Active')
			->where(function($query) {
				$query->where('transactions.seller_concreted','1')
					->where('transactions.buyer_concreted','1');
			})
			->orderBy('num', 'desc')
			->limit(5)
            ->get();
			
		return $pubs;
	}
}

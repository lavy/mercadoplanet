<?php

namespace App;

use App\Models\Message\Relationships\MessageRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Message
 * @package App
 */
class Message extends Model
{
    use MessageRelationship;
}

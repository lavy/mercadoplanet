<?php

namespace App;

use App\Models\User\Relationships\UserRelationship;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Notifications;
use App\Notifications\ResetPasswordNotification;
use App\Notifications\CanBeLeader as CanBeLeaderNotification;

class User extends Authenticatable
{
    use Notifiable, UserRelationship;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'phone_number', 'email',
        'username', 'password', 'status',  'latest_visits',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return string
     */
    public function getFullName() {
        return $this->first_name.' '.$this->last_name;
    }

    /**
     * @param string $token
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($this, $token));
    }

    /**
     * @param $publication
     * @param $cookie
     * @return mixed
     */
    public static function addLatestVisitsInfo($publication, $cookie) {
        if(Auth::check()) {
          $user = Auth::user();
          $latest_visits = json_decode($user->latest_visits, true);
        } else {
          $latest_visits = $cookie;
        }
          /*if(($key = array_search($publication->id, $latest_visits['products'])) !== false) {
              unset($latest_visits['products'][$key]);
          }
          if(($key = array_search($publication->category_id, $latest_visits['categories'])) !== false) {
              unset($latest_visits['categories'][$key]);
          }*/
          array_unshift($latest_visits['products'], $publication->id);
          array_unshift($latest_visits['categories'], $publication->category_id);
          $latest_visits['products'] = array_slice($latest_visits['products'],0,5);
          $latest_visits['categories'] = array_slice($latest_visits['categories'],0,5);
        if(Auth::check()) {
          $user->latest_visits = json_encode($latest_visits);
          $user->save();
        }
        return $latest_visits;
    }


    /**
     * @param $user
     * @return array
     */
    public static function user_reputation($user)
    {
        $min_sales = 10;

        $num_sales = $user->sales()->where(function($query) {
                $query->where('seller_concreted','1')
                      ->orWhere('buyer_concreted','1');
        })->count();
        
        
        $missing_sales = $min_sales - $num_sales;
        
        if ($missing_sales > 0) {
            $value = -1;
            return [ 'value' => $value, 'missing_sales' => $missing_sales];
        }
        

        $max_money = 1000;
        $max_vip = 5;
        $money_mul = 2; //if $sum_sales is greater than $max_money it gets the entire $money_mul
        $concreted_mul = 2; //multiplier for sales where the seller has marked as completed
        $calificated_mul = 0;
        $contradiction_mul = 5;
        $vip_mul = 1;

        $min_date = date('Y-m-d', strtotime(date('Y-m-d').' -5 months'));

        
        $num_pub_vip = $user->publications()
            ->where('created_at', '>=', $min_date)
            ->where(function ($query) {
                $query->where('cost_type','Bronce')
                      ->orWhere('cost_type','Plata')
                      ->orWhere('cost_type','Oro')
                      ->orWhere('cost_type','Premium');
            })->count();
        $vip = min([$max_vip, $num_pub_vip]) / $max_vip;
        
        $sum_sales = $user->sales()
            ->where('created_at', '>=', $min_date)
            ->where(function ($query) {
                $query->where('seller_concreted','1')
                      ->orWhere('buyer_concreted','1');
            })
            ->sum(DB::raw('transactions.quantity * transactions.price'));
        
        $money = min([$max_money, $sum_sales]) / $max_money;

        
        $concreted = ($num_sales == 0 ? 0 : $user->sales()->where('seller_concreted','1')->count() / $num_sales);
                
                
                
        $num_calificated_pos =
                $user->sales
                ->where('buyer_calification', 'Positivo')
                ->count();
        $num_calificated_neg =
                $user->sales
                ->where('buyer_calification', 'Negativo')
                ->count();
                    
        $calificated = ($num_calificated_pos <= $num_calificated_neg) ? 0 :
            ($num_calificated_pos - $num_calificated_neg) / ($num_calificated_pos + $num_calificated_neg);
            
        $sales = $user->sales->count();
        $num_no_contradictions = DB::table('transactions')->where('seller_id', $user->id)
                ->where(function($q) {
                    $q->orWhere('seller_concreted', 1)
                       ->orWhere('seller_calification', 'No calificado')
                       ->orWhere('buyer_concreted', 0);
                })->count();
        $not_contradictions = ($num_no_contradictions <= 2*($sales-$num_no_contradictions)) ? 0 :
             ($num_no_contradictions - 2*($sales-$num_no_contradictions))/ $sales;
 
        $value = $money * $money_mul + $concreted * $concreted_mul +
                $calificated * $calificated_mul +
                $not_contradictions * $contradiction_mul +
                $vip * $vip_mul;
        
        $num_total_recommendations =
                $user->sales()->where(function($query) {
                    $query->where('buyer_recommends', 'No')
                    ->orWhere('buyer_recommends', 'Sí');
                })->count();
        $num_positive_recommendations =
                $user->sales
                ->where('buyer_recommends', 'Sí')
                ->count();
        $porc_recommended = ($num_total_recommendations == 0 ? 0: 
            round($num_positive_recommendations/$num_total_recommendations*100));
        
        $seller_level = 1; //no tiene buena reputación en el sitio
        //Problemas con algunas ventas
        if($calificated >= 0.5  and $porc_recommended >=50) {
            $seller_level = 2;
        }
//        //Vendedor
//        if($calificated >= 0.75  and $value >= 7.5 and $porc_recommended >= 75) {
//            $seller_level = 3;
//        }
        //Destacado por sus buenas calificaciones
        if($calificated >= 0.80 and $porc_recommended >=80) {
            $seller_level = 4;
        }
        //Mejores del sitio
        if($calificated >= 0.95 and $porc_recommended >= 97 and $num_sales >= 20) {
            $seller_level = 5;
        }
        
        $value = 0;
        if($seller_level == 1) {
            $value = ($money * $money_mul + $concreted * $concreted_mul +
                $not_contradictions * $contradiction_mul +
                $vip * $vip_mul)/10*4;
        }
        if($seller_level == 2) {
            $value = 4+($money * $money_mul + $concreted * $concreted_mul +
                $not_contradictions * $contradiction_mul +
                $vip * $vip_mul)/10*2;
        }
        if($seller_level == 4) {
            $value = 5+($money * $money_mul + $concreted * $concreted_mul +
                $not_contradictions * $contradiction_mul +
                $vip * $vip_mul)/10*4;
        }
        if($seller_level == 5) {
            $value = 8+($money * $money_mul + $concreted * $concreted_mul +
                $not_contradictions * $contradiction_mul +
                $vip * $vip_mul)/10*2;
        }

        $first_trans = $user->sales()->where(function($query) {
                $query->where('seller_concreted','1')
                      ->orWhere('buyer_concreted','1');
        })->orderby('transactions.created_at')->limit(1)->first();
        
        $date_trans = $first_trans->created_at;

        $init_date = new \DateTime(date('Y-m-d', strtotime($date_trans)));
        $end_date = new \DateTime();
        $diff = $init_date->diff($end_date);
        if($diff->d >= 0) $seller_old = $diff->d . ' DIAS';
        if($diff->d == 1) $seller_old = $diff->d . ' DIA';
        if($diff->m > 0) $seller_old = $diff->m . ' MESES';
        if($diff->m == 1) $seller_old = $diff->m . ' MES';
        if($diff->y > 0) $seller_old = $diff->y . ' AÑOS';
        if($diff->y == 1) $seller_old = $diff->y . ' AÑO';

        return [ 'value' => $value, 'missing_sales' => 0, 'sales' => $num_sales, 'recommended' => $porc_recommended, 'level' => $seller_level, 'time' => $seller_old];
    }

    /**
     * @return string
     */
    public function location() {
        $address = $this->addresses()->where('default_for_sales', '1')->first();
        
        $location = ucfirst(strtolower($address->district->name)).", ".
            ucfirst(strtolower($address->district->province->department->name));
        return $location;
    }

    /**
     * @return mixed
     */
    public function getPoints() {
        $pos_sales = $this->sales()->where('buyer_calification', 'Positivo')->count();
        $neg_sales = $this->sales()->where('buyer_calification', 'Negativo')->count();
        $pos_purchases = $this->purchases()->where('seller_calification', 'Positivo')->count();
        $negs_purchases = $this->purchases()->where('seller_calification', 'Negativo')->count();
        
        return  $pos_sales + $pos_purchases - $neg_sales - $negs_purchases;
    }

    /** 
     * return if a user can be a leader 
     * @return 0 : no leader - 1 : leader - 2 : leader gold
     */
    public function canBeLeader() {

        $type = 2;
        // sales concreted
        $num_concreted = $this->sales()->where(function($query) {
            $query->where('seller_concreted','1')
                    ->orWhere('buyer_concreted','1');
        })->count();
        if($num_concreted < 40) { $type = 1; }
        if($num_concreted < 20) { $type = 0; }

        //amount concreted sales
        $sum_sales = $this->sales()            
            ->where('seller_concreted','1')
            ->where('buyer_concreted','1')
            ->sum(DB::raw('transactions.quantity * transactions.price'));

        if($type == 2 && $sum_sales < 14000) {
            $type = 1;
        }
        if($type == 1 && $sum_sales < 6000) {
            $type = 0;
        }
        
        //No debts in lastest 30 days
        if($type != 0 && $this->latest_suspension && strtotime($this->latest_suspension) > strtotime("-30 days")) {
            $type = 0;
        }

        // more than 2 paid invoices
        $num_invoices = $this->tickets()
            ->where('tickets.status', 'Paid')
            ->whereRaw('tickets.created_at > (NOW() - INTERVAL 365 DAY)')
            ->count();
        if($type != 0 && $num_invoices < 2) {
            $type = 0;
        }

        //user_reputation
        $rep = self::user_reputation($this);
        if($type != 0 && isset($rep['level']) && $rep['level'] <= 4) {
            $type = 0;
        }

        if($type != 0 && strtotime($this->created_at) > strtotime("-4 months")) {
            $type = 5;
        }

        return $type;
    }

    /**
     *
     */
    public function notifyCanBeLeader() {
        $type = $this->canBeLeader();
        if($this->seller_type == 'Líder' && $type == '1' || $this->seller_type == 'Líder Gold') {
            return;
        }
        if($type != 0 && !$this->leader_notified) {
            $this->notify(new CanBeLeaderNotification($this, $type));
            $this->leader_notified = 1;
            $this->save();
        }
    }

    /**
     *
     */
    public function keppingLeader() {
        $type = $this->canBeLeader();
        if($type != 0 || $this->seller_type == 'Normal') {
            return;
        }
        $this->leader_notified = 0;
        $this->seller_type = 'Normal';
        $this->save();
    }

    /**
     * @param $type
     * @param $id
     * @param $title
     * @param null $publication_id
     */
    public function sendMobileNotification($type, $id, $title, $publication_id = null) {
        $user = null;
        if(Auth::check()) {
          $user = Auth::user();
        }
        
        //if (empty($this->fcm_token)){ return; }
        $client = new \GuzzleHttp\Client();
        $headers = [
            'Content-Type'  => 'application/json',
            'Authorization' => 'key=AAAAtWxa26k:APA91bEh1JTpzfvjETzVbDlW6HGX1QULzmH7AUFvdn5egmncTWGZYb7mXy7d7VDR2GtXn7ajb54ou4cPQSiRLwzFH_LIMCxkVfhad6EBNo6q1Fsf36Uo8vYDT2tfIkF2tqeF32jONKKF'
        ];
        
        switch ($type) {
            case 'sale':
                if (!empty($this->fcm_token)) {
                    $body = '{
                        "to" : "'.$this->fcm_token.'",
                        "notification" : {
                          "body" : "Tienes una nueva venta en tu artículo '.$title.'",
                          "title" : "¡Felicitaciones!",
                          "icon" : "ic_notification",
                          "sound": "default",
                          "click_action":"FCM_PLUGIN_ACTIVITY"
                        },
                        "data":{
                            "type":"'.$type.'",
                            "id": '.$id.',
                            "message": "Tienes una nueva venta en tu artículo '.$title.'"
                        }
                    }';
                    $promise = $client->postAsync('https://fcm.googleapis.com/fcm/send', ['headers' => $headers, 'body' => $body]);
                    $promise->wait();
                }
                $notification = new \App\Notifications();
                $notification->title = "¡Felicitaciones!";
                $notification->body = "Tienes una nueva venta en tu artículo '.$title.'";
                $notification->type = $type;
                $notification->user_id = $this->id;
                $notification->publication_id = $publication_id;
                $notification->transaction_id = $id;
                $notification->save();
            break;
            
            case 'message-seller':
                if (!empty($this->fcm_token)) {
                    $body = '{
                        "to" : "'.$this->fcm_token.'",
                        "notification" : {
                          "body" : "Tienes un nuevo mensaje en tu venta de '.$title.'",
                          "title" : "¡Tienes un nuevo mensaje!",
                          "icon" : "ic_notification",
                          "sound": "default",
                          "click_action":"FCM_PLUGIN_ACTIVITY"
                        },
                        "data":{
                            "type":"'.$type.'",
                            "id": '.$id.',
                            "message": "Tienes un nuevo mensaje en tu venta de '.$title.'"
                        }
                    }';
                    $promise = $client->postAsync('https://fcm.googleapis.com/fcm/send', ['headers' => $headers, 'body' => $body]);
                    $promise->wait();
                }
                $notification = new \App\Notifications();
                $notification->title = "¡Tienes un nuevo mensaje!";
                $notification->body = "Tienes un nuevo mensaje en tu venta de '.$title.'";
                $notification->type = $type;
                $notification->user_id = $this->id;
                $notification->publication_id = $publication_id;
                $notification->transaction_id = $id;
                $notification->save();
            break;
                  
            case 'message-buyer':
                if (!empty($this->fcm_token)) {
                    $body = '{
                        "to" : "'.$this->fcm_token.'",
                        "notification" : {
                          "body" : "Tienes un nuevo mensaje en tu compra de '.$title.'",
                          "title" : "¡Tienes un nuevo mensaje!",
                          "icon" : "ic_notification",
                          "sound": "default",
                          "click_action":"FCM_PLUGIN_ACTIVITY"
                        },
                        "data":{
                            "type":"'.$type.'",
                            "id": '.$id.',
                            "message": "Tienes un nuevo mensaje en tu compra de '.$title.'"
                        }
                    }';
                    $promise = $client->postAsync('https://fcm.googleapis.com/fcm/send', ['headers' => $headers, 'body' => $body]);
                    $promise->wait();
                }
                $notification = new \App\Notifications();
                $notification->title = "¡Tienes un nuevo mensaje!";
                $notification->body = "Tienes un nuevo mensaje en tu compra de '.$title.'";
                $notification->type = $type;
                $notification->user_id = $this->id;
                $notification->publication_id = $publication_id;
                $notification->transaction_id = $id;
                $notification->save();
            break;
        
            case 'question':
                if (!empty($this->fcm_token)) {
                    $body = '{
                        "to" : "'.$this->fcm_token.'",
                        "notification" : {
                          "body" : "Tienes una nueva pregunta en tu artículo '.$title.'",
                          "title" : "¡Tienes una nueva pregunta!",
                          "icon" : "ic_notification",
                          "sound": "default",
                          "click_action":"FCM_PLUGIN_ACTIVITY"
                        },
                        "data":{
                            "type":"'.$type.'",
                            "id": '.$id.',
                            "message": "Tienes una nueva pregunta en tu artículo '.$title.'"
                        }
                    }';
                    $promise = $client->postAsync('https://fcm.googleapis.com/fcm/send', ['headers' => $headers, 'body' => $body]);
                    $promise->wait();
                }
                $notification = new \App\Notifications();
                $notification->title = "¡Tienes una nueva pregunta!";
                $notification->body = "Tienes una nueva pregunta en tu artículo '.$title.'";
                $notification->type = $type;
                $notification->user_id = $this->id;
                $notification->publication_id = $publication_id;
                $notification->save();
            break;
        
            case 'response':
                if (!empty($this->fcm_token)) {
                    $body = '{
                        "to" : "'.$this->fcm_token.'",
                        "notification" : {
                          "body" : "Han respondido tu pregunta en el artículo '.$title.'",
                          "title" : "¡Han respondido tu pregunta!",
                          "icon" : "ic_notification",
                          "sound": "default",
                          "click_action":"FCM_PLUGIN_ACTIVITY"
                        },
                        "data":{
                            "type":"'.$type.'",
                            "id": '.$id.',
                            "message": "Han respondido tu pregunta en el artículo '.$title.'"
                        }
                    }';
                    $promise = $client->postAsync('https://fcm.googleapis.com/fcm/send', ['headers' => $headers, 'body' => $body]);
                    $promise->wait();
                }
                $notification = new \App\Notifications();
                $notification->title = "¡Han respondido tu pregunta!";
                $notification->body = "Han respondido tu pregunta en el artículo '.$title.'";
                $notification->type = $type;
                $notification->user_id = $this->id;
                $notification->publication_id = $publication_id;
                $notification->save();
            break;
        }
    }

    /**
     *
     */
    public static function updateLatestNotificationView()
    {
        $user = Auth::user();
        $user->latest_notification_view = date('Y-m-d H:i:s');
        $user->save();
    }

    /**
     * @param Request $request
     */
    public static function updateDocumentToUserLoggedIn(Request $request)
    {
        $user = Auth::user();
        $user->document_type = $request->document_type;
        $user->document = $request->document;
        $user->save();
        return $user;
    }
}

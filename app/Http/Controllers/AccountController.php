<?php

namespace App\Http\Controllers;

use App\BankAccount;
use App\Configuration;
use App\Helpers\PlanHelper;
use App\Http\Requests\AddressRequest;
use App\Image;
use Illuminate\Http\Response;
use \Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Validator;
use App\Department;
use App\Province;
use App\Address;
use App\District;
use App\User;
use App\Message;
use App\TicketItem;
use App\Category;
use App\PublicationIndex;
use App\ShippingCost;
use App\Parameter;
use App\Question;
use App\Ticket;
use App\Notifications\AnsweredQuestion as AnsweredQuestionNotification;
use App\Notifications\QualificationSeller as QualificationSellerNotification;
use App\Notifications\QualificationBuyer as QualificationBuyerNotification;
use App\Notifications\QualificationSellerChanged as QualificationSellerChangedNotification;
use App\Notifications\QualificationBuyerChanged as QualificationBuyerChangedNotification;
use App\Notifications\QualificationSellerConfirm as QualificationSellerConfirmNotification;
use App\Notifications\QualificationBuyerConfirm as QualificationBuyerConfirmNotification;

/**
 * Class AccountController
 * @package App\Http\Controllers
 */
class AccountController extends Controller
{

    /**
     * AccountController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/login');
        }

        $user = Auth::user();
        $reputation = User::user_reputation($user);

        $num_positives = $user->sales()->where('buyer_calification', 'Positivo')->count();
        $num_negatives = $user->sales()->where('buyer_calification', 'Negativo')->count();
        $num_neutral = $user->sales()->where('buyer_calification', 'Neutral')->count();

        return response()->view('account.reputation', [
                'reputation' => $reputation,
                'num_positives' => $num_positives,
                'num_negatives' => $num_negatives,
                'num_neutral' => $num_neutral,
            ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function personal_data(Request $request)
    {
        $addresses = Auth::user()->addresses()->orderBy('default_for_sales', 'DESC')->get();
        return response()->view('account.personal_data', ['addresses' => $addresses]);
    }

	/**
	 * @param Request $request
	 * @return mixed
	 */
    public function modify_username(Request $request)
    {
	    $validator = Validator::make($request->all(), [
            'username' => 'required|min:6|max:20|unique:users',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/profile')->with(['result' => false, 'message' =>
	            'Error modificando nombre de usuario. Campos incompletos']);
        }

        $user = Auth::user();

        if (!Auth::attempt(['email' => $user->email, 'password' => $request->get('password')])) {
            return redirect('/profile')->with(['result' => false, 'message' =>
	            'Error modificando nombre de usuario. Contraseña incorrecta']);
        }

        $user->username = $request->get('username');
        $user->save();

        return redirect('/profile')->with(['result' => true, 'message' =>
	        'Nombre de usuario modificado con éxito']);
    }

	/**
	 * @param Request $request
	 * @return mixed
	 */
    public function modify_password(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'password' => 'required|min:6|max:20|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect('/profile')->with(['result' => false, 'message' =>
	            'Error modificando contraseña. Campos incompletos']);
        }

        $user = Auth::user();

        if (!Auth::attempt(['email' => $user->email, 'password' => $request->get('old_password')])) {
            return redirect('/profile')->with(['result' => false, 'message' =>
	            'Error modificando contraseña. Antigua contraseña incorrecta']);
        }

        $user->password = bcrypt($request->get('password'));
        $user->save();

        return redirect('/profile')->with(['result' => true, 'message' => 'Contraseña modificada con éxito']);
    }

    public function modify_email(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:100|unique:users|confirmed',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            Session::flash('result', false);
            Session::flash('message',
                'Error modificando e-mail. Campos incompletos');
            return redirect('/profile');
        }

        $user = Auth::user();

        if (!Auth::attempt(['email' => $user->email, 'password' => $request->get('password')])) {
            return redirect('/profile')->with(['result' => false, 'message' => 'Error modificando e-mail. Contraseña incorrecta']);
        }

        $user->email = $request->get('email');
        $user->save();

        return redirect('/profile')->with(['result' => true, 'message' => 'E-mail modificado con éxito']);
    }

    public function modify_phone(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|numeric|unique:users',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/profile')->with(['result' => false, 'message', 'Error modificando número de teléfono. Campos incompletos']);
        }

        $user = Auth::user();

        if (!Auth::attempt(['email' => $user->email, 'password' => $request->get('password')])) {
            return redirect('/profile')->with(['result' => false, 'message' => 'Error modificando número de teléfono. Contraseña incorrecta']);
        }

        $user->phone_number = $request->get('phone_number');
        $user->save();

        return redirect('/profile')->with(['result' => true, 'message' => 'Número de teléfono modificado con éxito']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function new_address(Request $request)
    {
        $vars = ['departments' => Department::all(),
            'provinces' => [], 'districts' => []];

        if (!empty(old('department'))) {
            $vars['provinces'] = Department::find(old('department'))->provinces;

            if (!empty(old('province'))) {
                $vars['districts'] = Province::find(old('province'))->districts;
            }
        }

        return response()->view('account.add_address', $vars);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function add_address(AddressRequest $request)
    {

        /*$validator = Validator::make($request->all(), [
            'address_type' => 'required',
            'address_text' => 'required|max:60',
            'number' => 'required|numeric',
            'other_data' => 'max:80',
            'reference' => 'max:80',
            'department' => 'required',
            'province' => 'required',
            'district' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/newaddress')
                ->withErrors($validator)
                ->withInput();
        }*/

        $user = Auth::user();

        $address = new Address([
            'address_type' => $request->get('address_type'),
            'address_text' => $request->get('address_text'),
            'number' => $request->get('number'),
            'other_data' => $request->get('other_data'),
            'reference' => $request->get('reference'),
            'district_id' => $request->get('district'),
            'user_id' => $user->id,
        ]);

        if ($request->get('default_for_sales') != null) {
            $dft_address = $user->addresses()->where('default_for_sales', true)->first();
            $dft_address->default_for_sales = false;
            $dft_address->send_with_email = false;
            $dft_address->save();
            $address->default_for_sales = true;
        }

        $address->save();

        return redirect('/profile')->with(['result' => true, 'message' => 'Domicilio agregado con éxito']);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function edit_address($id)
    {
        $address = Auth::user()->addresses()->find($id);

        if ($address == null) {
            return redirect('/profile');
        }

        $departments = Department::all();

        $selected_district = empty(old('district')) ? $address->district : District::find(old('district'));
        $selected_province = empty(old('province')) ? $selected_district->province : Province::find(old('province'));
        $selected_department = empty(old('department')) ? $selected_province->department : Department::find(old('department'));

        $provinces = $selected_department->provinces;
        $districts = $selected_province->districts;

        if (!empty(old('address_type'))) {
            $address->address_type = old('address_type');
        }

        if (!empty(old('address_text'))) {
            $address->address_text = old('address_text');
        }

        if (!empty(old('number'))) {
            $address->number = old('number');
        }

        if (!empty(old('other_data'))) {
            $address->other_data = old('other_data');
        }

        if (!empty(old('reference'))) {
            $address->reference = old('reference');
        }

        return response()->view('account.mod_address', ['address' => $address,
                'departments' => $departments,
                'provinces' => $provinces,
                'districts' => $districts,
                'sel_department' => $selected_department,
                'sel_province' => $selected_province,
                'sel_district' => $selected_district
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function mod_address(Request $request)
    {
        $address = Auth::user()->addresses()->find($request->id);

        if ($address == null) {
            return redirect('/profile');
        }

        $validator = Validator::make($request->all(), [
            'address_type' => 'required',
            'address_text' => 'required|max:60',
            'number' => 'required|numeric',
            'other_data' => 'max:80',
            'reference' => 'max:80',
            'department' => 'required',
            'province' => 'required',
            'district' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/edit_address/' . $id)
                ->withErrors($validator)
                ->withInput();
        }

        $address->address_type = $request->get('address_type');
        $address->address_text = $request->get('address_text');
        $address->number = $request->get('number');
        $address->other_data = $request->get('other_data');
        $address->reference = $request->get('reference');
        $address->district_id = $request->get('district');
        $address->save();

        return redirect('/profile')->with(['result' => true, 'message', 'Domicilio modificado con éxito']);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function use_address_for_sales($id)
    {
        $address = Auth::user()->addresses()->find($id);

        if ($address == null) {
            return redirect('/profile');
        }

        if ($address->default_for_sales) {
            return redirect('/profile');
        }

        $dft_address = Auth::user()->addresses()->where('default_for_sales', true)->first();
        $dft_address->default_for_sales = false;
        $dft_address->send_with_email = false;
        $dft_address->save();

        $address->default_for_sales = true;
        $address->save();

        return redirect('/profile');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function use_address_for_purchases($id)
    {
        $address = Auth::user()->addresses()->find($id);

        if ($address == null) {
            return redirect('/profile');
        }

        if ($address->default_for_purchases) {
            return redirect('/profile');
        }

        $dft_address = Auth::user()->addresses()->where('default_for_purchases', true)->first();

        if ($dft_address != null) {
            $dft_address->default_for_purchases = false;
            $dft_address->save();
        }
        $address->default_for_purchases = true;
        $address->save();

        return redirect('/profile');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function remove_address($id)
    {
        $address = Auth::user()->addresses()->find($id);

        if (is_null($address)) {
            return redirect('/profile');
        }

        if ($address->default_for_sales) {
            Session::flash('result', false);
            Session::flash('message', 'No se puede eliminar el domicilio que se usa para ventas');
        } else {
            Session::flash('result', true);
            Session::flash('message', 'Se eliminó el domicilio');
            $address->delete();
        }

        return redirect('/profile');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function set_send_with_email(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }

        $dft_address = Auth::user()->addresses()->where('default_for_sales', true)->first();

        $dft_address->send_with_email = $request->get('value');
        $dft_address->save();

        return response()->json(['result' => true]);
    }

    // Preferencias de email
    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function email_preferences(Request $request)
    {
        $emails = Auth::user()->email_preference()->first();
        return response()->view('account.emails', ['emails' => $emails]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update_email_preferences(Request $request)
    {
        $emails = Auth::user()->email_preference()->first();
        $emails->sold_product = $request->get('sold_product');
        $emails->somebody_asked_you = $request->get('somebody_asked_you');
        $emails->your_publication_finished = $request->get('your_publication_finished');
        $emails->automatic_republishing = $request->get('automatic_republishing');
        $emails->bought_product = $request->get('bought_product');
        $emails->used_auto_offer = $request->get('used_auto_offer');
        $emails->publication_almost_finish = $request->get('publication_almost_finish');
        $emails->offers_and_promotions = $request->get('offers_and_promotions');
        $emails->terms_and_conditions_changes = $request->get('terms_and_conditions_changes');
        $emails->save();

        return redirect('/emails')->with(['message' => 'La configuración fue guardada con éxito']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function summary(Request $request)
    {
        $user = Auth::user();
        $num_questions = $user->publications()
            ->select('publications.*')
            ->addSelect('questions.*')
            ->join('questions', 'publications.id', '=', 'questions.publication_id')
            ->where('publications.status', '<>', 'Canceled')
            ->where('publications.status', '<>', 'Finished')
            ->where('publications.status', '<>', 'Draft')
            ->where('questions.answer', '')
            ->whereRaw('questions.created_at > (NOW() - INTERVAL 60 DAY)')->count();

        $num_actives = $user->publications()
            ->where('status', 'Active')
            ->count();

        $num_paused = $user->publications()
            ->where('status', 'Paused')->count();

        $num_finished = $user->publications()
            ->where('status', 'Finished')
            ->whereRaw('publications.created_at > (NOW() - INTERVAL 365 DAY)')->count();

        $available_for_qualifying = date('Y-m-d H:i:s', strtotime("-" . config('app.global.days_to_qualify') . " days"));
        $num_purchases = $user->purchases()
            ->where('buyer_calification', 'No calificado')
            ->where('created_at', '>=', $available_for_qualifying)->count();

        $num_sales = $user->sales()
            ->where('seller_calification', 'No calificado')
            ->where('created_at', '>=', $available_for_qualifying)->count();

        $num_sales_no_buyer = $user->sales()
            ->where('buyer_calification', 'No calificado')
            ->where('seller_calification', '!=', 'No calificado')
            ->where('created_at', '>=', $available_for_qualifying)->count();

        return response()->view('account.summary', [
                'num_questions' => $num_questions,
                'num_actives' => $num_actives,
                'num_paused' => $num_paused,
                'num_finished' => $num_finished,
                'num_purchases' => $num_purchases,
                'num_sales' => $num_sales,
                'num_sales_no_buyer' => $num_sales_no_buyer,
            ]);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function favorites(Request $request)
    {
        $user = Auth::user();
        $vars = [];

        $vars['favorites'] = $user->favorites()
            ->where(function ($query) {
                $query->where('publications.status', 'Active')
                    ->orWhere('publications.status', 'Paused')
                    ->orWhere('publications.status', 'Finished');
            })
            ->whereRaw('publications.created_at > (NOW() - INTERVAL 90 DAY)')
            ->orderBy('favorites.id', 'desc')->get();

        return response()->view('account.favorites', $vars);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function myquestions(Request $request)
    {

        $user = Auth::user();
        $vars = [];

        $vars['pubs'] = $user->questions()
            ->select('publications.*')
            ->join('publications', 'publications.id', '=', 'questions.publication_id')
            ->where('publications.status', '<>', 'Canceled')
            ->where('publications.status', '<>', 'Finished')
            ->where('publications.status', '<>', 'Draft')
            ->whereRaw('questions.created_at > (NOW() - INTERVAL 60 DAY)')
            ->orderBy('questions.created_at', 'desc')->distinct()->get();

        return response()->view('account.myquestions', $vars);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function delete_questions(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', 401);
        }
        if (!Auth::check()) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }
        $user = Auth::user();
        $user->questions()->where('publication_id', $request->id)->delete();

        return response()->json(['is_ok' => true, 'value' => true]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getInfoPayment(Request $request)
    {
        if ($request->ajax()) {
            return BankAccount::where('bank', $request->get('payment'))->get();
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function purchases(Request $request)
    {
        $user = Auth::user();
        $vars = [];

        $vars['purchases'] = $user->purchases()
            ->whereRaw('transactions.created_at > (NOW() - INTERVAL 365 DAY)')
            ->orderBy('transactions.created_at', 'desc')
            ->orderBy('transactions.id', 'desc')->paginate(10);

        return response()->view('account.purchases', $vars);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function purchase_detail(Request $request)
    {

        $user = Auth::user();
        $vars = [];

        if (!$vars['purchase'] = $user->purchases()->where('id', $request->get('id'))->first()) {
            abort(404);
        }
        if ($vars['purchase']->seller->id != $user->id and $vars['purchase']->buyer->id != $user->id) {
            abort(404);
        }
        $vars['p'] = $vars['purchase']->publication;
        $vars['messages'] = $vars['purchase']->messages()
            ->orderBy('created_at', 'asc')->get();

        return response()->view('account.purchase_detail', $vars);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function save_message(Request $request)
    {

        $user = Auth::user();
        $publication = null;
        $type = 'purchase';
        if (!$purchase = $user->purchases()->where('id', $request->get('id'))->first()) {
            $type = 'sale';
            if (!$purchase = $user->sales()->where('id', $request->get('id'))->first()) {
                return response()->view('account.sale_detail');
            }
        }

        if (!$publication = $purchase->publication) {
            return response()->view('account.' . $type . '_detail');
        }
//        $this->validate($request, [
//            'files[]' => 'required|image|mimes:jpeg,png,jpg,gif|max:4096',
//        ]);

        $message = new Message();
        if ($request->file('files')) {
            foreach ($request->file('files') as $key => $f) {
                $imageName = md5(time() . $request->get('id') . $key) . $user->id . '.' . $f->getClientOriginalExtension();
                $f->move(public_path('uploads/messages'), $imageName);
                $message->{"image" . ($key + 1)} = $imageName;
                $message->{"name" . ($key + 1)} = $f->getClientOriginalName();
            }
        }

        $message->publication_id = $publication->id;
        //$image->name = $imageName;
        $message->user_id = $user->id;
        $message->transaction_id = $request->get('id');
        $message->message = $request->get('message');
        $message->save();

        if ($type == 'sale') {
            $buyer = User::find($purchase->buyer_id);
            $buyer->sendMobileNotification('message-buyer', $purchase->id, $publication->title, $publication->id);
        }

        if ($type == 'purchase') {
            $seller = User::find($publication->user_id);
            $seller->sendMobileNotification('message-seller', $purchase->id, $publication->title, $publication->id);
        }

        return redirect('/' . $type . '-detail?id=' . $purchase->id);

    }

    /**
     * @param Request $request
     * @return BinaryFileResponse
     */
    public function download(Request $request)
    {
        $file = $request->get('file');
        if (empty($file)) {
            abort(404);
        }
        $name = (empty($request->get('name')) ? $file : $request->get('name'));

        return response()->download(public_path('uploads/messages') . '/' . $file, $name);
    }

    /**
     * @param Request $request
     * @return BinaryFileResponse
     */
    public function download_payments(Request $request)
    {
        $file = $request->get('file');
        if (empty($file)) {
            abort(404);
        }
        $name = (empty($request->get('name')) ? $file : $request->get('name'));

        return response()->download(public_path('uploads/payments') . '/' . $file, $name);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function buyer_qualification(Request $request)
    {
        $user = Auth::user();
        $publication = null;
        if (!$purchase = $user->purchases()->where('id', $request->get('id'))->first() or
            $purchase->buyer_calification != 'No calificado'
        ) {
            abort(404);
        }

        if ($purchase->buyer_qualification_modified) {
            return redirect('/purchases');
        }

        $datetime1 = new \DateTime($purchase->created_at);
        $datetime2 = new \DateTime(date("Y-m-d H:i:s"));
        $interval = $datetime1->diff($datetime2);
        if ($interval->format('%a') > config('app.global.days_to_modify_qualify')) {
            return redirect('/purchases');
        }

        if (!$publication = $purchase->publication) {
            abort(404);
        }

        $vars['publication'] = $publication;
        $vars['purchase'] = $purchase;
        $vars['id'] = $request->get('id');

        return response()->view('account.buyer_qualification', $vars);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function buyer_qualification_edit(Request $request)
    {
        $user = Auth::user();
        $publication = null;
        if (!$purchase = $user->purchases()->where('id', $request->get('id'))->first()) {
            abort(404);
        }

        if (!$publication = $purchase->publication) {
            abort(404);
        }

        if ($purchase->buyer_qualification_modified) {
            return redirect('/purchases');
        }
        $datetime1 = new \DateTime($purchase->created_at);
        $datetime2 = new \DateTime(date("Y-m-d H:i:s"));
        $interval = $datetime1->diff($datetime2);
        if ($interval->format('%a') > config('app.global.days_to_modify_qualify')) {
            return redirect('/purchases');
        }

        $vars['publication'] = $publication;
        $vars['purchase'] = $purchase;
        $vars['id'] = $request->get('id');

        return response()->view('account.buyer_qualification_edit', $vars);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function buyer_qualification_save(Request $request)
    {
        $messages = [
            'concreted.required' => 'Indícanos si recibiste el producto.',
            'recommended.required' => 'Indícanos como te fue con el vendedor.',
            'no_concreted_type.required' => 'Indícanos que tipo de problema tuviste con el vendedor.',
            'opinion.required' => 'Comentanos tu opinión del vendedor.',
            'opinion.max' => 'Longitud máxima del comentario :max caracteres.',
        ];
        $fields = [
            'concreted' => 'required',
            'recommended' => 'required',
            'opinion' => 'required|max:200',
            'id' => 'required',
        ];

        if ($request->get('concreted') == '-1') {
            $fields['no_concreted_type'] = 'required';
        }

        $validator = Validator::make($request->all(), $fields, $messages);

        if ($validator->fails()) {
            return redirect('/buyer-qualification?id=' . $request->get('id'))
                ->withErrors($validator)
                ->withInput();
        }
        $user = Auth::user();
        $publication = null;
        if (!$purchase = $user->purchases()->where('id', $request->get('id'))->first() or
            $purchase->buyer_calification != 'No calificado'
        ) {
            abort(404);
        }

        if ($purchase->buyer_qualification_modified) {
            return redirect('/purchases');
        }

        $datetime1 = new \DateTime($purchase->created_at);
        $datetime2 = new \DateTime(date("Y-m-d H:i:s"));
        $interval = $datetime1->diff($datetime2);
        if ($interval->format('%a') > config('app.global.days_to_modify_qualify')) {
            return redirect('/purchases');
        }

        if (!$publication = $purchase->publication) {
            abort(404);
        }

        $purchase->buyer_concreted = ($request->get('concreted') == '1' ? '1' : '0');
        if ($request->get('concreted') == '-1') {
            $purchase->concreted_problem = $request->get('no_concreted_type');
        } else {
            $purchase->concreted_problem = '';
        }

        $purchase->buyer_recommends = $request->get('recommended');
        if ($request->get('recommended') == 'Sí') {
            $purchase->buyer_calification = 'Positivo';
        } elseif ($request->get('recommended') == 'No') {
            $purchase->buyer_calification = 'Negativo';
        } elseif ($request->get('recommended') == 'No estoy seguro') {
            $purchase->buyer_calification = 'Neutral';
        }
        $purchase->buyer_qualification_date = date('Y-m-d H:i:s');
        $purchase->buyer_comment = $request->get('opinion');
        $purchase->save();

        $seller = User::find($publication->user_id);
        $seller->notify(new QualificationSellerNotification($seller, $publication, $purchase));
        $seller->notifyCanBeLeader();
        $seller->keppingLeader();

        return redirect('/purchases');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function buyer_qualification_edit_save(Request $request)
    {
        $messages = [
            'concreted.required' => 'Indícanos si recibiste el producto.',
            'recommended.required' => 'Indícanos como te fue con el vendedor.',
            'no_concreted_type.required' => 'Indícanos que tipo de problema tuviste con el vendedor.',
            'opinion.required' => 'Comentanos tu opinión del vendedor.',
            'opinion.max' => 'Longitud máxima del comentario :max caracteres.',
        ];
        $fields = [
            'concreted' => 'required',
            'recommended' => 'required',
            'opinion' => 'required|max:200',
            'id' => 'required',
        ];

        if ($request->get('concreted') == '-1') {
            $fields['no_concreted_type'] = 'required';
        }

        $validator = Validator::make($request->all(), $fields, $messages);

        if ($validator->fails()) {
            return redirect('/buyer-qualification-edit?id=' . $request->get('id'))
                ->withErrors($validator)
                ->withInput();
        }
        $user = Auth::user();
        $publication = null;
        if (!$purchase = $user->purchases()->where('id', $request->get('id'))->first()) {
            abort(404);
        }

        if ($purchase->buyer_qualification_modified) {
            return redirect('/purchases');
        }

        $datetime1 = new \DateTime($purchase->created_at);
        $datetime2 = new \DateTime(date("Y-m-d H:i:s"));
        $interval = $datetime1->diff($datetime2);
        if ($interval->format('%a') > config('app.global.days_to_modify_qualify')
            or ($request->get('recommended') == 'No' and $purchase->buyer_calification != 'Negativo')
        ) {
            return redirect('/purchases');
        }

        if (!$publication = $purchase->publication) {
            abort(404);
        }

        $purchase->buyer_concreted = ($request->get('concreted') == '1' ? '1' : '0');
        if ($request->get('concreted') == '-1') {
            $purchase->concreted_problem = $request->get('no_concreted_type');
        } else {
            $purchase->concreted_problem = '';
        }

        $purchase->buyer_recommends = $request->get('recommended');
        if ($request->get('recommended') == 'Sí') {
            $purchase->buyer_calification = 'Positivo';
        } elseif ($request->get('recommended') == 'No') {
            $purchase->buyer_calification = 'Negativo';
        } elseif ($request->get('recommended') == 'No estoy seguro') {
            $purchase->buyer_calification = 'Neutral';
        }
        $purchase->buyer_comment = $request->get('opinion');
        $purchase->buyer_qualification_modified_date = date('Y-m-d H:i:s');
        $purchase->buyer_qualification_modified = true;
        $purchase->save();

        $seller = User::find($publication->user_id);
        if ($purchase->seller_calification != 'No calificado' and
            $purchase->buyer_calification != 'No calificado' and
            $purchase->seller_concreted != $purchase->buyer_concreted
        ) {
            $seller->notify(new \App\Notifications\ContraQualificationSeller($seller, $publication, $purchase));
        }

        $seller->notify(new QualificationSellerChangedNotification($seller, $publication, $purchase));
        $seller->notifyCanBeLeader();
        $seller->keppingLeader();

        return redirect('/purchases')->with(['result' => true, 'message' => 'Calificación Modificada']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function qualification(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', 401);
        }

        $user = Auth::user();
        $publication = null;
        if (!$purchase = $user->purchases()->where('id', $request->get('id'))->first()) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }

        if (!$publication = $purchase->publication) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }

        $result = [];
        $result['buyer_qualification'] = $purchase->buyer_calification;
        $result['buyer_comment'] = $purchase->buyer_comment;

        $datetime1 = new \DateTime($purchase->created_at);
        $datetime2 = new \DateTime(date("Y-m-d H:i:s"));
        $interval = $datetime1->diff($datetime2);
        if ($purchase->buyer_qualification_modified || $interval->format('%a') > config('app.global.days_to_modify_qualify')) {
            $result['can_modify'] = 'false';
        } else {
            $result['can_modify'] = 'true';
        }
        $result['seller_qualification'] = $purchase->seller_calification;
        $result['seller_comment'] = $purchase->seller_comment;
        $result['buyer_reply'] = $purchase->buyer_reply;
        $result['seller_reply'] = $purchase->seller_reply;

        return response()->json(['is_ok' => true, 'value' => json_encode($result)]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function qualification_seller(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', 401);
        }

        $user = Auth::user();
        $publication = null;
        if (!$purchase = $user->sales()->where('id', $request->get('id'))->first()) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }

        if (!$publication = $purchase->publication) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }

        $result = [];
        $result['buyer_qualification'] = $purchase->buyer_calification;
        $result['buyer_comment'] = $purchase->buyer_comment;

        $datetime1 = new \DateTime($purchase->created_at);
        $datetime2 = new \DateTime(date("Y-m-d H:i:s"));
        $interval = $datetime1->diff($datetime2);
        if ($purchase->seller_qualification_modified || $interval->format('%a') > config('app.global.days_to_modify_qualify')) {
            $result['can_modify'] = 'false';
        } else {
            $result['can_modify'] = 'true';
        }
        $result['seller_qualification'] = $purchase->seller_calification;
        $result['seller_comment'] = $purchase->seller_comment;
        $result['buyer_reply'] = $purchase->buyer_reply;
        $result['seller_reply'] = $purchase->seller_reply;

        return response()->json(['is_ok' => true, 'value' => json_encode($result)]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function buyer_reply_edit(Request $request)
    {
        $user = Auth::user();
        $publication = null;
        if (!$purchase = $user->purchases()->where('id', $request->get('id'))->first() or
            $purchase->seller_calification == 'No calificado'
        ) {
            abort(404);
        }

        if (!$publication = $purchase->publication) {
            abort(404);
        }

        $datetime1 = new \DateTime($purchase->created_at);
        $datetime2 = new \DateTime(date("Y-m-d H:i:s"));
        $interval = $datetime1->diff($datetime2);
        if ($interval->format('%a') > config('app.global.days_to_modify_qualify')) {
            return redirect('/purchases');
        }

        $vars['publication'] = $publication;
        $vars['purchase'] = $purchase;
        $vars['id'] = $request->get('id');

        return response()->view('account.buyer_reply_edit', $vars);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function buyer_reply_edit_save(Request $request)
    {
        $fields = [
            'replica' => 'required|max:200',
            'id' => 'required',
        ];

        $validator = Validator::make($request->all(), $fields);

        if ($validator->fails()) {
            return redirect('/buyer-reply-edit?id=' . $request->get('id'))
                ->withErrors($validator)
                ->withInput();
        }
        $user = Auth::user();
        $publication = null;
        if (!$purchase = $user->purchases()->where('id', $request->get('id'))->first() or
            $purchase->seller_calification == 'No calificado'
        ) {
            abort(404);
        }

        $datetime1 = new \DateTime($purchase->created_at);
        $datetime2 = new \DateTime(date("Y-m-d H:i:s"));
        $interval = $datetime1->diff($datetime2);
        if ($interval->format('%a') > config('app.global.days_to_modify_qualify')) {
            return redirect('/purchases');
        }

        if (!$publication = $purchase->publication) {
            abort(404);
        }

        $purchase->buyer_reply = $request->get('replica');
        $purchase->save();

        return redirect('/purchases');

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function publications(Request $request)
    {

        $user = Auth::user();
        $vars = [];

        $query_string = $request->all();
        unset($query_string['page']);
        $vars['query'] = $query_string;
        $status = (!empty($request->get('status')) ? $request->get('status') : 'active');
        $sort = (!empty($request->get('sort')) ? $request->get('sort') : '1');

        if ($status == 'revision') {
            $q = $user->publications()
                ->where(function ($query) {
                    $query->where('publications.status', 'Revision')
                        ->orWhere('publications.status', 'Canceled');
                });
        } else {
            $q = $user->publications()
                ->where('publications.status', $status);
        }

        if ($status == 'finished' or $status == 'revision') {
            $q->whereRaw('publications.created_at > (NOW() - INTERVAL 365 DAY)');
        }

        switch ($sort) {
            case 1:
                $q->orderBy('publications.ends_at', 'asc');
                break;
            case 2:
                $q->orderBy('publications.ends_at', 'desc');
                break;
            case 3:
                $q->orderBy('publications.available_quantity', 'desc');
                break;
            case 4:
                $q->orderBy('publications.available_quantity', 'asc');
                break;
            case 5:
                $q->orderBy('publications.price', 'desc');
                break;
            case 6:
                $q->orderBy('publications.price', 'asc');
                break;
        }

        $vars['publications'] = $q->paginate(10);
        $vars['status'] = $status;
        $vars['sort'] = $sort;

        $vars['actives'] = $user->publications()
            ->where('publications.status', 'Active')->count();

        $vars['paused'] = $user->publications()
            ->where('publications.status', 'Paused')->count();

        $vars['finished'] = $user->publications()
            ->where('publications.status', 'Finished')
            ->whereRaw('publications.created_at > (NOW() - INTERVAL 365 DAY)')
            ->count();

        $vars['revision'] = $user->publications()
            ->where(function ($query) {
                $query->where('publications.status', 'Revision')
                    ->orWhere('publications.status', 'Canceled');
            })
            ->whereRaw('publications.created_at > (NOW() - INTERVAL 365 DAY)')
            ->count();

        if ($status == 'active') $vars['status_txt'] = 'activa';
        if ($status == 'paused') $vars['status_txt'] = 'pausada';
        if ($status == 'finished') $vars['status_txt'] = 'finalizada';
        if ($status == 'revision') $vars['status_txt'] = 'en revisión';
        $vars['conf'] = DB::table('configurations')->limit(1)->first();

        return response()->view('account.publications', $vars);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function plan(Request $request)
    {
        $user = Auth::user();
        $publication = null;
        if (!$publication = $user->publications()
            ->where('id', $request->get('id'))
            ->where('status', 'Active')
            ->where('cost_type', '<>', 'Fijo')
            ->where('cost_type', '<>', 'Premium')
            ->first()
        ) {
            abort(404);
        }
        return response()->view('account.plan', ['publication' => $publication]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save_plan(Request $request)
    {
        $user = Auth::user();
        $publication = null;
        if (!$publication = $user->publications()
            ->where('id', $request->get('id'))
            ->where('status', 'Active')
            ->where('cost_type', '<>', 'Fijo')
            ->where('cost_type', '<>', 'Premium')
            ->first()
        ) {
            return redirect()->route('publications');
        }
        if ($request->get('plan') != 'Bronce' &&
            $request->get('plan') != 'Plata' &&
            $request->get('plan') != 'Oro' &&
            $request->get('plan') != 'Premium'
        ) {
            return redirect()->route('publications');
        }
        $conf = Configuration::limit(1)->first();
        $pub_type = 'Premium';
        if ($publication->getProductType() === 'producto') {
            $price = $conf->premium_cost;
	        $price = PlanHelper::getPlan($request, $conf);
            $publication->cost_type = $request->get('plan');
            $publication->ends_at = date('Y-m-d H:i:s', strtotime("+" . config('app.global.publication_lifetime_days') . " days"));
            $publication->cost = $price;
        } else {
            return redirect()->route('publications');
        }

        //$publication->status = 'Active';
        //$publication->publicated_as_active_at = date("Y-m-d H:i:s");

        $publication->save();

        //Creating the item in ticket
        if ($pub_type !== 'Gratis') {
            if (!$ticket = $user->tickets()->where('status', 'Open')->first()) {
                $ticket = new Ticket();
                $ticket->user_id = $user->id;
                $ticket->status = 'Open';
                $limit_emitted = strtotime("-" . config('app.global.days_before_cutoff_notification') . " days", strtotime(date('Y-m-' . $user->cutoff_day . ' 23:30')));
                if (time() > $limit_emitted) {
                    $emit_on = date('Y-m-d', strtotime("+1 month", strtotime(date('Y-m-' . $user->cutoff_day))) - config('app.global.days_before_cutoff_notification') * 86400);
                } else {
                    $emit_on = date('Y-m-d', $limit_emitted);
                }
                $ticket->emit_on = $emit_on;
                $ticket->save();
            }

            $item = new TicketItem();
            $item->ticket_id = $ticket->id;
            $item->publication_id = $publication->id;
            $item->amount = $price;
            $item->description = "Exposición de publicación nivel $pub_type.";

            $item->save();


            //if user has not a cutoff day
            if (!$user->cutoff_day) {
                $user->cutoff_day = (date("d") <= 28 ? date("d") : '28');
                $user->save();
            }
        }
        Session::flash('message', 'Tu publicación #' . $publication->id . ' ha sido modificada.');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('publications');

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function pause(Request $request)
    {
        $user = Auth::user();
        $publication = null;
        if (!$publication = $user->publications()
            ->where('id', $request->get('id'))
            ->where('status', 'Active')
            ->first()
        ) {
            return redirect()->route('publications');
        }

        $publication->status = 'Paused';
        $publication->save();

        Session::flash('message', 'Tu publicación #' . $publication->id . ' ha sido pausada.');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('publications');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate(Request $request)
    {
        $user = Auth::user();
        $publication = null;
        if (!$publication = $user->publications()
            ->where('id', $request->get('id'))
            ->where('status', 'Paused')
            ->first()
        ) {
            return redirect()->route('publications');
        }
        if ($user->suspended) {
            return redirect('/billing')->with(['message' => 'Has sido suspendido para vender, revisa si tienes facturas pendientes por pagar, si no es así por favor contacta a soporte.',
	            'alert-class' => 'alert-danger']);
        }

        if ($publication->lack_stock) {
            return redirect()->route('publications');
        }
        $publication->status = 'Active';
        $publication->save();

        return redirect('/publications')->with(['message', 'Tu publicación #' . $publication->id . ' ha sido activada.', 'alert-class' => 'alert-success']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function finish(Request $request)
    {
        $user = Auth::user();
        $publication = null;
        if (!$publication = $user->publications()
            ->where('id', $request->get('id'))
            ->where('status', 'Active')
            ->first()
        ) {
            return redirect()->route('publications');
        }

        $publication->status = 'Finished';
        $publication->save();

        Session::flash('message', 'Tu publicación #' . $publication->id . ' ha sido finalizada.');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('publications');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function shipping_cost(Request $request)
    {
        $user = Auth::user();
        $publication = null;
        if (!$publication = $user->publications()
                ->where('id', $request->get('id'))
                ->where(function ($query) {
                    $query->where('status', 'Active')->orWhere('status', 'Paused');
                })
                ->first()
            or $publication->getProductType() != 'producto'
        ) {
            return redirect()->route('publications');
        }
        if ($user->suspended) {
            Session::flash('message', 'Has sido suspendido para vender, revisa si tienes facturas pendientes por pagar, si no es así por favor contacta a soporte.');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('billing');
        }

        $vars['publication'] = $publication;
        $vars['id'] = $request->get('id');
        $vars['make_shipments'] = false;
        if ($publication->free_shipment or
            count($publication->shippingCosts)
        ) {
            $vars['make_shipments'] = true;
        }

        return response()->view('account.shipping_cost', $vars);

    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function change_category($id)
    {
        $user = Auth::user();
        $publication = null;
        if (!$publication = $user->publications()
            ->where('id', $id)
            ->where(function ($query) {
                $query->where('status', 'Active')->orWhere('status', 'Paused');
            })
            ->first()
        ) {
            return redirect()->route('publications');
        }
        if ($user->suspended) {
            Session::flash('message', 'Has sido suspendido para vender, revisa si tienes facturas pendientes por pagar, si no es así por favor contacta a soporte.');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('billing');
        }

        $category = $publication->topLevelCategory();
        $subcategories = $category->children()->orderBy('name')->get();

        $category_path = null;
        $features = null;

        $cp = $publication->category_path;
        $category_path = Category::getPathFromCodePath($cp);
        $features = $publication->features()->orderBy('feature_id')->get();

        return response()->view('account.change_category', [
            'category' => $category,
            'subcategories' => $subcategories,
            'category_path' => $category_path,
            'features' => $features,
            'publication' => $publication
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save_change_category(Request $request)
    {
        $user = Auth::user();
        $publication = null;
        if (!$publication = $user->publications()
            ->where('id', $request->get('id'))
            ->where(function ($query) {
                $query->where('status', 'Active')->orWhere('status', 'Paused');
            })
            ->first()
        ) {
            return redirect()->route('publications');
        }
        if ($user->suspended) {
            return redirect('/billing')->with(['message' => 'Has sido suspendido para vender, revisa si tienes facturas pendientes por pagar, si no es así por favor contacta a soporte.',
	            'alert-class' => 'alert-danger']);
        }

        $final_category = null;
        $features = [];
        for ($i = 1; $i < 10; $i++) {
            if ($request->get('category_id_' . $i)) {
                $final_category = $request->get('category_id_' . $i);
            } else {
                if ($request->get('feature_id_' . $i)) {
                    $features[] = $request->get('feature_id_' . $i);
                }
            }
        }

        if (!$category = Category::find($final_category)) {
            abort(404);
        }

        $publication->category_id = $category->id;
        $publication->category_path = $category->code;
        $publication->save();
        $publication->features()->detach();
        if (count($features)) {
            $publication->features()->attach($features);
        }

        if (!$publication_index = PublicationIndex::where('publication_id', $publication->id)->first()) {
            $publication_index = new PublicationIndex();
        }
        $publication_index->category_branch_string = $publication->getCategoryBranchString();
        $publication_index->save();

        Session::flash('message', 'Tu publicación #' . $publication->id . ' ha sido modificada.');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('publications');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function shipping_cost_save(Request $request)
    {
        $user = Auth::user();
        $publication = null;
        if (!$publication = $user->publications()
            ->where('id', $request->get('id'))
            ->where(function ($query) {
                $query->where('status', 'Active')->orWhere('status', 'Paused');
            })
            ->first()
        ) {
            return redirect()->route('publications');
        }
        if ($user->suspended) {
            Session::flash('message', 'Has sido suspendido para vender, revisa si tienes facturas pendientes por pagar, si no es así por favor contacta a soporte.');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('billing');
        }

        $publication->personal_pickup = ($request->get('personal_pickup') == 1 ? '1' : '0');
        $publication->free_shipment = 0;
        $publication->shippingCosts()->delete();

        if ($request->get('make_shipments') == '1') {
            if ($request->get('free_shipment') == '1') {
                $publication->free_shipment = '1';
            } else {
                $cost_names = $request->get('shipping_cost_name');
                $costs = $request->get('shipping_cost');

                if (count($cost_names)) {
                    foreach ($cost_names as $key => $value) {
                        if (!empty($value) and isset($costs[$key]) and !empty($costs[$key])) {
                            $cost = new ShippingCost();
                            $cost->name = $value;
                            $costs[$key] = str_replace('.', '', $costs[$key]);
                            $costs[$key] = str_replace(',', '.', $costs[$key]);
                            $cost->cost = $costs[$key];
                            $publication->shippingCosts()->save($cost);
                        }
                    }
                }
            }
        }

        $publication->save();

        Session::flash('message', 'Tu publicación #' . $publication->id . ' ha sido modificada.');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('publications');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function modify_publication($id)
    {
        $user = Auth::user();
        $publication = null;
        if (!$publication = $user->publications()
            ->where('id', $id)
            ->where(function ($query) {
                $query->where('status', 'Active')->orWhere('status', 'Paused');
            })
            ->first()
        ) {
            return redirect()->route('publications');
        }
        if ($user->suspended) {
            Session::flash('message', 'Has sido suspendido para vender, revisa si tienes facturas pendientes por pagar, si no es así por favor contacta a soporte.');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('billing');
        }

        $departments = Department::all();
        $images_not_ordered = $publication->images()->where(function ($query) {
            $query->where('position', '0')
                ->orWhereNull('position');
        })->orderBy('position')->get();
        $images = $publication->images()->where('position', '<>', '0')->orderBy('position')->get();
        $images = $images->merge($images_not_ordered);
        $vars = [
            'publication' => $publication,
            'images' => $images,
            'departments' => $departments,
            'provinces' => [],
            'districts' => []
        ];

        //Parameters
        $seguridad = [];
        foreach ($publication->parameters()->where('class', 'Seguridad')->get() as $s) {
            $seguridad[] = $s->name;
        }
        $vars['seguridad'] = $seguridad;

        $confort = [];
        foreach ($publication->parameters()->where('class', 'Confort')->get() as $s) {
            $confort[] = $s->name;
        }
        $vars['confort'] = $confort;

        $sonido = [];
        foreach ($publication->parameters()->where('class', 'Sonido')->get() as $s) {
            $sonido[] = $s->name;
        }
        $vars['sonido'] = $sonido;

        $exterior = [];
        foreach ($publication->parameters()->where('class', 'Exterior')->get() as $s) {
            $exterior[] = $s->name;
        }
        $vars['exterior'] = $exterior;

        $ambientes = [];
        foreach ($publication->parameters()->where('class', 'Ambientes')->get() as $s) {
            $ambientes[] = $s->name;
        }
        $vars['ambientes'] = $ambientes;

        $comodidades = [];
        foreach ($publication->parameters()->where('class', 'Comodidades')->get() as $s) {
            $comodidades[] = $s->name;
        }
        $vars['comodidades'] = $comodidades;

        $caracteristicas = [];
        foreach ($publication->parameters()->where('class', 'Caracteristicas Adicionales')->get() as $s) {
            $caracteristicas[] = $s->name;
        }
        $vars['caracteristicas'] = $caracteristicas;

        $zonas = [];
        foreach ($publication->parameters()->where('class', 'Zonas')->get() as $s) {
            $zonas[] = $s->name;
        }
        $vars['zonas'] = $zonas;
        ////

        $district = '';
        $province = '';
        $department = '';
        $vars['departamento'] = null;
        $vars['provincia'] = null;
        $vars['distrito'] = null;
        if (!empty($publication->address_district_id)) {
            $district = District::find($publication->address_district_id);
            $province = $district->province;
            $department = $province->department;
            $vars['distrito'] = $district->id;
            $vars['provincia'] = $province->id;
            $vars['departamento'] = $department->id;
        }
        if (!empty(old('departamento', $department))) {
            $d = (old('departamento') ?: ($department ? $department->id : ''));
            if ($d) {
                $provinces = Department::find($d)->provinces;
            }
            $vars['provinces'] = $provinces;

            if (!empty(old('provincia', $province))) {
                $d = (old('provincia') ?: ($province ? $province->id : ''));
                if ($d) {
                    $districts = Province::find($d)->districts;
                }
                $vars['districts'] = $districts;
            }
        }

        $vars['cannot_modify_title'] = $vars['publication']->transactions()
            ->where(function ($query) {
                $query->where('seller_concreted', '1')
                    ->orWhere('buyer_concreted', '1');
            })->count();


        return response()->view('account.modify_publication', $vars);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function modify_publication_save(Request $request)
    {
        $user = Auth::user();
        $publication = null;
        $id = $request->get('id');
        if (!$publication = $user->publications()
            ->where('id', $id)
            ->where(function ($query) {
                $query->where('status', 'Active')->orWhere('status', 'Paused');
            })
            ->first()
        ) {
            return redirect()->route('publications');
        }
        if ($user->suspended) {
            Session::flash('message', 'Has sido suspendido para vender, revisa si tienes facturas pendientes por pagar, si no es así por favor contacta a soporte.');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('billing');
        }

        $tipo_producto = $publication->getProductType();
        $data = $request->all();
        $data['precio'] = str_replace('.', '', $data['precio']);
        $data['precio'] = str_replace(',', '.', $data['precio']);

        $messages = [
            'title.required' => 'Introduce un título para tu publicación',
            'title.max' => 'El título de tu publicación debe contener :max caracteres como máximo.',
            'precio.min' => 'El precio de tu publicación debe ser mayor a 0.',
            'precio.max' => 'El precio máximo de publicación es de S/. 9.999.999,99',
            'cantidad.min' => 'La cantidad mínima de publicación es de :min producto',
            'cantidad.max' => 'La cantidad máxima de publicación es de :max productos',
        ];
        $validations = [
            'title' => 'required|max:60',
            'condition' => 'required',
            'cantidad' => 'required|integer|min:1|max:9999',
            'precio' => 'required|numeric|min:0.1|max:9999999',
        ];

        if ($tipo_producto === 'servicio' && $request->get('convenir') === '1') {
            unset($validations['precio']);
            $data['precio'] = 0;
        }

        $validator = Validator::make($data, $validations, $messages);


        if ($validator->fails()) {
            return redirect('/modify-publication/' . $id)
                ->withErrors($validator)
                ->withInput();
        }

        $publication->parameters()->delete();
        //If it is a vehicle
        if ($request->get('tipo_producto') === 'vehículo' || $request->get('tipo_producto') === 'vehículo de colección' || $request->get('tipo_producto') === 'moto' || $request->get('tipo_producto') === 'lancha' || $request->get('tipo_producto') === 'vehiculo') {

            if ($request->get('tipo_producto') === 'vehículo') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'año' => 'required',
                    'puertas' => 'required',
                    'kilometros' => 'required|integer|min:1|max:9999999',
                    'version' => 'required|max:30',
                ], $messages);
            }

            if ($request->get('tipo_producto') === 'vehículo de colección') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'año' => 'required',
                    'puertas' => 'required',
                    'kilometros' => 'required|integer|min:1|max:9999999',
                    'marca' => 'required',
                    'modelo' => 'required|max:30',
                ], $messages);
            }

            if ($request->get('tipo_producto') === 'moto') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'año' => 'required',
                    'kilometros' => 'required|integer|min:1|max:9999999',
                    'modelo' => 'required|max:30',
                    'version' => 'required|max:30',
                ], $messages);
            }

            if ($request->get('tipo_producto') === 'lancha') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'año' => 'required',
                    'marca' => 'required|max:30',
                    'tipo' => 'required',
                ], $messages);
            }
            if ($request->get('tipo_producto') === 'vehiculo') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'año' => 'required',
                    'marca' => 'required|max:30',
                    'modelo' => 'required|max:30',
                    'kilometros' => 'required|integer|min:1|max:9999999',
                ], $messages);
            }

            if ($validator->fails()) {
                return redirect('modify-publication/' . $id)
                    ->withErrors($validator)
                    ->withInput();
            }

            $publication->address_district_id = $request->get('distrito');
            $publication->vehicle_year = $request->get('año');
            $publication->vehicle_doors = $request->get('puertas');
            $publication->vehicle_kilometers = $request->get('kilometros');
            if ($request->get('tipo_producto') === 'vehículo') {
                $publication->vehicle_brand = $publication->category->parentCategory->name;
                $publication->vehicle_model = $publication->category->name;
            }
            if ($request->get('tipo_producto') === 'vehículo de colección') {
                $publication->vehicle_model = $request->get('modelo');
                $publication->vehicle_brand = $request->get('marca');
            }
            if ($request->get('tipo_producto') === 'moto') {
                $publication->vehicle_type = $publication->category->parentCategory->name;
                $publication->vehicle_brand = $publication->category->name;
                $publication->vehicle_model = $request->get('modelo');
            }
            if ($request->get('tipo_producto') === 'lancha') {
                $publication->vehicle_type = $request->get('tipo');
                $publication->vehicle_brand = $request->get('marca');
            }
            if ($request->get('tipo_producto') === 'vehiculo') {
                $publication->vehicle_model = $request->get('modelo');
                $publication->vehicle_brand = $request->get('marca');
            }

            $publication->vehicle_version = $request->get('version');
            $publication->vehicle_color = $request->get('color');
            $publication->vehicle_fuel = $request->get('combustible');
            $publication->vehicle_steering = $request->get('direccion');
            $publication->vehicle_transmission = $request->get('transmision');
            $publication->vehicle_owner = $request->get('dueño');
            $publication->vehicle_schedule = $request->get('horario');
            $publication->contact_phone = (empty($request->get('telefono')) ? $user->phone_number : $request->get('telefono'));


            if ($request->get('tipo_producto') === 'moto') {
                if (!empty($request->get('alarma'))) {
                    Parameter::saveParameters($publication->id, 'Alarma', $request->get('alarma'), '');
                }

                if (!empty($request->get('frenos'))) {
                    Parameter::saveParameters($publication->id, 'Frenos', $request->get('frenos'), '');
                }

                if (!empty($request->get('arranque'))) {
                    Parameter::saveParameters($publication->id, 'Sistema de Arranque', $request->get('arranque'), '');
                }

                if (!empty($request->get('motor'))) {
                    Parameter::saveParameters($publication->id, 'Tipo de motor', $request->get('motor'), '');
                }
            }

            if ($request->get('Seguridad')) {
                foreach ($request->get('Seguridad') as $par) {
                    Parameter::saveParameters($publication->id, $par, '-1', 'Seguridad');
                }
            }
            if ($request->get('Confort')) {
                foreach ($request->get('Confort') as $par) {
                    Parameter::saveParameters($publication->id, $par, '-1', 'Confort');
                }
            }
            if ($request->get('Sonido')) {
                foreach ($request->get('Sonido') as $par) {
                    Parameter::saveParameters($publication->id, $par, '-1', 'Sonido');
                }
            }
            if ($request->get('Exterior')) {
                foreach ($request->get('Exterior') as $par) {
                    Parameter::saveParameters($publication->id, $par, '-1', 'Exterior');
                }
            }
        }

        //If its a service
        if ($request->get('tipo_producto') === 'servicio') {
            $validator = Validator::make($request->all(), [
                'departamento' => 'required',
                'provincia' => 'required',
                'distrito' => 'required',
                'calle' => 'required|max:50',
                'numero' => 'required|max:50',
            ], $messages);


            if ($validator->fails()) {
                return redirect('modify-publication/' . $id)
                    ->withErrors($validator)
                    ->withInput();
            }

            $publication->address_street = $request->get('calle');
            $publication->address_number = $request->get('numero');
            $publication->address_district_id = $request->get('distrito');
            $publication->contact_phone = (empty($request->get('telefono')) ? $user->phone_number : $request->get('telefono'));


            if ($request->get('Zonas')) {
                foreach ($request->get('Zonas') as $par) {
                    Parameter::saveParameters($publication->id, $par, '-1', 'Zonas');
                }
            }
        }
        /////

        // If its a property
        if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina' || $request->get('tipo_producto') === 'terreno' || $request->get('tipo_producto') === 'inmueble') {

            if ($request->get('tipo_producto') === 'casa') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'nambientes' => 'required',
                    'años' => 'required|integer|min:0|max:100',
                    'construido' => 'required|integer|min:1|max:9999999',
                    'terreno' => 'required|integer|min:1|max:9999999',
                    'baños' => 'required',
                    'dormitorios' => 'required',
                    'calle' => 'required|max:50',
                    'numero' => 'required|max:50',
                ], $messages);
            }

            if ($request->get('tipo_producto') === 'departamento') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'nambientes' => 'required',
                    'años' => 'required|integer|min:0|max:100',
                    'construido' => 'required|integer|min:1|max:9999999',
                    'terreno' => 'required|integer|min:1|max:9999999',
                    'baños' => 'required',
                    'dormitorios' => 'required',
                    'calle' => 'required|max:50',
                    'numero' => 'required|max:50',
                ], $messages);
            }

            if ($request->get('tipo_producto') === 'local comercial') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'nambientes' => 'required',
                    'años' => 'required|integer|min:0|max:100',
                    'construido' => 'required|integer|min:1|max:9999999',
                    'terreno' => 'required|integer|min:1|max:9999999',
                    'baños' => 'required',
                    'calle' => 'required|max:50',
                    'numero' => 'required|max:50',
                ], $messages);
            }

            if ($request->get('tipo_producto') === 'oficina') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'nambientes' => 'required',
                    'años' => 'required|integer|min:0|max:100',
                    'construido' => 'required|integer|min:1|max:9999999',
                    'terreno' => 'required|integer|min:1|max:9999999',
                    'baños' => 'required',
                    'calle' => 'required|max:50',
                    'numero' => 'required|max:50',
                ], $messages);
            }

            if ($request->get('tipo_producto') === 'terreno') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'terreno' => 'required|integer|min:1|max:9999999',
                    'calle' => 'required|max:50',
                    'numero' => 'required|max:50',
                ], $messages);
            }

            if ($request->get('tipo_producto') === 'inmueble') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'años' => 'required|integer|min:0|max:100',
                    'construido' => 'required|integer|min:1|max:9999999',
                    'terreno' => 'required|integer|min:1|max:9999999',
                    'tipoinmueble' => 'required|max:50',
                    'calle' => 'required|max:50',
                    'numero' => 'required|max:50',
                ], $messages);
            }

            if ($validator->fails()) {
                return redirect('modify-publication/' . $id)
                    ->withErrors($validator)
                    ->withInput();
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina' || $request->get('tipo_producto') === 'terreno' || $request->get('tipo_producto') === 'inmueble') {
                $publication->address_street = $request->get('calle');
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina' || $request->get('tipo_producto') === 'terreno' || $request->get('tipo_producto') === 'inmueble') {
                $publication->address_number = $request->get('numero');
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina') {
                $publication->property_environments = $request->get('nambientes');
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina' || $request->get('tipo_producto') === 'inmueble') {
                $publication->property_age = $request->get('años');
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina' || $request->get('tipo_producto') === 'terreno' || $request->get('tipo_producto') === 'inmueble') {
                $publication->property_construction_area = $request->get('construido');
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina' || $request->get('tipo_producto') === 'terreno' || $request->get('tipo_producto') === 'inmueble') {
                $publication->property_land_area = $request->get('terreno');
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina') {
                $publication->property_bathrooms = $request->get('baños');
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento') {
                $publication->property_bedrooms = $request->get('dormitorios');
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina' || $request->get('tipo_producto') === 'terreno') {
                $publication->vehicle_schedule = $request->get('horario');
            }

            if ($request->get('tipo_producto') === 'inmueble') {
                $publication->vehicle_type = $request->get('tipoinmueble');
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina') {
                if (!empty($request->get('pisos'))) {
                    Parameter::saveParameters($publication->id, 'Cantidad de pisos', $request->get('pisos'), '');
                }
            }

            if ($request->get('tipo_producto') === 'departamento') {
                if (!empty($request->get('pisos'))) {
                    Parameter::saveParameters($publication->id, 'Cant.de pisos del edificio', $request->get('pisos'), '');
                }
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina' || $request->get('tipo_producto') === 'inmueble') {
                if (!empty($request->get('estado'))) {
                    Parameter::saveParameters($publication->id, 'Condición', $request->get('estado'), '');
                }
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina') {
                if (!empty($request->get('garaje'))) {
                    Parameter::saveParameters($publication->id, 'Garaje', $request->get('garaje'), '');
                }
            }

            if ($request->get('tipo_producto') === 'departamento') {
                if (!empty($request->get('amoblado'))) {
                    Parameter::saveParameters($publication->id, 'Amoblado', $request->get('amoblado'), '');
                }
            }

            if ($request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina') {
                if (!empty($request->get('ascensores'))) {
                    Parameter::saveParameters($publication->id, 'Ascensores', $request->get('ascensores'), '');
                }
            }

            if ($request->get('tipo_producto') === 'terreno') {
                if (!empty($request->get('acceso'))) {
                    Parameter::saveParameters($publication->id, 'Acceso', $request->get('acceso'), '');
                }
            }

            if ($request->get('tipo_producto') === 'terreno') {
                if (!empty($request->get('uso'))) {
                    Parameter::saveParameters($publication->id, 'Uso', $request->get('uso'), '');
                }
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento') {
                if ($request->get('Ambientes')) {
                    foreach ($request->get('Ambientes') as $par) {
                        Parameter::saveParameters($publication->id, $par, '-1', 'Ambientes');
                    }
                }
                if ($request->get('Comodidades')) {
                    foreach ($request->get('Comodidades') as $par) {
                        Parameter::saveParameters($publication->id, $par, '-1', 'Comodidades');
                    }
                }
            }

            if ($request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina' || $request->get('tipo_producto') === 'terreno') {
                if ($request->get('Caracteristicas')) {
                    foreach ($request->get('Caracteristicas') as $par) {
                        Parameter::saveParameters($publication->id, $par, '-1', 'Caracteristicas Adicionales');
                    }
                }
            }

            $publication->address_district_id = $request->get('distrito');
            $publication->contact_phone = (empty($request->get('telefono')) ? $user->phone_number : $request->get('telefono'));
        }
        //////////

        $cannot_modify_title = $publication->transactions()
            ->where(function ($query) {
                $query->where('seller_concreted', '1')
                    ->orWhere('buyer_concreted', '1');
            })->count();

        if (!$cannot_modify_title) {
            $publication->title = $request->get('title');
        }
        $publication->condition = $request->get('condition');
        $publication->description_html = $request->get('description_html');


        $publication->quantity = $data['cantidad'];
        $publication->available_quantity = $data['cantidad'];
        $publication->price = $data['precio'];

        $publication->save();

        if (!$publication_index = PublicationIndex::where('publication_id', $publication->id)->first()) {
            $publication_index = new PublicationIndex();
        }
        $publication_index->publication_id = $publication->id;
        $publication_index->category_branch_string = $publication->getCategoryBranchString();
        $publication_index->title = $publication->title;
        $publication_index->description = $publication->description_txt . ' ' . strip_tags($publication->description_html);
        $publication_index->save();

        Image::updateImagePositionFromPubId($publication->id, 0);

        $pictures_info = json_decode($request->get('pictures_info'), TRUE);
        foreach ($pictures_info as $pi) {
            Image::updateImagePositionFromPubIdAndImageId($publication->id, $pi['imageid'], ++$pi['position']);
        }

        //delete garbage images
        $images = $publication->images()->where('position', 0)->get();
        foreach ($images as $img) {
            File::delete(public_path('uploads/images/products/') . $img->name);
            $img->delete();
        }

        Session::flash('message', 'Tu publicación #' . $publication->id . ' ha sido modificada.');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('publications');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function questions(Request $request)
    {
        $user = Auth::user();
        $vars = [];

        $vars['pubs'] = $user->publications()
            ->select('publications.*')
            ->addSelect('questions.*')
            ->join('questions', 'publications.id', '=', 'questions.publication_id')
            ->where('publications.status', '<>', 'Canceled')
            ->where('publications.status', '<>', 'Finished')
            ->where('publications.status', '<>', 'Draft')
            ->where('questions.answer', '')
            ->whereRaw('questions.created_at > (NOW() - INTERVAL 60 DAY)')
            ->orderBy('questions.created_at', 'asc')->get();
        //print_r($vars['pubs']); die();
        return response()->view('account.questions', $vars);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function answer_questions(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }
        if (!Auth::check()) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }
        $user = Auth::user();
        $question = Question::where('id', $request->get('id'))->first();
        if ($question->publication->user != $user) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }
        $question->answer = $request->get('answer');
        $question->save();
        $resp = ['is_ok' => true, 'value' => true];
        $buyer = User::find($question->asker_id);
        $buyer->notify(new AnsweredQuestionNotification($buyer, $question->publication, $question));
        $buyer->sendMobileNotification('response', $question->publication->id, $question->publication->title, $question->publication->id);
        return response()->json($resp);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function sales(Request $request)
    {
        $user = Auth::user();
        $vars = [];

        $query_string = $request->all();
        unset($query_string['page']);

        $sort = (!empty($request->get('sort')) ? $request->get('sort') : '1');
        $charge = (!empty($request->get('charge')) ? $request->get('charge') : '3');
        $qualification = (!empty($request->get('qualification')) ? $request->get('qualification') : '6');
        $status = (!empty($request->get('status')) ? $request->get('status') : 'open');
        $vars['search'] = $request->get('search');
        if (empty($vars['search'])) {
            unset($query_string['search']);
        }

        $q = $user->sales()
            ->whereRaw('transactions.created_at > (NOW() - INTERVAL 365 DAY)');
        if (!empty($vars['search'])) {
            $q->join('users', 'users.id', '=', 'transactions.buyer_id');
            $q->join('publications', 'publications.id', '=', 'transactions.publication_id');
            $q->where(function ($query) use ($vars) {
                $query->where('users.first_name', 'like', '%' . $vars['search'] . '%')
                    ->orWhere('users.last_name', 'like', '%' . $vars['search'] . '%')
                    ->orWhere('publications.title', 'like', '%' . $vars['search'] . '%');
            });
        }
        $q->whereRaw("
            (transactions.seller_calification = 'No calificado' or
                transactions.buyer_calification = 'No calificado' or
                transactions.seller_concreted != transactions.buyer_concreted
            ) and 
            transactions.created_at > (NOW() - INTERVAL 20 DAY)
            ");

        $qc = $user->sales()
            ->whereRaw('transactions.created_at > (NOW() - INTERVAL 365 DAY)');
        if (!empty($vars['search'])) {
            $qc->join('users', 'users.id', '=', 'transactions.buyer_id');
            $qc->join('publications', 'publications.id', '=', 'transactions.publication_id');
            $qc->where(function ($query) use ($vars) {
                $query->where('users.first_name', 'like', '%' . $vars['search'] . '%')
                    ->orWhere('users.last_name', 'like', '%' . $vars['search'] . '%')
                    ->orWhere('publications.title', 'like', '%' . $vars['search'] . '%');
            });
        }
        $qc->whereRaw("
            ((transactions.seller_calification != 'No calificado' and
                transactions.buyer_calification != 'No calificado' and
                transactions.seller_concreted = transactions.buyer_concreted
            ) or 
            transactions.created_at < (NOW() - INTERVAL 20 DAY))
            ");


        switch ($sort) {
            case 1:
                $q->orderBy('transactions.created_at', 'desc');
                $qc->orderBy('transactions.created_at', 'desc');
                break;
            case 2:
                $q->orderBy('transactions.created_at', 'asc');
                $qc->orderBy('transactions.created_at', 'asc');
                break;
        }
        switch ($charge) {
            case 1:
                $q->where('transactions.charged', '0');
                $qc->where('transactions.charged', '0');
                break;
            case 2:
                $q->where('transactions.charged', '1');
                $qc->where('transactions.charged', '1');
                break;
        }
        switch ($qualification) {
            case 1:
                $q->where('transactions.seller_calification', 'No calificado');
                $qc->where('transactions.seller_calification', 'No calificado');
                break;
            case 2:
                $q->where('transactions.seller_concreted', '1');
                $qc->where('transactions.seller_concreted', '1');
                break;
            case 3:
                $q->where('transactions.seller_concreted', '0');
                $q->where('transactions.seller_calification', '!=', 'No calificado');
                $qc->where('transactions.seller_concreted', '0');
                $qc->where('transactions.seller_calification', '!=', 'No calificado');
                break;
            case 4:
                $q->where('transactions.buyer_calification', '!=', 'No calificado');
                $q->where('transactions.seller_calification', '!=', 'No calificado');
                $q->where('transactions.buyer_calification', '!=', 'transactions.seller_calification');
                $qc->where('transactions.buyer_calification', '!=', 'No calificado');
                $qc->where('transactions.seller_calification', '!=', 'No calificado');
                $qc->where('transactions.buyer_calification', '!=', 'transactions.seller_calification');
                break;
            case 5:
                $q->where('transactions.buyer_calification', '=', 'No calificado');
                $q->where('transactions.seller_calification', '!=', 'No calificado');
                $qc->where('transactions.buyer_calification', '=', 'No calificado');
                $qc->where('transactions.seller_calification', '!=', 'No calificado');
                break;
        }

        $qxo = $q;
        $qxc = $qc;
        $vars['num_open'] = count($qxo->get());
        $vars['num_closed'] = count($qxc->get());
        $vars['sort'] = $sort;
        $vars['charge'] = $charge;
        $vars['qualification'] = $qualification;
        $vars['saleso'] = $q->paginate(10);
        $vars['salesc'] = $qc->paginate(10);
        $vars['query'] = $query_string;
        $vars['status'] = $status;
        return response()->view('account.sales', $vars);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function sale_detail(Request $request)
    {
        $user = Auth::user();
        $vars = [];

        if (!$vars['sale'] = $user->sales()->where('id', $request->get('id'))->first()) {
            abort(404);
        }

        if ($vars['sale']->seller->id != $user->id and $vars['sale']->buyer->id != $user->id) {
            abort(404);
        }
        $vars['p'] = $vars['sale']->publication;
        $vars['messages'] = $vars['sale']->messages()
            ->orderBy('created_at', 'asc')->get();
        $vars['notes'] = $vars['sale']->sellerNotes()->orderBy('created_at', 'desc')->get();
        $vars['go_notes'] = $request->has('go_notes');
        $vars['go_messages'] = $request->has('go_messages');

        return response()->view('account.sale_detail', $vars);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function seller_qualification(Request $request)
    {
        $user = Auth::user();
        $publication = null;
        if (!$sale = $user->sales()->where('id', $request->get('id'))->first() or
            $sale->seller_calification != 'No calificado'
        ) {
            abort(404);
        }

        if ($sale->seller_qualification_modified) {
            return redirect('/sales');
        }

        $datetime1 = new \DateTime($sale->created_at);
        $datetime2 = new \DateTime(date("Y-m-d H:i:s"));
        $interval = $datetime1->diff($datetime2);
        if ($interval->format('%a') > config('app.global.days_to_modify_qualify')) {
            return redirect('/sales');
        }

        if (!$publication = $sale->publication) {
            abort(404);
        }

        $vars['publication'] = $publication;
        $vars['sale'] = $sale;
        $vars['id'] = $request->get('id');

        return response()->view('account.seller_qualification', $vars);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function seller_qualification_save(Request $request)
    {
        $messages = [
            'concreted.required' => 'Indícanos si entregaste el producto.',
            'recommended.required' => 'Indícanos como te fue con el comprador.',
            'no_concreted_type.required' => 'Indícanos que tipo de problema tuviste con el comprador.',
            'opinion.required' => 'Comentanos tu opinión del comprador.',
            'opinion.max' => 'Longitud máxima del comentario :max caracteres.',
        ];
        $fields = [
            'concreted' => 'required',
            'recommended' => 'required',
            'opinion' => 'required|max:200',
            'id' => 'required',
        ];

        if ($request->get('concreted') == '-1') {
            $fields['no_concreted_type'] = 'required';
        }

        $validator = Validator::make($request->all(), $fields, $messages);

        if ($validator->fails()) {
            return redirect('/seller-qualification?id=' . $request->get('id'))
                ->withErrors($validator)
                ->withInput();
        }
        $user = Auth::user();
        $publication = null;
        if (!$purchase = $user->sales()->where('id', $request->get('id'))->first() or
            $purchase->seller_calification != 'No calificado'
        ) {
            abort(404);
        }

        if ($purchase->seller_qualification_modified) {
            return redirect('/sales');
        }

        $datetime1 = new \DateTime($purchase->created_at);
        $datetime2 = new \DateTime(date("Y-m-d H:i:s"));
        $interval = $datetime1->diff($datetime2);
        if ($interval->format('%a') > config('app.global.days_to_modify_qualify')) {
            return redirect('/sales');
        }

        if (!$publication = $purchase->publication) {
            abort(404);
        }

        $purchase->seller_concreted = ($request->get('concreted') == '1' ? '1' : '0');
        if ($request->get('concreted') == '-1') {
            $purchase->seller_concreted_problem = $request->get('no_concreted_type');
        } else {
            $purchase->seller_concreted_problem = '';
        }

        $purchase->seller_recommends = $request->get('recommended');
        if ($request->get('recommended') == 'Sí') {
            $purchase->seller_calification = 'Positivo';
        } elseif ($request->get('recommended') == 'No') {
            $purchase->seller_calification = 'Negativo';
        } elseif ($request->get('recommended') == 'No estoy seguro') {
            $purchase->seller_calification = 'Neutral';
        }
        $purchase->seller_comment = $request->get('opinion');
        $purchase->seller_qualification_date = date("Y-m-d H:i:s");
        $purchase->save();
        $buyer = User::find($purchase->buyer_id);
        $buyer->notify(new QualificationBuyerNotification($buyer, $publication, $purchase));

        return redirect('/sales');

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function seller_qualification_edit(Request $request)
    {
        $user = Auth::user();
        $publication = null;
        if (!$purchase = $user->sales()->where('id', $request->get('id'))->first()) {
            abort(404);
        }

        if (!$publication = $purchase->publication) {
            abort(404);
        }

        if ($purchase->seller_qualification_modified) {
            return redirect('/sales');
        }

        $datetime1 = new \DateTime($purchase->created_at);
        $datetime2 = new \DateTime(date("Y-m-d H:i:s"));
        $interval = $datetime1->diff($datetime2);
        if ($interval->format('%a') > config('app.global.days_to_modify_qualify')) {
            return redirect('/sales');
        }

        $vars['publication'] = $publication;
        $vars['purchase'] = $purchase;
        $vars['id'] = $request->get('id');

        return response()->view('account.seller_qualification_edit', $vars);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function seller_qualification_edit_save(Request $request)
    {
        $messages = [
            'concreted.required' => 'Indícanos si entregaste el producto.',
            'recommended.required' => 'Indícanos como te fue con el comprador.',
            'no_concreted_type.required' => 'Indícanos que tipo de problema tuviste con el comprador.',
            'opinion.required' => 'Comentanos tu opinión del comprador.',
            'opinion.max' => 'Longitud máxima del comentario :max caracteres.',
        ];
        $fields = [
            'concreted' => 'required',
            'recommended' => 'required',
            'opinion' => 'required|max:200',
            'id' => 'required',
        ];

        if ($request->get('concreted') == '-1') {
            $fields['no_concreted_type'] = 'required';
        }

        $validator = Validator::make($request->all(), $fields, $messages);

        if ($validator->fails()) {
            return redirect('/seller-qualification-edit?id=' . $request->get('id'))
                ->withErrors($validator)
                ->withInput();
        }
        $user = Auth::user();
        $publication = null;
        if (!$purchase = $user->sales()->where('id', $request->get('id'))->first()) {
            abort(404);
        }

        if ($purchase->seller_qualification_modified) {
            return redirect('/sales');
        }

        $datetime1 = new \DateTime($purchase->created_at);
        $datetime2 = new \DateTime(date("Y-m-d H:i:s"));
        $interval = $datetime1->diff($datetime2);
        if ($interval->format('%a') > config('app.global.days_to_modify_qualify')
            or ($request->get('recommended') == 'No' and $purchase->seller_calification != 'Negativo')
        ) {
            return redirect('/sales');
        }

        if (!$publication = $purchase->publication) {
            abort(404);
        }

        $purchase->seller_concreted = ($request->get('concreted') == '1' ? '1' : '0');
        if ($request->get('concreted') == '-1') {
            $purchase->seller_concreted_problem = $request->get('no_concreted_type');
        } else {
            $purchase->seller_concreted_problem = '';
        }

        $purchase->seller_recommends = $request->get('recommended');
        if ($request->get('recommended') == 'Sí') {
            $purchase->seller_calification = 'Positivo';
        } elseif ($request->get('recommended') == 'No') {
            $purchase->seller_calification = 'Negativo';
        } elseif ($request->get('recommended') == 'No estoy seguro') {
            $purchase->seller_calification = 'Neutral';
        }
        $purchase->seller_comment = $request->get('opinion');
        $purchase->seller_qualification_modified_date = date("Y-m-d H:i:s");
        $purchase->seller_qualification_modified = true;
        $purchase->save();

        $buyer = User::find($purchase->buyer_id);
        $buyer->notify(new QualificationBuyerChangedNotification($buyer, $publication, $purchase));

        return redirect('/sales')->with(['result' => true, 'message' => 'Calificación Modificada']);


    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function seller_reply_edit(Request $request)
    {
        $user = Auth::user();
        $publication = null;
        if (!$purchase = $user->sales()->where('id', $request->get('id'))->first() or
            $purchase->buyer_calification == 'No calificado'
        ) {
            abort(404);
        }

        if (!$publication = $purchase->publication) {
            abort(404);
        }

        $datetime1 = new \DateTime($purchase->created_at);
        $datetime2 = new \DateTime(date("Y-m-d H:i:s"));
        $interval = $datetime1->diff($datetime2);
        if ($interval->format('%a') > config('app.global.days_to_modify_qualify')) {
            return redirect('/purchases');
        }

        $vars['publication'] = $publication;
        $vars['purchase'] = $purchase;
        $vars['id'] = $request->get('id');

        return response()->view('account.seller_reply_edit', $vars);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function seller_reply_edit_save(Request $request)
    {
        $fields = [
            'replica' => 'required|max:200',
            'id' => 'required',
        ];

        $validator = Validator::make($request->all(), $fields);

        if ($validator->fails()) {
            return redirect('/seller-reply-edit?id=' . $request->get('id'))
                ->withErrors($validator)
                ->withInput();
        }
        $user = Auth::user();
        $publication = null;
        if (!$purchase = $user->sales()->where('id', $request->get('id'))->first() or
            $purchase->buyer_calification == 'No calificado'
        ) {
            abort(404);
        }

        $datetime1 = new \DateTime($purchase->created_at);
        $datetime2 = new \DateTime(date("Y-m-d H:i:s"));
        $interval = $datetime1->diff($datetime2);
        if ($interval->format('%a') > config('app.global.days_to_modify_qualify')) {
            return redirect('/sales');
        }

        if (!$publication = $purchase->publication) {
            abort(404);
        }

        $purchase->seller_reply = $request->get('replica');
        $purchase->save();
        return redirect('/sales')->with(['result' => true, 'message' => 'Replica Modificada']);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function save_note(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }

        $user = Auth::user();
        if (!$sale = $user->sales()->where('id', $request->get('id'))->first()) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }

        $note = new \App\SellerNote();
        $note->description = $request->get('note');
        $sale->sellerNotes()->save($note);
        $result['id'] = $note->id;
        $result['description'] = $note->description;

        return response()->json(['is_ok' => true, 'value' => json_encode($result)]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function update_note(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }

        $user = Auth::user();
        if (!$sale = $user->sales()->where('id', $request->get('id'))->first()) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }

        if (!$note = \App\SellerNote::find($request->get('note_id'))) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }
        $note->description = $request->get('note');
        $note->save();
        $result['id'] = $note->id;
        $result['description'] = $note->description;

        return response()->json(['is_ok' => true, 'value' => json_encode($result)]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function delete_note(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }

        $user = Auth::user();
        if (!$sale = $user->sales()->where('id', $request->get('id'))->first()) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }

        if (!$note = \App\SellerNote::find($request->get('note_id'))) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }

        $note->delete();

        $value['id'] = '';

        if ($last_note = $sale->sellerNotes()->orderBy('created_at', 'desc')->first()) {
            $value['id'] = $last_note->id;
            $value['description'] = $last_note->description;
        }

        return response()->json(['is_ok' => true, 'value' => json_encode($value)]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function charged(Request $request)
    {
        $user = Auth::user();
        if (!$sale = $user->sales()->where('id', $request->get('id'))->first()) {
            return redirect('/sales');
        }
        if ($sale->charged) {
            $sale->charged = 0;
            $message = 'Transacción marcada como no cobrada.';
        } else {
            $sale->charged = 1;
            $message = 'Transacción marcada como cobrada.';
        }
        $sale->save();
        return redirect('/sales')->with(['result' => true, 'message' => $message]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function billing(Request $request)
    {
        $user = Auth::user();
        $vars['facturas'] = $user->tickets()
            ->where('status', '!=', 'Open')
            ->orderBy('emitted_at', 'desc')->paginate(10);

        $vars['pendiente'] = $user->tickets()->where('status', '=', 'Open')->first();

        return response()->view('account.billing', $vars);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function invoice_detail(Request $request)
    {
        $user = Auth::user();
        if (!$vars['factura'] = $user->tickets()->where('id', $request->get('id'))->first()) {
            return redirect('/billing');
        }

        $query_string = $request->all();
        unset($query_string['page']);

        $vars['items'] = $vars['factura']->ticketItems()->orderBy('created_at', 'desc')->paginate(10);
        $vars['query'] = $query_string;

        return response()->view('account.invoice_detail', $vars);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function payment(Request $request)
    {
        $user = Auth::user();
        if (!$vars['factura'] = $user->tickets()
                ->where('id', $request->get('id'))->first() or $vars['factura']->status != 'Emitted'
        ) {

            return redirect('/billing');
        }

        return response()->view('account.payment', $vars);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function payment_save(Request $request)
    {
        $user = Auth::user();
        if (!$factura = $user->tickets()->where('id', $request->get('id'))->first()) {
            return redirect('/billing');
        }

        $paymentId = BankAccount::where('bank', '=', $request->get('payment'))->first();

        $factura->bank_account_id = $paymentId->id;
        $factura->payment_description = $request->get('description');
        $factura->payment_date = date("Y-m-d H:i:s");
        $factura->payment_status = 'Por revisar';

        if ($request->file('files')) {
            $factura->image1 = null;
            $factura->image2 = null;
            $factura->image3 = null;
            $factura->name1 = null;
            $factura->name2 = null;
            $factura->name3 = null;
            foreach ($request->file('files') as $key => $f) {
                $imageName = md5(time() . $request->get('id') . $key) . $user->id . '.' . $f->getClientOriginalExtension();
                $f->move(public_path('uploads/payments'), $imageName);
                $factura->{"image" . ($key + 1)} = $imageName;
                $factura->{"name" . ($key + 1)} = $f->getClientOriginalName();
            }

        }
        $factura->save();
        return redirect('/billing')->with(['result' => true, 'message' => 'Pago modificado con éxito']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete_question(Request $request)
    {
        if (!Auth::check()) {
            abort(404);
        }
        $user = Auth::user();
        $question_id = $request->get('question_id');

        $question = Question::find($question_id);
        if ($question->publication->user_id != $user->id) {
            return redirect('/questions');
        }
        if ($request->get('no_questions') == '1' or $request->get('no_offers') == '1') {
            if (!$block = \App\Block::where('seller_id', $user->id)->where('buyer_id', $question->asker_id)->first()) {
                $block = new \App\Block();
                $block->buyer_id = $question->asker_id;
                $block->seller_id = $user->id;
            }
            if ($request->get('no_questions') == '1') {
                $block->no_questions = true;
            }
            if ($request->get('no_offers') == '1') {
                $block->no_offers = true;
            }
            $block->save();

        }
        $question->delete();
        return redirect('/questions')->with(['result' => true, 'message' => 'Pregunta eliminada']);
    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function republish_publication($id)
    {
        $user = Auth::user();
        $publication = null;
        if (!$publication = $user->publications()
            ->where('id', $id)
            ->where(function ($query) {
                $query->where('status', 'Finished');
            })
            ->first()
        ) {
            return redirect()->route('publications');
        }
        if ($user->suspended) {
            Session::flash('message', 'Has sido suspendido para vender, revisa si tienes facturas pendientes por pagar, si no es así por favor contacta a soporte.');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('billing');
        }

        $vars = ['publication' => $publication];

        return response()->view('account.republish_publication', $vars);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function republish_publication_save(Request $request)
    {
        $user = Auth::user();
        $publication = null;
        $id = $request->get('id');
        if (!$publication = $user->publications()
            ->where('id', $id)
            ->where(function ($query) {
                $query->where('status', 'Finished');
            })
            ->first()
        ) {
            return redirect()->route('publications');
        }
        if ($user->suspended) {
            Session::flash('message', 'Has sido suspendido para vender, revisa si tienes facturas pendientes por pagar, si no es así por favor contacta a soporte.');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('billing');
        }

        $tipo_producto = $publication->getProductType();
        $data = $request->all();
        $data['precio'] = str_replace('.', '', $data['precio']);
        $data['precio'] = str_replace(',', '.', $data['precio']);

        $messages = [
            'title.required' => 'Introduce un título para tu publicación',
            'title.max' => 'El título de tu publicación debe contener :max caracteres como máximo.',
            'precio.min' => 'El precio de tu publicación debe ser mayor a 0.',
            'precio.max' => 'El precio máximo de publicación es de S/. 9.999.999,99',
            'cantidad.min' => 'La cantidad mínima de publicación es de :min producto',
            'cantidad.max' => 'La cantidad máxima de publicación es de :max productos',
        ];
        $validations = [
            'title' => 'required|max:60',
            'cantidad' => 'required|integer|min:1|max:9999',
            'precio' => 'required|numeric|min:0.1|max:9999999',
        ];

        if ($tipo_producto === 'servicio' && $request->get('convenir') === '1') {
            unset($validations['precio']);
            $data['precio'] = 0;
        }

        $validator = Validator::make($data, $validations, $messages);

        if ($validator->fails()) {
            return redirect('/republish-publication/' . $id)
                ->withErrors($validator)
                ->withInput();
        }
        /////////////////

        $publication->title = $request->get('title');
        $publication->quantity = $data['cantidad'];
        $publication->available_quantity = $data['cantidad'];
        $publication->price = $data['precio'];

        if (!$publication_index = PublicationIndex::where('publication_id', $publication->id)->first()) {
            $publication_index = new PublicationIndex();
        }
        $publication_index->publication_id = $publication->id;
        $publication_index->title = $publication->title;
        $publication_index->save();

        $conf = Configuration::limit(1)->first();
        $pub_type = 'Gratis';
        if ($publication->getProductType() === 'producto') {
            $price = 0;
            //Plansss from another controller (service) $price
            $pub_type = $request->get('plan');
            $price = PlanHelper::getPlan($request, $conf);
            $publication->cost_type = $request->get('plan');
        } else {
            $price = $conf->advertisement_cost;
            $publication->cost_type = 'Fijo';
            $pub_type = 'Clasificado';
        }

        $publication->status = 'Active';
        $publication->publicated_as_active_at = date("Y-m-d H:i:s");
        $publication->ends_at = date('Y-m-d H:i:s', strtotime("+" . config('app.global.publication_lifetime_days') . " days"));
        $publication->cost = $price;
        $publication->save();

        //if user has not a cutoff day
        if (!$user->cutoff_day) {
            $user->cutoff_day = (date("d") <= 28 ? date("d") : '28');
            $user->save();
        }

        //Creating the item in ticket
        if ($pub_type !== 'Gratis') {
            if (!$ticket = $user->tickets()->where('status', 'Open')->first()) {
                $ticket = new Ticket();
                $ticket->user_id = $user->id;
                $ticket->status = 'Open';
                $limit_emitted = strtotime("-" . config('app.global.days_before_cutoff_notification') . " days", strtotime(date('Y-m-' . $user->cutoff_day . ' 23:30')));
                if (time() > $limit_emitted) {
                    $emit_on = date('Y-m-d', strtotime("+1 month", strtotime(date('Y-m-' . $user->cutoff_day))) - config('app.global.days_before_cutoff_notification') * 86400);
                } else {
                    $emit_on = date('Y-m-d', $limit_emitted);
                }
                $ticket->emit_on = $emit_on;
                $ticket->save();
            }

            $item = new TicketItem();
            $item->ticket_id = $ticket->id;
            $item->publication_id = $publication->id;
            $item->amount = $price;
            if ($pub_type !== 'Clasificado') {
                $item->description = "Exposición de publicación nivel $pub_type.";
            } else {
                $item->description = "Publicación de clasificado.";
            }
            $item->save();
        }

        Session::flash('message', 'Tu publicación #' . $publication->id . ' ha sido republicada.');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('publications');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function seller_request_change_qualification(Request $request)
    {
        $user = Auth::user();

        if (!$purchase = $user->sales()->where('id', $request->get('id'))->where('seller_request_change_qualification', '0')->first()) {
            return redirect()->route('sales');
        }
        $buyer = $purchase->buyer;
        Mail::to($buyer->email)
            ->send(new \App\Mail\SellerRequestChangeQualification($purchase));

        $purchase->buyer_qualification_modified = '0';
        $purchase->seller_request_change_qualification = '1';
        $purchase->seller_request_change_qualification_date = date("Y-m-d H:i:s");
        $purchase->save();

        Session::flash('message', 'La solicitud de cambio de calificación fue enviada.');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('sales');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function buyer_request_change_qualification(Request $request)
    {
        $user = Auth::user();

        if (!$purchase = $user->purchases()->where('id', $request->get('id'))->where('buyer_request_change_qualification', '0')->first()) {
            return redirect()->route('sales');
        }
        $seller = $purchase->seller;
        Mail::to($seller->email)
            ->send(new \App\Mail\BuyerRequestChangeQualification($purchase));

        $purchase->seller_qualification_modified = '0';
        $purchase->buyer_request_change_qualification = '1';
        $purchase->buyer_request_change_qualification_date = date("Y-m-d H:i:s");
        $purchase->save();

        Session::flash('message', 'La solicitud de cambio de calificación fue enviada.');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('purchases');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function buyer_qualification_confirm(Request $request)
    {
        $user = Auth::user();

        if (!$purchase = $user->purchases()->where('id', $request->get('id'))->where('buyer_qualification_modified', '0')->first()) {
            return redirect()->route('purchases');
        }

        $purchase->buyer_qualification_modified = '1';
        $purchase->buyer_qualification_modified_date = date("Y-m-d H:i:s");
        $purchase->save();

        $seller = User::find($publication->user_id);
        $seller->notify(new QualificationSellerConfirmNotification($seller, $purchase->publication, $purchase));

        Session::flash('message', 'Has confirmado tu calificación al vendedor.');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('purchases');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function seller_qualification_confirm(Request $request)
    {
        $user = Auth::user();

        if (!$sale = $user->sales()->where('id', $request->get('id'))->where('seller_qualification_modified', '0')->first()) {
            return redirect()->route('sales');
        }

        $sale->seller_qualification_modified = '1';
        $sale->seller_qualification_modified_date = date("Y-m-d H:i:s");
        $sale->save();

        $buyer = User::find($sale->buyer_id);
        $buyer->notify(new QualificationBuyerConfirmNotification($buyer, $sale->publication, $sale));

        Session::flash('message', 'Has confirmado tu calificación al comprador.');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('sales');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function stock(Request $request)
    {
        $user = Auth::user();
        $publication = null;
        if (!$publication = $user->publications()
                ->where('id', $request->get('id'))
                ->where('lack_stock', '1')
                ->first()
            or $publication->getProductType() != 'producto'
        ) {
            return redirect()->route('publications');
        }

        $vars['publication'] = $publication;
        $vars['id'] = $request->get('id');
        $vars['make_shipments'] = false;
        if ($publication->free_shipment or
            count($publication->shippingCosts)
        ) {
            $vars['make_shipments'] = true;
        }

        return response()->view('account.stock', $vars);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function stock_save(Request $request)
    {
        $user = Auth::user();
        $publication = null;
        $id = $request->get('id');
        if (!$publication = $user->publications()
                ->where('id', $id)
                ->where('lack_stock', '1')
                ->first()
            or $publication->getProductType() != 'producto'
        ) {
            return redirect()->route('publications');
        }
        $messages = [
            'cantidad.required' => 'Indícanos la cantidad que deseas agregar.',
            'cantidad.max' => 'La cantidad máxima es de :max',
            'cantidad.min' => 'La cantidad mínima es de :min'
        ];
        $validator = Validator::make($request->all(), [
            'cantidad' => 'required|integer|min:1|max:9999'
        ], $messages);

        if ($validator->fails()) {
            return redirect('/stock?id=' . $publication->id)
                ->withErrors($validator)
                ->withInput();
        }

        $publication->quantity = $request->get('cantidad');
        $publication->available_quantity = $request->get('cantidad');
        $publication->lack_stock = '0';
        $publication->save();

        Session::flash('message', 'Has agregado stock a tu publicación, ahora podrás activarla nuevamente.');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('publications', ['status' => 'paused']);
    }
}

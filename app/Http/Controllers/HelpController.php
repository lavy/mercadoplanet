<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Help;

/**
 * Class HelpController
 * @package App\Http\Controllers
 */
class HelpController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function index(Request $request)
    {
        $vars = [];
        $vars['article'] = null;
        $id = $request->get('id');
        if (empty($id)) {
            $vars['categories'] = Help::whereNull('parent_id')->get();
        } else {
            if (!$vars['article'] = Help::find($id)) {
                abort(404);
            }
            if (!empty($vars['article']->link)) {
                return redirect($vars['article']->link);
            }
            $vars['categories'] = Help::where('parent_id', $id)->get();
        }
        $vars['breadcrumb'] = $this->array_breadcrumb($id);

        return response()->view('help.index', $vars);
    }

    /**
     * @param $id
     * @return array
     */
    private function array_breadcrumb($id)
    {
        if (!$id) return [];

        $breadcrumb = [];
        $item = Help::find($id);
        $breadcrumb[$item->id] = $item->title;

        while ($item = $item->parent) {
            $breadcrumb = [$item->id => $item->title] + $breadcrumb;
        }

        return $breadcrumb;
    }
}

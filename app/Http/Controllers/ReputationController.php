<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

/**
 * Class ReputationController
 * @package App\Http\Controllers
 */
class ReputationController extends Controller
{

	/**
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
	 */
	public function index()
	{
		if (!Auth::check()) {
			return redirect('/login');
		}

		$user = Auth::user();
		$vars = User::user_reputation($user);
		//$userReputation = User::user_reputation($user);
		return response()->view('account.reputation', $vars);
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function reputation_as_seller(Request $request)
	{
		if (empty($request->get('user_id'))) {
			abort(404);
		}

		$user = User::find($request->get('user_id'));

		$query_string = $request->all();
		unset($query_string['page']);
		$vars['query'] = $query_string;

		$vars['user'] = $user;
		$vars['address'] = $user
			->addresses()->where('default_for_sales', true)
			->first();

		$pos_sales = $user->sales()->where('buyer_calification', 'Positivo')->count();
		$neg_sales = $user->sales()->where('buyer_calification', 'Negativo')->count();
		$pos_purchases = $user->purchases()->where('seller_calification', 'Positivo')->count();
		$negs_purchases = $user->purchases()->where('seller_calification', 'Negativo')->count();

		$vars['reputation'] = User::user_reputation($user);
		$vars['num_positives'] = $pos_sales;
		$vars['num_neutrals'] = $user->sales()->where('buyer_calification', 'Neutral')->count();
		$vars['num_negatives'] = $neg_sales;
		$vars['total_points'] = $pos_sales + $pos_purchases - $neg_sales - $negs_purchases;

		switch ($request->type) {
			case 'neutral':
				$vars['qualifications'] = $user->sales()->where('buyer_calification', 'Neutral')->paginate(10);
				break;

			case 'positive':
				$vars['qualifications'] = $user->sales()->where('buyer_calification', 'Positivo')->paginate(10);
				break;

			case 'negative':
				$vars['qualifications'] = $user->sales()->where('buyer_calification', 'Negativo')->paginate(10);
				break;

			default:
				$vars['qualifications'] = $user->sales()->where('buyer_calification', '<>', 'No calificado')->paginate(10);
				break;
		}

		return response()->view('account.reputation_as_seller', $vars);
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function reputation_as_buyer(Request $request)
	{
		if (empty($request->get('user_id'))) {
			abort(404);
		}

		$user = User::find($request->get('user_id'));

		$query_string = $request->all();
		unset($query_string['page']);
		$vars['query'] = $query_string;

		$vars['user'] = $user;
		$vars['address'] = $user->addresses()->where('default_for_sales', true)->first();

		$pos_sales = $user->sales()->where('buyer_calification', 'Positivo')->count();
		$neg_sales = $user->sales()->where('buyer_calification', 'Negativo')->count();
		$pos_purchases = $user->purchases()->where('seller_calification', 'Positivo')->count();
		$negs_purchases = $user->purchases()->where('seller_calification', 'Negativo')->count();

		$vars['num_positives'] = $pos_purchases;
		$vars['num_neutrals'] = $user->purchases()->where('seller_calification', 'Neutral')->count();
		$vars['num_negatives'] = $negs_purchases;
		$vars['total_points'] = $pos_sales + $pos_purchases - $neg_sales - $negs_purchases;

		switch ($request->type) {
			case 'neutral':
				$vars['qualifications'] = $user->purchases()->where('seller_calification', 'Neutral')->paginate(10);
				break;

			case 'positive':
				$vars['qualifications'] = $user->purchases()->where('seller_calification', 'Positivo')->paginate(10);
				break;

			case 'negative':
				$vars['qualifications'] = $user->purchases()->where('seller_calification', 'Negativo')->paginate(10);
				break;

			default:
				$vars['qualifications'] = $user->purchases()->where('seller_calification', '<>', 'No calificado')->paginate(10);
				break;
		}

		return response()->view('account.reputation_as_buyer', $vars);
	}
}

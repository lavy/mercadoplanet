<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

/**
 * Class ContactController
 * @package App\Http\Controllers
 */
class ContactController extends Controller
{
	/**
	 * ContactController constructor.
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$vars = [];
		$vars['user'] = Auth::user();
		return response()->view('contact.index', $vars);
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function send_contact(ContactRequest $request)
	{
		$vars = [];
		$vars['user'] = Auth::user();
		/*$validator = Validator::make($request->all(),
			[
				'asunto' => 'required|max:80',
				'contenido' => 'required'
			]
		);
		if ($validator->fails())
		{
			return redirect('/contact')
						->withErrors($validator)
						->withInput();
		}*/
		$tmp_files = [];
		if ($request->file('files')) {
			foreach ($request->file('files') as $key => $f) {
				$tmp_files[$f->getClientOriginalName()] = $f->getPathName();
			}
		}

		Mail::to(config('app.global.mail_address_support'))
			->send(new \App\Mail\Contact($vars['user'],
				[
					'subject' => $request->get('asunto'),
					'content' => $request->get('contenido'),
					'tmp_files' => $tmp_files
				]));

		Mail::to($vars['user']->email)
			->send(new \App\Mail\ContactCopy($vars['user'],
				[
					'subject' => $request->get('asunto'),
					'content' => $request->get('contenido'),
					'tmp_files' => $tmp_files
				]));

		return redirect('/contact')->with([
			'message' => 'Tu mensaje ha sido enviado.',
			'alert-class' => 'alert-success'
		]);
	}
}

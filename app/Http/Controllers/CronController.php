<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\Notifications\Ticket as TicketNotification;
use App\Notifications\SellerQualificationReminder as SellerQualificationReminderNotification;
use App\Notifications\BuyerQualificationReminder as BuyerQualificationReminderNotification;
use App\Notifications\InvoicePaymentReminder as InvoicePaymentReminderNotification;
use App\Notifications\PublicationEndsReminder as PublicationEndsReminderNotification;
use App\Notifications\FinishPublication as FinishPublicationNotification;
use App\Notifications\AvoidSellerQualificationBuyer as AvoidSellerQualificationBuyerNotification;
use App\Notifications\AvoidBuyerQualificationSeller as AvoidBuyerQualificationSellerNotification;
use App\Notifications\InvoicePaymentUserSuspension as InvoicePaymentUserSuspensionNotification;

class CronController extends Controller
{
	/**
	 * @param Request $request
	 */
    public function execute(Request $request) {
        $this->generateTickets();
        $this->qualificationReminder();
        $this->invoicePaymentReminder();
        $this->publicationEndsReminder();
        $this->finishPublications();
        $this->AvoidQualifications();
        $this->invoicePaymentPendingSuspend();
        
    }
    
    /* making invoices */
    private function generateTickets() {
        $tickets = \App\Ticket::where('tickets.status', 'Open')
            ->where('tickets.emit_on', '<=', date('Y-m-d'))
            ->get();
        foreach($tickets as $t) {
            $t->status = 'Emitted';
            $t->emitted_at = date('Y-m-d H:i:s');
            $t->save();
            $user = \App\User::find($t->user_id);
            $user->notify(new TicketNotification($user, $t));
            
        }
    }
    
    /* notify to sellers and buyers who have not given a qualification  */
    private function qualificationReminder() {
        $this->sellerQualificationReminder();
        $this->buyerQualificationReminder();
    }

    /* notify to sellers who have not given a qualification  */
    private function sellerQualificationReminder() {

        $transactions = \App\Transaction::where('seller_calification', 'No calificado')
            ->whereRaw('DATE(transactions.created_at) = DATE(NOW() - INTERVAL '.(config('app.global.days_to_qualify')-3).' DAY)')
            ->get();

        foreach($transactions as $t) {
            $user = \App\User::find($t->seller_id);
            $user->notify(new SellerQualificationReminderNotification($user, $t));
            
        }
    }

    /* notify to buyers who have not given a qualification  */
    private function buyerQualificationReminder() {

        $transactions = \App\Transaction::where('buyer_calification', 'No calificado')
            ->whereRaw('DATE(transactions.created_at) = DATE(NOW() - INTERVAL '.(config('app.global.days_to_qualify')-3).' DAY)')
            ->get();

        foreach($transactions as $t) {
            $user = \App\User::find($t->buyer_id);
            $user->notify(new BuyerQualificationReminderNotification($user, $t));
            
        }
    }

    /* send invoice payment remider  */
    private function invoicePaymentReminder() {

        $tickets = \App\Ticket::where('status', 'Emitted')
            ->whereRaw('DATE(tickets.emitted_at) = DATE(NOW() - INTERVAL '.config('app.global.days_before_cutoff_notification').' DAY)')
            ->get();

        foreach($tickets as $t) {
            $user = \App\User::find($t->user_id);
            $user->notify(new InvoicePaymentReminderNotification($user, $t));
            
        }
    }

    /* suspend user with invoice payment pending  */
    private function invoicePaymentPendingSuspend() {
        $tickets = \App\User::query()
            ->join('tickets', 'tickets.user_id', '=', 'users.id')
            ->where('tickets.status', 'Emitted')
            ->where('users.suspended', '0')
            ->whereRaw('DATE(tickets.emitted_at) <= DATE(NOW() - INTERVAL '.config('app.global.days_before_suspension_notification').' DAY)')
            ->get();

        foreach($tickets as $t) {
            $user = \App\User::find($t->user_id);
            $user->suspended = '1';
            $user->latest_suspension = date('Y-m-d H:i:s');
            //Delete Leader Mark
            $user->seller_type = 'Normal';
            $user->leader_notified = 0;
            $user->save();
            $user->notify(new InvoicePaymentUserSuspensionNotification($user, $t));
            
        }
    }

    /* send reminder before the publication ends  */
    private function publicationEndsReminder() {

        $publications = \App\Publication::where(function($q) {
            $q->where('publications.status', 'Active')
            ->orWhere('publications.status', 'Paused');
        })
        ->where('publications.cost_type', '!=', 'Premium')
        ->where('publications.cost_type', '!=', 'Fijo')
        ->whereRaw('DATE(ends_at) = DATE(NOW() + INTERVAL 5 DAY)')
            ->get();

        foreach($publications as $p) {
            $user = \App\User::find($p->user_id);
            $user->notify(new PublicationEndsReminderNotification($user, $p));
            
        }
    }

    /* finish publications when time runs out  */
    private function finishPublications() {

        $publications = \App\Publication::where(function($q) {
            $q->where('publications.status', 'Active')
            ->orWhere('publications.status', 'Paused');
        })
        ->whereRaw('ends_at < NOW()')
            ->get();

        foreach($publications as $p) {
            $user = \App\User::find($p->user_id);
            $user->notify(new FinishPublicationNotification($user, $p));
            $p->status = 'Finished';
            $p->save();
        }
    }
    
    /* avoid qualifications  */
    private function AvoidQualifications() {
        $this->avoidSellerQualifications();
        $this->avoidBuyerQualifications();
    }

    /* avoid seller qualifications  */
    private function avoidSellerQualifications() {

        $transactions = \App\Transaction::where('seller_calification', '!=', 'No calificado')
            ->where('buyer_request_change_qualification', '1')
            ->where('seller_qualification_modified', '0')
            ->whereRaw('transactions.buyer_request_change_qualification_date < (NOW() - INTERVAL '.(config('app.global.days_before_avoid_qualification')).' DAY)')
            ->get();

        foreach($transactions as $t) {
            $t->seller_qualification_modified = '1';
            $t->seller_calification = 'No calificado';
            $t->save();

            //Notify the buyer
            $user = \App\User::find($t->buyer_id);
            $user->notify(new AvoidSellerQualificationBuyerNotification($user, $t));
            
        }
    }

    /* avoid buyer qualifications  */
    private function avoidBuyerQualifications() {

        $transactions = \App\Transaction::where('buyer_calification', '!=', 'No calificado')
            ->where('seller_request_change_qualification', '1')
            ->where('buyer_qualification_modified', '0')
            ->whereRaw('transactions.seller_request_change_qualification_date < (NOW() - INTERVAL '.(config('app.global.days_before_avoid_qualification')).' DAY)')
            ->get();

        foreach($transactions as $t) {
            $t->buyer_qualification_modified = '1';
            $t->buyer_calification = 'No calificado';
            $t->save();

            //Notify the seller
            $user = \App\User::find($t->seller_id);
            $user->notify(new AvoidBuyerQualificationSellerNotification($user, $t));
            $user->notifyCanBeLeader();
            
        }
    }
}

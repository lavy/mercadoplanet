<?php

namespace App\Http\Controllers;

use \Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Publication;
use App\Category;
use App\Department;
use App\User;
use App\Question;
use App\Message;
use App\Transaction;
use App\Notifications\SellBuyer;
use App\Notifications\SellSeller;
use App\Notifications\Question as QuestionNotification;
use App\Notifications\AnsweredQuestion as AnsweredQuestionNotification;
use App\Notifications\Consultation as Consultation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Response;
use Illuminate\Http;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\JWTException;

/**
 * Class APIController
 * @package App\Http\Controllers
 */
class APIController extends Controller
{
    /**
     * @return mixed
     */
    public function signin()
    {
        $credentials = Input::only('email', 'password');

        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['error' => 'invalid_credentials'], Response::HTTP_UNAUTHORIZED);
        }

        return response()->json(compact('token'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $response = [];
        $featured = Publication::getMobileLatest($request->get('filter'), $request->get('offset'), $request->get('limit'));
        $count = Publication::countMobileLatest($request->get('filter'));
        foreach ($featured as $f) {
            if (count($f->images) > 0) {
                $image = url('/photo/120x120?url=' . urlencode($f->images[0]->name));
            } else {
                $image = url('/photo/120x120?url=nophoto.jpg');
            }
            $response[] = [
                'id' => $f->id,
                'title' => $f->title,
                'price' => number_format($f->price, 2, ',', '.'),
                'image' => $image
            ];
        }
        //echo count($response)."-".$request->get('limit')."-".count($featured);
        if (count($response) < $request->get('limit')) {
            $featured = Publication::getMobileLatestAll($request->get('filter'), $request->get('offset') - $count > 0 ? $request->get('offset') - $count : 0, $request->get('limit') - count($response));
            //  echo $request->get('offset')."-".$count."-".$request->get('limit')."-".count($response);
            foreach ($featured as $f) {
                if (count($f->images) > 0) {
                    $image = url('/photo/120x120?url=' . urlencode($f->images[0]->name));
                } else {
                    $image = url('/photo/120x120?url=nophoto.jpg');
                }
                $response[] = [
                    'id' => $f->id,
                    'title' => $f->title,
                    'price' => number_format($f->price, 2, ',', '.'),
                    'image' => $image
                ];
            }
        }

        return response()->json($response, 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function categories(Request $request)
    {
        $response = Category::where('parent_id', 4)->orderBy(DB::raw('name="Otras categorías", name'))->get();
        return response()->json($response, 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function subcategories(Request $request)
    {
        $categories = Category::where('parent_id', $request->id)->orderBy(DB::raw('name="Otras categorías", name'))->get();
        $res = [];
        foreach ($categories as $c) {
            $res[] = ['id' => $c->id, 'name' => $c->name, 'num' => Category::getCountPubs($c)];
        }
        return response()->json($res, 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function searchAutocomplete(Request $request)
    {
        $response = Publication::where('title', 'like', '%' . $request->search . '%')->orderBy('title')
            ->where('status', 'Active')
            ->get();
        $results = [];
        foreach ($response as $res) {
            if ($match = $this->resaltar($request->search, $res->title)) {
                $results[] = $match;
            }
        }
        $results = array_values(array_intersect_key($results, array_unique(array_map('strtolower', $results))));
        return response()->json($results, 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function productList(Request $request)
    {
        $category = Category::find($request->categoryId);
        $features = null;
        $sort = $request->sort ?: 'relevance';
        $condition = $request->condition ?: '';
        $location = $request->location ?: '';
        $locations = Department::query()
            ->select(DB::raw('districts.id'))
            ->join('provinces', 'provinces.department_id', '=', 'departments.id')
            ->join('districts', 'districts.province_id', '=', 'provinces.id')
            ->where('departments.id', $location)->pluck('districts.id')->toArray();
        $feature_pubs = null;
        $min = $request->min ?: null;
        $max = $request->max ?: null;
        $search = $request->search;
        $limit = $request->limit ?: 6;
        $offset = $request->offset ?: 0;
        $response = [];
        $pubs_query = Publication::getFilteredAPI($category, $features, $sort, $condition, $locations, $feature_pubs, $min, $max, $search, $limit, $offset);
        $pubs = $pubs_query->get();
        $count = count(Publication::getFilteredAPI($category, $features, $sort, $condition, $locations, $feature_pubs, $min, $max, $search, 0, 0)->get());
        $pubs_ids = $pubs_query->pluck('publications.id')->toArray();
        $count_locations = Department::getCountLocations($pubs_ids);
        $breadcrumb_cp = Category::getBreadCrumbCodePath($pubs_query->get());
        $breadcrumb_array = Category::getBreadCrumbArrayFromCodePath($breadcrumb_cp);
        $pubs_price = Publication::getFilteredAPI($category, $features, 'lowtohigh', $condition, $locations, $feature_pubs, null, null, $search, 0, 0)->pluck('price')->toArray();

        foreach ($pubs as $p) {
            if (count($p->images) > 0) {
                $image = url('/photo/120x120?url=' . urlencode($p->images[0]->name));
            } else {
                $image = url('/photo/120x120?url=nophoto.jpg');
            }
            $response[] = [
                'id' => $p->id,
                'title' => $p->title,
                'price' => number_format($p->price, 2, ',', '.'),
                'image' => $image
            ];
        }
        return response()->json(
            [
                'results' => $count,
                'list' => $response,
                'locations' => $count_locations,
                'breadcrumb' => $breadcrumb_array,
                'minPrice' => array_shift($pubs_price),
                'maxPrice' => array_pop($pubs_price)
            ], Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return Http\RedirectResponse
     */
    public function productDescription(Request $request)
    {

        if (!$request->id or !($pub = Publication::find($request->id))) {
            return redirect()->route('home');
        }
        if ($pub->status == 'Canceled' or $pub->status == 'Draft') {
            abort(404);
        }

        $parameters_seguridad = $pub->parameters()->where('value', '-1')->where('class', 'Seguridad')->orderBy('id')->get();
        $parameters_confort = $pub->parameters()->where('value', '-1')->where('class', 'Confort')->orderBy('id')->get();
        $parameters_sonido = $pub->parameters()->where('value', '-1')->where('class', 'Sonido')->orderBy('id')->get();
        $parameters_exterior = $pub->parameters()->where('value', '-1')->where('class', 'Exterior')->orderBy('id')->get();
        $parameters_caracteristicas = $pub->parameters()->where('value', '-1')->where('class', 'Caracteristicas Adicionales')->orderBy('id')->get();

        $parameters_ambientes = $pub->parameters()->where('value', '-1')->where('class', 'Ambientes')->orderBy('id')->get();
        $parameters_comodidades = $pub->parameters()->where('value', '-1')->where('class', 'Comodidades')->orderBy('id')->get();

        $parameters_zonas = $pub->parameters()->where('value', '-1')->where('class', 'Zonas')->orderBy('id')->get();

        $parameters_title = $pub->parameters()->where('value', '<>', '-1')->orderBy('id')->get();

        $toplevel = Category::getTopLevelCategory($pub->category->id);

        return response()->json([
            'description' => $pub->description_html,
            'pub' => $pub,
            'pubType' => $toplevel->parent_id,
            'parameters_seguridad' => $parameters_seguridad,
            'parameters_confort' => $parameters_confort,
            'parameters_sonido' => $parameters_sonido,
            'parameters_exterior' => $parameters_exterior,
            'parameters_title' => $parameters_title,
            'parameters_ambientes' => $parameters_ambientes,
            'parameters_comodidades' => $parameters_comodidades,
            'parameters_caracteristicas' => $parameters_caracteristicas,
            'parameters_zonas' => $parameters_zonas
        ], Response::HTTP_OK);
    }

    public function productQuestions(Request $request)
    {
        $limit = $request->limit ?: 10;
        $offset = $request->offset ?: 0;

        if (!$request->id or !($pub = Publication::find($request->id))) {
            return redirect()->route('home');
        }
        if ($pub->status == 'Canceled' or $pub->status == 'Draft') {
            abort(404);
        }


        $questions = $pub->questions()->orderBy('id', 'DESC')
            ->limit($limit)
            ->offset($offset)
            ->get();
        return response()->json(array(
            'questions' => $questions
        ), 200);
    }

    /**
     * @param $palabra
     * @param $texto
     * @return mixed|null
     */
    private function resaltar($palabra, $texto)
    {
        $matches = [];
        preg_match("/($palabra)[a-z0-9]*/i", $texto, $matches);
        if (count($matches) > 0) {
            return $matches[0];
        }
        return null;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function productShipment(Request $request)
    {
        if (!$request->id or !($pub = Publication::find($request->id))) {
            return response()->json(['is_ok' => false]);
        }
        if ($pub->status == 'Canceled' or $pub->status == 'Draft') {
            abort(404);
        }

        return response()->json([
            'freeShipment' => $pub->free_shipment,
            'personalPickup' => $pub->personal_pickup,
            'shippingCosts' => array_map(function ($el) {
                $el['cost'] = number_format($el['cost'], 2, ',', '.');
                return $el;
            }, $pub->shippingCosts->toArray()),
        ], Response::HTTP_OK);
    }


    //Restricted endpoints
    /**
     * @return mixed
     */
    public function validateToken()
    {
        try {
            if ($token = JWTAuth::getToken()) {
                $curUser = JWTAuth::toUser($token);
                return response()->json([
                    'valid' => true
                ]);
            }
        } catch (TokenExpiredException $e) {
        } catch (TokenInvalidException $e) {
        } catch (JWTException $e) {
        }
        return response()->json([
            'valid' => false
        ]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function sendConsultation(Request $request)
    {
        $publication_id = $request->id;
        $publication = Publication::find($publication_id);
        $message = $request->message;

//        if (!Auth::check() || !$publication || empty($message))
//        {
//            return response()->json([ 'is_ok' => false, 'value' => false ]);
//        }

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $seller = User::find($publication->user_id);

        if ($block = \App\Block::where('seller_id', $seller->id)->where('buyer_id', $user->id)->first()) {
            if ($block->no_questions == '1') {
                return response()->json(['is_ok' => false, 'value' => 'blocked']);
            }
        }
        $seller->notify(new Consultation($user, $publication, $message));


        $question = new Question();
        $question->publication_id = $publication_id;
        $question->asker_id = $user->id;
        $question->question = $message;
        $question->answer = '';
        $question->save();

        $resp = ['is_ok' => true, 'value' => true, 'message' => $message];

        return response()->json($resp);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function markOrUnmark(Request $request)
    {
        $publication = Publication::find($request->id);

//        if (!Auth::check() || !$publication)
//        {
//            return response()->json([ 'is_ok' => false, 'value' => false ]);
//        }
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        if ($publication->user_id == $user->id) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }

        if ($user->favorites->contains($publication)) {
            $user->favorites()->detach($publication);
            $resp = ['is_ok' => true, 'value' => false];
        } else {
            $user->favorites()->attach($publication);
            $resp = ['is_ok' => true, 'value' => true];
        }

        return response()->json($resp);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function addQuestion(Request $request)
    {

        $publication_id = $request->id;
        $publication = Publication::find($publication_id);
        $message = $request->text;
        if (empty($message)) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }

//        if (!Auth::check() || !$publication)
//        {
//            return response()->json([ 'is_ok' => false, 'value' => false ]);
//        }
        //filtering email
        $pattern = "/[^@\s]*[@, at][^@\s]*\.[^@\s]*/";
        $replacement = "*****";
        $message = preg_replace($pattern, $replacement, $message);
        //filtering urls
        $pattern = "/[a-zA-Z]*[:\/\/]*[A-Za-z0-9\-_]+\.+[A-Za-z0-9\.\/%&=\?\-_]+/i";
        $replacement = "*****";
        $message = preg_replace($pattern, $replacement, $message);
        //more than 2 numbers together
        $pattern = "/[0-9]{2,}/";
        $replacement = "*****";
        $message = preg_replace($pattern, $replacement, $message);
        // facebook word
        $pattern = "/facebook/";
        $replacement = "*****";
        $message = preg_replace($pattern, $replacement, $message);
        // numbers through the text
        $pattern = "/([0-9]{1,}).+([0-9]{1,}).+([0-9]{1,})/";
        $replacement = "*****";
        $message = preg_replace($pattern, $replacement, $message);


        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if ($message != $request->text) {
            return response()->json(['is_ok' => false, 'value' => 'contact']);
        }

        if ($block = \App\Block::where('seller_id', $publication->user_id)->where('buyer_id', $user->id)->first()) {
            if ($block->no_questions == '1') {
                return response()->json(['is_ok' => false, 'value' => 'blocked']);
            }
        }

        $seller = User::find($publication->user_id);

        $question = new Question();
        $question->publication_id = $publication_id;
        $question->asker_id = $user->id;
        $question->question = $message;
        $question->answer = '';
        $question->save();
        $seller->notify(new QuestionNotification($user, $publication, $question));
        $seller->sendMobileNotification('question', $question->publication->id, $question->publication->title);
        $resp = ['is_ok' => true, 'value' => true, 'message' => $question];

        return response()->json($resp);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function product(Request $request)
    {
        if (!$request->id or !($pub = Publication::find($request->id))) {
            return redirect()->route('home');
        }
        if ($pub->status == 'Canceled' or $pub->status == 'Draft') {
            abort(404);
        }

        $questions = [];
        $parameters = null;
        $toplevel = Category::getTopLevelCategory($pub->category->id);
        $category = Category::find($pub->category->id);

        $user = $pub->user;
        $reputation = User::user_reputation($user);

        $parameters_seguridad = $pub->parameters()->where('value', '-1')->where('class', 'Seguridad')->orderBy('id')->get();
        $parameters_confort = $pub->parameters()->where('value', '-1')->where('class', 'Confort')->orderBy('id')->get();
        $parameters_sonido = $pub->parameters()->where('value', '-1')->where('class', 'Sonido')->orderBy('id')->get();
        $parameters_exterior = $pub->parameters()->where('value', '-1')->where('class', 'Exterior')->orderBy('id')->get();
        $parameters_caracteristicas = $pub->parameters()->where('value', '-1')->where('class', 'Caracteristicas Adicionales')->orderBy('id')->get();

        $parameters_ambientes = $pub->parameters()->where('value', '-1')->where('class', 'Ambientes')->orderBy('id')->get();
        $parameters_comodidades = $pub->parameters()->where('value', '-1')->where('class', 'Comodidades')->orderBy('id')->get();

        $parameters_zonas = $pub->parameters()->where('value', '-1')->where('class', 'Zonas')->orderBy('id')->get();

        $parameters_title = $pub->parameters()->where('value', '<>', '-1')->orderBy('id')->get();

        $question = $pub->questions()->orderBy('id', 'DESC')->first();
        $questions = $pub->questions()->get();
        $shipping_costs = $pub->shippingCosts;

        $breadcrumb_array = Category::getBreadCrumbArrayFromCodePath($pub->category_path);
        $images = $pub->images()->where('position', '<>', '0')->orderBy('position')->get();

        $imgs = [];
        foreach ($images as $img) {
            $imgs[] = url('/photo/500x500?url=' . urlencode($img->name));
        }
        if (count($imgs) < 1) {
            $imgs[0] = url('/photo/500x500?url=nophoto.jpg');
        }
        $curUser = null;
        try {
            if ($token = JWTAuth::getToken()) {
                $curUser = JWTAuth::toUser($token);
            }
        } catch (TokenExpiredException $e) {
        } catch (TokenInvalidException $e) {
        } catch (JWTException $e) {
        }

        $publication = [
            'title' => $pub->title,
            'price' => number_format($pub->price, 2, ',', '.'),
            'condition' => $pub->condition,
            'sales' => $pub->getSales(),
            'reputation' => $reputation,
            'location' => $pub->location(),
            'sellerType' => $user->seller_type,
            'images' => $imgs,
            'pubType' => $toplevel->parent_id,
            'question' => $question,
            'totalQuestions' => count($questions),
            'vehicleYear' => $pub->vehicle_year,
            'vehicleKilometers' => number_format($pub->vehicle_kilometers, 0, ',', '.'),
            'vehicleFullLocation' => $pub->vehicleFullLocation(),
            'addressStreet' => $pub->address_street,
            'addressNumber' => $pub->address_number,
            'availableQuantity' => $pub->available_quantity,
            'fullName' => $pub->user->getFullName(),
            'contactPhone' => $pub->contact_phone,
            'isFavorite' => $curUser ? $curUser->favorites->contains($pub) : false,
            'userId' => $pub->user_id,
            'status' => $pub->status
        ];

        return response()->json([
            'breadcrumb' => [],
            'publication' => $publication,
            'category' => $category,
            'authenticated' => Auth::check(),
            'toplevel' => $toplevel,
            'shipping_costs' => $shipping_costs,
            'breadcrumb' => $breadcrumb_array,
            'reputation' => $reputation,
            'parameters_seguridad' => $parameters_seguridad,
            'parameters_confort' => $parameters_confort,
            'parameters_sonido' => $parameters_sonido,
            'parameters_exterior' => $parameters_exterior,
            'parameters_title' => $parameters_title,
            'parameters_ambientes' => $parameters_ambientes,
            'parameters_comodidades' => $parameters_comodidades,
            'parameters_caracteristicas' => $parameters_caracteristicas,
            'parameters_zonas' => $parameters_zonas,
            'images' => $images,
            'go_questions' => $request->has('go_questions'),
            'go_buy' => $request->has('go_buy'),
            'user' => $user
        ], Response::HTTP_OK);
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function purchases(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $limit = $request->limit ?: 6;
        $offset = $request->offset ?: 0;

        $pur = $user->purchases()
            ->whereRaw('transactions.created_at > (NOW() - INTERVAL 365 DAY)')
            ->orderBy('transactions.created_at', 'desc')
            ->orderBy('transactions.id', 'desc')->limit($limit)->offset($offset)->get();
        $response = [];

        foreach ($pur as $pu) {

            if ($pu->buyer_calification === 'Positivo') {
                $qualification = 'positivo';
            } elseif ($pu->buyer_calification === 'Negativo') {
                $qualification = 'negativo';
            } elseif ($pu->buyer_calification === 'Neutral') {
                $qualification = 'neutral';
            } elseif (($pu->buyer_calification === 'No calificado' && (time() - strtotime($pu->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600))
                or ($pu->buyer_calification === 'No calificado' && $pu->buyer_qualification_modified)
            ) {
                $qualification = 'no calificado';
            } else {
                $qualification = 'por calificar';
            }

            $p = $pu->publication;
            if (count($p->images) > 0) {
                $image = url('/photo/120x120?url=' . urlencode($p->images[0]->name));
            } else {
                $image = url('/photo/120x120?url=nophoto.jpg');
            }
            $response[] = [
                'id' => $pu->id,
                'title' => $p->title,
                'price' => number_format($pu->price, 2, ',', '.'),
                'image' => $image,
                'qualification' => $qualification
            ];
        }

        return response()->json(['purchases' => $response], Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function sales(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $limit = $request->limit ?: 6;
        $offset = $request->offset ?: 0;

        $pur = $user->sales()
            ->whereRaw('transactions.created_at > (NOW() - INTERVAL 365 DAY)')
            ->orderBy('transactions.created_at', 'desc')
            ->orderBy('transactions.id', 'desc')->limit($limit)->offset($offset)->get();

        $response = [];

        foreach ($pur as $pu) {

            if ($pu->seller_calification === 'Positivo') {
                $qualification = 'positivo';
            } elseif ($pu->seller_calification === 'Negativo') {
                $qualification = 'negativo';
            } elseif ($pu->seller_calification === 'Neutral') {
                $qualification = 'neutral';
            } elseif (($pu->seller_calification === 'No calificado' && (time() - strtotime($pu->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600))
                or ($pu->seller_calification === 'No calificado' && $pu->seller_qualification_modified)
            ) {
                $qualification = 'no calificado';
            } else {
                $qualification = 'por calificar';
            }

            $p = $pu->publication;
            if (count($p->images) > 0) {
                $image = url('/photo/120x120?url=' . urlencode($p->images[0]->name));
            } else {
                $image = url('/photo/120x120?url=nophoto.jpg');
            }
            $response[] = [
                'id' => $pu->id,
                'title' => $p->title,
                'price' => number_format($pu->price, 2, ',', '.'),
                'image' => $image,
                'qualification' => $qualification
            ];
        }

        return response()->json(['sales' => $response], Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function purchaseDetail(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $vars = [];

        if (!$vars['purchase'] = $user->purchases()->where('id', $request->id)->first()) {
            abort(404);
        }
        if ($vars['purchase']->seller->id != $user->id and $vars['purchase']->buyer->id != $user->id) {
            abort(404);
        }
        $vars['p'] = $vars['purchase']->publication;
        if (count($vars['p']->images) > 0) {
            $vars['image'] = url('/photo/120x120?url=' . urlencode($vars['p']->images[0]->name));
        } else {
            $vars['image'] = url('/photo/120x120?url=nophoto.jpg');
        }
        $vars['created'] = date("d-m-Y h:i A", strtotime($vars['purchase']->created_at));
        $vars['messages'] = $vars['purchase']->messages()
            ->orderBy('created_at', 'asc')->get();
        $vars['sellerName'] = $vars['p']->user->getFullName();
        $vars['sellerUsername'] = strtoupper($vars['p']->user->username);
        $vars['sellerPoints'] = $vars['p']->user->getPoints();

        if ($vars['purchase']->buyer_calification === 'Positivo') {
            $qualification = 'positivo';
        } elseif ($vars['purchase']->buyer_calification === 'Negativo') {
            $qualification = 'negativo';
        } elseif ($vars['purchase']->buyer_calification === 'Neutral') {
            $qualification = 'neutral';
        } elseif (($vars['purchase']->buyer_calification === 'No calificado' && (time() - strtotime($vars['purchase']->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600))
            or ($vars['purchase']->buyer_calification === 'No calificado' && $vars['purchase']->buyer_qualification_modified)
        ) {
            $qualification = 'no calificado';
        } else {
            $qualification = 'por calificar';
        }
        $vars['buyerQualification'] = $qualification;

        if ($vars['purchase']->seller_calification === 'Positivo') {
            $qualification = 'positivo';
        } elseif ($vars['purchase']->seller_calification === 'Negativo') {
            $qualification = 'negativo';
        } elseif ($vars['purchase']->seller_calification === 'Neutral') {
            $qualification = 'neutral';
        } elseif (($vars['purchase']->seller_calification === 'No calificado' && (time() - strtotime($vars['purchase']->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600))
            or ($vars['purchase']->seller_calification === 'No calificado' && $vars['purchase']->seller_qualification_modified)
        ) {
            $qualification = 'no calificado';
        } else {
            $qualification = 'por calificar';
        }

        if ($vars['buyerQualification'] == 'por calificar' or $vars['buyerQualification'] == 'no calificado') {
            $vars['sellerQualification'] = '';
        } else {
            $vars['sellerQualification'] = $qualification;
        }

        return response()->json($vars, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function saleDetail(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $vars = [];

        if (!$vars['sale'] = $user->sales()->where('id', $request->id)->first()) {
            abort(404);
        }
        if ($vars['sale']->seller->id != $user->id and $vars['sale']->buyer->id != $user->id) {
            abort(404);
        }
        $vars['p'] = $vars['sale']->publication;
        if (count($vars['p']->images) > 0) {
            $vars['image'] = url('/photo/120x120?url=' . urlencode($vars['p']->images[0]->name));
        } else {
            $vars['image'] = url('/photo/120x120?url=nophoto.jpg');
        }
        $vars['created'] = date("d-m-Y h:i A", strtotime($vars['sale']->created_at));
        $vars['messages'] = $vars['sale']->messages()
            ->orderBy('created_at', 'asc')->get();
        $vars['buyerName'] = $vars['sale']->buyer->getFullName();
        $vars['buyerUsername'] = strtoupper($vars['sale']->buyer->username);
        $vars['buyerPoints'] = $vars['sale']->buyer->getPoints();

        if ($vars['sale']->seller_calification === 'Positivo') {
            $qualification = 'positivo';
        } elseif ($vars['sale']->seller_calification === 'Negativo') {
            $qualification = 'negativo';
        } elseif ($vars['sale']->seller_calification === 'Neutral') {
            $qualification = 'neutral';
        } elseif (($vars['sale']->seller_calification === 'No calificado' && (time() - strtotime($vars['sale']->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600))
            or ($vars['sale']->seller_calification === 'No calificado' && $vars['sale']->seller_qualification_modified)
        ) {
            $qualification = 'no calificado';
        } else {
            $qualification = 'por calificar';
        }

        $vars['sellerQualification'] = $qualification;

        if ($vars['sale']->buyer_calification === 'Positivo') {
            $qualification = 'positivo';
        } elseif ($vars['sale']->buyer_calification === 'Negativo') {
            $qualification = 'negativo';
        } elseif ($vars['sale']->buyer_calification === 'Neutral') {
            $qualification = 'neutral';
        } elseif (($vars['sale']->buyer_calification === 'No calificado' && (time() - strtotime($vars['sale']->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600))
            or ($vars['sale']->buyer_calification === 'No calificado' && $vars['sale']->buyer_qualification_modified)
        ) {
            $qualification = 'no calificado';
        } else {
            $qualification = 'por calificar';
        }

//        if ($vars['sellerQualification'] == 'por calificar' or $vars['sellerQualification'] == 'no calificado') {
//            $vars['buyerQualification'] = '';
//        } else {
        $vars['buyerQualification'] = $qualification;
//        }

        return response()->json($vars, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function purchaseMessages(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $vars = [];

        if (!$vars['purchase'] = $user->purchases()->where('id', $request->id)->first()) {
            abort(404);
        }
        if ($vars['purchase']->seller->id != $user->id and $vars['purchase']->buyer->id != $user->id) {
            abort(404);
        }
        $vars['p'] = $vars['purchase']->publication;
        $vars['messages'] = $vars['purchase']->messages()
            ->orderBy('created_at', 'asc')->get();

        foreach ($vars['messages'] as $m) {
            $m['strDate'] = date("d/m/Y h:i A", strtotime($m->created_at));
        }

        return response()->json($vars, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function saleMessages(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $vars = [];

        if (!$vars['sale'] = $user->sales()->where('id', $request->id)->first()) {
            abort(404);
        }
        if ($vars['sale']->buyer->id != $user->id and $vars['sale']->seller->id != $user->id) {
            abort(404);
        }
        $vars['p'] = $vars['sale']->publication;
        $vars['messages'] = $vars['sale']->messages()
            ->orderBy('created_at', 'asc')->get();

        foreach ($vars['messages'] as $m) {
            $m['strDate'] = date("d/m/Y h:i A", strtotime($m->created_at));
        }

        return response()->json($vars, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function sendMessage(Request $request)
    {

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $publication = null;
        $type = 'purchase';
        if (!$purchase = $user->purchases()->where('id', $request->id)->first()) {
            $type = 'sale';
            if (!$purchase = $user->sales()->where('id', $request->id)->first()) {
                return response()->json(['is_ok' => false, 'value' => 'error']);
            }
        }

        if (!$publication = $purchase->publication) {
            return response()->json(['is_ok' => false, 'value' => 'error']);
        }
//        $this->validate($request, [
//            'files[]' => 'required|image|mimes:jpeg,png,jpg,gif|max:4096',
//        ]);

        $message = new Message();
        $message->publication_id = $publication->id;
        //$image->name = $imageName;
        $message->user_id = $user->id;
        $message->transaction_id = $request->id;
        $message->message = $request->message;
        $message->save();

        if ($type == 'sale') {
            $buyer = User::find($purchase->buyer_id);
            $buyer->sendMobileNotification('message-buyer', $purchase->id, $publication->title);
        }

        if ($type == 'purchase') {
            $seller = User::find($publication->user_id);
            $seller->sendMobileNotification('message-seller', $purchase->id, $publication->title);
        }

        $resp = ['is_ok' => true, 'value' => true, 'message' => $message];

        return response()->json($resp);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function favorites(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $response = [];

        $limit = $request->limit ?: 6;
        $offset = $request->offset ?: 0;

        $favorites = $user->favorites()
            ->where(function ($query) {
                $query->where('publications.status', 'Active')
                    ->orWhere('publications.status', 'Paused')
                    ->orWhere('publications.status', 'Finished');
            })
            ->whereRaw('publications.created_at > (NOW() - INTERVAL 900 DAY)')
            ->orderBy('favorites.id', 'desc')->limit($limit)->offset($offset)->get();

        foreach ($favorites as $f) {


            if (count($f->images) > 0) {
                $image = url('/photo/120x120?url=' . urlencode($f->images[0]->name));
            } else {
                $image = url('/photo/120x120?url=nophoto.jpg');
            }
            $response[] = [
                'id' => $f->id,
                'title' => $f->title,
                'price' => number_format($f->price, 2, ',', '.'),
                'image' => $image
            ];
        }

        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function buyerQuestions(Request $request)
    {
        $user = Auth::user();

        $pubs = $user->questions()
            ->select('publications.*')
            ->join('publications', 'publications.id', '=', 'questions.publication_id')
            ->where('publications.status', '<>', 'Canceled')
            ->where('publications.status', '<>', 'Finished')
            ->where('publications.status', '<>', 'Draft')
            ->whereRaw('questions.created_at > (NOW() - INTERVAL 60 DAY)')
            ->orderBy('questions.created_at', 'desc')->distinct()->get();


        foreach ($pubs as $p) {

            $image = \App\Image::where('publication_id', $p->id)->first();
            if ($image) {
                $image = url('/photo/120x120?url=' . urlencode($image->name));
            } else {
                $image = url('/photo/120x120?url=nophoto.jpg');
            }
            $response[] = [
                'id' => $p->id,
                'title' => $p->title,
                'price' => number_format($p->price, 2, ',', '.'),
                'userId' => $p->user_id,
                'image' => $image
            ];
        }

        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function buyerQuestionsList(Request $request)
    {
        $user = Auth::user();
        $limit = $request->limit ?: 10;
        $offset = $request->offset ?: 0;

        $questions = $user->questions()->where('publication_id', $request->id)
            ->orderBy('id', 'DESC')->limit($limit)->offset($offset)->get();

        return response()->json($questions, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function publications(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $vars = [];
        $limit = $request->limit ?: 6;
        $offset = $request->offset ?: 0;

        $status = (!empty($request->get('status')) ? $request->get('status') : 'active');

        if ($status == 'revision') {
            $q = $user->publications()
                ->where(function ($query) {
                    $query->where('publications.status', 'Revision')
                        ->orWhere('publications.status', 'Canceled');
                });
        } else {
            $q = $user->publications()
                ->where('publications.status', $status);
        }

        if ($status == 'finished' or $status == 'revision') {
            $q->whereRaw('publications.created_at > (NOW() - INTERVAL 365 DAY)');
        }

        $q->orderBy('publications.id', 'desc');

        $publications = $q->limit($limit)->offset($offset)->get();
        $vars['publications'] = [];

        foreach ($publications as $p) {

            if (count($p->images) > 0) {
                $image = url('/photo/120x120?url=' . urlencode($p->images[0]->name));
            } else {
                $image = url('/photo/120x120?url=nophoto.jpg');
            }


            $vars['publications'][] = [
                'id' => $p->id,
                'title' => $p->title,
                'price' => number_format($p->price, 2, ',', '.'),
                'image' => $image,
                'costType' => $p->cost_type
            ];
        }

        return response()->json($vars, 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function publicationsCount(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $vars = [];

        $vars['actives'] = $user->publications()
            ->where('publications.status', 'Active')->count();

        $vars['paused'] = $user->publications()
            ->where('publications.status', 'Paused')->count();

        $vars['finished'] = $user->publications()
            ->where('publications.status', 'Finished')
            ->whereRaw('publications.created_at > (NOW() - INTERVAL 365 DAY)')
            ->count();

        $vars['revision'] = $user->publications()
            ->where(function ($query) {
                $query->where('publications.status', 'Revision')
                    ->orWhere('publications.status', 'Canceled');
            })
            ->whereRaw('publications.created_at > (NOW() - INTERVAL 365 DAY)')
            ->count();

        return response()->json($vars, 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function publicationQuestions(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $vars = [];
        $limit = $request->limit ?: 6;
        $offset = $request->offset ?: 0;

        $publications = $user->publications()
            ->select('publications.*')
            ->addSelect('questions.*')
            ->join('questions', 'publications.id', '=', 'questions.publication_id')
            ->where('publications.status', '<>', 'Canceled')
            ->where('publications.status', '<>', 'Finished')
            ->where('publications.status', '<>', 'Draft')
            ->where('questions.answer', '')
            ->where('questions.banned_question', '0')
            ->whereRaw('questions.created_at > (NOW() - INTERVAL 60 DAY)')
            ->orderBy('questions.created_at', 'asc')->limit($limit)->offset($offset)->get();
        //print_r($vars['pubs']); die();

        $vars['pubs'] = [];

        foreach ($publications as $p) {
            $image = \App\Image::where('publication_id', $p->publication_id)->first();
            if ($image) {
                $image = url('/photo/120x120?url=' . urlencode($image->name));
            } else {
                $image = url('/photo/120x120?url=nophoto.jpg');
            }

            $question = \App\Question::find($p->id);
            $vars['pubs'][] = [
                'id' => $p->publication_id,
                'title' => $p->title,
                'price' => number_format($p->price, 2, ',', '.'),
                'image' => $image,
                'costType' => $p->cost_type,
                'question' => $question
            ];
        }
        return response()->json($vars, 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function answerQuestions(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $question = Question::where('id', $request->id)->where('answer', '')->first();
        if (!$question || $question->publication->user != $user) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }
        $question->answer = $request->answer;
        $question->save();
        $resp = ['is_ok' => true, 'value' => true];
        $buyer = User::find($question->asker_id);
        $buyer->notify(new AnsweredQuestionNotification($buyer, $question->publication, $question));
        $buyer->sendMobileNotification('response', $question->publication->id, $question->publication->title);
        return response()->json($resp);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function deleteQuestion(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $question_id = $request->id;

        $question = Question::find($question_id);
        if ($question->publication->user_id != $user->id) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }
        if ($request->no_questions == '1' or $request->no_offers == '1') {
            if (!$block = \App\Block::where('seller_id', $user->id)->where('buyer_id', $question->asker_id)->first()) {
                $block = new \App\Block();
                $block->buyer_id = $question->asker_id;
                $block->seller_id = $user->id;
            }
            if ($request->no_questions == '1') {
                $block->no_questions = true;
            }
            if ($request->no_offers == '1') {
                $block->no_offers = true;
            }
            $block->save();

        }
        $question->delete();
        return response()->json(['is_ok' => true, 'value' => false]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function updateFCMToken(Request $request)
    {

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        if (!$request->token) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }
        if ($request->token != 'delete-token') {
            $user->fcm_token = $request->token;
        } else {
            $user->fcm_token = '';
        }
        $user->save();

        return response()->json(['is_ok' => true, 'value' => false]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function confirmPurchase(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $publication_id = $request->id;
        $publication = Publication::find($publication_id);
        $quantity = $request->quantity;

        if (!$publication || !$quantity || $quantity < 1 || $publication->available_quantity < $quantity || $publication->status != 'Active') {
            return response()->json(['is_ok' => false, 'value' => false]);
        }

        $seller = User::find($publication->user_id);

        if ($block = \App\Block::where('seller_id', $seller->id)->where('buyer_id', $user->id)->first()) {
            if ($block->no_offers == '1') {
                return response()->json(['is_ok' => false, 'value' => 'blocked']);
            }
        }

        $tran = new Transaction();
        $tran->seller_id = $publication->user_id;
        $tran->buyer_id = $user->id;
        $tran->publication_id = $publication_id;
        $tran->price = $publication->price;
        $tran->quantity = $quantity;
        $tran->seller_comment = '';
        $tran->buyer_comment = '';
        $tran->save();
        $publication->available_quantity = $publication->available_quantity - $quantity;
        if ($publication->available_quantity < 1) {
            if ($publication->condition == 'Nuevo' && $publication->cost_type != 'Gratis') {
                $publication->status = 'Paused';
                $publication->lack_stock = '1';
            } else {
                $publication->status = 'Finished';
            }
        }
        $publication->save();
        $user->notify(new SellBuyer($user, $publication, $tran));
        $seller->notify(new SellSeller($user, $publication, $tran));
        $seller->sendMobileNotification('sale', $tran->id, $publication->title);
        $resp = ['is_ok' => true, 'value' => true];

        return response()->json($resp);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function notifications(Request $request)
    {
        $limit = $request->limit ?: 10;
        $offset = $request->offset ?: 0;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $vars = [];
        $vars['nots'] = [];

        $notifications = $user->notifications()
            ->orderBy('id', 'desc')->limit($limit)->offset($offset)->get();

        foreach ($notifications as $n) {

            $image = \App\Image::where('publication_id', $n->publication_id)->first();
            if ($image) {
                $image = url('/photo/120x120?url=' . urlencode($image->name));
            } else {
                $image = url('/photo/120x120?url=nophoto.jpg');
            }

            $vars['nots'][] = [
                'id' => $n->publication_id,
                'title' => $n->body,
                'image' => $image,
                'type' => $n->type,
                'transactionId' => $n->transaction_id,
                'since' => $this->humanizeTime($n->created_at)

            ];
        }

        return response()->json($vars);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function countNotifications(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $notifications = $user->notifications()
            ->where('created_at', '>', $user->latest_notification_view)
            ->orderBy('id', 'desc')->get();

        $resp = ['is_ok' => true, 'value' => count($notifications)];
        return response()->json($resp);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function resetNotifications(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $user->latest_notification_view = date('Y-m-d H:i:s');
        $user->save();

        $resp = ['is_ok' => true, 'value' => true];
        return response()->json($resp);
    }

    private function humanizeTime($date)
    {
        $diff = time() - strtotime($date);
        if ($diff / 60 < 60) {
            $m = ceil($diff / 60);
            return $m == 1 ? "1 minuto" : $m . " minutos";
        } elseif ($diff / 3600 < 24) {
            $h = ceil($diff / 3600);
            return $h == 1 ? "1 hora" : $h . " horas";
        } else {
            $d = ceil($diff / (3600 * 24));
            return $d == 1 ? "1 día" : $d . " días";
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Http\Response;
use \Illuminate\Support\Facades\DB;
use App\Publication;
use App\Category;
use App\Department;
use App\Feature;
use App\Question;
use App\Transaction;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Notifications\SellBuyer;
use App\Notifications\SellSeller;
use App\Notifications\Question as QuestionNotification;
use App\Notifications\Consultation;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $latest_visits = $request->cookie('latest_visits', [
            'products' => ['', '', '', '', ''],
            'categories' => ['', '', '', '', '']
        ]);

        $latest_pub = null;
        $bestsellers = null;
        $suggestions = null;
        $featured = null;
        $latest_visited = null;

        if (is_array($latest_visits['products'])) {
            $latest_visited = Publication::whereIn('id', array_slice($latest_visits['products'], 0, 3))
                ->limit(3)->get();
        }

        $category_id = $latest_visits['categories'][0];
        $toplevel = Category::getTopLevelCategory($category_id);
        $category = Category::find($category_id);

        if ($category) {
            $latest_pub = Publication::getLatest($category->code, 15);
            $suggestions = Publication::getSuggested($category->code, 15);
        }
        if ($toplevel) {
            $bestsellers = Publication::getBestsellers($toplevel->code, 15);
        }

        $latest_pub2 = null;
        $bestsellers2 = null;
        $suggestions2 = null;

        $category_id2 = $latest_visits['categories'][1];
        $toplevel2 = Category::getTopLevelCategory($category_id2);
        $category2 = Category::find($category_id2);

        if ($category2) {
            $latest_pub2 = Publication::getLatest($category2->code, 15);
            $suggestions2 = Publication::getSuggested($category2->code, 15);
        }
        if ($toplevel2) {
            $bestsellers2 = Publication::getBestsellers($toplevel2->code, 15);
        }

        $advertisements = Category::whereIn('id', [1, 2, 3])->get();
        $prod_cats = Category::where('parent_id', 4)->orderBy(DB::raw('name="Otras categorías", name'))->get();
        $featured = Publication::getFeatured(15);

        return response()->view('home.index', array(
            'latest_visited' => $latest_visited,
            'category' => $category,
            'toplevel' => $toplevel,
            'latest_pub' => $latest_pub,
            'bestsellers' => $bestsellers,
            'suggestions' => $suggestions,
            'category2' => $category2,
            'toplevel2' => $toplevel2,
            'latest_pub2' => $latest_pub2,
            'bestsellers2' => $bestsellers2,
            'suggestions2' => $suggestions2,
            'advertisements' => $advertisements,
            'featured' => $featured,
            'prod_cats' => $prod_cats,
            'authenticated' => Auth::check(),
        ))->withCookie(cookie('latest_visits', $latest_visits, 3600));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function privacy()
    {
        return view('home.privacy');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function category(Request $request)
    {
        $category = Category::findOrFail($request->category_id);
        return response()->view('home.category', [
            'category' => $category->getHomeTopLevelCategory(),
            'children_categories' => Category::getCategoriesByParentId($category),
            'suggestions' => Publication::getSuggested($category->code, 15),
            'bestsellers' => Publication::getBestsellers($category->code, 15),
            'latest_pub' => Publication::getLatest($category->code, 15),
            'authenticated' => Auth::check()
        ]);
    }

	/**
	 * @param Request $request
	 * @return mixed
	 */
    public function productList(Request $request)
    {
        $category = null;
        $search = $request->get('search');
        if ((!$category = Category::find($request->category)) and
            (empty($search))
        ) {
            return redirect()->route('home');
        }
        if ($category and $category->isHomeTopLevelCategory() and empty($search)) {
            return redirect()->route('category', [$category->id]);
        }

        $features = $request->features;
        $query_string = $request->all();
        unset($query_string['page']);
        $sort = null;
        $sub_cats = null;
        $group_features = null;

        $count_features1 = null;
        $count_features2 = null;
        $count_features3 = null;
        $feature_pubs1 = [];
        $feature_pubs2 = [];
        $feature_pubs3 = [];
        $feature_pubs = [];
        $condition = $request->condition;
        $location = $request->location;
        $feature1 = $request->feature1;
        $feature2 = $request->feature2;
        $feature3 = $request->feature3;
        $min = (is_numeric($request->min) ? $request->min : '');
        $max = (is_numeric($request->max) ? $request->max : '');

        switch ($request->sort) {
            case 'lowtohigh':
            case 'hightolow':
                $sort = $request->sort;
                break;
            default:
                $sort = 'relevance';
                break;
        }

        $locations = Department::getArrayDistrictsIdFromDepartmentId($location);

        if ($feature1) {
            $feature_pubs_q = Category::getCategoriesByFeature($feature1);
            $feature_pubs1 = $feature_pubs_q->pluck('feature_publication.publication_id')->toArray();
        }

        if ($feature2) {
            $feature_pubs_q = Category::getCategoriesByFeature($feature2);
            $feature_pubs2 = $feature_pubs_q->pluck('feature_publication.publication_id')->toArray();
        }

        if ($feature3) {
            $feature_pubs_q = Category::getCategoriesByFeature($feature3);
            $feature_pubs3 = $feature_pubs_q->pluck('feature_publication.publication_id')->toArray();
        }

        if (count($feature_pubs) and count($feature_pubs1)) {
            $feature_pubs = array_intersect($feature_pubs, $feature_pubs1);
        } else {
            $feature_pubs = array_merge($feature_pubs, $feature_pubs1);
        }

        if (count($feature_pubs) and count($feature_pubs2)) {
            $feature_pubs = array_intersect($feature_pubs, $feature_pubs2);
        } else {
            $feature_pubs = array_merge($feature_pubs, $feature_pubs2);
        }

        if (count($feature_pubs) and count($feature_pubs3)) {
            $feature_pubs = array_intersect($feature_pubs, $feature_pubs3);
        } else {
            $feature_pubs = array_merge($feature_pubs, $feature_pubs3);
        }
        //print_r($feature_pubs); die();


        $pubs_query = Publication::getFiltered($category, $features, $sort, $condition, $locations, $feature_pubs, $min, $max, $search);
        $pubs_ids = $pubs_query->pluck('publications.id')->toArray();

        $breadcrumb_cp = Category::getBreadCrumbCodePath($pubs_query->get());
        $count_conditions = Publication::countConditions($pubs_ids);
        $count_locations = Department::getCountLocations($pubs_ids);
        $breadcrumb_array = Category::getBreadCrumbArrayFromCodePath($breadcrumb_cp);

        if ($category or !empty($search)) {
            $sub_cats = Category::getFilteredChildren($pubs_ids, $breadcrumb_cp);
        }
        if (!count($sub_cats) and count($breadcrumb_array) > 0) {
            $last_category = $breadcrumb_array[count($breadcrumb_array) - 1];
            $group_features = $last_category->groupFeatures;

            $count_features1 = Category::getCountFeatures($pubs_ids, @$group_features[0]->id);
            $count_features2 = Category::getCountFeatures($pubs_ids, @$group_features[1]->id);
            $count_features3 = Category::getCountFeatures($pubs_ids, @$group_features[2]->id);
        }

        $pubs = Publication::getFiltered($category, $features, $sort, $condition, $locations, $feature_pubs, $min, $max, $search)->paginate(18);

        //Filters
        $filters = [];
        if ($feature1) {
            $filters['feature1'] = Feature::find($feature1);
        }
        if ($feature2) {
            $filters['feature2'] = Feature::find($feature2);
        }
        if ($feature3) {
            $filters['feature3'] = Feature::find($feature3);
        }
        if ($location) {
            $filters['location'] = Department::find($location);
        }
        if ($condition) {
            $obj = null;
            if ($condition == 'new') {
                $obj = ['id' => 'new', 'name' => 'Nuevo'];
            }
            if ($condition == 'used') {
                $obj = ['id' => 'used', 'name' => 'Usado'];
            }
            if ($obj) {
                $filters['condition'] = (object)$obj;
            }
        }
        if (!empty($min)) {
            $obj = ['id' => 'min', 'name' => 'Más de S/ ' . number_format($min, 0, ',', '.')];
            $filters['min'] = (object)$obj;

        }
        if (!empty($max)) {
            $obj = ['id' => 'max', 'name' => 'Menos de S/ ' . number_format($max, 0, ',', '.')];
            $filters['max'] = (object)$obj;

        }

        return response()->view('home.list', [
            'query' => $query_string,
            'breadcrumb' => $breadcrumb_array,
            'count_conditions' => $count_conditions,
            'count_locations' => $count_locations,
            'group_features' => $group_features,
            'count_features1' => $count_features1,
            'count_features2' => $count_features2,
            'count_features3' => $count_features3,
            'min' => $min,
            'max' => $max,
            'filters' => $filters,
            'pubs' => $pubs,
            'sub_cats' => $sub_cats,
            'sort' => $sort,
            'authenticated' => Auth::check(),
            'search' => $search,
            'view' => $request->get('view')
        ]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function product(Request $request)
    {

        if (!$request->id or !($pub = Publication::find($request->id))) {
            return redirect()->route('home');
        }
        if ($pub->status == 'Canceled' or $pub->status == 'Draft') {
            abort(404);
        }

        $latest_visits = $request->cookie('latest_visits', [
            'products' => ['', '', '', '', ''],
            'categories' => ['', '', '', '', '']
        ]);

        $latest_visits = User::addLatestVisitsInfo($pub, $latest_visits);
        $pub->visited();

        $suggestions = null;
        $questions = [];
        $bestsellers = null;
        $parameters = null;
        $toplevel = Category::getTopLevelCategory($pub->category->id);
        $category = Category::find($pub->category->id);

        $user = $pub->user;
        $reputation = User::user_reputation($user);

        if ($category) {
            $suggestions = Publication::getSuggested($category->code, 15);
        }
        if ($toplevel) {
            $bestsellers = Publication::getBestsellers($toplevel->code, 15);
        }
        if ($toplevel) {
            $latest = Publication::getLatest($toplevel->code, 15);
        }
        $parameters_seguridad = $pub->parameters()->where('value', '-1')->where('class', 'Seguridad')->orderBy('id')->get();
        $parameters_confort = $pub->parameters()->where('value', '-1')->where('class', 'Confort')->orderBy('id')->get();
        $parameters_sonido = $pub->parameters()->where('value', '-1')->where('class', 'Sonido')->orderBy('id')->get();
        $parameters_exterior = $pub->parameters()->where('value', '-1')->where('class', 'Exterior')->orderBy('id')->get();
        $parameters_caracteristicas = $pub->parameters()->where('value', '-1')->where('class', 'Caracteristicas Adicionales')->orderBy('id')->get();

        $parameters_ambientes = $pub->parameters()->where('value', '-1')->where('class', 'Ambientes')->orderBy('id')->get();
        $parameters_comodidades = $pub->parameters()->where('value', '-1')->where('class', 'Comodidades')->orderBy('id')->get();

        $parameters_zonas = $pub->parameters()->where('value', '-1')->where('class', 'Zonas')->orderBy('id')->get();

        $parameters_title = $pub->parameters()->where('value', '<>', '-1')->orderBy('id')->get();

        $questions = $pub->questions()->orderBy('id', 'DESC')->limit(15)->get();
        $shipping_costs = $pub->shippingCosts;

        $breadcrumb_array = Category::getBreadCrumbArrayFromCodePath($pub->category_path);
        $images = $pub->images()->where('position', '<>', '0')->orderBy('position')->get();

        if (!is_null($pub->youtube_url) and !empty($pub->youtube_url)) {
            $sliceUrlYoutube = explode('=', $pub->youtube_url);
            $url = explode('/', $sliceUrlYoutube[0]);
            $urlToEmbed = $url[0] . '//' . $url[2] . '/embed/' . $sliceUrlYoutube[1];
        }

        return response()->view('home.product', array(
            'breadcrumb' => [],
            'publication' => $pub,
            'youtubeUrl' => (isset($urlToEmbed)) ? $urlToEmbed : null,
            'suggestions' => $suggestions,
            'category' => $category,
            'authenticated' => Auth::check(),
            'bestsellers' => $bestsellers,
            'toplevel' => $toplevel,
            'latest' => $latest,
            'questions' => $questions,
            'shipping_costs' => $shipping_costs,
            'breadcrumb' => $breadcrumb_array,
            'reputation' => $reputation,
            'parameters_seguridad' => $parameters_seguridad,
            'parameters_confort' => $parameters_confort,
            'parameters_sonido' => $parameters_sonido,
            'parameters_exterior' => $parameters_exterior,
            'parameters_title' => $parameters_title,
            'parameters_ambientes' => $parameters_ambientes,
            'parameters_comodidades' => $parameters_comodidades,
            'parameters_caracteristicas' => $parameters_caracteristicas,
            'parameters_zonas' => $parameters_zonas,
            'images' => $images,
            'go_questions' => $request->has('go_questions'),
            'go_buy' => $request->has('go_buy'),
            'user' => $user
        ))->withCookie(cookie('latest_visits', $latest_visits, 3600));
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function add_question(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }

        $publication_id = $request->id;
        $publication = Publication::find($publication_id);
        $message = $request->message;

        if (!Auth::check() || !$publication) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }
        //filtering email
        $pattern = "/[^@\s]*[@, at][^@\s]*\.[^@\s]*/";
        $replacement = "*****";
        $message = preg_replace($pattern, $replacement, $message);
        //filtering urls
        $pattern = "/[a-zA-Z]*[:\/\/]*[A-Za-z0-9\-_]+\.+[A-Za-z0-9\.\/%&=\?\-_]+/i";
        $replacement = "*****";
        $message = preg_replace($pattern, $replacement, $message);
        //more than 2 numbers together
        $pattern = "/[0-9]{2,}/";
        $replacement = "*****";
        $message = preg_replace($pattern, $replacement, $message);
        // facebook word
        $pattern = "/facebook/";
        $replacement = "*****";
        $message = preg_replace($pattern, $replacement, $message);
        // numbers through the text
        $pattern = "/([0-9]{1,}).+([0-9]{1,}).+([0-9]{1,})/";
        $replacement = "*****";
        $message = preg_replace($pattern, $replacement, $message);

        $user = Auth::user();
        if ($message != $request->message) {
            return response()->json(['is_ok' => false, 'value' => 'contact']);
        }

        if ($block = \App\Block::where('seller_id', $publication->user_id)->where('buyer_id', $user->id)->first()) {
            if ($block->no_questions == '1') {
                return response()->json(['is_ok' => false, 'value' => 'blocked']);
            }
        }

        $question = Question::saveQuestion($publication_id, $user->id, $message, '');

        $seller = User::find($publication->user_id);
        $seller->notify(new QuestionNotification($user, $publication, $question));
        $seller->sendMobileNotification('question', $question->publication->id, $question->publication->title, $question->publication->id);

        return response()->json(['is_ok' => true, 'value' => true, 'message' => $message]);
    }


    /**
     * @param Request $request
     * @return Response
     */
    public function get_questions(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }

        $publication = Publication::find($request->id);

        if (!$publication) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }

        $questions = [];
        $resp = $publication->questions()->where('id', '<', $request->latest)
            ->orderBy('id', 'DESC')->limit(15)->get();
        foreach ($resp as $item) {
            $question['id'] = $item->id;
            $question['date'] = date('d/m/y H:i', strtotime($item->updated_at));

            if ($item->banned_question == 0) $question['question'] = $item->question;
            else $question['question'] = 'XXX';

            if ($item->banned_answer == 0) $question['answer'] = $item->answer;
            else $question['answer'] = 'XXX';

            $questions[] = $question;
        }

        return response()->json(['is_ok' => true, 'value' => true, 'questions' => $questions]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function confirm_purchase(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }

        $publication_id = $request->id;
        $publication = Publication::find($publication_id);
        $quantity = $request->quantity;

        if (!Auth::check() || !$publication || !$quantity || $quantity < 1 || $publication->available_quantity < $quantity || $publication->status != 'Active') {
            return response()->json(['is_ok' => false, 'value' => false]);
        }
        $user = Auth::user();

        $seller = User::find($publication->user_id);

        if ($block = \App\Block::where('seller_id', $seller->id)->where('buyer_id', $user->id)->first()) {
            if ($block->no_offers == '1') {
                return response()->json(['is_ok' => false, 'value' => 'blocked']);
            }
        }

        $tran = new Transaction();
        $tran->seller_id = $publication->user_id;
        $tran->buyer_id = $user->id;
        $tran->publication_id = $publication_id;
        $tran->price = $publication->price;
        $tran->quantity = $quantity;
        $tran->seller_comment = '';
        $tran->buyer_comment = '';
        $tran->save();
        $publication->available_quantity = $publication->available_quantity - $quantity;
        if ($publication->available_quantity < 1) {
            if ($publication->condition == 'Nuevo' && $publication->cost_type != 'Gratis') {
                $publication->status = 'Paused';
                $publication->lack_stock = '1';
            } else {
                $publication->status = 'Finished';
            }
        }
        $publication->save();
        $user->notify(new SellBuyer($user, $publication, $tran));
        $seller->notify(new SellSeller($user, $publication, $tran));
        $seller->sendMobileNotification('sale', $tran->id, $publication->title, $publication_id);
        $resp = ['is_ok' => true, 'value' => true];

        return response()->json($resp);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function send_consultation(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }

        $publication_id = $request->id;
        $publication = Publication::find($publication_id);
        $message = $request->message;

        if (!Auth::check() || !$publication || empty($message)) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }

        $user = Auth::user();

        $seller = User::find($publication->user_id);

        if ($block = \App\Block::where('seller_id', $seller->id)->where('buyer_id', $user->id)->first()) {
            if ($block->no_questions == '1') {
                return response()->json(['is_ok' => false, 'value' => 'blocked']);
            }
        }
        $seller->notify(new Consultation($user, $publication, $message));

        Question::saveQuestion($publication_id, $user->id, $message, '');

        return response()->json(['is_ok' => true, 'value' => true, 'message' => $message]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function update_latest_notification_view(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }

        if (!Auth::check()) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }

        User::updateLatestNotificationView();

        return response()->json(['is_ok' => true, 'value' => true]);
    }

}

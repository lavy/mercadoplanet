<?php

namespace App\Http\Controllers\Admin;

use App\Currency;
use App\Http\Requests\CurrencyRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class CurrencyController
 * @package App\Http\Controllers\Admin
 */
class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $currency_search = $request->isMethod('get') ? $request->get('currency-search') : $request->session()->pull('currency-search', '');

        $request->session()->put('currency-search', ($currency_search));
        $query = Currency::orderBy('name');
        if (!empty($currency_search)) {
            $query->where('name', 'like', "%$currency_search%")
                ->orWhere('symbol', 'like', "%$currency_search%")
                ->orWhere('id', 'like', "%$currency_search%");
        }
        $currencies = $query->paginate(10);
        return response()->view('admin.currencies.index',
            [
                'currencies' => $currencies,
                'currency_search' => $currency_search
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.currencies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CurrencyRequest $request)
    {
        $currency = new Currency();
        $currency->name = $request->input('name');
        $currency->symbol = $request->input('symbol');
        $currency->save();
        return redirect('admin/currency')->with(['message' => 'Succesfully created an currency']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $currency = Currency::findOrFail($id);
        return view('admin.currencies.edit')->with(['currency' => $currency]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CurrencyRequest $request, $id)
    {
        $currency = Currency::findOrFail($id);
        $currency->name = $request->input('name');
        $currency->symbol = $request->input('symbol');
        $currency->save();
        return redirect('admin/currency')->with(['message' => 'Succesfully created an currency']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currency = Currency::findOrFail($id);
        $currency->delete();
        return redirect('admin/currency')->with(['message' => 'Currency Deleted Succesfully']);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\BankAccount;
use App\Http\Requests\BankAccountRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class BankAccountController
 * @package App\Http\Controllers\Admin
 */
class BankAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $bank_search = $request->isMethod('get') ? $request->get('bank-search') : $request->session()->pull('bank-search', '');

        $request->session()->put('bank-search', ($bank_search));
        $query = BankAccount::orderBy('bank');
        if (!empty($bank_search)) {
            $query->where('description', 'like', "%$bank_search%")
                ->orWhere('bank', 'like', "%$bank_search%")
                ->orWhere('id', 'like', "%$bank_search%");
        }
        $banks = $query->paginate(10);
        return response()->view('admin.bank_accounts.index',
            [
                'banks' => $banks,
                'bank_search' => $bank_search
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.bank_accounts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(BankAccountRequest $request)
    {
        $bank = new BankAccount();
        $bank->description = $request->input('description');
        $bank->bank = $request->input('payment');
        $bank->save();
        return redirect('admin/bank-account')->with(['success' => 'Successfully Added new Bank Account']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bank = BankAccount::findOrFail($id);
        return view('admin.bank_accounts.edit')->with(['bank' => $bank]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(BankAccountRequest $request, $id)
    {
        $bank = BankAccount::findOrFail($id);
        $bank->description = $request->input('description');
        $bank->bank = $request->input('payment');
        $bank->save();
        return redirect('admin/bank-account')->with(['message' => 'Successfull Updated the Bank Account']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bank = BankAccount::findOrFail($id);
        $bank->delete();
        return redirect('admin/bank-account')->with(['message' => 'Currency Added Succesfully']);
    }
}

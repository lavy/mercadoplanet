<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Session;
use App\User;
use App\Ticket;
use App\Publication;
use App\Question;
use App\Transaction;
use App\Configuration;
use App\Notifications\InvoicePaymentChangeStatus as InvoicePaymentChangeStatusNotification;
use App\Notifications\Leader as LeaderNotification;
use App\Notifications\PublicationChangeStatus as PublicationChangeStatusNotification;

/**
 * Class HomeController
 * @package App\Http\Controllers\Admin
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth.admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user_search = $request->isMethod('post') ? $request->get('user-search') : $request->session()->pull('user-search', '');

        $request->session()->put('user-search', ($user_search));
        $query = User::orderBy('first_name');
        if (!empty($user_search)) {
            $query->where('document', 'like', "%$user_search%")
                ->orWhere('first_name', 'like', "%$user_search%")
                ->orWhere('last_name', 'like', "%$user_search%")
                ->orWhere('email', 'like', "%$user_search%")
                ->orWhere('username', 'like', "%$user_search%");
        }
        $users = $query->paginate(10);
        return response()->view('admin.home', ['users' => $users, 'user_search' => $user_search]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function user_detail($id)
    {
        $user = User::findOrFail($id);
        return response()->view('admin.user_detail', ['user' => $user]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function user_modify($id)
    {
        $user = User::findOrFail($id);
        return response()->view('admin.user_modify', ['user' => $user]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function user_modify_save(Request $request)
    {
        $user = User::findOrFail($request->get('id'));

        $messages = [
        ];
        $fields = [
            'document_type' => 'required',
            'document' => ['required', 'numeric', Rule::unique('users')->ignore($user->id)],
            'first_name' => 'required|max:30',
            'last_name' => 'required|max:30',
            'email' => ['required', 'email', 'max:100', Rule::unique('users')->ignore($user->id)],
            'phone_number' => ['required', 'numeric', Rule::unique('users')->ignore($user->id)],
            'username' => ['required', 'min:6', 'max:20', Rule::unique('users')->ignore($user->id)],
            'status_cplx' => 'required',
            'seller_type' => 'required',
            'id' => 'required',
        ];
        $validator = Validator::make($request->all(), $fields, $messages);
        if ($validator->fails()) {
            return redirect('/admin/user-modify/' . $user->id)
                ->withErrors($validator)
                ->withInput();
        }

        $user->document_type = $request->get('document_type');
        $user->document = $request->get('document');
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->phone_number = $request->get('phone_number');
        $user->username = $request->get('username');
        $user->seller_type = $request->get('seller_type');
        $user->leader_notified = '0';
        $user->username = $request->get('username');
        if ($request->get('status_cplx') == 'Activo') {
            $user->status = 'Active';
            $user->suspended = '0';
        }
        if ($request->get('status_cplx') == 'Deshabilitado') {
            $user->status = 'Disabled';
            $user->suspended = '0';
        }
        if ($request->get('status_cplx') == 'Suspendido para vender') {
            $user->status = 'Active';
            $user->suspended = '1';
        }
        $user->save();

        return redirect('/admin/user-modify/' . $user->id);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function user_invoices(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $invoice_search = $request->isMethod('post') ? $request->get('invoice-search') : $request->session()->pull('invoice-search', '');
        $request->session()->put('invoice-search', ($invoice_search));

        $query = $user->tickets()->orderBy('id', 'desc');

        if (!empty($invoice_search)) {
            $query->where('tickets.id', 'like', "%$invoice_search%");
        }
        $invoices = $query->paginate(10);

        return response()->view('admin.invoices',
            [
                'user' => $user,
                'invoices' => $invoices,
                'invoice_search' => $invoice_search
            ]);

    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function user_invoice_detail(Request $request, $id)
    {
        $invoice = Ticket::findOrFail($id);
        $items = $invoice->ticketItems()->orderBy('id', 'desc')->paginate(10);
        return response()->view('admin.invoice_detail', ['invoice' => $invoice, 'items' => $items]);
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function user_purchases(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $purchase_search = $request->isMethod('post') ? $request->get('purchase_search') : $request->session()->pull('purchase_search', '');
        $request->session()->put('purchase_search', ($purchase_search));

        $query = $user->purchases()->orderBy('id', 'desc');

        if (!empty($purchase_search)) {
            $query->where('transactions.id', 'like', "%$purchase_search%");
        }
        $purchases = $query->paginate(10);

        return response()->view('admin.purchases',
            [
                'user' => $user,
                'purchases' => $purchases,
                'purchase_search' => $purchase_search
            ]);
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function user_sales(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $sale_search = $request->isMethod('post') ? $request->get('sale_search') : $request->session()->pull('sale_search', '');
        $request->session()->put('sale_search', ($sale_search));

        $query = $user->sales()->orderBy('id', 'desc');

        if (!empty($sale_search)) {
            $query->where('transactions.id', 'like', "%$sale_search%");
        }
        $sales = $query->paginate(10);

        return response()->view('admin.sales',
            [
                'user' => $user,
                'sales' => $sales,
                'sale_search' => $sale_search
            ]);

    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function user_invoice_payment(Request $request, $id)
    {
        $invoice = Ticket::findOrFail($id);
        return response()->view('admin.invoice_payment', ['invoice' => $invoice]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function change_payment_status(Request $request, $id)
    {
        $invoice = Ticket::findOrFail($id);
        $user = $invoice->user;
        switch ($request->get('status')) {
            case 'revisar':
                $invoice->payment_status = 'Por revisar';
                if ($invoice->status == 'Paid') {
                    $invoice->status = 'Emitted';
                }
                break;
            case 'aprobar':
                $invoice->payment_status = 'Aprobado';
                $user->notify(new InvoicePaymentChangeStatusNotification($user, $invoice));
                if ($invoice->status == 'Emitted') {
                    $invoice->status = 'Paid';
                    $user->suspended = '0';
                    $user->save();
                }
                break;
            case 'rechazar':
                $invoice->payment_status = 'Rechazado';
                $user->notify(new InvoicePaymentChangeStatusNotification($user, $invoice));
                if ($invoice->status == 'Paid') {
                    $invoice->status = 'Emitted';
                }
                break;
        }
        $invoice->save();
        return redirect('/admin/user-invoice-payment/' . $invoice->id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function change_leader_status($id)
    {
        $user = User::findOrFail($id);

        $status_leader = 'Normal';
        $status = $user->canBeLeader();
        switch ($status) {
            case '0':
                $status_leader = 'Normal';
                break;
            case '1':
                $status_leader = 'Líder';
                $user->notify(new LeaderNotification($user, $status));
                break;
            case '2':
                $status_leader = 'Líder Gold';
                $user->notify(new LeaderNotification($user, $status));
                break;
        }

        $user->seller_type = $status_leader;
        $user->leader_notified = '0';
        $user->save();

        return redirect('/admin/home/');

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function publications(Request $request)
    {
        $query_string = $request->all();
        unset($query_string['page']);
        $query = $query_string;
        $status = (!empty($request->get('status')) ? $request->get('status') : 'active');

        $publication_search = $request->isMethod('post') ? $request->get('publication_search') : $request->session()->pull('publication_search', '');

        $request->session()->put('publication_search', ($publication_search));
        $q = Publication::where('status', '!=', 'Draft')
            ->where('status', $status)
            ->whereRaw('created_at > (NOW() - INTERVAL 365 DAY)');

        if ($status == 'revision' || $status == 'canceled') {
            $q->orderByRaw("(CASE WHEN status_date IS NULL THEN 1 ELSE 0 END)", 'DESC');
            $q->orderBy('status_date', 'asc');
        } else {
            $q->orderBy('id', 'desc');
        }

        if (!empty($publication_search)) {
            $q->where(function ($q) use ($publication_search) {
                $q->where('id', 'like', "%$publication_search%")
                    ->orWhere('title', 'like', "%$publication_search%");
            });
        }
        $publications = $q->paginate(50);
        return response()->view('admin.publications', [
            'publications' => $publications,
            'publication_search' => $publication_search,
            'query' => $query,
            'status' => $status
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function status_modify($id)
    {
        $publication = Publication::findOrFail($id);
        return response()->view('admin.status_modify', ['publication' => $publication]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function status_modify_save(Request $request)
    {
        $publication = Publication::findOrFail($request->get('id'));
        $user = $publication->user;
        $publication->status = $request->get('status');
        $publication->status_description = $request->get('status_description');
        $publication->status_date = date('Y-m-d H:i:s');
        $publication->save();
        $user->notify(new PublicationChangeStatusNotification($user, $publication));

        Session::flash('message', 'La publicación #' . $publication->id . ' ha sido cambiada de estado.');
        Session::flash('alert-class', 'alert-success');
        return redirect('/admin/publications?status=' . strtolower($request->get('status')));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function questions(Request $request, $id)
    {
        $question_search = $request->isMethod('post') ? $request->get('question_search') : $request->session()->pull('question_search', '');

        $request->session()->put('question_search', ($question_search));

        $q = Question::where('publication_id', $id)
            ->orderBy('created_at', 'desc');

        if (!empty($question_search)) {
            $q->where(function ($q) use ($question_search) {
                $q->where('question', 'like', "%$question_search%")
                    ->orWhere('answer', 'like', "%$question_search%");
            });
        }
        $questions = $q->paginate(20);
        return response()->view('admin.questions',
            [
                'questions' => $questions,
                'question_search' => $question_search,
                'id' => $id,
                'publication' => Publication::find($id)
            ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function disable_question($id)
    {
        $question = Question::findOrFail($id);
        $question->banned_question = '1';
        $question->timestamps = false;
        $question->save();

        Session::flash('message', 'La pregunta ha sido deshabilitada.');
        Session::flash('alert-class', 'alert-success');
        return redirect('/admin/questions/' . $question->publication->id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function enable_question($id)
    {
        $question = Question::findOrFail($id);
        $question->banned_question = '0';
        $question->timestamps = false;
        $question->save();

        Session::flash('message', 'La pregunta ha sido habilitada.');
        Session::flash('alert-class', 'alert-success');
        return redirect('/admin/questions/' . $question->publication->id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function disable_answer($id)
    {
        $question = Question::findOrFail($id);
        $question->banned_answer = '1';
        $question->timestamps = false;
        $question->save();

        Session::flash('message', 'La respuesta ha sido deshabilitada.');
        Session::flash('alert-class', 'alert-success');
        return redirect('/admin/questions/' . $question->publication->id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function enable_answer($id)
    {
        $question = Question::findOrFail($id);
        $question->banned_answer = '0';
        $question->timestamps = false;
        $question->save();

        Session::flash('message', 'La respuesta ha sido habilitada.');
        Session::flash('alert-class', 'alert-success');
        return redirect('/admin/questions/' . $question->publication->id);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function seller_delete_qualification(Request $request, $id)
    {
        $transaction = Transaction::findOrFail($id);
        $transaction->seller_calification = 'No calificado';
        $transaction->seller_qualification_modified = '1';
        $transaction->save();

        Session::flash('message', 'La calificación del vendedor ha sido eliminada.');
        Session::flash('alert-class', 'alert-success');
        if ($request->get('origin') == '1')
            return redirect('/admin/user-purchases/' . $transaction->buyer->id);
        else
            return redirect('/admin/user-sales/' . $transaction->publication->user->id);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function buyer_delete_qualification(Request $request, $id)
    {
        $transaction = Transaction::findOrFail($id);
        $transaction->buyer_calification = 'No calificado';
        $transaction->buyer_qualification_modified = '1';
        $transaction->save();

        Session::flash('message', 'La calificación del comprador ha sido eliminada.');
        Session::flash('alert-class', 'alert-success');
        if ($request->get('origin') == '1')
            return redirect('/admin/user-purchases/' . $transaction->buyer->id);
        else
            return redirect('/admin/user-sales/' . $transaction->publication->user->id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function transaction_detail($id)
    {
        $transaction = Transaction::findOrFail($id);

        return response()->view('admin.transaction_detail',
            ['transaction' => $transaction]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function payment_invoices(Request $request)
    {
        $query_string = $request->all();
        unset($query_string['page']);
        $queryx = $query_string;
        $status = (!empty($request->get('status')) ? $request->get('status') : 'pending');

        $payment_search = $request->isMethod('post') ? $request->get('payment-search') : $request->session()->pull('payment-search', '');
        $request->session()->put('payment-search', ($payment_search));

        $query = Ticket::orderBy('payment_date', 'asc');

        if (!empty($payment_search)) {
            $query->where('tickets.id', 'like', "%$payment_search%");
        }
        switch ($status) {
            case 'pending':
                $query->where('payment_status', 'Por revisar');
                break;
            case 'approved':
                $query->where('payment_status', 'Aprobado');
                break;
            case 'rejected':
                $query->where('payment_status', 'Rechazado');
                break;
        }
        $invoices = $query->paginate(20);

        return response()->view('admin.payment_invoices',
            [
                'invoices' => $invoices,
                'payment_search' => $payment_search,
                'status' => $status,
                'query' => $queryx
            ]);

    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function activate_plans()
    {
        $conf = Configuration::find(1);
        $conf->activate_plans = ($conf->activate_plans === 1) ? 0 : 1;
        $conf->save();
        return redirect('/admin/');
    }
}
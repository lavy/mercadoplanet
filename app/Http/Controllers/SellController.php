<?php

namespace App\Http\Controllers;

use App\Configuration;
use App\Currency;
use App\Helpers\PlanHelper;
use App\ShippingCost;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Validator;
use App\Category;
use App\Publication;
use App\Image;
use App\PublicationIndex;
use App\Department;
use App\Province;
use App\District;
use App\Parameter;
use App\Ticket;
use App\TicketItem;

/**
 * Class SellController
 * @package App\Http\Controllers
 */
class SellController extends Controller
{
    /**
     * SellController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        if (!$user->addresses()->where('default_for_sales', '1')->first()) {
            return redirect('/completereg');
        }

        if ($user->suspended) {
            Session::flash('message', 'Has sido suspendido para vender, revisa si tienes facturas pendientes por pagar, si no es así por favor contacta a soporte.');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('billing');
        }

        return view('sell.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function category($id)
    {
        $user = Auth::user();

        if (!$user->addresses()->where('default_for_sales', '1')->first()) {
            return redirect('/completereg');
        }

        $category = Category::findOrFail($id);
        $subcategories = $category->children()->orderBy('name')->get();

        $category_path = null;
        $features = null;
        if ($publication = $user->publications()->where('status', 'Draft')->first()) {
            $cp = $publication->category_path;
            $category_path = Category::getPathFromCodePath($cp);
            $features = $publication->features()->orderBy('feature_id')->get();
        }

        return response()->view('sell.category', [
            'category' => $category,
            'subcategories' => $subcategories,
            'category_path' => $category_path,
            'features' => $features
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getSubcategory(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }

        $category = ($request->offset ? Category::find($request->category_id) : Category::find($request->id));
        $offset = ($request->offset ?: 0);

        if (!$category) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }

        $subcategories = [];
        $features = [];
        foreach ($category->children()->orderBy('name')->get() as $sc) {
            $item['id'] = $sc->id;
            $item['name'] = $sc->name;
            $subcategories[] = $item;
        }

        if (count($subcategories)) {
            $resp = ['is_ok' => true, 'value' => 'categories', 'subcategories' => $subcategories];
        } else {
            if ($g_feature = $category->groupFeatures()->orderBy('id')->limit(1)->offset($offset)->first()) {
                $features = $g_feature->features;
            }
            if (count($features)) {
                $resp = ['is_ok' => true, 'value' => 'features', 'offset' => ++$offset, 'category_id' => $category->id, 'features' => $features];
            } else {
                $resp = ['is_ok' => false, 'value' => 'complete'];
            }
        }

        return response()->json($resp);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveCategory(Request $request)
    {
        $final_category = null;
        $features = [];
        for ($i = 1; $i < 10; $i++) {
            if ($request->get('category_id_' . $i)) {
                $final_category = $request->get('category_id_' . $i);
            } else {
                if ($request->get('feature_id_' . $i)) {
                    $features[] = $request->get('feature_id_' . $i);
                }
            }
        }
        $category = Category::findOrFail($final_category);

        $user = Auth::user();
        $publication = null;
        if (!$publication = $user->publications()->where('status', 'Draft')->first()) {
            $publication = new Publication();
            $publication->user_id = $user->id;
            $publication->title = '';
            $publication->category_id = $category->id;
            $publication->condition = 'Usado';
            $publication->status = 'Draft';
            $publication->description_txt = '';
            $publication->description_html = '';
            $publication->currency = 'Pesos';
            $publication->quantity = '1';
            $publication->available_quantity = '1';
            $publication->price = '0';
            $publication->visits = '0';
            $publication->personal_pickup = '0';
            $publication->free_shipment = '0';
            $publication->cost_type = 'Gratis';
            $publication->cost = '0';
            $address = $user->addresses()->where('default_for_sales', '1')->first();
            $publication->address_id = $address->id;
            $publication->address_district_id = $address->district_id;
        }
        $publication->category_id = $category->id;
        $publication->category_path = $category->code;
        $publication->save();
        $publication->features()->detach();
        if (count($features)) {
            $publication->features()->attach($features);
        }

        return redirect()->route('sell-description');
    }

    /**
     * @return mixed
     */
    public function description()
    {
        $user = Auth::user();
        if (!$publication = $user->publications()->where('status', 'Draft')->first()) {
            abort(404);
        }
        $departments = Department::all();
        $images_not_ordered = $publication->images()->where(function ($query) {
            $query->where('position', '0')
                ->orWhereNull('position');
        })->orderBy('position')->get();
        $images = $publication->images()->where('position', '<>', '0')->orderBy('position')->get();
        $images = $images->merge($images_not_ordered);
        $vars = [
            'publication' => $publication,
            'images' => $images,
            'departments' => $departments,
            'provinces' => [],
            'districts' => []
        ];

        //Parameters
        $seguridad = [];
        foreach ($publication->parameters()->where('class', 'Seguridad')->get() as $s) {
            $seguridad[] = $s->name;
        }
        $vars['seguridad'] = $seguridad;

        $confort = [];
        foreach ($publication->parameters()->where('class', 'Confort')->get() as $s) {
            $confort[] = $s->name;
        }
        $vars['confort'] = $confort;

        $sonido = [];
        foreach ($publication->parameters()->where('class', 'Sonido')->get() as $s) {
            $sonido[] = $s->name;
        }
        $vars['sonido'] = $sonido;

        $exterior = [];
        foreach ($publication->parameters()->where('class', 'Exterior')->get() as $s) {
            $exterior[] = $s->name;
        }
        $vars['exterior'] = $exterior;

        $ambientes = [];
        foreach ($publication->parameters()->where('class', 'Ambientes')->get() as $s) {
            $ambientes[] = $s->name;
        }
        $vars['ambientes'] = $ambientes;

        $comodidades = [];
        foreach ($publication->parameters()->where('class', 'Comodidades')->get() as $s) {
            $comodidades[] = $s->name;
        }
        $vars['comodidades'] = $comodidades;

        $caracteristicas = [];
        foreach ($publication->parameters()->where('class', 'Caracteristicas Adicionales')->get() as $s) {
            $caracteristicas[] = $s->name;
        }
        $vars['caracteristicas'] = $caracteristicas;

        $zonas = [];
        foreach ($publication->parameters()->where('class', 'Zonas')->get() as $s) {
            $zonas[] = $s->name;
        }
        $vars['zonas'] = $zonas;
        ////

        $district = '';
        $province = '';
        $department = '';
        $vars['departamento'] = null;
        $vars['provincia'] = null;
        $vars['distrito'] = null;
        if (!empty($publication->address_district_id)) {
            $district = District::find($publication->address_district_id);
            $province = $district->province;
            $department = $province->department;
            $vars['distrito'] = $district->id;
            $vars['provincia'] = $province->id;
            $vars['departamento'] = $department->id;
        }
        if (!empty(old('departamento', $department))) {
            $d = (old('departamento') ?: ($department ? $department->id : ''));
            if ($d) {
                $provinces = Department::find($d)->provinces;
            }
            $vars['provinces'] = $provinces;

            if (!empty(old('provincia', $province))) {
                $d = (old('provincia') ?: ($province ? $province->id : ''));
                if ($d) {
                    $districts = Province::find($d)->districts;
                }
                $vars['districts'] = $districts;
            }
        }
        return response()->view('sell.description', $vars);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveDescription(Request $request)
    {
        $messages = [
            'title.required' => 'Introduce un título para tu publicación',
            'title.max' => 'El título de tu publicación debe contener :max caracteres como máximo.',
            'youtube_url.url' => 'La url del video debe ser de tipo URL',
        ];
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:60',
            'condition' => 'required',
            'youtube_url' => 'url',
        ], $messages);

        if ($validator->fails()) {
            return redirect('/sell/description')
                ->withErrors($validator)
                ->withInput();
        }

        $user = Auth::user();
        $publication = null;
        if (!$publication = $user->publications()->where('status', 'Draft')->first()) {
            abort(404);
        }

        $publication->parameters()->delete();
        //If it is a vehicle
        if ($request->get('tipo_producto') === 'vehículo' || $request->get('tipo_producto') === 'vehículo de colección' || $request->get('tipo_producto') === 'moto' || $request->get('tipo_producto') === 'lancha' || $request->get('tipo_producto') === 'vehiculo') {

            if ($request->get('tipo_producto') === 'vehículo') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'año' => 'required',
                    'puertas' => 'required',
                    'kilometros' => 'required|integer|min:1|max:9999999',
                    'version' => 'required|max:30',
                ], $messages);
            }

            if ($request->get('tipo_producto') === 'vehículo de colección') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'año' => 'required',
                    'puertas' => 'required',
                    'kilometros' => 'required|integer|min:1|max:9999999',
                    'marca' => 'required',
                    'modelo' => 'required|max:30',
                ], $messages);
            }

            if ($request->get('tipo_producto') === 'moto') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'año' => 'required',
                    'kilometros' => 'required|integer|min:1|max:9999999',
                    'modelo' => 'required|max:30',
                    'version' => 'required|max:30',
                ], $messages);
            }

            if ($request->get('tipo_producto') === 'lancha') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'año' => 'required',
                    'marca' => 'required|max:30',
                    'tipo' => 'required',
                ], $messages);
            }
            if ($request->get('tipo_producto') === 'vehiculo') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'año' => 'required',
                    'marca' => 'required|max:30',
                    'modelo' => 'required|max:30',
                    'kilometros' => 'required|integer|min:1|max:9999999',
                ], $messages);
            }

            if ($validator->fails()) {
                return redirect('/sell/description')
                    ->withErrors($validator)
                    ->withInput();
            }

            $publication->address_district_id = $request->get('distrito');
            $publication->vehicle_year = $request->get('año');
            $publication->vehicle_doors = $request->get('puertas');
            $publication->vehicle_kilometers = $request->get('kilometros');
            if ($request->get('tipo_producto') === 'vehículo') {
                $publication->vehicle_brand = $publication->category->parentCategory->name;
                $publication->vehicle_model = $publication->category->name;
            }
            if ($request->get('tipo_producto') === 'vehículo de colección') {
                $publication->vehicle_model = $request->get('modelo');
                $publication->vehicle_brand = $request->get('marca');
            }
            if ($request->get('tipo_producto') === 'moto') {
                $publication->vehicle_type = $publication->category->parentCategory->name;
                $publication->vehicle_brand = $publication->category->name;
                $publication->vehicle_model = $request->get('modelo');
            }
            if ($request->get('tipo_producto') === 'lancha') {
                $publication->vehicle_type = $request->get('tipo');
                $publication->vehicle_brand = $request->get('marca');
            }
            if ($request->get('tipo_producto') === 'vehiculo') {
                $publication->vehicle_model = $request->get('modelo');
                $publication->vehicle_brand = $request->get('marca');
            }

            $publication->vehicle_version = $request->get('version');
            $publication->vehicle_color = $request->get('color');
            $publication->vehicle_fuel = $request->get('combustible');
            $publication->vehicle_steering = $request->get('direccion');
            $publication->vehicle_transmission = $request->get('transmision');
            $publication->vehicle_owner = $request->get('dueño');
            $publication->vehicle_schedule = $request->get('horario');
            $publication->contact_phone = (empty($request->get('telefono')) ? $user->phone_number : $request->get('telefono'));


            if ($request->get('tipo_producto') === 'moto') {
                if (!empty($request->get('alarma'))) {
                    Parameter::saveParameters($publication->id, 'Alarma', $request->get('alarma'), '');
                }

                if (!empty($request->get('frenos'))) {
                    Parameter::saveParameters($publication->id, 'Frenos', $request->get('frenos'), '');
                }

                if (!empty($request->get('arranque'))) {
                    Parameter::saveParameters($publication->id, 'Sistema de Arranque', $request->get('arranque'), '');
                }

                if (!empty($request->get('motor'))) {
                    Parameter::saveParameters($publication->id, 'Tipo de motor', $request->get('motor'), '');
                }
            }

            if ($request->get('Seguridad')) {
                foreach ($request->get('Seguridad') as $par) {
                    Parameter::saveParameters($publication->id, $par, '-1', 'Seguridad');
                }
            }
            if ($request->get('Confort')) {
                foreach ($request->get('Confort') as $par) {
                    Parameter::saveParameters($publication->id, $par, '-1', 'Confort');
                }
            }
            if ($request->get('Sonido')) {
                foreach ($request->get('Sonido') as $par) {
                    Parameter::saveParameters($publication->id, $par, '-1', 'Sonido');
                }
            }
            if ($request->get('Exterior')) {
                foreach ($request->get('Exterior') as $par) {
                    Parameter::saveParameters($publication->id, $par, '-1', 'Exterior');
                }
            }
        }

        //If its a service
        if ($request->get('tipo_producto') === 'servicio') {
            $validator = Validator::make($request->all(), [
                'departamento' => 'required',
                'provincia' => 'required',
                'distrito' => 'required',
                'calle' => 'required|max:50',
                'numero' => 'required|max:50',
            ], $messages);


            if ($validator->fails()) {
                return redirect('/sell/description')
                    ->withErrors($validator)
                    ->withInput();
            }

            $publication->address_street = $request->get('calle');
            $publication->address_number = $request->get('numero');
            $publication->address_district_id = $request->get('distrito');
            $publication->contact_phone = (empty($request->get('telefono')) ? $user->phone_number : $request->get('telefono'));


            if ($request->get('Zonas')) {
                foreach ($request->get('Zonas') as $par) {
                    Parameter::saveParameters($publication->id, $par, '-1', 'Zonas');
                }
            }
        }
        /////

        // If its a property
        if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina' || $request->get('tipo_producto') === 'terreno' || $request->get('tipo_producto') === 'inmueble') {

            if ($request->get('tipo_producto') === 'casa') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'nambientes' => 'required',
                    'años' => 'required|integer|min:0|max:100',
                    'construido' => 'required|integer|min:1|max:9999999',
                    'terreno' => 'required|integer|min:1|max:9999999',
                    'baños' => 'required',
                    'dormitorios' => 'required',
                    'calle' => 'required|max:50',
                    'numero' => 'required|max:50',
                ], $messages);
            }

            if ($request->get('tipo_producto') === 'departamento') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'nambientes' => 'required',
                    'años' => 'required|integer|min:0|max:100',
                    'construido' => 'required|integer|min:1|max:9999999',
                    'terreno' => 'required|integer|min:1|max:9999999',
                    'baños' => 'required',
                    'dormitorios' => 'required',
                    'calle' => 'required|max:50',
                    'numero' => 'required|max:50',
                ], $messages);
            }

            if ($request->get('tipo_producto') === 'local comercial') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'nambientes' => 'required',
                    'años' => 'required|integer|min:0|max:100',
                    'construido' => 'required|integer|min:1|max:9999999',
                    'terreno' => 'required|integer|min:1|max:9999999',
                    'baños' => 'required',
                    'calle' => 'required|max:50',
                    'numero' => 'required|max:50',
                ], $messages);
            }

            if ($request->get('tipo_producto') === 'oficina') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'nambientes' => 'required',
                    'años' => 'required|integer|min:0|max:100',
                    'construido' => 'required|integer|min:1|max:9999999',
                    'terreno' => 'required|integer|min:1|max:9999999',
                    'baños' => 'required',
                    'calle' => 'required|max:50',
                    'numero' => 'required|max:50',
                ], $messages);
            }

            if ($request->get('tipo_producto') === 'terreno') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'terreno' => 'required|integer|min:1|max:9999999',
                    'calle' => 'required|max:50',
                    'numero' => 'required|max:50',
                ], $messages);
            }

            if ($request->get('tipo_producto') === 'inmueble') {
                $validator = Validator::make($request->all(), [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'años' => 'required|integer|min:0|max:100',
                    'construido' => 'required|integer|min:1|max:9999999',
                    'terreno' => 'required|integer|min:1|max:9999999',
                    'tipoinmueble' => 'required|max:50',
                    'calle' => 'required|max:50',
                    'numero' => 'required|max:50',
                ], $messages);
            }

            if ($validator->fails()) {
                return redirect('/sell/description')
                    ->withErrors($validator)
                    ->withInput();
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina' || $request->get('tipo_producto') === 'terreno' || $request->get('tipo_producto') === 'inmueble') {
                $publication->address_street = $request->get('calle');
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina' || $request->get('tipo_producto') === 'terreno' || $request->get('tipo_producto') === 'inmueble') {
                $publication->address_number = $request->get('numero');
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina') {
                $publication->property_environments = $request->get('nambientes');
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina' || $request->get('tipo_producto') === 'inmueble') {
                $publication->property_age = $request->get('años');
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina' || $request->get('tipo_producto') === 'terreno' || $request->get('tipo_producto') === 'inmueble') {
                $publication->property_construction_area = $request->get('construido');
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina' || $request->get('tipo_producto') === 'terreno' || $request->get('tipo_producto') === 'inmueble') {
                $publication->property_land_area = $request->get('terreno');
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina') {
                $publication->property_bathrooms = $request->get('baños');
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento') {
                $publication->property_bedrooms = $request->get('dormitorios');
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina' || $request->get('tipo_producto') === 'terreno') {
                $publication->vehicle_schedule = $request->get('horario');
            }

            if ($request->get('tipo_producto') === 'inmueble') {
                $publication->vehicle_type = $request->get('tipoinmueble');
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina') {
                if (!empty($request->get('pisos'))) {
                    Parameter::saveParameters($publication->id, 'Cantidad de pisos', $request->get('pisos'), '');
                }
            }

            if ($request->get('tipo_producto') === 'departamento') {
                if (!empty($request->get('pisos'))) {
                    Parameter::saveParameters($publication->id, 'Cant.de pisos del edificio', $request->get('pisos'), '');
                }
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina' || $request->get('tipo_producto') === 'inmueble') {
                if (!empty($request->get('estado'))) {
                    Parameter::saveParameters($publication->id, 'Condición', $request->get('estado'), '');
                }
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina') {
                if (!empty($request->get('garaje'))) {
                    Parameter::saveParameters($publication->id, 'Garaje', $request->get('garaje'), '');
                }
            }

            if ($request->get('tipo_producto') === 'departamento') {
                if (!empty($request->get('amoblado'))) {
                    Parameter::saveParameters($publication->id, 'Amoblado', $request->get('amoblado'), '');
                }
            }

            if ($request->get('tipo_producto') === 'departamento' || $request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina') {
                if (!empty($request->get('ascensores'))) {
                    Parameter::saveParameters($publication->id, 'Ascensores', $request->get('ascensores'), '');
                }
            }

            if ($request->get('tipo_producto') === 'terreno') {
                if (!empty($request->get('acceso'))) {
                    Parameter::saveParameters($publication->id, 'Acceso', $request->get('acceso'), '');
                }
            }

            if ($request->get('tipo_producto') === 'terreno') {
                if (!empty($request->get('uso'))) {
                    Parameter::saveParameters($publication->id, 'Uso', $request->get('uso'), '');
                }
            }

            if ($request->get('tipo_producto') === 'casa' || $request->get('tipo_producto') === 'departamento') {
                if ($request->get('Ambientes')) {
                    foreach ($request->get('Ambientes') as $par) {
                        Parameter::saveParameters($publication->id, $par, '-1', 'Ambientes');
                    }
                }
                if ($request->get('Comodidades')) {
                    foreach ($request->get('Comodidades') as $par) {
                        Parameter::saveParameters($publication->id, $par, '-1', 'Comodidades');
                    }
                }
            }

            if ($request->get('tipo_producto') === 'local comercial' || $request->get('tipo_producto') === 'oficina' || $request->get('tipo_producto') === 'terreno') {
                if ($request->get('Caracteristicas')) {
                    foreach ($request->get('Caracteristicas') as $par) {
                        Parameter::saveParameters($publication->id, $par, '-1', 'Caracteristicas Adicionales');
                    }
                }
            }

            $publication->address_district_id = $request->get('distrito');
            $publication->contact_phone = (empty($request->get('telefono')) ? $user->phone_number : $request->get('telefono'));
        }

        $publication->title = $request->get('title');
        $publication->youtube_url = $request->get('youtube_url');
        $publication->condition = $request->get('condition');
        $publication->description_html = $request->get('description_html');
        $publication->save();

        if (!$publication_index = PublicationIndex::where('publication_id', $publication->id)->first()) {
            $publication_index = new PublicationIndex();
        }
        $publication_index->publication_id = $publication->id;
        $publication_index->category_branch_string = $publication->getCategoryBranchString();
        $publication_index->title = $publication->title;
        $publication_index->description = $publication->description_txt . ' ' . strip_tags($publication->description_html);
        $publication_index->save();

        Image::updateImagePositionFromPubId($publication->id, 0);

        $pictures_info = json_decode($request->get('pictures_info'), TRUE);
        foreach ($pictures_info as $pi) {
            Image::updateImagePosition($publication->id, $pi['imageid'], ++$pi['position']);
        }

        //delete garbage images
        $images = $publication->images()->where('position', 0)->get();
        foreach ($images as $img) {
            File::delete(public_path('uploads/images/products/') . $img->name);
            $img->delete();
        }

        return redirect()->route('sell-price');
    }

    public function saveImage(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }

        $user = Auth::user();
        $publication = null;
        $id = $request->get('id');
        if (!$publication = $user->publications()
            ->where('id', $id)
            ->where(function ($query) {
                $query->where('status', 'Active')->orWhere('status', 'Paused');
            })
            ->first()
        ) {
            if (!$publication = $user->publications()->where('status', 'Draft')->first()) {
                return response()->json(['is_ok' => false]);
            }
        }

        $this->validate($request, [
            'filedata' => 'required|image|mimes:jpeg,png,jpg,gif|max:4096',
        ]);

        $uid = $request->get('uid');
        $imageName = md5(time() . $uid) . '.' . $request->filedata->getClientOriginalExtension();
        $request->filedata->move(public_path('uploads/images/products'), $imageName);

        $image = Image::addImage($publication->id, $imageName, $uid);

        return response()->json(['is_ok' => true, 'id' => $image->id]);

    }

    public function removeImage(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }

        $user = Auth::user();
        $publication = null;
        $id = $request->get('id');
        if (!$publication = $user->publications()
            ->where('id', $id)
            ->where(function ($query) {
                $query->where('status', 'Active')->orWhere('status', 'Paused');
            })
            ->first()
        ) {
            if (!$publication = $user->publications()->where('status', 'Draft')->first()) {
                return response()->json(['is_ok' => false]);
            }
        }
        $uid = $request->get('uid');
        if ($image = $publication->images()->where('fileapi_id', $uid)->first()) {
            File::delete(public_path('uploads/images/products/') . $image->name);
            $image->delete();
        }
        return response()->json(['is_ok' => true]);
    }

    /**
     * @return mixed
     */
    public function price()
    {
        $user = Auth::user();
        if (!$publication = $user->publications()->where('status', 'Draft')->first()) {
            abort(404);
        }

        return response()->view('sell.precio', [
            'publication' => $publication,
            'currencies' => Currency::all()
        ]);
    }

    public function savePrice(Request $request)
    {
        $user = Auth::user();
        $publication = null;

        if (!$publication = $user->publications()->where('status', 'Draft')->first()) {
            abort(404);
        }

        $tipo_producto = $publication->getProductType();

        $messages = [
            'precio.min' => 'El precio de tu publicación debe ser mayor a 0.',
            'precio.max' => 'El precio máximo de publicación es de S/. 9.999.999,99',
            'costo_envio.min' => 'El costo de envio de tu publicación debe ser mayor a 0.',
            'costo_envio.max' => 'El costo de envio máximo de publicación es de S/. 9.999.999,99',
            'cantidad.min' => 'La cantidad mínima de publicación es de :min producto',
            'cantidad.max' => 'La cantidad máxima de publicación es de :max productos',
            'currency.required' => 'La Moneda es obligatoria'
        ];

        $data = $request->all();
        $data['precio'] = str_replace('.', '', $data['precio']);
        $data['precio'] = str_replace(',', '.', $data['precio']);
        $data['costo_envio'] = str_replace('.', '', $data['costo_envio']);
        $data['costo_envio'] = str_replace(',', '.', $data['costo_envio']);
        $validations = [
            'cantidad' => 'required|integer|min:1|max:9999',
            'precio' => 'required|numeric|min:0.1|max:9999999',
            'costo_envio' => 'required|min:0.1|max:9999999',
            'currency' => 'required',
        ];

        if ($tipo_producto === 'servicio' && $request->get('convenir') === '1') {
            unset($validations['precio']);
            $data['precio'] = 0;
        }

        $validator = Validator::make($data, $validations, $messages);

        if ($validator->fails()) {
            return redirect('/sell/price')
                ->withErrors($validator)
                ->withInput();
        }

        $publication->quantity = $data['cantidad'];
        $publication->available_quantity = $data['cantidad'];
        $publication->price = $data['precio'];
        $publication->currency_id = $request->currency;
        $publication->save();

        ShippingCost::addShippingCost($publication->id, $data['costo_envio'], 'sarasa');

        return redirect()->route('sell-confirm');

    }

    /**
     * @return mixed
     */
    public function confirm()
    {
        $user = Auth::user();
        if (!$publication = $user->publications()->where('status', 'Draft')->first()) {
            abort(404);
        }
        //$conf = DB::table('configurations')->limit(1)->first();
        return response()->view('sell.confirm', [
            'publication' => $publication,
            //'conf' => $conf
            'conf' => Configuration::limit(1)->first()
        ]);
    }

    public function saveConfirm(Request $request)
    {
        $user = Auth::user();
        $publication = null;
        if (!$publication = $user->publications()->where('status', 'Draft')->first()) {
            abort(404);
        }

        if ($user->suspended) {
            Session::flash('message', 'Has sido suspendido para vender, revisa si tienes facturas pendientes por pagar, si no es así por favor contacta a soporte.');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('billing');
        }

        $conf = Configuration::limit(1)->first();
        $pub_type = 'Gratis';
        if ($publication->getProductType() === 'producto') {
            $price = 0;
            $price = PlanHelper::getPlan($request, $conf);
            $pub_type = $request->plan;
            $publication->cost_type = $request->get('plan');
        } else {
            $price = $conf->advertisement_cost;
            $publication->cost_type = 'Fijo';
            $pub_type = 'Clasificado';
        }

        $publication->status = 'Active';
        $publication->publicated_as_active_at = date("Y-m-d H:i:s");
        $publication->ends_at = date('Y-m-d H:i:s', strtotime("+" . config('app.global.publication_lifetime_days') . " days"));
        $publication->cost = $price;
        $publication->save();

        //if user has not a cutoff day
        if (!$user->cutoff_day) {
            $user->cutoff_day = (date("d") <= 28 ? date("d") : '28');
            $user->save();
        }

        //Creating the item in ticket
        if ($pub_type !== 'Gratis') {
            if (!$ticket = $user->tickets()->where('status', 'Open')->first()) {
                $ticket = new Ticket();
                $ticket->user_id = $user->id;
                $ticket->status = 'Open';
                $limit_emitted = strtotime("-" . config('app.global.days_before_cutoff_notification') . " days", strtotime(date('Y-m-' . $user->cutoff_day . ' 23:30')));
                /*if (time() > $limit_emitted) {
                    $emit_on = date('Y-m-d', strtotime("+1 month", strtotime(date('Y-m-' . $user->cutoff_day))) - config('app.global.days_before_cutoff_notification') * 86400);
                } else {
                    $emit_on = date('Y-m-d', $limit_emitted);
                }
                $ticket->emit_on = $emit_on;*/
                $ticket->emit_on = (time() > $limit_emitted)
                    ? date('Y-m-d', strtotime("+1 month", strtotime(date('Y-m-' . $user->cutoff_day))) - config('app.global.days_before_cutoff_notification') * 86400) : date('Y-m-d', $limit_emitted);
                $ticket->save();
            }

            $item = new TicketItem();
            $item->ticket_id = $ticket->id;
            $item->publication_id = $publication->id;
            $item->amount = $price;
            $item->description = ($pub_type !== 'Clasificado')
                ? "Exposición de publicación nivel $pub_type." : "Publicación de clasificado.";
            /*if ($pub_type !== 'Clasificado') {
                $item->description = "Exposición de publicación nivel $pub_type.";
            } else {
                $item->description = "Publicación de clasificado.";
            }*/
            $item->save();
        }

        return redirect()->route('product', [$publication->id]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getListOfImages(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }

        $user = Auth::user();
        $publication = null;

        if ($request->get('id')) {
            if (!$publication = $user->publications()->where('id', $request->get('id'))->first()) {
                abort(404);
            }
        } else {
            if (!$publication = $user->publications()->where('status', 'Draft')->first()) {
                abort(404);
            }
        }

        $images = $publication->images()->orderBy('position')->get();

        $output = [];
        $count = 0;
        foreach ($images as $img) {
            $count++;
            $item = [];
            $item['title'] = "Imagen $count";
            $item['value'] = url('uploads/images/products') . "/" . $img->name;
            $output[] = $item;
        }

        return response()->json($output);
    }
}

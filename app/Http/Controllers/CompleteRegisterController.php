<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompleteRegisterRequest;
use App\User;
use Illuminate\Http\Request;
use App\Department;
use App\District;
use App\Province;
use App\Address;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;
use Illuminate\Support\Facades\Auth;

/**
 * Class CompleteRegisterController
 * @package App\Http\Controllers
 */
class CompleteRegisterController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param CompleteRegisterRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function completeregister(CompleteRegisterRequest $request)
    {
        DB::beginTransaction();
        try {
            $user = User::updateDocumentToUserLoggedIn($request);
            Address::createAddressforUser($request, $user);
            DB::commit();
            return redirect('/home');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index(Request $request)
    {
        if (!is_null(Auth::user()->document)) {
            return redirect('/home');
        }

        $vars = ['departments' => Department::all(),
            'provinces' => [], 'districts' => []];

        if (!empty(old('department'))) {
            $vars['provinces'] = Department::find(old('department'))->provinces;

            if (!empty(old('province'))) {
                $vars['districts'] = Province::find(old('province'))->districts;
            }
        }

        return response()->view('user.complete_register', $vars);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchprovinces(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }

        return response()->json(Province::where('department_id', $request->id)->get());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchdistricts(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }

        return response()->json(District::where('province_id', $request->id)->get());
    }
}

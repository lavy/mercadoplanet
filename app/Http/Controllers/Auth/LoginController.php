<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Redirect;
use Session;
use Illuminate\Http\Request;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        if(URL::previous() !== url('register') and
           URL::previous() !== url('login')) {
            // echo   URL::previous().Session::get('url.intended');die();
             Session::put('url.intended',URL::previous());
            //echo URL::previous(); die();
        }

        return view('auth.login');
    }

     protected function authenticated(Request $request, $user)
     {
         //Redirect::to($request->session()->get('url.intended'));
     }


    protected function credentials($request)
    {
        $field = filter_var($request->input($this->username()), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $request->merge([$field => $request->input($this->username())]);
        $request->merge(['status' => 'Active']);
        return $request->only($field, 'password', 'status');
    }
}

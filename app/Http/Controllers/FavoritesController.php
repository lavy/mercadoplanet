<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Publication;

/**
 * Class FavoritesController
 * @package App\Http\Controllers
 */
class FavoritesController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function mark_or_unmark(Request $request)
    {
        if (!$request->ajax()) {
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }

        $publication = Publication::find($request->id);

        if (!Auth::check() || !$publication) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }

        $user = Auth::user();
        if ($publication->user_id == $user->id) {
            return response()->json(['is_ok' => false, 'value' => false]);
        }

        if ($user->favorites->contains($publication)) {
            $user->favorites()->detach($publication);
            $resp = ['is_ok' => true, 'value' => false];
        } else {
            $user->favorites()->attach($publication);
            $resp = ['is_ok' => true, 'value' => true];
        }

        return response()->json($resp);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function mark_or_unmark_favorite(Request $request)
    {
        $publication = Publication::find($request->id);

        if (!Auth::check() || !$publication) {
            return redirect()->route('home');
        }

        $user = Auth::user();
        if ($publication->user_id == $user->id) {
            return redirect()->route('home');
        }

        if ($user->favorites->contains($publication)) {
            $user->favorites()->detach($publication);
            return redirect()->route('home');
        } else {
            $user->favorites()->attach($publication);
            return redirect()->route('home');
        }
    }
}

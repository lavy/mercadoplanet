<?php
/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 13/04/20
 * Time: 00:52
 */

namespace App\Http\Controllers;


class AddressController extends Controller
{
	/**
	 * @param Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function new_address(Request $request)
	{
		$vars = ['departments' => Department::all(),
			'provinces' => [], 'districts' => []];

		if (!empty(old('department'))) {
			$vars['provinces'] = Department::find(old('department'))->provinces;

			if (!empty(old('province'))) {
				$vars['districts'] = Province::find(old('province'))->districts;
			}
		}

		return response()->view('account.add_address', $vars);
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function add_address(AddressRequest $request)
	{

		/*$validator = Validator::make($request->all(), [
			'address_type' => 'required',
			'address_text' => 'required|max:60',
			'number' => 'required|numeric',
			'other_data' => 'max:80',
			'reference' => 'max:80',
			'department' => 'required',
			'province' => 'required',
			'district' => 'required',
		]);

		if ($validator->fails()) {
			return redirect('/newaddress')
				->withErrors($validator)
				->withInput();
		}*/

		$user = Auth::user();

		$address = new Address([
			'address_type' => $request->get('address_type'),
			'address_text' => $request->get('address_text'),
			'number' => $request->get('number'),
			'other_data' => $request->get('other_data'),
			'reference' => $request->get('reference'),
			'district_id' => $request->get('district'),
			'user_id' => $user->id,
		]);

		if ($request->get('default_for_sales') != null) {
			$dft_address = $user->addresses()->where('default_for_sales', true)->first();
			$dft_address->default_for_sales = false;
			$dft_address->send_with_email = false;
			$dft_address->save();
			$address->default_for_sales = true;
		}

		$address->save();

		return redirect('/profile')->with(['result' => true, 'message' => 'Domicilio agregado con éxito']);
	}

	/**
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
	 */
	public function edit_address($id)
	{
		$address = Auth::user()->addresses()->find($id);

		if ($address == null) {
			return redirect('/profile');
		}

		$departments = Department::all();

		$selected_district = empty(old('district')) ? $address->district : District::find(old('district'));
		$selected_province = empty(old('province')) ? $selected_district->province : Province::find(old('province'));
		$selected_department = empty(old('department')) ? $selected_province->department : Department::find(old('department'));

		$provinces = $selected_department->provinces;
		$districts = $selected_province->districts;

		if (!empty(old('address_type'))) {
			$address->address_type = old('address_type');
		}

		if (!empty(old('address_text'))) {
			$address->address_text = old('address_text');
		}

		if (!empty(old('number'))) {
			$address->number = old('number');
		}

		if (!empty(old('other_data'))) {
			$address->other_data = old('other_data');
		}

		if (!empty(old('reference'))) {
			$address->reference = old('reference');
		}

		return response()->view('account.mod_address', ['address' => $address,
				'departments' => $departments,
				'provinces' => $provinces,
				'districts' => $districts,
				'sel_department' => $selected_department,
				'sel_province' => $selected_province,
				'sel_district' => $selected_district
			]
		);
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function mod_address(Request $request)
	{
		$address = Auth::user()->addresses()->find($request->id);

		if ($address == null) {
			return redirect('/profile');
		}

		$validator = Validator::make($request->all(), [
			'address_type' => 'required',
			'address_text' => 'required|max:60',
			'number' => 'required|numeric',
			'other_data' => 'max:80',
			'reference' => 'max:80',
			'department' => 'required',
			'province' => 'required',
			'district' => 'required',
		]);

		if ($validator->fails()) {
			return redirect('/edit_address/' . $id)
				->withErrors($validator)
				->withInput();
		}

		$address->address_type = $request->get('address_type');
		$address->address_text = $request->get('address_text');
		$address->number = $request->get('number');
		$address->other_data = $request->get('other_data');
		$address->reference = $request->get('reference');
		$address->district_id = $request->get('district');
		$address->save();

		return redirect('/profile')->with(['result' => true, 'message', 'Domicilio modificado con éxito']);
	}

	/**
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function use_address_for_sales($id)
	{
		$address = Auth::user()->addresses()->find($id);

		if ($address == null) {
			return redirect('/profile');
		}

		if ($address->default_for_sales) {
			return redirect('/profile');
		}

		$dft_address = Auth::user()->addresses()->where('default_for_sales', true)->first();
		$dft_address->default_for_sales = false;
		$dft_address->send_with_email = false;
		$dft_address->save();

		$address->default_for_sales = true;
		$address->save();

		return redirect('/profile');
	}

	/**
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function use_address_for_purchases($id)
	{
		$address = Auth::user()->addresses()->find($id);

		if ($address == null) {
			return redirect('/profile');
		}

		if ($address->default_for_purchases) {
			return redirect('/profile');
		}

		$dft_address = Auth::user()->addresses()->where('default_for_purchases', true)->first();

		if ($dft_address != null) {
			$dft_address->default_for_purchases = false;
			$dft_address->save();
		}
		$address->default_for_purchases = true;
		$address->save();

		return redirect('/profile');
	}

	/**
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function remove_address($id)
	{
		$address = Auth::user()->addresses()->find($id);

		if (is_null($address)) {
			return redirect('/profile');
		}

		if ($address->default_for_sales) {
			Session::flash('result', false);
			Session::flash('message', 'No se puede eliminar el domicilio que se usa para ventas');
		} else {
			Session::flash('result', true);
			Session::flash('message', 'Se eliminó el domicilio');
			$address->delete();
		}

		return redirect('/profile');
	}
}
<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class DescriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        switch ($request->tipo_producto){
            case 'vehiculo':
                return [
                    'departamento' => 'required',
                    'provincia' => 'required',
                    'distrito' => 'required',
                    'año' => 'required',
                    'puertas' => 'required',
                    'kilometros' => 'required|integer|min:1|max:9999999',
                    'version' => 'required|max:30'
                ];
            case 'vehiculo_de_coleccion':
        }
        return [
            //
        ];
    }
}

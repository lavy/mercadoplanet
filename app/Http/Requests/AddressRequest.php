<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address_type' => 'required',
            'address_text' => 'required|max:60',
            'number' => 'required|numeric',
            'other_data' => 'max:80',
            'reference' => 'max:80',
            'department' => 'required',
            'province' => 'required',
            'district' => 'required',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PriceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cantidad' => 'required|integer|min:1|max:9999',
            'precio' => 'required|numeric|min:0.1|max:9999999',
            'costo_envio' => 'required|min:0.1|max:9999999',
            'currency' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'cantidad' => 'required|integer|min:1|max:9999',
            'precio' => 'required|numeric|min:0.1|max:9999999',
            'costo_envio' => 'required|min:0.1|max:9999999',
            'currency' => 'required',
        ];
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: martingomes
 * Date: 13/04/20
 * Time: 01:53
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIfUserNotSuspended
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @param  string|null $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = null)
	{
		if (Auth::user()->suspended) {
			return redirect()->back()->with([
				'message' => 'Has sido suspendido para vender, revisa si tienes facturas pendientes por pagar, si no es así por favor contacta a soporte.',
				'alert-class' => 'alert-danger']);
		}

		return $next($request);
	}
}
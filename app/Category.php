<?php

namespace App;

use App\Models\Category\Relationships\CategoryRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Category
 * @package App
 */
class Category extends Model
{
    use CategoryRelationship;

    /**
     * @param $category_id
     * @return mixed
     */
	public static function getTopLevelCategory($category_id) {
		if($category = Category::find($category_id)) {
			while(1) {
				$parent = $category->parentCategory;
				if($parent->id == '1' or $parent->id == '2' or $parent->id == '3' or $parent->id == '4') {
					return $category;
				}
				$category = $parent;
			}
		}
	}

    /**
     * @return mixed
     */
	public function getHomeTopLevelCategory() {
        $category = $this;
        while($parent = $category->parentCategory) {
            if($parent->id == '4') {
                return $category;
            }
            if($parent->id == '1' or $parent->id == '2' or $parent->id == '3') {
                return $parent;
            }
            $category = $parent;
        }
        return $category;
	}

    /**
     * @return bool
     */
	public function isHomeTopLevelCategory() {
        $category = $this;
        while($parent = $category->parentCategory) {
            if($parent->id == '4') {
                return $category->id === $this->id;
            }
            if($parent->id == '1' or $parent->id == '2' or $parent->id == '3') {
                return $parent->id === $this->id;
            }
            $category = $parent;
        }
        return $category->id === $this->id;
	}

    /**
     * @param $pubs_ids
     * @param $code_path
     * @return mixed
     */
	public static function getFilteredChildren($pubs_ids, $code_path) {

            $categories = Category::query()
                ->select(DB::raw('categories.id, categories.name, count(publications.id) as num'))
                ->join('publications', 'publications.category_path', 'like', DB::raw('CONCAT(`categories`.`code`, \'%\')'))
                ->whereIn('publications.id', $pubs_ids);
            if(!empty($code_path)) {
                $categories->where('categories.code', 'like', $code_path.'__');
            } else {

                $categories->where(function($query) {
                    $query->orWhere('categories.code', 'like', '01__')
                            ->orWhere('categories.code', 'like', '02__')
                            ->orWhere('categories.code', 'like', '03__')
                            ->orWhere('categories.code', 'like', '04__');
                });
            }
            $categories->groupBy('categories.id', 'categories.name')
            ->orderBy('num', 'desc');


		return $categories->get();
	}

    /**
     * @param $publications
     * @return null|string
     */
    public static function getBreadCrumbCodePath($publications) {
		if(count($publications) < 1) return null;

        $pub_base = $publications[0];
        $pub_base_code = $pub_base->category->code;
        $piv = strlen($pub_base_code);

        while($piv > 0) {
            $success = true;
            foreach($publications as $pub) {
                $pub_code = $pub->category->code;
                if(substr($pub_base_code, 0, $piv) != substr($pub_code, 0, $piv)) {
                    $piv -= 2;
                    $success = false;
                    break;
                }
            }
            if($success) {
                return substr($pub_base_code, 0, $piv);
            }
        }
        return null;

    }

    /**
     * @param $publications
     * @return array
     */
    public static function getBreadCrumbArray($publications) {
        $cp = Category::getBreadCrumbCodePath($publications);
        $bc_array = [];

        for($i = 2; $i <= strlen($cp); $i += 2) {
            $category = Category::where('code', substr($cp, 0, $i))->first();
            $bc_array[] = $category;
        }

        return $bc_array;

    }

    /**
     * @param $cp
     * @return array
     */
    public static function getBreadCrumbArrayFromCodePath($cp) {
        $bc_array = [];
        
        for($i = 2; $i <= strlen($cp); $i += 2) {
            $category = Category::where('code', substr($cp, 0, $i))->first();
            if($category->id != 4) {
                $bc_array[] = $category;
            }
        }

        return $bc_array;

    }

    /**
     * @param $cp
     * @return array
     */
    public static function getPathFromCodePath($cp) {
        $bc_array = [];
        
        for($i = 2; $i <= strlen($cp); $i += 2) {
            $category = Category::where('code', substr($cp, 0, $i))->first();
			$bc_array[] = $category;
        }

        return $bc_array;

    }

    /**
     * @param $pubs_ids
     * @param $gf_id
     * @return null
     */
    public static function getCountFeatures($pubs_ids , $gf_id) {
		if(count($pubs_ids) < 1) return null;


		$features = Category::query()
            ->select(DB::raw('features.id, features.name, count(features.id) as num'))
            ->join('group_features', 'group_features.category_id', '=', 'categories.id')
            ->join('features', 'features.group_feature_id', '=', 'group_features.id')
            ->join('feature_publication', 'feature_publication.feature_id', '=', 'features.id')
            ->join('publications', 'feature_publication.publication_id', '=', 'publications.id')
            ->where('group_features.id', $gf_id)
            ->whereIn('publications.id', $pubs_ids)
            ->groupBy('features.id', 'features.name')
            ->orderBy('num', 'desc');


		return $features->get();
    }

    /**
     * @param $category
     * @return mixed
     */
    public static function getCountPubs($category) {
        $pubs = Publication::query()
        ->where('status', 'Active')
        ->where('category_path', 'like', $category->code.'%')
        ->count();
        
        return $pubs;
    }

    /**
     * @param Category $category
     * @return mixed
     */
    public static function getCategoriesByParentId(self $category)
    {
        return self::where('parent_id', $category->id)
            ->orderBy(DB::raw('name="Otros", name'))->get();
    }


    /**
     * @param $feature
     * @return mixed
     */
    public static function getCategoriesByFeature($feature)
    {
        return self::query()
            ->select(DB::raw('feature_publication.publication_id'))
            ->join('group_features', 'group_features.category_id', '=', 'categories.id')
            ->join('features', 'features.group_feature_id', '=', 'group_features.id')
            ->join('feature_publication', 'feature_publication.feature_id', '=', 'features.id')
            ->where('features.id', $feature);
    }
}

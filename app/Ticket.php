<?php

namespace App;

use App\Models\Ticket\Relationships\TicketRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Ticket
 * @package App
 */
class Ticket extends Model
{
    use TicketRelationship;

    /**
     * @return mixed
     */
    public function getTotal()
    {
        $total = $this->ticketItems()->sum('ticket_items.amount');
        return $total;
    }
}

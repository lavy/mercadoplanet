<?php

namespace App;

use App\Models\BuyerNote\Relationships\BuyerNoteRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BuyerNote
 * @package App
 */
class BuyerNote extends Model
{
    use BuyerNoteRelationship;
}

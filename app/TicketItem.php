<?php

namespace App;

use App\Models\TicketItem\Relationships\TicketItemRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TicketItem
 * @package App
 */
class TicketItem extends Model
{
    use TicketItemRelationship;
}

<?php

function compute_stars($value)
{
    $num_stars = 5;
    
    if ($value < 9)
    {
        $num_stars--;
    }
    if ($value < 6)
    {
        $num_stars--;
    }
    if ($value < 5)
    {
        $num_stars--;
    }
    if ($value < 2)
    {
        $num_stars--;
    }
    if ($value <= 0)
    {
        $num_stars--;
    }
    
    $ret = '';
    
    for ($i = 0; $i < $num_stars; $i++)
    {
        $ret = $ret.'<i class="fa fa-star fa-star-reputation fa-star-reputation-yellow"></i>';
    }
    
    for ($i = 0; $i < 5 - $num_stars; $i++)
    {
        $ret = $ret.'<i class="fa fa-star fa-star-reputation fa-star-reputation-gray"></i>';
    }
    
    return $ret;
}

function required()
{
    return '<i class="fa fa-asterisk fa-required" data-toggle="required-field-tooltip" data-placement="right" title="Campo requerido"></i>';
}
<?php

namespace App;

use App\Models\UserEmailPreference\Relationships\UserEmailRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserEmailPreference
 * @package App
 */
class UserEmailPreference extends Model
{
    use UserEmailRelationship;
}

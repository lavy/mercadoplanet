<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class PublicationChangeStatus
 * @package App\Notifications
 */
class PublicationChangeStatus extends Notification// implements ShouldQueue
{
    use Queueable;

    private $user;
    private $publication;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $publication)
    {
        $this->user = $user;
        $this->publication = $publication;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($this->publication->status == 'Active') $status = 'activada';
        if ($this->publication->status == 'Revision') $status = 'colocada en proceso de revisión';
        if ($this->publication->status == 'Canceled') $status = 'cancelada';
        $message = (new MailMessage)
            ->subject("Tu publicación #{$this->publication->id} ha sido cambiada de estatus.")
            ->greeting('Hola ' . $this->user->first_name . ',')
            ->line("Tu publicación #{$this->publication->id} ha sido $status")
            ->line($this->publication->title);
        if ($this->publication->status_description != '') {
            $message->line('<strong>Motivo:</strong> ' . $this->publication->status_description);
        }
        $message->action('Ver publicaciones', route('publications'))
            ->line('Gracias por confiar en nosotros.');
        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

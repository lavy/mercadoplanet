<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class SellerQualificationReminder
 * @package App\Notifications
 */
class SellerQualificationReminder extends Notification// implements ShouldQueue
{
    use Queueable;

    private $user;
    private $transaction;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $transaction)
    {
        $this->user = $user;
        $this->transaction = $transaction;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Recuerda calificar a tu comprador')
            ->greeting('Hola ' . $this->user->first_name . ',')
            ->line('Aún no has calificado a tu comprador por la compra de ' . $this->transaction->publication->title)
            ->line('Te quedan pocos días para poder calificar, recuerda que tu calificación es importante para la comunidad de ' . config('app.name'))
            ->action('Ver venta', route('sale_detail', ['id' => $this->transaction->id]))
            ->line('Gracias por confiar en nosotros.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

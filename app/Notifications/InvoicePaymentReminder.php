<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class InvoicePaymentReminder
 * @package App\Notifications
 */
class InvoicePaymentReminder extends Notification// implements ShouldQueue
{
    use Queueable;

    private $user;
    private $ticket;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $ticket)
    {
        $this->user = $user;
        $this->ticket = $ticket;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('No olvides pagar tu factura')
            ->greeting('Hola ' . $this->user->first_name . ',')
            ->line('Tienes una factura pendiente por pagar.')
            ->action('Ver factura', route('invoice_detail', ['id' => $this->ticket->id]))
            ->line('Recuerda pagar tu factura y notificar el pago en las próximas <b>' . (config('app.global.days_before_cutoff_notification') * 24) . ' horas</b>, evita ser suspendido para vender.')
            ->line('Gracias por confiar en nosotros.');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class AnsweredQuestion
 * @package App\Notifications
 */
class AnsweredQuestion extends Notification// implements ShouldQueue
{
    use Queueable;

    private $user;
    private $publication;
    private $question;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $publication, $question)
    {
        $this->user = $user;
        $this->publication = $publication;
        $this->question = $question;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Han respondido tu pregunta de ' . $this->publication->title)
            ->greeting('Hola ' . $this->user->first_name . ',')
            ->line('Han respondido tu pregunta del artículo <a href="' . route('product', [$this->publication->id]) . '">' . $this->publication->title . '</a> S/' . number_format($this->publication->price, 2, ',', '.'))
            ->line('Pregunta: ' . $this->question->question)
            ->line('Respuesta: ' . $this->question->answer)
            ->action('Ver preguntas.', route('myquestions'))
            ->line('Gracias por confiar en nosotros.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class InvoicePaymentUserSuspension
 * @package App\Notifications
 */
class InvoicePaymentUserSuspension extends Notification// implements ShouldQueue
{
    use Queueable;

    private $user;
    private $ticket;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $ticket)
    {
        $this->user = $user;
        $this->ticket = $ticket;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Has sido suspendido para vender')
            ->greeting('Hola ' . $this->user->first_name . ',')
            ->line('Tienes una factura con un retraso en el pago, por tal motivo has sido suspendido para vender.')
            ->line('Pero no te preocupes, sólo tienes que pagar tu factura y notificar el pago de esta manera podrás vender nuevamente.')
            ->action('Ver factura', route('invoice_detail', ['id' => $this->ticket->id]))
            ->line('Recuerda que no pagar tu factura a tiempo puede influir en tu reputación.')
            ->line('Gracias por confiar en nosotros.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

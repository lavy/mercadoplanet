<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class ResetPasswordNotification
 * @package App\Notifications
 */
class ResetPasswordNotification extends Notification
{
    use Queueable;

    private $user;
    private $token;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Restablecer Contraseña')
                    ->greeting('Hola ' . $this->user->first_name . ',')
                    ->line('Estás recibiendo este correo porque hemos recibido una solicitud para restablecer la contraseña de tu cuenta.')
                    ->action('Restablecer Contraseña.', url('/password/reset', [$this->token]))
                    ->line('Si tu no solicitaste restablecer tu contraseña, haz caso omiso a este correo.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

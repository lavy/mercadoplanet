<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class SellSeller
 * @package App\Notifications
 */
class SellSeller extends Notification// implements ShouldQueue
{
    use Queueable;

    private $user;
    private $publication;
    private $transaction;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $publication, $transaction)
    {
        $this->user = $user;
        $this->publication = $publication;
        $this->transaction = $transaction;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message =  (new MailMessage)
                    ->subject('¡Felicitaciones! Han comprado tu artículo '.$this->publication->title)
                    ->greeting('Hola ' . $this->publication->user->first_name . ',')
                    ->line('Has recibido una oferta por tu artículo '.$this->publication->title)
                    ->line('Contacta al comprador para coordinar el pago y la entrega del producto');

                    if (count($this->publication->images) > 0)
                      $message->line('<img src="'.url('/photo/120x120?url='.urlencode($this->publication->images[0]->name)).'">');
                    else
                      $message->line('<img src="'.url('/photo/120x120?url=nophoto.jpg').'">');

                    $message->line('Producto: '.$this->publication->title.'<br>Cantidad: '.$this->transaction->quantity.'<br>Precio: S/'.number_format($this->transaction->price,2,',','.'))
                    ->line('<strong>Datos del comprador:</strong><br>'.$this->user->getFullName().'<br>'.$this->user->email.'<br>'.$this->user->phone_number)
                    ->line('Gracias por confiar en nosotros.');
          return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

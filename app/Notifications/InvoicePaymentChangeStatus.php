<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class InvoicePaymentChangeStatus
 * @package App\Notifications
 */
class InvoicePaymentChangeStatus extends Notification// implements ShouldQueue
{
    use Queueable;

    private $user;
    private $ticket;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $ticket)
    {
        $this->user = $user;
        $this->ticket = $ticket;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Tu notificación de pago ha sido ' . ($this->ticket->payment_status == 'Aprobado' ? 'Aprobada' : 'Rechazada'))
            ->greeting('Hola ' . $this->user->first_name . ',')
            ->line('Tu notificación de pago por la factura #' . $this->ticket->id . ' ha sido ' . ($this->ticket->payment_status == 'Aprobado' ? 'Aprobada' : 'Rechazada'))
            ->action('Ver facturas', route('billing'))
            ->line('Gracias por confiar en nosotros.');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

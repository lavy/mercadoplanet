<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class FinishPublication extends Notification// implements ShouldQueue
{
    use Queueable;

    private $user;
    private $publication;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $publication)
    {
        $this->user = $user;
        $this->publication = $publication;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = (new MailMessage)
            ->subject("Finalizó tu publicación de {$this->publication->title}.")
            ->greeting('Hola ' . $this->user->first_name . ',')
            ->line('Tu publicación ha finalizado.')
            ->line($this->publication->title)
            ->line('Puedes republicarla si así lo deseas.')
            ->action('Republicar', route('republish-publication', ['id' => $this->publication->id]))
            ->line('Gracias por confiar en nosotros.');
        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

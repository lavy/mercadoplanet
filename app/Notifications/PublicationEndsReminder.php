<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class PublicationEndsReminder
 * @package App\Notifications
 */
class PublicationEndsReminder extends Notification// implements ShouldQueue
{
    use Queueable;

    private $user;
    private $publication;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $publication)
    {
        $this->user = $user;
        $this->publication = $publication;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Tu publicación está por terminar, aumenta las posibilidades de venderlo.')
            ->greeting('Hola ' . $this->user->first_name . ',')
            ->line('Aumenta las visitas y obten más posibilidades de vender tu producto.')
            ->line($this->publication->title)
            ->line('Tu publicación aparecerá más arriba en los listados y estará disponible por ' . config('app.global.publication_lifetime_days') . ' días contados a partir del momento que aumentes su exposición. Esto te ayudará a obtener más compradores potenciales.')
            ->action('Aumentar exposición', route('plan', ['id' => $this->publication->id]))
            ->line('Gracias por confiar en nosotros.');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class QualificationSellerConfirm
 * @package App\Notifications
 */
class QualificationSellerConfirm extends Notification// implements ShouldQueue
{
    use Queueable;

    private $user;
    private $publication;
    private $transaction;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $publication, $transaction)
    {
        $this->user = $user;
        $this->publication = $publication;
        $this->transaction = $transaction;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('El comprador ha confirmado su calificación por el artículo' . $this->publication->title)
            ->greeting('Hola ' . $this->user->first_name . ',')
            ->line('El comprador del artículo ' . $this->publication->title . ' ha confirmado su calificación.')
            ->action('Ver detalles de la venta', route('sale_detail', ['id' => $this->transaction->id]))
            ->line('Gracias por confiar en nosotros.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

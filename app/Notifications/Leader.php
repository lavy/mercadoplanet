<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class Leader
 * @package App\Notifications
 */
class Leader extends Notification// implements ShouldQueue
{
    use Queueable;

    private $user;
    private $type;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $type)
    {
        $this->user = $user;
        $this->type = $type;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Felicitaciones ya eres un vendedor'.($this->type == 2 ? ' Oro':'Plata'))
                    ->greeting('Hola ' . $this->user->first_name . ',')
                    ->line('¡Felicitaciones! Ya eres un vendedor '.($this->type == 2 ? ' Oro':'Plata'))
                    ->line('Un vendedor Lider genera mas confianza en los compradores ya que forma parte de los mejores vendedores del sitio, esto te ayuda a generar más ventas en tus productos.')
                    ->action('Ver tu reputación', url('/myreputation'))
                    ->line('Gracias por confiar en nosotros.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

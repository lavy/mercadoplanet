<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class AvoidBuyerQualificationSeller
 * @package App\Notifications
 */
class AvoidBuyerQualificationSeller extends Notification// implements ShouldQueue
{
    use Queueable;

    private $user;
    private $transaction;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $transaction)
    {
        $this->user = $user;
        $this->transaction = $transaction;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Calificación eliminada')
            ->greeting('Hola ' . $this->user->first_name . ',')
            ->line('No hemos obtenido respuesta del comprador del artículo ' . $this->transaction->publication->title . ' a tu solicitud de cambiar la calificación.')
            ->line('Por tal motivo hemos decidido eliminarla.')
            ->action('Ver venta', route('sale_detail', ['id' => $this->transaction->id]))
            ->line('Gracias por confiar en nosotros.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

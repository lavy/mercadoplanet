<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class QualificationBuyer
 * @package App\Notifications
 */
class QualificationBuyer extends Notification// implements ShouldQueue
{
    use Queueable;

    private $user;
    private $publication;
    private $transaction;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $publication, $transaction)
    {
        $this->user = $user;
        $this->publication = $publication;
        $this->transaction = $transaction;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = (new MailMessage)
            ->subject('Has recibido una calificación por el artículo ' . $this->publication->title)
            ->greeting('Hola ' . $this->user->first_name . ',')
            ->line('El vendedor del artículo ' . $this->publication->title . ' te ha calificado.');
        if ($this->transaction->buyer_calification !== 'No calificado') {
            $message->action('Ver detalles de la compra', route('purchase_detail', ['id' => $this->transaction->id]));
        } else {
            $message->action('Califica a tu contraparte.', route('buyer_qualification', ['id' => $this->transaction->id]));
        }

        $message->line('Gracias por confiar en nosotros.');
        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class ContraQualificationSeller
 * @package App\Notifications
 */
class ContraQualificationSeller extends Notification// implements ShouldQueue
{
    use Queueable;

    private $user;
    private $publication;
    private $transaction;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $publication, $transaction)
    {
        $this->user = $user;
        $this->publication = $publication;
        $this->transaction = $transaction;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = (new MailMessage)
            ->subject('Tienes una calificación contradictoria en el artículo ' . $this->publication->title)
            ->greeting('Hola ' . $this->user->first_name . ',')
            ->line('El comprador del artículo ' . $this->publication->title . ' ha modificado su calificación y es contradictoria a la tuya.');
        if ($this->transaction->seller_calification !== 'No calificado') {
            if ($this->transaction->seller_concreted != $this->transaction->buyer_concreted) {
                $message->line('La calificación recibida es <b>contradictoria</b>, verifica tu calificación o comunicate con tu contraparte para que corrija la suya.');
            }
            $message->action('Ver detalles de la venta', route('sale_detail', ['id' => $this->transaction->id]));
        } else {
            $message->action('Califica a tu contraparte.', route('seller_qualification', ['id' => $this->transaction->id]));
        }

        $message->line('Gracias por confiar en nosotros.');
        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

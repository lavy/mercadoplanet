<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class Consultation
 * @package App\Notifications
 */
class Consultation extends Notification// implements ShouldQueue
{
    use Queueable;

    private $user;
    private $publication;
    private $message;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $publication, $message)
    {
        $this->user = $user;
        $this->publication = $publication;
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Consulta de ' . $this->publication->title)
            ->greeting('Hola ' . $this->publication->user->first_name . ',')
            ->line('Has recibido una consulta en tu artículo <a href="' . route('product', [$this->publication->id]) . '">' . $this->publication->title . '</a> S/' . number_format($this->publication->price, 2, ',', '.'))
            ->action('Ver artículo.', route('product', [$this->publication->id]))
            ->line('Contacta al interesado para responder su consulta')
            ->line('Consulta: ' . $this->message)
            ->line('<strong>Datos del interesado:</strong><br>' . $this->user->getFullName() . '<br>' . $this->user->email . '<br>' . $this->user->phone_number)
            ->line('Gracias por confiar en nosotros.');
        // ->from($this->publication->user->email);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

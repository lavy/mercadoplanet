<?php

namespace App;

use App\Models\Province\Relationships\ProvinceRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Province
 * @package App
 */
class Province extends Model
{
    use ProvinceRelationship;
}

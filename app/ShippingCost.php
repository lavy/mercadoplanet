<?php

namespace App;

use App\Models\ShippingCost\Relationships\ShippingCostRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ShippingCost
 * @package App
 */
class ShippingCost extends Model
{
    use ShippingCostRelationship;

    /**
     * @param int $publicationId
     * @param $priceShipping
     * @param $name
     */
    public static function addShippingCost(int $publicationId, $priceShipping, $name)
    {
        $shippingCost = new self();
        $shippingCost->publication_id = $publicationId;
        $shippingCost->cost = $priceShipping;
        $shippingCost->name = $name;
        $shippingCost->save();
    }
}

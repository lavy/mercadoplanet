<?php

namespace App;

use App\Models\GroupFeature\Relationships\GroupFeatureRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GroupFeature
 * @package App
 */
class GroupFeature extends Model
{
	use GroupFeatureRelationship;
}

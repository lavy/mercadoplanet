<?php

namespace App;

use App\Models\Image\Relationships\ImageRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Image
 * @package App
 */
class Image extends Model
{
    use ImageRelationship;

    /**
     * @param $publicationId
     * @param $imageName
     * @param $fileApiId
     */
    public static function addImage($publicationId, $imageName, $fileApiId)
    {
        $image = new self();
        $image->publication_id = $publicationId;
        $image->name = $imageName;
        $image->fileapi_id = $fileApiId;
        $image->save();

        return $image;
    }

    /**
     * @param $publicationId
     * @param $imageId
     * @param $position
     */
    public static function updateImagePositionFromPubIdAndImageId($publicationId, $imageId, $position)
    {
        self::where('publication_id', $publicationId)
            ->where('fileapi_id', $imageId)
            ->update(['position' => $position]);
    }

    /**
     * @param $publicationId
     * @param $position
     */
    public static function updateImagePositionFromPubId($publicationId, $position)
    {
        self::where('publication_id', $publicationId)
            ->update(['position' => $position]);
    }

}

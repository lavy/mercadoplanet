<?php
/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 13/04/20
 * Time: 01:16
 */

namespace App\Helpers;

use App\Configuration;

class PlanHelper
{
	/**
	 * @param Request $request
	 * @param Configuration $configuration
	 * @return int|mixed|static
	 */
	public static function getPlan(Request $request, Configuration $configuration)
	{
		switch ($request->plan) {
			case 'Premium':
				return $configuration->premium_cost;
				break;
			case 'Oro':
				return $configuration->gold_cost;
				break;
			case 'Plata':
				return $configuration->silver_cost;
				break;
			case 'Bronce':
				return $configuration->bronze_cost;
				break;
		}
	}
}
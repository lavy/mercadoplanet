<?php
namespace App\Helpers;
/**
 * Created by PhpStorm.
 * User: manuelcasco
 * Date: 12/04/20
 * Time: 04:15
 */
class TimeHelper
{
	/**
	 * @param $time
	 * @return string
	 */
	public static function humanizeTime($time)
	{
		$time = ($time < 1) ? 1 : $time;
		$tokens = [
			31536000 => 'año',
			2592000 => 'mes',
			86400 => 'día',
			3600 => 'hora',
			60 => 'minuto',
			1 => 'segundo'
		];

		foreach ($tokens as $unit => $text) {
			if ($time < $unit) continue;
			$numberOfUnits = floor($time / $unit);
			return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '');
		}
	}
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Notifications
 * @package App
 */
class Notifications extends Model
{
	use NotificationsRelationship;
}

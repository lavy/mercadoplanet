@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <form method="POST" action="{{ route('bank-account.store') }}">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="pull-left">Crear Moneda</h4>
                            <a href="{{ route('home') }}" class="btn btn-default pull-right">Regresar</a>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <!-- Table -->
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Campo</th>
                                        <th>Valor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>Medio de Pago</th>
                                        <td>
                                            <img src="{{ asset('img/payment_methods/BCP.png') }}" width="80px" height="50px">
                                            <input type="radio" name="payment" value="banco_credito">
                                            <img src="{{ asset('img/payment_methods/bbva-continental.png') }}" width="80px" height="50px">
                                            <input type="radio" name="payment" value="bbva_continental">
                                            <img src="{{ asset('img/payment_methods/interbank.png') }}" width="80px" height="50px">
                                            <input type="radio" name="payment" value="interbank">
                                            <img src="{{ asset('img/payment_methods/bitcoin.png') }}" width="80px" height="50px">
                                            <input type="radio" name="payment" value="bitcoin">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Describe tu cuenta</th>
                                        <td>
                                            @if ($errors->has('description'))
                                                <br>
                                                <span class="text-danger">
                                                    {{ $errors->first('description') }}
                                                </span>
                                            @endif
                                            <textarea class="form-control" name="description" id="description" rows="3" style="max-width: 300px; margin-top: 10px"></textarea>
                                            <div style="font-size: 12px;">Quedan <span id="num-chars">200</span> caracteres.</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: right"><button type="submit" class="btn btn-primary">Guardar</button></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
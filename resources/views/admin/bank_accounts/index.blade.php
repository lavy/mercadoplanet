@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <!-- Default panel contents -->
                        <form method="GET" action="{{ url('/admin/bank-account') }}">
                            <div class="panel-heading">
                                <h4 class="pull-left">Cuentas Bancarias</h4>
                                <div class="input-group col-md-4 pull-right" style="margin-top: 5px">
                                    <input type="text" class="form-control" name="bank-search" value="{{ $bank_search }}" placeholder="Buscar por: banco, descripcion ...">
                                    <span class="input-group-btn">
                                  <button class="btn btn-default" type="submit">Buscar</button>
                                </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                    <div class="panel-body">
                        <!-- Table -->
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Descripción</th>
                                    <th>Logo</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($banks))
                                    @foreach($banks as $bank)
                                        <tr>
                                            <td>{{ $bank->id }}</td>
                                            <td>{{ $bank->description }}</td>
                                            <td>
                                                @if($bank->bank == 'bitcoin')
                                                    <img src="{{ asset('img/payment_methods/bitcoin.png') }}" width="80px" height="50px">
                                                @elseif($bank->bank == 'bbva_continental')
                                                    <img src="{{ asset('img/payment_methods/bbva-continental.png') }}" width="80px" height="50px">
                                                @elseif($bank->bank == 'banco_credito')
                                                    <img src="{{ asset('img/payment_methods/BCP.png') }}" width="80px" height="50px">
                                                @elseif($bank->bank == 'interbank')
                                                    <img src="{{ asset('img/payment_methods/interbank.png') }}" width="80px" height="50px">
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ url('admin/bank-account/'.$bank->id.'/edit') }}" title="Modificar"><i class="glyphicon glyphicon-pencil"></i></a>
                                                {{--<form method="POST" action="{{url('admin/bank-account/'.$bank->id)}}">
                                                    {{method_field('DELETE')}}
                                                    {{csrf_field()}}
                                                    <button><i class="glyphicon glyphicon-trash"></i></button>
                                                </form>--}}
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr><td colspan="6"><div   class="alert alert-warning">No se encontraron registros</div></td></tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        @if(count($banks))
                            <div class="panel-footer" style="text-align: center">
                                {{ $banks->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
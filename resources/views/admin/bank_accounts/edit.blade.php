
@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <form method="post" action="{{ url('admin/bank-account/'.$bank->id) }}">
            {{method_field('PUT')}}
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="pull-left">Modificar Cuenta Bancaria: {{ $bank->bank }} </h4>
                            <a href="{{ route('home') }}" class="btn btn-default pull-right">Regresar</a>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <!-- Table -->
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Campo</th>
                                        <th>Valor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>Metodo de Pago</th>
                                        <td>
                                            <img src="{{ asset('img/payment_methods/BCP.png') }}" width="80px" height="50px">
                                            <input type="radio" name="payment" value="banco_credito">
                                            <img src="{{ asset('img/payment_methods/bbva-continental.png') }}" width="80px" height="50px">
                                            <input type="radio" name="payment" value="bbva_continental">
                                            <img src="{{ asset('img/payment_methods/interbank.png') }}" width="80px" height="50px">
                                            <input type="radio" name="payment" value="interbank">
                                            <img src="{{ asset('img/payment_methods/bitcoin.png') }}" width="80px" height="50px">
                                            <input type="radio" name="payment" value="bitcoin">
                                            @if ($errors->has('bank'))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('bank') }}</strong>
                                        </span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Descripciòn</th>
                                        <td>
                                            <input id="description" type="text" class="form-control" name="description" value="{{ $bank->description }}">
                                            @if ($errors->has('description'))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: right"><button type="submit" class="btn btn-primary">Guardar</button></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
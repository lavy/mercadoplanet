@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- Default panel contents -->
                    <form method="POST" action="{{ route('admin_questions', ['id' => $id]) }}">
                        <div class="panel-heading">
                            <a href="{{ url('/admin/publications') }}" class="btn btn-default">Publicaciones</a>
                            <div class="clearfix"></div>
                            <h4 class="pull-left">Preguntas y Respuestas</h4>
                            <div class="input-group col-md-4 pull-right" style="margin-top: 5px">
                                <input type="text" class="form-control" name="question_search" value="{{ $question_search }}" placeholder="Buscar ...">
                                <span class="input-group-btn">
                                  <button class="btn btn-default" type="submit">Buscar</button>
                                </span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="panel-body">
                    <div><a href="{{ route('product', ['id' => $id]) }}" target="_blank">{{ $publication->title }}</a></div>
                    <!-- Table -->
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th style="width:40%">Pregunta</th>
                                    <th>&nbsp;</th>
                                    <th style="width:40%">Respuesta</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($questions))
                                    @foreach($questions as $question)
                                    <?php $user = $question->publication->user;?>
                                    <?php $asker = $question->asker;?>
                                    <tr>
                                        <td>
                                            <a href="{{ route('user_detail', ['id' => $user->id]) }}" target="_bank">{{ $user->username }}</a>: <span style="font-style: italic">"{{ $question->question }}"</span> - <span style="color:#999; font-size: 10px">{{ date('d-m-Y h:i a', strtotime($question->created_at)) }}</span>
                                            @if($question->banned_question == 1)
                                            <span class="label label-warning lb-lg">Pregunta deshabilitada.</span>
                                            @endif
                                        </td>                                        
                                        <td>
                                            @if($question->banned_question != 1)
                                            <a href="{{ route('disable_question', ['id' => $question->id]) }}" title="Deshabilitar pregunta"><i class="glyphicon glyphicon-minus-sign"></i></a>
                                            @else
                                            <a href="{{ route('enable_question', ['id' => $question->id]) }}" title="Habilitar pregunta"><i class="glyphicon glyphicon-ok"></i></a>
                                            @endif
                                        </td>
                                        <td><a href="{{ route('user_detail', ['id' => $asker->id]) }}" target="_bank">{{ $asker->username }}</a>: <span style="font-style: italic">"{{ $question->answer }}"</span>
                                            @if(!empty($question->answer))
                                            - <span style="color:#999; font-size: 10px">{{ date('d-m-Y h:i a', strtotime($question->updated_at)) }}</span>
                                            @endif
                                            @if($question->banned_answer == 1)
                                            <span class="label label-warning lb-lg">Respuesta deshabilitada.</span>
                                            @endif
                                        </td>                                        
                                        <td>
                                            @if($question->banned_answer != 1)
                                            <a href="{{ route('disable_answer', ['id' => $question->id]) }}" title="Deshabilitar respuesta"><i class="glyphicon glyphicon-minus-sign"></i></a>
                                            @else
                                            <a href="{{ route('enable_answer', ['id' => $question->id]) }}" title="Habilitar respuesta"><i class="glyphicon glyphicon-ok"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                <tr><td colspan="7"><div   class="alert alert-warning">No se encontraron registros</div></td></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    @if(count($questions))
                        <div class="panel-footer" style="text-align: center">
                            {{ $questions->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('admin.layouts.app')

@section('content')
<div class="container">
    <form method="POST" action="{{ route('user_modify_save') }}">
        <input type="hidden" name="id" value="<?=$user->id?>">
        {{ csrf_field() }}
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="pull-left">Modificar usuario</h4>
                    <a href="{{ route('home') }}" class="btn btn-default pull-right">Regresar</a>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <!-- Table -->
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Campo</th>
                                    <th>Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>Tipo de identificación</th>
                                    <td>
                                        <label>
                                            <input id="document_type_1" type="radio" name="document_type" value="DNI" autofocus @if($user->document_type == "DNI") checked="checked" @endif>
                                            D.N.I.
                                        </label>
                                         <label>
                                            <input id="document_type_2" type="radio" name="document_type" value="CE" autofocus @if($user->document_type == "CE") checked="checked" @endif>
                                            C.E.
                                        </label>
                                        @if ($errors->has('document_type'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('document_type') }}</strong>
                                        </span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Número de identificación</th>
                                    <td>
                                        <input id="document" type="text" class="form-control" name="document" value="{{ $user->document }}">
                                        @if ($errors->has('document'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('document') }}</strong>
                                        </span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Nombre</th>
                                    <td>
                                        <input id="first_name" type="text" class="form-control" name="first_name" value="{{ $user->first_name }}">
                                        @if ($errors->has('first_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Apellido</th>
                                    <td>
                                        <input id="last_name" type="text" class="form-control" name="last_name" value="{{ $user->last_name }}">
                                        @if ($errors->has('last_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Usuario</th>
                                    <td>
                                        <input id="username" type="username" class="form-control" name="username" value="{{ $user->username }}">
                                        @if ($errors->has('username'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>E-mail</th>
                                    <td>
                                        <input id="email-confirm" type="email" class="form-control" name="email" value="{{ $user->email }}">
                                        @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Número de teléfono</th>
                                    <td>
                                        <input id="phone_number" type="text" class="form-control" name="phone_number" value="{{ $user->phone_number }}">
                                        @if ($errors->has('phone_number'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('phone_number') }}</strong>
                                        </span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Tipo de Vendedor</th>
                                    <td>
                                        <select name="seller_type" class="form-control">
                                            <option <?=($user->seller_type == 'Normal' ? 'selected' : '')?>>Normal</option>
                                            <option <?=($user->seller_type == 'Líder' ? 'selected' : '')?> value="Líder">Plata</option>
                                            <option <?=($user->seller_type == 'Líder Gold' ? 'selected' : '')?> value="Líder Gold">Oro</option>
                                        </select>
                                        @if ($errors->has('seller_type'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('seller_type') }}</strong>
                                        </span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Estado del usuario</th>
                                    <td>
                                        <select name="status_cplx" class="form-control">
                                            <option <?=($user->status == 'Active' && !$user->suspended ? 'selected' : '')?>>Activo</option>
                                            <option <?=($user->status == 'Active' &&  $user->suspended ? 'selected' : '')?>>Suspendido para vender</option>
                                            <option <?=($user->status != 'Active' ? 'selected' : '')?>>Deshabilitado</option>
                                        </select>
                                        @if ($errors->has('status_cplx'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('status_cplx') }}</strong>
                                        </span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: right"><button type="submit" class="btn btn-primary">Guardar</button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
@endsection
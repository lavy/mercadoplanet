@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- Default panel contents -->
                    <form method="POST" action="{{ route('admin_publications', ['status' => $status]) }}">
                        <div class="panel-heading">
                            <h4 class="pull-left">Publicaciones</h4>
                            <div class="input-group col-md-4 pull-right" style="margin-top: 5px">
                                <input type="text" class="form-control" name="publication_search" value="{{ $publication_search }}" placeholder="Buscar por: id, título ...">
                                <span class="input-group-btn">
                                  <button class="btn btn-default" type="submit">Buscar</button>
                                </span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="panel-body">
                    <div>
                        <a href="{{ route('admin_publications', ['status' => 'active']) }}" class="btn btn-default @if($status == 'active') btn-primary @endif">Activas</a>
                        <a href="{{ route('admin_publications', ['status' => 'paused']) }}" class="btn btn-default @if($status == 'paused') btn-primary @endif">Pausadas</a>
                        <a href="{{ route('admin_publications', ['status' => 'finished']) }}" class="btn btn-default @if($status == 'finished') btn-primary @endif">Finalizadas</a>
                        <a href="{{ route('admin_publications', ['status' => 'revision']) }}" class="btn btn-default @if($status == 'revision') btn-primary @endif">En revisión</a>
                        <a href="{{ route('admin_publications', ['status' => 'canceled']) }}" class="btn btn-default @if($status == 'canceled') btn-primary @endif">Canceladas</a>
                    </div>
                    <!-- Table -->
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th style="width:260px">Título</th>
                                    <th style="width:130px">Vendedor</th>
                                    <th>Plan</th>
                                    <th>Fecha pub.</th>
                                    <th>Fecha fin.</th>
                                    @if($status == 'revision' || $status == 'canceled')
                                        <th>Fecha cambio de estado</th>
                                    @endif
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($publications))
                                    @foreach($publications as $publication)
                                    <tr>
                                        <td>{{ $publication->id }}</td>
                                        <td>
                                            @if($publication->status == 'Canceled')
                                            <div style="width: 250px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;display: block;">{{ $publication->title }}</div>
                                            @else
                                            <a href="{{ route('product', ['id' => $publication->id]) }}" target="_blank" style="width: 250px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;display: block;">{{ $publication->title }}</a>
                                            @endif
                                            <?php $breadcrumb = \App\Category::getBreadCrumbArrayFromCodePath($publication->category_path); ?>
                                            <?php $bread = '';?>
                                            @foreach($breadcrumb as $key => $b)
                                                @if($key != 0)
                                                <?php $bread .= "&nbsp;-&nbsp;"; ?>
                                                @endif
                                                <?php $bread .= $b->name; ?>
                                            @endforeach
                                            
                                            <div style="width: 250px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;display: block;" title="{{ $bread }}">
                                            {{ $bread }}
                                            </div>
                                        </td>
                                        <td>
                                            <?php $user = $publication->user; ?>
                                            {{ $user->getFullName() }}<br>
                                            {{ $user->username }}<br>
                                            {{ $user->email }}
                                        </td>
                                        <td>
                                            @if($publication->cost_type == 'Premium')
                                            Premium
                                            @elseif($publication->cost_type == 'Gold')
                                            Oro
                                            @elseif($publication->cost_type == 'Silver')
                                            Plata
                                            @elseif($publication->cost_type == 'Bronze')
                                            Bronce
                                            @elseif($publication->cost_type == 'Fijo')
                                            Clasificado
                                            @else
                                            Gratis
                                            @endif
                                        </td>
                                        <td>
                                            {{ date('d-m-Y', strtotime($publication->publicated_as_active_at)) }}                                            
                                        </td>
                                        <td>
                                            {{ date('d-m-Y', strtotime($publication->ends_at)) }}                                            
                                        </td>
                                        @if(($publication->status == 'Revision' || $publication->status == 'Canceled') && $publication->status_date)
                                            <td>
                                            {{ date('d-m-Y', strtotime($publication->status_date)) }}    
                                            </td>
                                        @endif
                                        
                                        <td>
                                            @if($publication->status != 'Finished')
                                            <a href="{{ route('status_modify', ['id' => $publication->id]) }}" title="Revisión/Cancelar"><i class="glyphicon glyphicon-minus-sign"></i></a>
                                            @endif
                                            @if($publication->topLevelCategory()->id == '4')
                                            <a href="{{ route('admin_questions', ['id' => $publication->id]) }}" title="Preguntas"><i class="glyphicon glyphicon-question-sign"></i></a>
                                            @else
                                            <i class="glyphicon glyphicon-question-sign" style="color: #999"></i>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                <tr><td colspan="7"><div   class="alert alert-warning">No se encontraron registros</div></td></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    @if(count($publications))
                        <div class="panel-footer" style="text-align: center">
                            {{ $publications->appends($query)->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
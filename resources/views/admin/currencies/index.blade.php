@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <!-- Default panel contents -->
                        <form method="GET" action="{{ url('/admin/currencies') }}">
                            <div class="panel-heading">
                                <h4 class="pull-left">Monedas</h4>
                                <div class="input-group col-md-4 pull-right" style="margin-top: 5px">
                                    <input type="text" class="form-control" name="currency-search" value="{{ $currency_search }}" placeholder="Buscar por: identificación, nombre, apellido ...">
                                    <span class="input-group-btn">
                                  <button class="btn btn-default" type="submit">Buscar</button>
                                </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                    <div class="panel-body">
                        <!-- Table -->
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Identificación</th>
                                    <th>Nombre</th>
                                    <th>Simbolo</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($currencies))
                                    @foreach($currencies as $currency)
                                        <tr>
                                            <td>{{ $currency->id }}</td>
                                            <td>{{ $currency->name }}</td>
                                            <td>{{ $currency->symbol }}</td>
                                            <td>
                                                <a href="{{ url('admin/currency/'.$currency->id.'/edit')}}" title="Modificar"><i class="glyphicon glyphicon-pencil"></i></a>
                                                {{--<form method="POST" action="{{url('admin/currency/'.$currency->id)}}">
                                                    {{method_field('DELETE')}}
                                                    {{csrf_field()}}
                                                    <button><i class="glyphicon glyphicon-trash"></i></button>
                                                </form>--}}
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr><td colspan="6"><div   class="alert alert-warning">No se encontraron registros</div></td></tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        @if(count($currencies))
                            <div class="panel-footer" style="text-align: center">
                                {{ $currencies->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
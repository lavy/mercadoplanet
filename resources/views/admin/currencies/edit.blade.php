@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <form method="post" action="{{url('admin/currency/'.$currency->id)}}">
            {{method_field('PUT')}}
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="pull-left">Modificar Moneda: {{ $currency->name }} </h4>
                            <a href="{{ route('home') }}" class="btn btn-default pull-right">Regresar</a>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <!-- Table -->
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Campo</th>
                                        <th>Valor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>Nombre</th>
                                        <td>
                                            <input id="name" type="text" class="form-control" name="name" value="{{ $currency->name }}">
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Symbol</th>
                                        <td>
                                            <input id="symbol" type="text" class="form-control" name="symbol" value="{{ $currency->symbol }}">
                                            @if ($errors->has('symbol'))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('symbol') }}</strong>
                                        </span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: right"><button type="submit" class="btn btn-primary">Guardar</button></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- Default panel contents -->
                    <form method="POST" action="{{ url('/admin/home') }}">
                        <div class="panel-heading">
                            <h4 class="pull-left">Usuarios</h4>
                            <div class="input-group col-md-4 pull-right" style="margin-top: 5px">
                                <input type="text" class="form-control" name="user-search" value="{{ $user_search }}" placeholder="Buscar por: identificación, nombre, apellido ...">
                                <span class="input-group-btn">
                                  <button class="btn btn-default" type="submit">Buscar</button>
                                </span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="panel-body">
                    <!-- Table -->
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Identificación</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Usuario</th>
                                    <th>E-mail</th>
                                    <th>Tipo de vendedor</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($users))
                                    @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->document_type }}: {{ $user->document }}</td>
                                        <td>{{ $user->first_name }}</td>
                                        <td>{{ $user->last_name }}</td>
                                        <td>{{ $user->username }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                            <div>
                                                @if($user->seller_type=='Normal')
                                                Normal
                                                @else
                                                @if($user->seller_type=='Líder Gold') Oro @else Plata @endif
                                                @endif
                                            </div>
                                            @if($user->canBeLeader() == '1' && $user->leader_notified)
                                            <a href="{{ route('change_leader_status', ['id' => $user->id]) }}" title="Candidato Líder" style="text-decoration: none;"><div class="label label-default">Convertir en Vendedor Plata</div></a>
                                            @elseif($user->canBeLeader() == '2' && $user->leader_notified)
                                            <a href="{{ route('change_leader_status', ['id' => $user->id]) }}" title="Candidato Líder" style="text-decoration: none;"><div class="label label-warning">Convertir en Vendedor Oro</div></a>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('user_detail', ['id' => $user->id]) }}" title="Detalles"><i class="glyphicon glyphicon-eye-open"></i></a>
                                            <a href="{{ route('user_modify', ['id' => $user->id]) }}" title="Modificar"><i class="glyphicon glyphicon-pencil"></i></a>
                                            <a href="{{ route('user_invoices', ['id' => $user->id]) }}" title="Facturas"><i class="glyphicon glyphicon-list-alt"></i></a>
                                            <a href="{{ route('user_purchases', ['id' => $user->id]) }}" title="Compras"><i class="glyphicon glyphicon-shopping-cart"></i></a>
                                            <a href="{{ route('user_sales', ['id' => $user->id]) }}" title="Ventas"><i class="glyphicon glyphicon-tag"></i></a>
                                            
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                <tr><td colspan="6"><div   class="alert alert-warning">No se encontraron registros</div></td></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    @if(count($users))
                        <div class="panel-footer" style="text-align: center">
                            {{ $users->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
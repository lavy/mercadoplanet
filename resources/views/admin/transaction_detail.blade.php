@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="pull-left">Detalles de Compra/Venta</h4>
                    <a href="{{ route('home') }}" class="btn btn-default pull-right">Regresar</a>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <!-- Table -->
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Campo</th>
                                    <th>Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th># transacción</th>
                                    <td>{{ $transaction->id }}</td>
                                </tr>
                                <tr>
                                    <th>Publicación</th>
                                    <td>
                                        <?php $publication = $transaction->publication; ?>
                                        @if($publication->status == 'Canceled')
                                        <div>{{ $publication->title }}</div>
                                        @else
                                        <a href="{{ route('product', ['id' => $publication->id]) }}" target="_blank">{{ $publication->title }}</a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Vendedor</th>
                                    <td>{{ $publication->user->getFullName() }}</td>
                                </tr>
                                <tr>
                                    <th>Comprador</th>
                                    <td>{{ $transaction->buyer->getFullName() }}</td>
                                </tr>
                                <tr>
                                    <th>Precio - Cantidad</th>
                                    <td><span>S/{{ number_format($publication->price, 2, ',', '.') }}</span> x {{ $publication->quantity }} @if($publication->quantity == 1)unidad @else unidades @endif</td>
                                </tr>
                                <tr>
                                    <th>Calificación Vendedor</th>
                                    <td>
                                                                                  
                                            
                                            @if($transaction->seller_calification === 'Positivo')
                                            <i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i> 
                                            Calificó positivo
                                            <div><a href="{{ route('seller_delete_qualification', ['id' => $transaction->id, 'origin' => '1']) }}"  onclick="javascript: return confirm('¿Seguro desea eliminar la calificación del vendedor? (Esta acción es irreversible)')">Eliminar calificación</a></div>
                                            @elseif($transaction->seller_calification === 'Negativo')
                                            <i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i> 
                                            Calificó negativo
                                            <div><a href="{{ route('seller_delete_qualification', ['id' => $transaction->id, 'origin' => '1']) }}"  onclick="javascript: return confirm('¿Seguro desea eliminar la calificación del vendedor? (Esta acción es irreversible)')">Eliminar calificación</a></div>
                                            @elseif($transaction->seller_calification === 'Neutral')
                                            <i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i> 
                                            Calificó neutral
                                            <div><a href="{{ route('seller_delete_qualification', ['id' => $transaction->id, 'origin' => '1']) }}"  onclick="javascript: return confirm('¿Seguro desea eliminar la calificación del vendedor? (Esta acción es irreversible)')">Eliminar calificación</a></div>
                                            @elseif(($transaction->seller_calification === 'No calificado' && (time() - strtotime($transaction->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600))
                                                or ($transaction->seller_calification === 'No calificado' && $transaction->seller_qualification_modified)
                                            )
                                            No calificó
                                            @else
                                            <i class="fa fa-clock-o" style="font-size: 16px;color:#666"></i> {{ $transaction->getTimeToQualify() }} dias para calificar
                                            @endif              
                                    </td>
                                </tr>
                                <tr>
                                    <th>Fecha calificación vendedor</th>
                                    <td>{{ date("d-m-Y h:i a", strtotime($transaction->seller_qualification_date)) }}</td>
                                </tr>
                                <tr>
                                    <th>Calificación Comprador</th>
                                    <td>
                                        @if($transaction->buyer_calification === 'Positivo')
                                            <i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i> 
                                            Calificó positivo
                                            <div><a href="{{ route('buyer_delete_qualification', ['id' => $transaction->id, 'origin' => '1']) }}"  onclick="javascript: return confirm('¿Seguro desea eliminar la calificación del comprador? (Esta acción es irreversible)')">Eliminar calificación</a></div>
                                            @elseif($transaction->buyer_calification === 'Negativo')
                                            <i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i> 
                                            Calificó negativo
                                            <div><a href="{{ route('buyer_delete_qualification', ['id' => $transaction->id, 'origin' => '1']) }}"  onclick="javascript: return confirm('¿Seguro desea eliminar la calificación del comprador? (Esta acción es irreversible)')">Eliminar calificación</a></div>
                                            @elseif($transaction->buyer_calification === 'Neutral')
                                            <i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i> 
                                            Calificó neutral
                                            <div><a href="{{ route('buyer_delete_qualification', ['id' => $transaction->id, 'origin' => '1']) }}"  onclick="javascript: return confirm('¿Seguro desea eliminar la calificación del comprador? (Esta acción es irreversible)')">Eliminar calificación</a></div>
                                            @elseif(($transaction->buyer_calification === 'No calificado' && (time() - strtotime($transaction->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600))
                                                or ($transaction->buyer_calification === 'No calificado' && $transaction->buyer_qualification_modified)
                                            )
                                            No calificó
                                            @else
                                            <i class="fa fa-clock-o" style="font-size: 16px;color:#666"></i> {{ $transaction->getTimeToQualify() }} dias para calificar
                                            @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Fecha calificación comprador</th>
                                    <td>{{ date("d-m-Y h:i a", strtotime($transaction->buyer_qualification_date)) }}</td>
                                </tr>
                                <tr>
                                    <th>Comentario Vendedor</th>
                                    <td>{{ $transaction->seller_comment }}</td>
                                </tr>
                                <tr>
                                    <th>Replica comprador</th>
                                    <td>{{ $transaction->buyer_reply }}</td>
                                </tr>
                                <tr>
                                    <th>Comentario Comprador</th>
                                    <td>{{ $transaction->buyer_comment }}</td>
                                </tr>
                                <tr>
                                    <th>Replica comprador</th>
                                    <td>{{ $transaction->seller_reply }}</td>
                                </tr>
                                <tr>
                                    <th>Fecha de transacción</th>
                                    <td>{{ date("d-m-Y h:i a", strtotime($transaction->created_at)) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
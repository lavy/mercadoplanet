@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h4 class="pull-left">Detalles de factura #{{ $invoice->id }} - {{ $invoice->user->getFullName() }}</h4>
                        <a href="{{ route('user_invoices', ['id' => $invoice->user->id]) }}" class="btn btn-default pull-right">Regresar</a>                            
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="panel-body">
                    <!-- Table -->
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th># publicación</th>
                                    <th>Título publicación</th>
                                    <th>Descripción</th>
                                    <th>Monto</th>
                                    <th>Fecha</th>
                                    <th>Fecha de Vencimiento</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($items))
                                    @foreach($items as $item)
                                    <tr>
                                        <td>{{ $item->publication->id }}</td>
                                        <td>
                                            {{ $item->publication->title }}
                                        </td>
                                        <td>{{ $item->description }}</td>
                                        <td>
                                            {{ number_format($item->amount,2,',','.') }}
                                        </td>
                                        <td>{{ date('d-m-Y h:i a', strtotime($item->created_at)) }}</td>
                                        <td>{{ $item->created_at->addMonth(1)->format('d-m-Y h:i a') }}</td>
                                    </tr>
                                    @endforeach
                                @else
                                <tr><td colspan="6"><div   class="alert alert-warning">No se encontraron registros</div></td></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    @if(count($items))
                        <div class="panel-footer" style="text-align: center">
                            {{ $items->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
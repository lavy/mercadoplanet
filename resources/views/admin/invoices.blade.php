@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- Default panel contents -->
                    <form method="POST" action="{{ route('user_invoices', ['id' => $user->id]) }}">
                        <div class="panel-heading">
                            <a href="{{ url('/admin') }}" class="btn btn-default">Usuarios</a>
                             <div class="clearfix"></div>
                            <h4 class="pull-left">Facturas de {{ $user->first_name }} {{ $user->last_name }}</h4>
                            <div class="input-group col-md-4 pull-right" style="margin-top: 5px">
                                <input type="text" class="form-control" name="invoice-search" value="{{ $invoice_search }}" placeholder="Buscar por número de factura...">
                                <span class="input-group-btn">
                                  <button class="btn btn-default" type="submit">Buscar</button>
                                </span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="panel-body">
                    <!-- Table -->
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th># factura</th>
                                    <th>Estado</th>
                                    <th>Fecha de emisión</th>
                                    <th>Monto</th>
                                    <th>Pago reportado</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($invoices))
                                    @foreach($invoices as $invoice)
                                    <tr>
                                        <td>{{ $invoice->id }}</td>
                                        <td>
                                            @if($invoice->status == 'Open')
                                                Próxima a facturar
                                            @elseif($invoice->status == 'Emitted')
                                                Pendiente de pago
                                            @elseif($invoice->status == 'Paid')
                                                Pagada
                                            @elseif($invoice->status == 'Canceled')
                                                Anulada
                                            @endif
                                            
                                        </td>
                                        <td>
                                            @if($invoice->emitted_at) 
                                                {{ date("d-m-Y", strtotime($invoice->emitted_at)) }} 
                                            @else
                                                No emitida
                                            @endif
                                        </td>
                                        <td>{{ number_format($invoice->getTotal(),2,',','.') }}</td>
                                        <td>@if($invoice->payment_description) Sí @else No @endif</td>
                                        <td>
                                            <a href="{{ route('user_invoice_detail', ['id' => $invoice->id]) }}" title="Detalles"><i class="glyphicon glyphicon-eye-open"></i></a>
                                            @if($invoice->payment_status)
                                                <a href="{{ route('user_invoice_payment', ['id' => $invoice->id]) }}" title="Notificación de pago ({{ $invoice->payment_status }})">
                                                    @if($invoice->payment_status == 'Por revisar')
                                                    <i class="glyphicon glyphicon-info-sign" style="color:#f0ad4e"></i>
                                                    @elseif($invoice->payment_status == 'Aprobado')
                                                        <i class="glyphicon glyphicon-info-sign" style="color:#5cb85c"></i>
                                                    @elseif($invoice->payment_status == 'Rechazado')
                                                        <i class="glyphicon glyphicon-info-sign" style="color: #d9534f"></i>
                                                    @endif
                                                </a>
                                            @else
                                            <i class="glyphicon glyphicon-info-sign" style="color:#aaa" title="Ningún pago notiicado"></i>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                <tr><td colspan="6"><div   class="alert alert-warning">No se encontraron registros</div></td></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    @if(count($invoices))
                        <div class="panel-footer" style="text-align: center">
                            {{ $invoices->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
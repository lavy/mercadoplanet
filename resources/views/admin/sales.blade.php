@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- Default panel contents -->
                    <form method="POST" action="{{ route('user_sales', ['id' => $user->id]) }}">
                        <div class="panel-heading">
                            <a href="{{ url('/admin') }}" class="btn btn-default">Usuarios</a>
                             <div class="clearfix"></div>
                            <h4 class="pull-left">Ventas de {{ $user->first_name }} {{ $user->last_name }}</h4>
                            <div class="input-group col-md-4 pull-right" style="margin-top: 5px">
                                <input type="text" class="form-control" name="sale_search" value="{{ $sale_search }}" placeholder="Buscar por número de venta...">
                                <span class="input-group-btn">
                                  <button class="btn btn-default" type="submit">Buscar</button>
                                </span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="panel-body">
                    <!-- Table -->
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th># compra</th>
                                    <th>Publicación</th>
                                    <th>Comprador</th>
                                    <th>Precio - Cantidad</th>
                                    <th>Calificación Vendedor</th>
                                    <th>Calificación Comprador</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($sales))
                                    @foreach($sales as $sale)
                                    <tr>
                                        <td>{{ $sale->id }}</td>
                                        <td>
                                            <?php $publication = $sale->publication; ?>
                                            @if($publication->status == 'Canceled')
                                            <div style="width: 250px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;display: block;">{{ $publication->title }}</div>
                                            @else
                                            <a href="{{ route('product', ['id' => $publication->id]) }}" target="_blank" style="width: 250px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;display: block;">{{ $publication->title }}</a>
                                            @endif
                                        </td>
                                        <td>
                                            <?php $user = $sale->buyer; ?>
                                            {{ $user->getFullName() }}<br>
                                            {{ $user->username }}<br>
                                            {{ $user->email }}
                                        </td>
                                        <td><span>S/{{ number_format($publication->price, 2, ',', '.') }}</span> x {{ $publication->quantity }} @if($publication->quantity == 1)unidad @else unidades @endif</td>
                                        <td>                                           
                                            
                                            @if($sale->seller_calification === 'Positivo')
                                            <i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i> 
                                            Calificó positivo
                                            <div><a href="{{ route('seller_delete_qualification', ['id' => $sale->id, 'origin' => '2']) }}"  onclick="javascript: return confirm('¿Seguro desea eliminar la calificación del vendedor? (Esta acción es irreversible)')">Eliminar calificación</a></div>
                                            @elseif($sale->seller_calification === 'Negativo')
                                            <i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i> 
                                            Calificó negativo
                                            <div><a href="{{ route('seller_delete_qualification', ['id' => $sale->id, 'origin' => '2']) }}"  onclick="javascript: return confirm('¿Seguro desea eliminar la calificación del vendedor? (Esta acción es irreversible)')">Eliminar calificación</a></div>
                                            @elseif($sale->seller_calification === 'Neutral')
                                            <i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i> 
                                            Calificó neutral
                                            <div><a href="{{ route('seller_delete_qualification', ['id' => $sale->id, 'origin' => '2']) }}"  onclick="javascript: return confirm('¿Seguro desea eliminar la calificación del vendedor? (Esta acción es irreversible)')">Eliminar calificación</a></div>
                                            @elseif(($sale->seller_calification === 'No calificado' && (time() - strtotime($sale->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600))
                                                or ($sale->seller_calification === 'No calificado' && $sale->seller_qualification_modified)
                                            )
                                            No calificó
                                            @else
                                            <i class="fa fa-clock-o" style="font-size: 16px;color:#666"></i> {{ $sale->getTimeToQualify() }} dias para calificar
                                            @endif                                            
                                        </td>
                                        <td>
                                            
                                            @if($sale->buyer_calification === 'Positivo')
                                            <i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i> 
                                            Calificó positivo
                                            <div><a href="{{ route('buyer_delete_qualification', ['id' => $sale->id, 'origin' => '2']) }}"  onclick="javascript: return confirm('¿Seguro desea eliminar la calificación del comprador? (Esta acción es irreversible)')">Eliminar calificación</a></div>
                                            @elseif($sale->buyer_calification === 'Negativo')
                                            <i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i> 
                                            Calificó negativo
                                            <div><a href="{{ route('buyer_delete_qualification', ['id' => $sale->id, 'origin' => '2']) }}"  onclick="javascript: return confirm('¿Seguro desea eliminar la calificación del comprador? (Esta acción es irreversible)')">Eliminar calificación</a></div>
                                            @elseif($sale->buyer_calification === 'Neutral')
                                            <i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i> 
                                            Calificó neutral
                                            <div><a href="{{ route('buyer_delete_qualification', ['id' => $sale->id, 'origin' => '2']) }}"  onclick="javascript: return confirm('¿Seguro desea eliminar la calificación del comprador? (Esta acción es irreversible)')">Eliminar calificación</a></div>
                                            @elseif(($sale->buyer_calification === 'No calificado' && (time() - strtotime($sale->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600))
                                                or ($sale->buyer_calification === 'No calificado' && $sale->buyer_qualification_modified)
                                            )
                                            No calificó
                                            @else
                                            <i class="fa fa-clock-o" style="font-size: 16px;color:#666"></i> {{ $sale->getTimeToQualify() }} dias para calificar
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('transaction_detail', ['id' => $sale->id]) }}" title="Detalles"><i class="glyphicon glyphicon-eye-open"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                <tr><td colspan="6"><div   class="alert alert-warning">No se encontraron registros</div></td></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    @if(count($sales))
                        <div class="panel-footer" style="text-align: center">
                            {{ $sales->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
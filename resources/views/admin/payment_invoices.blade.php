@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- Default panel contents -->
                    <form method="POST" action="{{ route('payment_invoices', ['status' => $status]) }}">
                        <div class="panel-heading">
                            <h4 class="pull-left">Notificaciones de pago</h4>
                            <div class="input-group col-md-4 pull-right" style="margin-top: 5px">
                                <input type="text" class="form-control" name="payment-search" value="{{ $payment_search }}" placeholder="Buscar por número de factura...">
                                <span class="input-group-btn">
                                  <button class="btn btn-default" type="submit">Buscar</button>
                                </span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="panel-body">
                    <div>
                        <a href="{{ route('payment_invoices', ['status' => 'pending']) }}" class="btn btn-default @if($status == 'pending') btn-primary @endif">Por revisar</a>
                        <a href="{{ route('payment_invoices', ['status' => 'approved']) }}" class="btn btn-default @if($status == 'approved') btn-primary @endif">Aprobados</a>
                        <a href="{{ route('payment_invoices', ['status' => 'rejected']) }}" class="btn btn-default @if($status == 'rejected') btn-primary @endif">Rechazados</a>
                    </div>
                    <!-- Table -->
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th># factura</th>
                                    <th>Usuario</th>
                                    <th>Estado</th>
                                    <th>Fecha de emisión</th>
                                    <th>Monto</th>
                                    <th>Pago reportado</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($invoices))
                                    @foreach($invoices as $invoice)
                                    <tr>
                                        <td>{{ $invoice->id }}</td>
                                        <td>{{ $invoice->user->getFullName() }}</td>
                                        <td>
                                            @if($invoice->status == 'Open')
                                                Próxima a facturar
                                            @elseif($invoice->status == 'Emitted')
                                                Pendiente de pago
                                            @elseif($invoice->status == 'Paid')
                                                Pagada
                                            @elseif($invoice->status == 'Canceled')
                                                Anulada
                                            @endif
                                            
                                        </td>
                                        <td>
                                            @if($invoice->emitted_at) 
                                                {{ date("d-m-Y", strtotime($invoice->emitted_at)) }} 
                                            @else
                                                No emitida
                                            @endif
                                        </td>
                                        <td>{{ number_format($invoice->getTotal(),2,',','.') }}</td>
                                        <td>@if($invoice->payment_description) Sí @else No @endif</td>
                                        <td>
                                            @if($invoice->payment_status)
                                                <a href="{{ route('user_invoice_payment', ['id' => $invoice->id]) }}" title="Notificación de pago ({{ $invoice->payment_status }})">
                                                    @if($invoice->payment_status == 'Por revisar')
                                                    <i class="glyphicon glyphicon-info-sign" style="color:#f0ad4e"></i>
                                                    @elseif($invoice->payment_status == 'Aprobado')
                                                        <i class="glyphicon glyphicon-info-sign" style="color:#5cb85c"></i>
                                                    @elseif($invoice->payment_status == 'Rechazado')
                                                        <i class="glyphicon glyphicon-info-sign" style="color: #d9534f"></i>
                                                    @endif
                                                </a>
                                            @else
                                            <i class="glyphicon glyphicon-info-sign" style="color:#aaa" title="Ningún pago notiicado"></i>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                <tr><td colspan="6"><div   class="alert alert-warning">No se encontraron registros</div></td></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    @if(count($invoices))
                        <div class="panel-footer" style="text-align: center">
                            {{ $invoices->appends($query)->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
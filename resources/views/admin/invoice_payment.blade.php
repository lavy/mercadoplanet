@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h4 class="pull-left">Notificación de pago de factura #{{ $invoice->id }} - {{ $invoice->user->getFullName() }}</h4>
                        <a href="{{ route('payment_invoices') }}" class="btn btn-default pull-right">Notificaciones de pago</a>                            
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="panel-body">
                    @if($invoice->payment_status != 'Rechazado')
                    <a href="{{ route('change_payment_status', ['id' => $invoice->id, 'status' => 'rechazar']) }}" class="btn btn-danger">Rechazar pago</a>
                    @endif
                    @if($invoice->payment_status != 'Aprobado')
                    <a href="{{ route('change_payment_status', ['id' => $invoice->id, 'status' => 'aprobar']) }}" class="btn btn-success">Aprobar pago</a>
                    @endif
                    @if($invoice->payment_status != 'Por revisar')
                    <a href="{{ route('change_payment_status', ['id' => $invoice->id, 'status' => 'revisar']) }}" class="btn btn-warning">Por revisar pago</a>
                    @endif
                    <br>
                    <br>
                    <strong>Estatus</strong><br>
                    {{ $invoice->payment_status }}<br>
                    <strong>Descripción</strong><br>
                    {{ $invoice->payment_description }}<br>
                    @if($invoice->image1)
                        <strong>Soportes de pago</strong><br>
                        @if($invoice->image1)
                        <div><a href="{{ route('download-payments', ['file' => $invoice->image1, 'name' => $invoice->name1]) }}" class="btn-link"><i class="glyphicon glyphicon-file"></i> {{ $invoice->name1 }}</a></div>
                        @endif 
                        @if($invoice->image2)
                        <div><a href="{{ route('download-payments', ['file' => $invoice->image2, 'name' => $invoice->name2]) }}" class="btn-link"><i class="glyphicon glyphicon-file"></i> {{ $invoice->name2 }}</a></div>
                        @endif
                        @if($invoice->image3)
                        <div><a href="{{ route('download-payments', ['file' => $invoice->image3, 'name' => $invoice->name3]) }}" class="btn-link"><i class="glyphicon glyphicon-file"></i> {{ $invoice->name3 }}</a></div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
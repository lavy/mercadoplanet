@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- Default panel contents -->
                    <form method="POST" action="{{ route('user_purchases', ['id' => $user->id]) }}">
                        <div class="panel-heading">
                            <a href="{{ url('/admin') }}" class="btn btn-default">Usuarios</a>
                             <div class="clearfix"></div>
                            <h4 class="pull-left">Compras de {{ $user->first_name }} {{ $user->last_name }}</h4>
                            <div class="input-group col-md-4 pull-right" style="margin-top: 5px">
                                <input type="text" class="form-control" name="purchase_search" value="{{ $purchase_search }}" placeholder="Buscar por número de compra...">
                                <span class="input-group-btn">
                                  <button class="btn btn-default" type="submit">Buscar</button>
                                </span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="panel-body">
                    <!-- Table -->
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th># compra</th>
                                    <th>Publicación</th>
                                    <th>Vendedor</th>
                                    <th>Precio - Cantidad</th>
                                    <th>Calificación Vendedor</th>
                                    <th>Calificación Comprador</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($purchases))
                                    @foreach($purchases as $purchase)
                                    <tr>
                                        <td>{{ $purchase->id }}</td>
                                        <td>
                                            <?php $publication = $purchase->publication; ?>
                                            @if($publication->status == 'Canceled')
                                            <div style="width: 250px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;display: block;">{{ $publication->title }}</div>
                                            @else
                                            <a href="{{ route('product', ['id' => $publication->id]) }}" target="_blank" style="width: 250px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;display: block;">{{ $publication->title }}</a>
                                            @endif
                                        </td>
                                        <td>
                                            <?php $user = $publication->user; ?>
                                            {{ $user->getFullName() }}<br>
                                            {{ $user->username }}<br>
                                            {{ $user->email }}
                                        </td>
                                        <td><span>S/{{ number_format($publication->price, 2, ',', '.') }}</span> x {{ $publication->quantity }} @if($publication->quantity == 1)unidad @else unidades @endif</td>
                                        <td>                                           
                                            
                                            @if($purchase->seller_calification === 'Positivo')
                                            <i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i> 
                                            Calificó positivo
                                            <div><a href="{{ route('seller_delete_qualification', ['id' => $purchase->id, 'origin' => '1']) }}"  onclick="javascript: return confirm('¿Seguro desea eliminar la calificación del vendedor? (Esta acción es irreversible)')">Eliminar calificación</a></div>
                                            @elseif($purchase->seller_calification === 'Negativo')
                                            <i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i> 
                                            Calificó negativo
                                            <div><a href="{{ route('seller_delete_qualification', ['id' => $purchase->id, 'origin' => '1']) }}"  onclick="javascript: return confirm('¿Seguro desea eliminar la calificación del vendedor? (Esta acción es irreversible)')">Eliminar calificación</a></div>
                                            @elseif($purchase->seller_calification === 'Neutral')
                                            <i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i> 
                                            Calificó neutral
                                            <div><a href="{{ route('seller_delete_qualification', ['id' => $purchase->id, 'origin' => '1']) }}"  onclick="javascript: return confirm('¿Seguro desea eliminar la calificación del vendedor? (Esta acción es irreversible)')">Eliminar calificación</a></div>
                                            @elseif(($purchase->seller_calification === 'No calificado' && (time() - strtotime($purchase->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600))
                                                or ($purchase->seller_calification === 'No calificado' && $purchase->seller_qualification_modified)
                                            )
                                            No calificó
                                            @else
                                            <i class="fa fa-clock-o" style="font-size: 16px;color:#666"></i> {{ $purchase->getTimeToQualify() }} dias para calificar
                                            @endif                                            
                                        </td>
                                        <td>
                                            
                                            @if($purchase->buyer_calification === 'Positivo')
                                            <i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i> 
                                            Calificó positivo
                                            <div><a href="{{ route('buyer_delete_qualification', ['id' => $purchase->id, 'origin' => '1']) }}"  onclick="javascript: return confirm('¿Seguro desea eliminar la calificación del comprador? (Esta acción es irreversible)')">Eliminar calificación</a></div>
                                            @elseif($purchase->buyer_calification === 'Negativo')
                                            <i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i> 
                                            Calificó negativo
                                            <div><a href="{{ route('buyer_delete_qualification', ['id' => $purchase->id, 'origin' => '1']) }}"  onclick="javascript: return confirm('¿Seguro desea eliminar la calificación del comprador? (Esta acción es irreversible)')">Eliminar calificación</a></div>
                                            @elseif($purchase->buyer_calification === 'Neutral')
                                            <i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i> 
                                            Calificó neutral
                                            <div><a href="{{ route('buyer_delete_qualification', ['id' => $purchase->id, 'origin' => '1']) }}"  onclick="javascript: return confirm('¿Seguro desea eliminar la calificación del comprador? (Esta acción es irreversible)')">Eliminar calificación</a></div>
                                            @elseif(($purchase->buyer_calification === 'No calificado' && (time() - strtotime($purchase->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600))
                                                or ($purchase->buyer_calification === 'No calificado' && $purchase->buyer_qualification_modified)
                                            )
                                            No calificó
                                            @else
                                            <i class="fa fa-clock-o" style="font-size: 16px;color:#666"></i> {{ $purchase->getTimeToQualify() }} dias para calificar
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('transaction_detail', ['id' => $purchase->id]) }}" title="Detalles"><i class="glyphicon glyphicon-eye-open"></i></a>
                                            
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                <tr><td colspan="6"><div   class="alert alert-warning">No se encontraron registros</div></td></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    @if(count($purchases))
                        <div class="panel-footer" style="text-align: center">
                            {{ $purchases->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
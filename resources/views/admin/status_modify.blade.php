@extends('admin.layouts.app')

@section('content')
<div class="container">
    <form method="POST" action="{{ route('status_modify_save') }}">
        <input type="hidden" name="id" value="<?=$publication->id?>">
        {{ csrf_field() }}
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="pull-left">Cambiar estado de la publicación</h4>
                    <a href="{{ route('admin_publications') }}" class="btn btn-default pull-right">Regresar</a>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <!-- Table -->
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>
                                        <label>Título:</label> {{ $publication->title }}<br>
                                        <label>Estado actual:</label> 
                                        @if($publication->status == 'Active')
                                        Activa
                                        @elseif($publication->status == 'Paused')
                                        Pausada
                                        @elseif($publication->status == 'Canceled')
                                        Cancelada
                                        @elseif($publication->status == 'Revision')
                                        En revisión
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Asignar estado</label>
                                        <select name="status" class="form-control">
                                            @if($publication->status != 'Active')
                                            <option value="Active" <?=($publication->status == 'Active' ? 'selected' : '')?>>Activa</option>
                                            @endif
                                            @if($publication->status != 'Revision')
                                            <option value="Revision" <?=($publication->status == 'Revision' ? 'selected' : '')?>>En revisión</option>
                                            @endif
                                            @if($publication->status != 'Canceled')
                                            <option value="Canceled" <?=($publication->status == 'Canceled' ? 'selected' : '')?>>Cancelada</option>
                                            @endif
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Motivo (será enviado por email al vendedor - opcional)</label>
                                        <textarea class="form-control" rows="10" name="status_description"></textarea>
                                    </td>
                                <tr>
                                <tr>
                                    <td colspan="2" style="text-align: right"><button type="submit" class="btn btn-primary" name="status_description">Cambiar Estado</button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
@endsection
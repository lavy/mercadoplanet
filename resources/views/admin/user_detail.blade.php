@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="pull-left">Detalles del usuario</h4>
                    <a href="{{ route('home') }}" class="btn btn-default pull-right">Regresar</a>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <!-- Table -->
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Campo</th>
                                    <th>Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>Identificación</th>
                                    <td>{{ $user->document_type }}: {{ $user->document }}</td>
                                </tr>
                                <tr>
                                    <th>Nombre</th>
                                    <td>{{ $user->first_name }}</td>
                                </tr>
                                <tr>
                                    <th>Apellido</th>
                                    <td>{{ $user->last_name }}</td>
                                </tr>
                                <tr>
                                    <th>Usuario</th>
                                    <td>{{ $user->username }}</td>
                                </tr>
                                <tr>
                                    <th>E-mail</th>
                                    <td>{{ $user->email }}</td>
                                </tr>
                                <tr>
                                    <th>Número de teléfono</th>
                                    <td>{{ $user->phone_number }}</td>
                                </tr>
                                <tr>
                                    <th>Tipo de Vendedor</th>
                                    <td>
                                        @if($user->seller_type=='Normal')
                                        Normal
                                        @else
                                        @if($user->seller_type=='Líder Gold') Oro @else Plata @endif
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Estado del usuario</th>
                                    <td>
                                        @if($user->status == 'Active')
                                            @if($user->suspended)
                                                Suspendido para vender
                                            @else
                                                Activo
                                            @endif
                                        @else
                                            Deshabilitado
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Día de corte de factura</th>
                                    <td>{{ $user->cutoff_day }}</td>
                                </tr>
                                <tr>
                                    <th>Última fecha de suspensión por deuda</th>
                                    <td>{{ date("d-m-Y", strtotime($user->latest_suspension)) }}</td>
                                </tr>
                                <tr>
                                    <th>Fecha de registro en la plataforma</th>
                                    <td>{{ date("d-m-Y h:i a", strtotime($user->created_at)) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
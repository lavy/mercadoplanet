<?php $buyer = $purchase->buyer; ?>
<?php $publication = $purchase->publication; ?>
<?php $seller = $publication->user; ?>
<p>Hola {{ $buyer->first_name }},</p>
<p>{{ $seller->getFullName() }}  te pide cambiar la calificación que le diste por la compra de {{ $publication->title }}.
    Si no obtenemos tu respuesta en 5 días, tendremos que eliminarla.
    No recuerdas cuál fue tu calificación? 
    <br>Esto es lo que has dicho acerca de tu vendedor:</p>
    <strong>Tu calificación</strong><br>
<ul>
    <li>
        @if($purchase->buyer_concreted)
            Concretada
        @else
            No Concretada
        @endif
    </li>
    <li>
        @if($purchase->buyer_qualification == 'Positivo')
            Lo recomiendas
        @elseif($purchase->buyer_qualification == 'Neutral')
            No estas seguro si recomiendas al vendedor
        @else
            No lo recomiendas
        @endif
    </li>
    <li>
        {{ $purchase->buyer_comment }}
    </li>
</ul>
    <br>Esto es lo que ha dicho  tu vendedor:<br><br>
<strong>Calificación de tu vendedor</strong><br>

<ul>
    <li>
        @if($purchase->seller_concreted)
            Concretada
        @else
            No Concretada
        @endif
    </li>
    <li>
        @if($purchase->seller_qualification == 'Positivo')
            Te recomiendan
        @elseif($purchase->seller_qualification == 'Neutral')
            El vendedor no está seguro de recomendarte
        @else
            No te recomiendan
        @endif
    </li>
    <li>
        {{ $purchase->seller_comment }}
    </li>
</ul>

<p>Gracias por confiar en nosotros.</p>

<a href="{{ route('buyer_qualification_edit', ['id' => $purchase->id]) }}">Cambiar calificación</a><br><br>
<a href="{{ route('buyer_qualification_confirm', ['id' => $purchase->id]) }}">No cambiar calificación</a>

<p>
    Atentamente,<br>
{{ config('app.name') }}
</p>
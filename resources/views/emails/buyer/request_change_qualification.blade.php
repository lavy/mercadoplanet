<?php $buyer = $sale->buyer; ?>
<?php $seller = $sale->seller; ?>
<?php $publication = $sale->publication; ?>
<p>Hola {{ $seller->first_name }},</p>
<p>{{ $buyer->getFullName() }}  te pide cambiar la calificación que le diste por la venta de {{ $publication->title }}.
    Si no obtenemos tu respuesta en 5 días, tendremos que eliminarla.
    No recuerdas cuál fue tu calificación? 
    <br>Esto es lo que has dicho acerca de tu comprador:</p>
    <strong>Tu calificación</strong><br>
<ul>
    <li>
        @if($sale->seller_concreted)
            Concretada
        @else
            No Concretada
        @endif
    </li>
    <li>
        @if($sale->seller_qualification == 'Positivo')
            Lo recomiendas
        @elseif($sale->seller_qualification == 'Neutral')
            No estas seguro si recomiendas al comprador
        @else
            No lo recomiendas
        @endif
    </li>
    <li>
        {{ $sale->seller_comment }}
    </li>
</ul>
    <br>Esto es lo que ha dicho  tu comprador:<br><br>
<strong>Calificación de tu comprador</strong><br>

<ul>
    <li>
        @if($sale->buyer_concreted)
            Concretada
        @else
            No Concretada
        @endif
    </li>
    <li>
        @if($sale->buyer_qualification == 'Positivo')
            Te recomiendan
        @elseif($sale->buyer_qualification == 'Neutral')
            El comprador no está seguro de recomendarte
        @else
            No te recomiendan
        @endif
    </li>
    <li>
        {{ $sale->buyer_comment }}
    </li>
</ul>

<p>Gracias por confiar en nosotros.</p>

<a href="{{ route('seller_qualification_edit', ['id' => $sale->id]) }}">Cambiar calificación</a><br><br>
<a href="{{ route('seller_qualification_confirm', ['id' => $sale->id]) }}">No cambiar calificación</a>

<p>
    Atentamente,<br>
{{ config('app.name') }}
</p>
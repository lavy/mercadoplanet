<div>
    Hola {{ $user->getFullName() }},
    <br><br>
    a la brevedad posible estaremos dandole respuesta a tu solicitud.
</div>
<h2>Datos de la solicitud: </h2>
<div>    
    <strong>Usuario:</strong> {{ $user->username }}
</div>
<div>
    <strong>Nombre:</strong> {{ $user->getFullName() }}
</div>
<div>
    <strong>Asunto:</strong> {{ $data['subject'] }}
</div>
<div>
    <strong>Consulta:</strong> {{ $data['content'] }}
</div>



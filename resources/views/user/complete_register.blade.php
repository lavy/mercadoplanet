@extends('layouts.app')

@section('title', 'Completar registro')   

@section('content')

<section class="mainContent clearfix logIn">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Completar registro</h3></div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/completeregister') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('document_type') ? ' has-error' : '' }}">
                            <label for="document_type">Tipo de documento de identificación{!! required() !!}</label>
                            <div class "btn-group" data-toogle="buttons">
                                <label>
                                    <input id="document_type_1" type="radio" name="document_type" value="DNI" autofocus @if(old('document_type') == "DNI") checked="checked" @endif>
                                    D.N.I.
                                </label>
                                 <label>
                                    <input id="document_type_2" type="radio" name="document_type" value="CE" autofocus @if(old('document_type') == "CE") checked="checked" @endif>
                                    C.E.
                                </label>
                                @if ($errors->has('document_type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('document_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('document') ? ' has-error' : '' }}">
                            <label for="document">Número de documento{!! required() !!}</label>

                            <div>
                                <input id="document" type="text" class="form-control" name="document" value="{{ old('document') }}">

                                @if ($errors->has('document'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('document') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group{{ $errors->has('address_type') ? ' has-error' : '' }}">
                            <label for="address_type">Tipo de dirección{!! required() !!}</label>
                            <div class "btn-group" data-toogle="buttons">
                                <label>
                                    <input id="address_type_1" type="radio" name="address_type" value="Calle" autofocus @if(old('address_type') == "Calle") checked="checked" @endif>
                                    Calle
                                </label>
                                <label>
                                    <input id="address_type_2" type="radio" name="address_type" value="Avenida" autofocus @if(old('address_type') == "Avenida") checked="checked" @endif>
                                    Avenida
                                </label>
                                <label>
                                    <input id="address_type_3" type="radio" name="address_type" value="Paje" autofocus @if(old('address_type') == "Paje") checked="checked" @endif>
                                    Paje
                                </label>
                                <label>
                                    <input id="address_type_4" type="radio" name="address_type" value="Jirón" autofocus @if(old('address_type') == "Jirón") checked="checked" @endif>
                                    Jirón
                                </label>
                                @if ($errors->has('address_type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            
                        <div class="form-group{{ $errors->has('address_text') ? ' has-error' : '' }}">
                            <label for="address_text">Dirección{!! required() !!}</label>

                            <div>
                                <input id="address_text" type="text" class="form-control" name="address_text" value="{{ old('address_text') }}">

                                @if ($errors->has('address_text'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address_text') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            
                        <div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
                            <label for="number">Número{!! required() !!}</label>

                            <div>
                                <input id="number" type="text" class="form-control" name="number" value="{{ old('number') }}">

                                @if ($errors->has('number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            
                            
                        <div class="form-group{{ $errors->has('other_data') ? ' has-error' : '' }}">
                            <label for="other_data">Otros datos</label>

                            <div>
                                <input id="other_data" type="text" class="form-control" name="other_data" value="{{ old('other_data') }}">

                                @if ($errors->has('other_data'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('other_data') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            
                        <div class="form-group{{ $errors->has('reference') ? ' has-error' : '' }}">
                            <label for="reference">Referencia</label>

                            <div>
                                <input id="reference" type="text" class="form-control" name="reference" value="{{ old('reference') }}">

                                @if ($errors->has('reference'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('reference') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            
                        <div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
                            <label for="deparment">Departamento{!! required() !!}</label>

                            <div>
                                <select class="form-control" id="department" name="department">
                                    <option value="">Seleccione</option>
                                    @foreach($departments as $department)
                                    <option value="{{$department->id}}" @if(old('department') == $department->id) selected="selected" @endif>{{$department->name}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('department'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('department') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            
                        <div class="form-group{{ $errors->has('province') ? ' has-error' : '' }}">
                            <label for="province">Provincia{!! required() !!}</label>

                            <div>
                                <select class="form-control" id="province" name="province">
                                <option value="">Seleccione</option>   
                                @foreach($provinces as $province)
                                <option value="{{$province->id}}" @if(old('province') == $province->id) selected="selected" @endif>{{$province->name}}</option>
                                @endforeach
                                </select>

                                @if ($errors->has('province'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('province') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            
                        <div class="form-group{{ $errors->has('district') ? ' has-error' : '' }}">
                            <label for="district">Distrito{!! required() !!}</label>

                            <div>
                                <select class="form-control" id="district" name="district">
                                <option value="">Seleccione</option>
                                @foreach($districts as $district)
                                <option value="{{$district->id}}" @if(old('district') == $district->id) selected="selected" @endif>{{$district->name}}</option>
                                @endforeach
                                </select>

                                @if ($errors->has('district'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('district') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <div>
                                <button type="submit" class="btn btn-primary btn-block">
                                    Enviar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    </section>
@endsection

@section('scripts')
    @parent
    <script>
        $(document).ready(function() {
           
            $("#department").change(function (){
                $("#province").empty();
                $("#province").append($("<option>", {
                    value: '',
                    text: 'Seleccione'
                }));
                $("#district").empty();
                $("#district").append($("<option>", {
                    value: '',
                    text: 'Seleccione'
                }));
                      
                if ($("#department").val() == '') {
                    return;
                }
                    
                $.ajax({
                    url: "{{ url('/searchprovinces') }}",
                    type: "POST",
                    data: {
                        id: $("#department").val(),
                        _token: "{{ csrf_token() }}", 
                    },
                    success: function(res) {
                 
                        for (var i = 0; i < res.length; ++i) {
                            $("#province").append($("<option>", {
                                value: res[i].id,
                                text: res[i].name
                            }));
                        }
                    }
                });
            });
            
            $("#province").change(function (){
                $("#district").empty();
                $("#district").append($("<option>", {
                    value: '',
                    text: 'Seleccione'
                }));
                      
                if ($("#province").val() == '') {
                    return;
                }
                    
                $.ajax({
                    url: "{{ url('/searchdistricts') }}",
                    type: "POST",
                    data: {
                        id: $("#province").val(),
                        _token: "{{ csrf_token() }}", 
                    },
                    success: function(res) {
                            
                        for (var i = 0; i < res.length; ++i) {
                            $("#district").append($("<option>", {
                                value: res[i].id,
                                text: res[i].name
                            }));
                        }
                    }
                });
            });
        })
    </script>
@endsection
@extends('layouts.app')

@section('title', 'Formulario de contacto')

@section('content')
<link href="/css/jquery.filer.css" rel="stylesheet">
<section class="mainContent clearfix logIn">
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Formulario de contacto</h3></div>
                <div class="panel-body">
                    @if (Session::has('message'))
                    <div class="row">
                        <div class="col-xs-12">
                          <div class="alert  {{ Session::get('alert-class', 'alert-info') }}" role="alert">
                              <strong>{{ Session::get('message') }}</strong>
                          </div>
                        </div>
                    </div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('send_contact') }}"  enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('asunto') ? ' has-error' : '' }}">
                            <label for="asunto">Asunto{!! required() !!}</label>

                            <div>
                                <input id="asunto" type="text" class="form-control" name="asunto" value="{{ old('asunto') }}" autofocus>

                                @if ($errors->has('asunto'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('asunto') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('contenido') ? ' has-error' : '' }}">
                            <label for="contenido">Contenido{!! required() !!}</label>

                            <div>
                                <textarea id="contenido" type="text" style="height:200px" class="form-control" name="contenido">{{ old('contenido') }}</textarea>

                                @if ($errors->has('contenido'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('contenido') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-10">
                                 <input type="file" name="files[]" id="filer_input" multiple="multiple">
                            </div
                        </div>
                        <div class="form-group">
                            <div>
                                <button type="submit" class="btn btn-primary btn-block">
                                    Enviar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection

@section('scripts')
    @parent
    <script src="/js/jquery.filer.min.js" type="text/javascript"></script>
    <script>
	$(document).ready(function(){

            $('#filer_input').filer({
                    limit: 3,
                    maxSize: 3,
                    extensions: ["jpg", "png", "gif", "pdf"],
                    showThumbs: true,
                    addMore: true,
                    allowDuplicates: false
            });

	});
    </script>
@endsection

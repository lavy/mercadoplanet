@extends('layouts.myaccount')

@section('title', 'Detalles de la compra')

@section('myaccountcontent')
<link href="/css/jquery.filer.css" rel="stylesheet">
<h3>Detalles de la venta</h3>
<div class="row">
    <div class="alert alert-info">Venta # {{ $sale->id }} en fecha: {{ date("d-m-Y h:i A", strtotime($sale->created_at)) }}</div>
</div>
<div class="row">
    <div class="col-xs-12">
	<h4>Producto</h4>
    </div>
    <div class="col-xs-12">
	<div style="float: left">
	    @if(count($p->images))
	    <img src="{{ url('/photo/40x40?url='.urlencode($p->images[0]->name)) }}">
	    @else
	    <img src="{{ url('/photo/40x40?url=nophoto.jpg') }}">
	    @endif
	</div>
	<div style="float:left; margin-left: 10px;line-height: 20px ">
	    <div><a href="{{ route('product',[$p->id]) }}" style="padding: 0" class="btn-link">{{ $p->title }}</a></div>
	    <div style="color:#999">S/{{ number_format($p->price, 2, ',', '.') }} x {{ $sale->quantity }} @if($sale->quantity == 1)unidad @else unidades @endif</div>
	</div>
    </div>
</div>
<br><br>
<div class="row">
    <div class="col-xs-12">
	<h4>Comprador</h4>
    </div>
    <div class="col-xs-12" style="line-height: 25px">
	<strong>{{ $sale->buyer->getFullName() }}</strong><br>
	<div><a href="{{ route('reputationbuyer', ['user_id' => $sale->buyer->id]) }}" class="btn-link">{{ strtoupper($sale->buyer->username)}} ({{ $sale->buyer->getPoints() }})</a></div>
	<div style="color:#666">{{ $sale->buyer->email }}</div>
	<div>{{ $sale->buyer->phone_number }}</div>
    </div>
</div>
<br><br>
    <div class="row">
	<div class="col-xs-12">
	    <h4 style="margin-bottom: 30px">Calificación</h4>
	</div>
        @if($sale->seller_calification !== 'No calificado' && (time() - strtotime($sale->created_at)) < (config('app.global.days_to_qualify') * 24 * 3600))
        
        <div class="col-xs-12" style="margin-bottom: 10px">
            @if(($sale->seller_concreted and $sale->buyer_concreted) or ($sale->seller_concreted and $sale->buyer_calification == 'No calificado'))
            <i class="fa fa-check" style="color:green; font-size: 16px"></i> Concretado
            @elseif((!$sale->seller_concreted and !$sale->buyer_concreted) or (!$sale->seller_concreted and $sale->buyer_calification == 'No calificado'))
            <i class="fa fa-remove" style="color:red; font-size: 16px"></i> Cancelado
            @else
            <div style="float:left"><i class="fa fa-ban" style="color:red; font-size: 16px"></i> Contradicción</div>
            @endif
            <div style="clear: both"></div>
        </div>
        @endif
	<div class="col-xs-12" style="margin-bottom: 10px"> 
            @if($sale->seller_calification === 'Positivo')
            <a href="" data-toggle="modal" data-target="#qualification-modal" class="btn-link" data-purchase="{{ $sale->id }}">
            <i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i> 
            Calificaste positivo
            </a>
            @elseif($sale->seller_calification === 'Negativo')
            <a href="" data-toggle="modal" data-target="#qualification-modal" class="btn-link" data-purchase="{{ $sale->id }}">
            <i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i> 
            Calificaste negativo
            </a>
            @elseif($sale->seller_calification === 'Neutral')
            <a href="" data-toggle="modal" data-target="#qualification-modal" class="btn-link" data-purchase="{{ $sale->id }}">
            <i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i> 
            Calificaste neutral
            </a>
            @elseif(($sale->seller_calification === 'No calificado' && (time() - strtotime($sale->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600))
                or ($sale->seller_calification === 'No calificado' && $sale->seller_qualification_modified)
            )
            No calificaste 
            @else
            <a href="{{ route('seller_qualification', ['id' => $sale->id]) }}" class="btn-link"><i class="fa fa-clock-o" style="font-size: 16px;color:#666"></i> {{ ceil(20 - ((time() - strtotime($sale->created_at))/ (24 * 3600))) }} dias para calificar</a>
            @endif
	</div>
            @if($sale->seller_calification != 'No calificado')
                @if($sale->buyer_calification == 'No calificado')
                    @if((time() - strtotime($sale->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600))
                    <div class="col-xs-12">No te calificaron</div>
                    @else
                    <div class="col-xs-12">{{ $sale->getTimeToQualify() }} para que califique</div>
                    @endif
                @else
                    @if($sale->buyer_calification === 'Positivo')
                    <div class="col-xs-12"><i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i>
                    Te calificaron positivo</div>
                    @elseif($sale->buyer_calification === 'Negativo')
                    <div class="col-xs-12"><i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i>
                    Te calificaron  negativo</div>
                    @elseif($sale->buyer_calification === 'Neutral')
                    <div class="col-xs-12"><i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i>
                    Te calificaron neutral</div>
                    @endif
                    @if(!$sale->seller_request_change_qualification)
                    <div class="col-xs-12"><a href="{{ route('seller_request_change_qualification', ['id' => $sale->id]) }}" class="btn-link">Solicitar cambio de calificación</a></div>
                    @endif
                @endif
            @endif
    </div>

<br><br>
    <div class="row">
	<div class="col-xs-12">
	    <h4 style="margin-bottom: 10px">Cobro</h4>
	</div>
        <div class="col-xs-12">
            @if($sale->charged) 
                <div style="line-height: 30px"><i  class="fa fa-money" style="color: #47bac1"></i> cobrado</div>
                <a href="{{ route('charged', ['id' => $sale->id]) }}" class="btn-link">Marcar como no cobrado</a>
            @else
                <div style="line-height: 30px"><i  class="fa fa-money"></i> no cobrado</div>
                <a href="{{ route('charged', ['id' => $sale->id]) }}" class="btn-link">Marcar como cobrado</a>
            @endif
        </div>
    </div>
<br><br>
<form action="{{ route('save_message', ['id' => $sale->id]) }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="row" id="messages">
	<div class="col-xs-12">
	    <h4 style="margin-bottom: 30px">Mensajes</h4>
	</div>
	@foreach($messages as $m)
	<div class="row">
	@if($m->user_id == $sale->buyer_id)
	<div class="col-xs-10">	    
                <div class="popover-qualification">
                    <div class="popover right">
                      <div class="arrow"></div>
                      <div class="popover-content">
			  <p>
			    <strong>{{ $sale->buyer->first_name }}</strong><br>
			    {{ $m->message }} 
			    @if($m->image1)
			    <div><a href="{{ route('download', ['file' => $m->image1, 'name' => $m->name1]) }}" class="btn-link"><i class="fa fa-file"></i> {{ $m->name1 }}</a></div>
			    @endif 
			    @if($m->image2)
			    <div><a href="{{ route('download', ['file' => $m->image2, 'name' => $m->name2]) }}" class="btn-link"><i class="fa fa-file"></i> {{ $m->name2 }}</a></div>
			    @endif
			    @if($m->image3)
			    <div><a href="{{ route('download', ['file' => $m->image3, 'name' => $m->name3]) }}" class="btn-link"><i class="fa fa-file"></i> {{ $m->name3 }}</a></div>
			    @endif
			    <small style="float:right;">{{ date("d/m/Y h:i A", strtotime($m->created_at)) }}</small>
			  </p>
                      </div>
                    </div>
                </div>
	</div>
	@else
	<div class="col-xs-10 col-xs-offset-2">	    
                <div class="popover-qualification">
                    <div class="popover left">
                      <div class="arrow"></div>
                      <div class="popover-content">
			  <p>{{ $m->message }} 
			    @if($m->image1)
			    <div><a href="{{ route('download', ['file' => $m->image1, 'name' => $m->name1]) }}" class="btn-link"><i class="fa fa-file"></i> {{ $m->name1 }}</a></div>
			    @endif 
			    @if($m->image2)
			    <div><a href="{{ route('download', ['file' => $m->image2, 'name' => $m->name2]) }}" class="btn-link"><i class="fa fa-file"></i> {{ $m->name2 }}</a></div>
			    @endif
			    @if($m->image3)
			    <div><a href="{{ route('download', ['file' => $m->image3, 'name' => $m->name3]) }}" class="btn-link"><i class="fa fa-file"></i> {{ $m->name3 }}</a></div>
			    @endif
			    <small style="float:right;">{{ date("d/m/Y h:i A", strtotime($m->created_at)) }}</small>
			  </p>
                      </div>
                    </div>
                </div>
	</div>
	@endif
	</div>
	@endforeach
	<div class="col-xs-12" style="margin-top: 20px">
	    <textarea class="form-control" name="message" id="message" style="height: 110px"></textarea>
	    <div style="font-size: 12px;">Quedan <span id="num-chars">500</span> caracteres.</div>
	</div>
    </div>
    <br>
    <div class="row">
	<div class="col-md-10">
	     <input type="file" name="files[]" id="filer_input" multiple="multiple">
	</div>
	<div class="col-md-2">
	    <button type="submit" class="btn btn-primary" id="btn-message" disabled="true">Enviar mensaje</button>
	</div>
    </div>
</form>
<br>
<div class="row" id="notes">
    <div class="col-xs-12">
	<h4>Notas</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <textarea class="form-control" name="note" id="note" placeholder="Escribe una nota..."></textarea>
        <div style="font-size: 12px;">Quedan <span id="num-chars2">250</span> caracteres.</div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <button type="submit" class="btn btn-primary" id="btn-note" disabled="true">Guardar</button><img src="/img/assets/ajax-loader.gif" id="spinner2" style="display: none">
    </div>
</div>
<br>
<div id="notes-container">
@foreach($notes as $note)
    <div class="row" style="height: 25px">
        <div class="col-md-8">
            <div style="float:left"><i class="fa fa-pencil-square-o" style="font-size: 16px; color: #999"></i></div>
            <div style="float:left; width: 60%; margin-left: 5px;" class="note-description">{{ $note->description }}</div>
            <div style="float:left; width: 60%; margin-left: 5px;display: none"  class="note-edit-description">
                <textarea class="form-control" data-note-id="{{ $note->id }}">{{ $note->description }}</textarea>
                <div style="font-size: 12px;">Quedan <span class="num-chars3">{{ 250 - strlen($note->description) }}</span> caracteres.</div>
                <button class="btn-link btn-save">Guardar</button>
                <button class="btn-link btn-cancel">Cancelar</button>
            </div>
            <div style="float:left; width: 15%; display: none" class="note-options"><button class="btn-link btn-mod-option">Modificar</button></div>
            <div style="float:left; width: 15%; display: none" class="note-options"><button class="btn-link btn-del-option">Eliminar</button></div>
        </div>
    </div>
@endforeach
</div>

<div class="modal fade" id="qualification-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Calificación</h4>
      </div>
      <div class="modal-body">
	  <img src="/img/assets/ajax-loader.gif" id="spinner">
	  <div class="modal-container"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
    @parent
    <script src="/js/jquery.filer.min.js" type="text/javascript"></script>
    <script>
	$(document).ready(function(){

            $(document).on({
                mouseenter: function () {
                    $(this).find('.note-options').show();         
                },
                mouseleave: function () {
                    $(this).find('.note-options').hide();         
                }
            }, '#notes-container .row');
            
            $(document).on('click', '.btn-mod-option', function() {
                $(this).parent().parent().find('.note-edit-description').show();
                $(this).parent().parent().find('.note-description').hide();
            });
            
            $(document).on('click', '.btn-del-option', function() {
                var row = $(this).parent().parent().parent();
                var note_id = $(this).parent().parent().find('textarea').attr('data-note-id');
                $.ajax({
                    method: "POST",
                    url: "{{url('/delete-note')}}",
                    cache: false,
                    data: { id: '{{  $sale->id }}', note_id: note_id, _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
		    var output = '';
                    if(msg.is_ok) {
                        row.remove();

                    }
                });
            });
            
            $(document).on('click', '.btn-cancel', function() {
                $(this).parent().parent().find('.note-edit-description').hide();
                $(this).parent().parent().find('.note-description').show();
            });
            
            $(document).on('click', '.btn-save', function() {
                $('.btn-save').prop('disabled', true);
                var note_id = $(this).parent().find('textarea').attr('data-note-id');
                var note_value = $(this).parent().find('textarea').val();
                var textarea = $(this).parent().parent().find('.note-edit-description');
                var row = $(this).parent().parent().find('.note-description');
                $.ajax({
                    method: "POST",
                    url: "{{url('/update-note')}}",
                    cache: false,
                    data: { id: '{{  $sale->id }}', note_id: note_id, note: note_value, _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
		    var output = '';
                    if(msg.is_ok) {
                        var resp = JSON.parse(msg.value);
                        row.text(resp.description);
                        textarea.hide();
                        row.show();
                        $('.btn-save').prop('disabled', false);

                    }
                    $("#spinner2").hide();
                });
            });
            
            $('#filer_input').filer({
                    limit: 3,
                    maxSize: 3,
                    extensions: ["jpg", "png", "gif", "pdf"],
                    showThumbs: true,
                    addMore: true,
                    allowDuplicates: false
            });

            var maxChars = 500;
            $("#message").keyup(controlChars);
            
            var maxChars2 = 250;
            $("#note").keyup(controlChars2);
            $(document).on('keyup', ".note-edit-description textarea", controlChars3);


            function controlChars(e) {
                var rest = maxChars - $(this).val().length;
                if(rest < 0) rest = 0;
                if($(this).val().length > 0) $("#btn-message").prop('disabled', false);
                else $("#btn-message").prop('disabled', true);
                $("#num-chars").text(rest);
                if (e.which < 0x20) {
                        // e.which < 0x20, then it's not a printable character
                        // e.which === 0 - Not a character
                        return;     // Do nothing
                }
                if ($(this).val().length == maxChars) {
                        e.preventDefault();
                } else if ($(this).val().length > maxChars) {
                        // Maximum exceeded
                        $(this).val($(this).val().substring(0, maxChars));
                }
            }
            
            function controlChars2(e) {
                var rest = maxChars2 - $(this).val().length;
                if(rest < 0) rest = 0;
                if($(this).val().length > 0) $("#btn-note").prop('disabled', false);
                else $("#btn-note").prop('disabled', true);
                $("#num-chars2").text(rest);
                if (e.which < 0x20) {
                        // e.which < 0x20, then it's not a printable character
                        // e.which === 0 - Not a character
                        return;     // Do nothing
                }
                if ($(this).val().length == maxChars2) {
                        e.preventDefault();
                } else if ($(this).val().length > maxChars2) {
                        // Maximum exceeded
                        $(this).val($(this).val().substring(0, maxChars2));
                }
            }
            
            function controlChars3(e) {
                var rest = maxChars2 - $(this).val().length;
                if(rest < 0) rest = 0;
                if($(this).val().length > 0) $(this).parent().find('.btn-save').prop('disabled', false);
                else $(this).parent().find('.btn-save').prop('disabled', true);
                $(this).parent().find('.num-chars3').text(rest);
                if (e.which < 0x20) {
                        // e.which < 0x20, then it's not a printable character
                        // e.which === 0 - Not a character
                        return;     // Do nothing
                }
                if ($(this).val().length == maxChars2) {
                        e.preventDefault();
                } else if ($(this).val().length > maxChars2) {
                        // Maximum exceeded
                        $(this).val($(this).val().substring(0, maxChars2));
                }
            }

            
	  $('#qualification-modal').on('show.bs.modal', function (event) {
	    var button = $(event.relatedTarget);
                var recipient = button.data('purchase');
                var modal = $(this);
                $("#spinner").show();
	    modal.find('.modal-container').html('');
	    $.ajax({
                    method: "POST",
                    url: "{{url('/qualification-seller')}}",
                    cache: false,
                    data: { id: recipient, _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
		    var output = '';
                    if(msg.is_ok) {
			var resp = JSON.parse(msg.value);
			if(resp.seller_qualification == 'Positivo') {
			    output += '<div><i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i> Calificaste positivo</div>';
			}
			if(resp.seller_qualification == 'Neutral') {
			    output += '<div><i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i> Calificaste neutral</div>';
			}
			if(resp.seller_qualification == 'Negativo') {
			    output += '<div><i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i> Calificaste negativo</div>';
			}
			
			if(resp.seller_qualification != 'No calificado') {
			    output += '<div style="margin-top: 5px">"'+resp.seller_comment+'"</div>';
                            if(resp.buyer_reply !== '' && resp.buyer_reply) {
                                output += '<div style="margin-top: 5px;margin-left: 15px">Replica: "'+resp.buyer_reply+'"</div>';
                            }
			    if(resp.can_modify == 'true') {
				output += '<a href="{{ route('seller_qualification_edit') }}?id='+recipient+'" class="btn-link">Modificar</a>';
			    }
			}
			if(resp.buyer_qualification != 'No calificado') {
			    output += '<hr>';
			    if(resp.buyer_qualification == 'Positivo') {
				output += '<div><i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i> Te calificó positivo</div>';
			    }
			    if(resp.buyer_qualification == 'Neutral') {
				output += '<div><i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i> Te calificó neutral</div>';
			    }
			    if(resp.buyer_qualification == 'Negativo') {
				output += '<div><i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i> Te calificó negativo</div>';
			    }
			    output += '<div style="margin-top: 5px">"'+resp.buyer_comment+'"</div>';
                            
                            if(resp.seller_reply !== '' && resp.seller_reply) {
                                output += '<div style="margin-top: 5px;margin-left: 15px">Replica: "'+resp.seller_reply+'"</div>';
                            }
                            
                            if(resp.can_modify == 'true') {
				output += '<a href="{{ route('seller_reply_edit') }}?id='+recipient+'" class="btn-link">Replicar</a>';
			    }
			}
			$("#spinner").hide();
                    }
		    
		    modal.find('.modal-container').html(output);
                });
	    
	    //modal.find('.modal-body').text('New message to ' + recipient);
	    });
            
            $("#btn-note").click(function() {
                var value = $("#note").val();                
                $("#spinner2").show();
                $.ajax({
                    method: "POST",
                    url: "{{url('/save-note')}}",
                    cache: false,
                    data: { id: '{{  $sale->id }}', note: value, _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
		    var output = '';
                    if(msg.is_ok) {
			$("#note").val('');
                        $("#btn-note").prop('disabled', true);
                        $("#num-chars2").text(maxChars2);
                        var resp = JSON.parse(msg.value);
                        var output = '';
                        output += '<div class="row" style="height: 25px">';
                        output += '<div class="col-md-8">';
                        output += '<div style="float:left"><i class="fa fa-pencil-square-o" style="font-size: 16px; color: #999"></i></div>';
                        output += '<div style="float:left; width: 60%; margin-left: 5px;" class="note-description">'+htmlEntities(resp.description)+'</div>';
                        output += '<div style="float:left; width: 60%; margin-left: 5px;display: none"  class="note-edit-description">';
                        output += '<textarea class="form-control" data-note-id="'+resp.id+'">'+htmlEntities(resp.description)+'</textarea>';
                        output += '<div style="font-size: 12px;">Quedan <span class="num-chars3">'+(250 - htmlEntities(resp.description).length)+'</span> caracteres.</div>';
                        output += '<button class="btn-link btn-save">Guardar</button>';
                        output += '<button class="btn-link btn-cancel">Cancelar</button>';
                        output += '</div>';
                        output += '<div style="float:left; width: 15%; display: none" class="note-options"><button class="btn-link btn-mod-option">Modificar</button></div>';
                        output += '<div style="float:left; width: 15%; display: none" class="note-options"><button class="btn-link btn-del-option">Eliminar</button></div>';
                        output += '</div>';
                        output += '</div>';
                        

                        $("#notes-container").prepend(output);
                    }
                    $("#spinner2").hide();
                });
            });
            
            
            function goToNotes() {
              var target = $("#notes");

              $('html, body').animate({
                  'scrollTop': target.offset().top-80
              }, 500, 'swing', function () { });
            }
            
            function goToMessages() {
              var target = $("#messages");

              $('html, body').animate({
                  'scrollTop': target.offset().top-80
              }, 500, 'swing', function () { });
            }
            
            @if($go_notes)
            setTimeout(function(){ goToNotes(); }, 1000);
            @endif
            
            @if($go_messages)
            setTimeout(function(){ goToMessages(); }, 1000);
            @endif
            
            function htmlEntities(str) {
                return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
            }
	});
    </script>
@endsection
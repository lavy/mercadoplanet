@extends('layouts.myaccount')

@section('title', 'Resumen')

@section('myaccountcontent')
<div style="margin-bottom: -15px;"><h3>Resumen</h3></div>
    <div class="summary-header"><i class="fa fa-comments-o fa-light-gray"></i><span class="title-summary-light-gray">Preguntas</span></div>
    <hr />
    <div class="summary-item"><div class="@if($num_questions) box-number-enabled @else box-number-disabled @endif">{{ $num_questions }}</div> @if($num_questions) <a href="{{ route('questions') }}" class="btn-link">sin responder</a> @else sin responder @endif</div>
    
    <div class="summary-header"><i class="fa fa-tag fa-light-gray"></i><span class="title-summary-light-gray">Publicaciones</span></div>
    <hr />
    <div class="summary-item"><div class="@if($num_actives) box-number-enabled @else box-number-disabled @endif">{{ $num_actives }}</div>@if($num_actives)<a href="{{ route('publications', ['status' => 'active']) }}" class="btn-link">Activas</a>@else Activas @endif</div>
    <hr />
    <div class="summary-item"><div class="@if($num_paused) box-number-enabled @else box-number-disabled @endif">{{ $num_paused }}</div>@if($num_paused)<a href="{{ route('publications', ['status' => 'paused']) }}" class="btn-link">Pausadas</a>@else Pausadas @endif</div>
    <hr />
    <div class="summary-item"><div class="@if($num_finished) box-number-enabled @else box-number-disabled @endif">{{ $num_finished }}</div>@if($num_finished)<a href="{{ route('publications', ['status' => 'finished']) }}" class="btn-link">Finalizadas</a>@else Finalizadas @endif</div>
    
    <div class="summary-header"><i class="fa fa-shopping-cart fa-light-gray"></i><span class="title-summary-light-gray">Operaciones</span></div>
    <hr />
    <div class="summary-item"><div class="@if($num_purchases) box-number-enabled @else box-number-disabled @endif">{{ $num_purchases }}</div>@if($num_purchases)<a href="{{ route('purchases') }}" class="btn-link">Compras sin calificar</a>@else Compras sin calificar @endif</div>
    <hr />
    <div class="summary-item"><div class="@if($num_sales) box-number-enabled @else box-number-disabled @endif">{{ $num_sales }}</div>@if($num_sales)<a href="{{ route('sales', ['qualification' => '1']) }}" class="btn-link">Ventas sin calificar</a>@else Ventas sin calificar @endif</div>
    <hr />
    <div class="summary-item"><div class="@if($num_sales_no_buyer) box-number-enabled @else box-number-disabled @endif">{{ $num_sales_no_buyer }}</div>@if($num_sales_no_buyer)<a href="{{ route('sales', ['qualification' => '5']) }}" class="btn-link">Ventas sin calificación del comprador</a>@else Ventas sin calificación del comprador @endif</div>
@endsection
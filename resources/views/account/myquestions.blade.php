@extends('layouts.myaccount')

@section('title', 'Favoritos')

@section('myaccountcontent')
<h3>Preguntas</h3>
@if(count($pubs))
    @foreach($pubs as $p)
    <div class="row" style="margin-top: 10px;margin-top: 30px;background-color: #f6f6f6;">
        <div class="col-md-8" style="padding: 10px 15px;">
          <?php $image = \App\Image::where('publication_id', $p->id)->first(); ?>
          @if($image)
          <img src="{{ url('/photo/50x50?url='.urlencode($image->name)) }}" style="margin-bottom: 10px;">
          @else
          <img src="{{ url('/photo/50x50?url=nophoto.jpg') }}" style="margin-bottom: 10px;">
          @endif
          <a href="{{ route('product',[$p->id]) }}" class="btn btn-link">{{ $p->title }}</a>
          <span>S/{{ number_format($p->price, 2, ',', '.') }}</span>
          <span class="ends_at">
          @if($p->status === 'Paused')
          Publicación pausada
          @endif
          @if($p->status === 'Finished')
          Publicación finalizada
          @endif
          @if($p->status === 'Active')
          Finaliza <span class="moment-ends" data-date="{{ $p->ends_at }}"></span>
          @endif
          </span>
        </div>
        <div class="col-md-4" style="text-align: right;line-height: 80px;">
            
            <div>                      
                <div class="btn-group">
                  @if($p->status === 'Active')
                  <a href="{{ route('product',['id' => $p->id, 'go_buy' => 'true']) }}" class="btn btn-primary">Comprar</a>
                  @else
                  <a href="{{ route('product',['id' => $p->id]) }}" class="btn btn-primary">Ver publicación</a>
                  @endif

                  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-list"></i> <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    @if($p->status == 'Active')
                    <li><a href="{{ route('product',['id' => $p->id, 'go_questions' => 'true']) }}">Preguntar</a></li>
                    @endif
                    <li><a href="javascript: void(0);" onclick="javascript: delete_questions({{ $p->id }})">Eliminar todas las preguntas</a></li>
                  </ul>
                </div>
            </div>
        </div>
    </div>
    <?php $questions = Auth::user()->questions()->where('publication_id', $p->id)->orderBy('id', 'DESC')->get(); ?>
    @foreach($questions as $index => $question)
    <div class="row @if($index%2 != 0) row-shadow @endif" style="line-height: 20px;padding: 10px;">
        <div class="col-md-8 user-question"><i class="fa fa-comment"></i>
          @if ($question->banned_question == 0)
          {{ $question->question }}
          @else
            <span class="label label-warning lb-lg">Esta pregunta fue eliminada por infringir las normas del sitio.</span>
          @endif
        </div>
        <div class="col-md-4" style="text-align: right">
          <span class="moment-ends" data-date="{{ $question->created_at }}"></span>
        </div>
    </div>
    @if (!empty($question->answer))
    <div class="row @if($index%2 != 0) row-shadow @endif" style="line-height: 20px; color: #666;padding: 0px 10px 10px 10px;">
      <div class="col-xs-12 user-answer">
        <i class="fa fa-comments"></i>
        @if ($question->banned_answer == 0)
          {{ $question->answer }} 
        @else
          <span class="label label-warning lb-lg">Esta respuesta fue eliminada por infringir las normas del sitio.</span>
        @endif
      </div>
    </div>
    @endif
    @endforeach
    @endforeach
@else
<div class="row">
    <div class="col-xs-12">
      <div class="alert alert-warning" role="alert">
          <strong>No has realizado ninguna pregunta.</strong>
      </div>
    </div>
</div>
@endif
@endsection
@section('scripts')
    @parent
    <script src="/js/moment.js"></script>
    <script>
        $(document).ready(function() {
          moment.locale('es');
          $('.moment-ends').each(function(i, obj) {
              var date = $(this).attr('data-date');
              $(this).text(moment(date, "YYYY-MM-DD H:m:s").fromNow());
          });

        });
        
          
          function delete_questions(id) {
                $.ajax({
                    method: "POST",
                    url: "{{url('/delete_questions')}}",
                    cache: false,
                    data: { id: id, _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
                    if(msg.is_ok) {
                      location.reload();
                    }
                });
            }
    </script>
@endsection
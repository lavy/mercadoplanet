@extends('layouts.myaccount')

@section('title', 'Facturación')

@section('myaccountcontent')
@if (Session::has('message'))
<div class="row">
    <div class="col-xs-12">
      <div class="alert  {{ Session::get('alert-class', 'alert-info') }}" role="alert">
          <strong>{{ Session::get('message') }}</strong>
      </div>
    </div>
</div>
@endif
<h3>Detalle de factura</h3>
<div class="row" style="padding: 10px;border: 1px solid #ddd;margin-bottom: 10px;border-radius: 5px">
    <div class="col-xs-12">
        <b>Número de factura:</b> {{ $factura->id }}
    </div>
    <div class="col-xs-12">
        <b>Fecha de emisión:</b>  
        @if( $factura->status == 'Open')
        No emitida
        @else
        {{ date('d-m-Y', strtotime($factura->emitted_at)) }}
        @endif
    </div>
    <div class="col-xs-12">
        <b>Monto:</b> {{ number_format($factura->getTotal(),2,',','.') }}
    </div>
    <div class="col-xs-12">
        <b>Fecha de Vencimiento:</b>  @if($items->first()) {{ $items->first()->created_at->addMonth(1) }} @else "Aun Por Definir" @endif
    </div>º
    <div class="col-xs-12">
        <b>Estado:</b>
        @if($factura->status == 'Emitted')
        Pendiente de pago
        @elseif($factura->status == 'Paid')
        Pagada
        @elseif($factura->status == 'Canceled')
        Anulada
        @else
        Por facturar
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-3"><a href="{{ url('billing') }}" class="btn-link">Regresar</a></div>
        @if($factura->status == 'Emitted')
        <div class="col-md-offset-6 col-md-3"><a href="{{ route('payment', ['id' => $factura->id]) }}" class="btn btn-default">Notificar pago</a></div>
        @endif
</div>

<div class="table-responsive div-publications">
  <table class="table table-hover table-publications">
      <thead>
          <tr>
              <th>Publicación</th>
              <th>Descripción</th>
              <th>Monto</th>
              <th>Fecha</th>
          </tr>
      </thead>
      <tbody>
          @foreach($items as $i)
          <tr>
              <td>
               {{ $i->publication->title }}
              </td>
              <td>
               {{ $i->description }}
              </td>
              <td>
               {{ number_format($i->amount,2,',','.') }}
              </td>
              <td>
               {{ date('d-m-Y', strtotime($i->created_at)) }}
              </td>
          </tr>
          @endforeach
      </tbody>
  </table>
</div>
<div class="col-xs-12">
    <div class="text-center">
        {{ $items->appends($query)->links() }}
    </div>
</div>



@endsection
@section('scripts')
    @parent
    <script>
        $(document).ready(function() {
        });
	  
	  
    </script>
@endsection

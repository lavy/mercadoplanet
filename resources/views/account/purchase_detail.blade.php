@extends('layouts.myaccount')

@section('title', 'Detalles de la compra')

@section('myaccountcontent')
<link href="/css/jquery.filer.css" rel="stylesheet">
<h3>Detalles de la compra</h3>
<div class="row">
    <div class="alert alert-info">Compra # {{ $purchase->id }} en fecha: {{ date("d-m-Y h:i A", strtotime($purchase->created_at)) }}</div>
</div>
<div class="row">
    <div class="col-xs-12">
	<h4>Producto</h4>
    </div>
    <div class="col-xs-12">
	<div style="float: left">
	    @if(count($p->images))
	    <img src="{{ url('/photo/40x40?url='.urlencode($p->images[0]->name)) }}">
	    @else
	    <img src="{{ url('/photo/40x40?url=nophoto.jpg') }}">
	    @endif
	</div>
	<div style="float:left; margin-left: 10px;line-height: 20px ">
	    <div><a href="{{ route('product',[$p->id]) }}" style="padding: 0" class="btn-link">{{ $p->title }}</a></div>
	    <div style="color:#999">S/{{ number_format($p->price, 2, ',', '.') }} x {{ $purchase->quantity }} @if($purchase->quantity == 1)unidad @else unidades @endif</div>
	</div>
    </div>
</div>
<br><br>
<div class="row">
    <div class="col-xs-12">
	<h4>Vendedor</h4>
    </div>
    <div class="col-xs-12" style="line-height: 25px">
	<strong>{{ $p->user->getFullName() }}</strong><br>
	<div><a href="{{ route('reputationseller', ['user_id' => $p->user->id]) }}" class="btn-link">{{ strtoupper($p->user->username)}} ({{ $p->user->getPoints() }})</a></div>
	<div style="color:#666">{{ $p->user->email }}</div>
	<div>{{ $p->user->phone_number }}</div>
    </div>
</div>
<br><br>
    <div class="row">
	<div class="col-xs-12">
	    <h4 style="margin-bottom: 30px">Calificación</h4>
	</div>
	<div class="col-xs-12" style="margin-bottom: 10px">
            @if($purchase->buyer_calification === 'Positivo')
            <a href="" data-toggle="modal" data-target="#qualification-modal" class="btn-link" data-purchase="{{ $purchase->id }}">
            <i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i> 
            Calificaste positivo
            </a>
            @elseif($purchase->buyer_calification === 'Negativo')
            <a href="" data-toggle="modal" data-target="#qualification-modal" class="btn-link" data-purchase="{{ $purchase->id }}">
            <i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i> 
            Calificaste negativo
            </a>
            @elseif($purchase->buyer_calification === 'Neutral')
            <a href="" data-toggle="modal" data-target="#qualification-modal" class="btn-link" data-purchase="{{ $purchase->id }}">
            <i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i> 
            Calificaste neutral
            </a>
            @elseif(($purchase->buyer_calification === 'No calificado' && (time() - strtotime($purchase->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600))
                or ($purchase->buyer_calification === 'No calificado' && $purchase->buyer_qualification_modified)
            )
            No calificaste 
            @else
            <a href="{{ route('buyer_qualification', ['id' => $purchase->id]) }}" class="btn-link"><i class="fa fa-clock-o" style="font-size: 16px;color:#666"></i> {{ $purchase->getTimeToQualify() }} dias para calificar</a>
            @endif
	</div>
        
            @if($purchase->buyer_calification != 'No calificado')
                @if($purchase->seller_calification == 'No calificado')
                    @if((time() - strtotime($sale->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600))
                    <div class="col-xs-12">No te calificaron</div>
                    @else
                    <div class="col-xs-12">{{ $sale->getTimeToQualify() }} para que califique</div>
                    @endif
                @else
                    @if($purchase->seller_calification === 'Positivo')
                    <div class="col-xs-12"><i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i>
                    Te calificaron positivo</div>
                    @elseif($purchase->seller_calification === 'Negativo')
                    <div class="col-xs-12"><i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i>
                    Te calificaron  negativo</div>
                    @elseif($purchase->seller_calification === 'Neutral')
                    <div class="col-xs-12"><i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i>
                    Te calificaron neutral</div>
                    @endif
                    @if(!$purchase->buyer_request_change_qualification)
                    <div class="col-xs-12"><a href="{{ route('buyer_request_change_qualification', ['id' => $purchase->id]) }}" class="btn-link">Solicitar cambio de calificación</a></div>
                    @endif
                @endif
            @endif
    </div>
<br><br>
<form action="{{ route('save_message', ['id' => $purchase->id]) }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="row">
	<div class="col-xs-12">
	    <h4 style="margin-bottom: 30px">Mensajes</h4>
	</div>
	@foreach($messages as $m)
	<div class="row">
	@if($m->user_id == $purchase->seller_id)
	<div class="col-xs-10">	    
                <div class="popover-qualification">
                    <div class="popover right">
                      <div class="arrow"></div>
                      <div class="popover-content">
			  <p>
			    <strong>{{ $purchase->seller->first_name }}</strong><br>
			    {{ $m->message }} 
			    @if($m->image1)
			    <div><a href="{{ route('download', ['file' => $m->image1, 'name' => $m->name1]) }}" class="btn-link"><i class="fa fa-file"></i> {{ $m->name1 }}</a></div>
			    @endif 
			    @if($m->image2)
			    <div><a href="{{ route('download', ['file' => $m->image2, 'name' => $m->name2]) }}" class="btn-link"><i class="fa fa-file"></i> {{ $m->name2 }}</a></div>
			    @endif
			    @if($m->image3)
			    <div><a href="{{ route('download', ['file' => $m->image3, 'name' => $m->name3]) }}" class="btn-link"><i class="fa fa-file"></i> {{ $m->name3 }}</a></div>
			    @endif
			    <small style="float:right;">{{ date("d/m/Y h:i A", strtotime($m->created_at)) }}</small>
			  </p>
                      </div>
                    </div>
                </div>
	</div>
	@else
	<div class="col-xs-10 col-xs-offset-2">	    
                <div class="popover-qualification">
                    <div class="popover left">
                      <div class="arrow"></div>
                      <div class="popover-content">
			  <p>{{ $m->message }} 
			    @if($m->image1)
			    <div><a href="{{ route('download', ['file' => $m->image1, 'name' => $m->name1]) }}" class="btn-link"><i class="fa fa-file"></i> {{ $m->name1 }}</a></div>
			    @endif 
			    @if($m->image2)
			    <div><a href="{{ route('download', ['file' => $m->image2, 'name' => $m->name2]) }}" class="btn-link"><i class="fa fa-file"></i> {{ $m->name2 }}</a></div>
			    @endif
			    @if($m->image3)
			    <div><a href="{{ route('download', ['file' => $m->image3, 'name' => $m->name3]) }}" class="btn-link"><i class="fa fa-file"></i> {{ $m->name3 }}</a></div>
			    @endif
			    <small style="float:right;">{{ date("d/m/Y h:i A", strtotime($m->created_at)) }}</small>
			  </p>
                      </div>
                    </div>
                </div>
	</div>
	@endif
	</div>
	@endforeach
	<div class="col-xs-12" style="margin-top: 20px">
	    <textarea class="form-control" name="message" id="message" style="height: 110px"></textarea>
	    <div style="font-size: 12px;">Quedan <span id="num-chars">500</span> caracteres.</div>
	</div>
    </div>
    <br>
    <div class="row">
	<div class="col-md-10">
	     <input type="file" name="files[]" id="filer_input" multiple="multiple">
	</div>
	<div class="col-md-2">
	    <button type="submit" class="btn btn-primary" id="btn-message" disabled="true">Enviar mensaje</button>
	</div>
    </div>
</form>


<div class="modal fade" id="qualification-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Calificación</h4>
      </div>
      <div class="modal-body">
	  <img src="/img/assets/ajax-loader.gif" id="spinner">
	  <div class="modal-container"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
    @parent
    <script src="/js/jquery.filer.min.js" type="text/javascript"></script>
    <script>
	$(document).ready(function(){

            $('#filer_input').filer({
                    limit: 3,
                    maxSize: 3,
                    extensions: ["jpg", "jpeg", "png", "gif", "pdf"],
                    showThumbs: true,
                    addMore: true,
                    allowDuplicates: false
            });

            var maxChars = 500;
            $("#message").keyup(controlChars);


            function controlChars(e) {
                var rest = maxChars - $(this).val().length;
                if(rest < 0) rest = 0;
                if($(this).val().length > 0) $("#btn-message").prop('disabled', false);
                else $("#btn-message").prop('disabled', true);
                $("#num-chars").text(rest);
                if (e.which < 0x20) {
                        // e.which < 0x20, then it's not a printable character
                        // e.which === 0 - Not a character
                        return;     // Do nothing
                }
                if ($(this).val().length == maxChars) {
                        e.preventDefault();
                } else if ($(this).val().length > maxChars) {
                        // Maximum exceeded
                        $(this).val($(this).val().substring(0, maxChars));
                }
            }

            $('#qualification-modal').on('show.bs.modal', function (event) {
	    var button = $(event.relatedTarget);
            var recipient = button.data('purchase');
            var modal = $(this);
            $("#spinner").show();
	    modal.find('.modal-container').html('');
	    $.ajax({
                    method: "POST",
                    url: "{{url('/qualification')}}",
                    cache: false,
                    data: { id: recipient, _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
		    var output = '';
                    if(msg.is_ok) {
			var resp = JSON.parse(msg.value);
			if(resp.buyer_qualification == 'Positivo') {
			    output += '<div><i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i> Calificaste positivo</div>';
			}
			if(resp.buyer_qualification == 'Neutral') {
			    output += '<div><i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i> Calificaste neutral</div>';
			}
			if(resp.buyer_qualification == 'Negativo') {
			    output += '<div><i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i> Calificaste negativo</div>';
			}
			
			if(resp.buyer_qualification != 'No calificado') {
			    output += '<div style="margin-top: 5px">"'+resp.buyer_comment+'"</div>';
                            if(resp.seller_reply !== '' && resp.seller_reply) {
                                output += '<div style="margin-top: 5px;margin-left: 15px">Replica: "'+resp.seller_reply+'"</div>';
                            }
			    if(resp.can_modify == 'true') {
				output += '<a href="{{ route('buyer_qualification_edit') }}?id='+recipient+'" class="btn-link">Modificar</a>';
			    }
			}
			if(resp.seller_qualification != 'No calificado') {
			    output += '<hr>';
			    if(resp.seller_qualification == 'Positivo') {
				output += '<div><i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i> Te calificó positivo</div>';
			    }
			    if(resp.seller_qualification == 'Neutral') {
				output += '<div><i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i> Te calificó neutral</div>';
			    }
			    if(resp.seller_qualification == 'Negativo') {
				output += '<div><i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i> Te calificó negativo</div>';
			    }
			    output += '<div style="margin-top: 5px">"'+resp.seller_comment+'"</div>';
                            
                            if(resp.buyer_reply !== '' && resp.buyer_reply) {
                                output += '<div style="margin-top: 5px;margin-left: 15px">Replica: "'+resp.buyer_reply+'"</div>';
                            }
                            
                            if(resp.can_modify == 'true') {
				output += '<a href="{{ route('buyer_reply_edit') }}?id='+recipient+'" class="btn-link">Replicar</a>';
			    }
			}
			$("#spinner").hide();
                    }
		    
		    modal.find('.modal-container').html(output);
                });
	    
	    //modal.find('.modal-body').text('New message to ' + recipient);
	    });
	});
    </script>
@endsection
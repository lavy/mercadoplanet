@extends('layouts.myaccount')

@section('title', 'Publicaciones')

@section('myaccountcontent')
@if (Session::has('message'))
<div class="row">
    <div class="col-xs-12">
      <div class="alert  {{ Session::get('alert-class', 'alert-info') }}" role="alert">
          <strong>{{ Session::get('message') }}</strong>
      </div>
    </div>
</div>
@endif
<h3>Publicaciones</h3>
<section class="mainContent clearfix">
    <div class="row  userProfile">
      <div class="col-md-9">
        <div class="btn-group" role="group" id="pub-labels">
          <a href="{{ route('publications', array_merge($query, ['status' => 'active'])) }}" class="btn btn-default @if($status == 'active') active @endif"><i class="fa fa-play-circle" aria-hidden="true"></i>Activas ({{ $actives }})</a>
          <a href="{{ route('publications', array_merge($query, ['status' => 'paused'])) }}" class="btn btn-default @if($status == 'paused') active @endif"><i class="fa fa-pause-circle" aria-hidden="true"></i>Pausadas ({{ $paused }})</a>
          <a href="{{ route('publications', array_merge($query, ['status' => 'finished'])) }}" class="btn btn-default @if($status == 'finished') active @endif"><i class="fa fa-stop-circle" aria-hidden="true"></i>Finalizadas ({{ $finished }})</a>
          @if($revision)
          <a href="{{ route('publications', array_merge($query, ['status' => 'revision'])) }}" class="btn btn-default @if($status == 'revision') active @endif"><i class="fa fa-stop-circle" aria-hidden="true"></i>Revisión ({{ $revision }})</a>
          @endif
        </div>
      </div>
        <div class="col-md-3 filterArea pub-filter">
            <select name="sort-box" id="sort-box" class="select-drop">
                <option value="1" {{ ($sort=='1' ? 'selected':'') }}>Finalizan primero</option>
                <option value="2" {{ ($sort=='2' ? 'selected':'') }}>Finalizan último</option>
                <option value="3" {{ ($sort=='3' ? 'selected':'') }}>Mayor cantidad disponible</option>
                <option value="4" {{ ($sort=='4' ? 'selected':'') }}>Menor cantidad disponible</option>
                <option value="5" {{ ($sort=='5' ? 'selected':'') }}>Mayor precio</option>
                <option value="6" {{ ($sort=='6' ? 'selected':'') }}>Menor precio</option>
            </select>
        </div>
    </div>
@if(count($publications))
<div class="table-responsive div-publications">
  <table class="table table-hover table-publications">
      <tbody>
          @foreach($publications as $p)
          <?php $tipo_producto = $p->getProductType(); ?>
          <tr>
              <td style="width: 40px">
                  @if(count($p->images))
                  <img src="{{ url('/photo/40x40?url='.urlencode($p->images[0]->name)) }}">
                  @else
                  <img src="{{ url('/photo/40x40?url=nophoto.jpg') }}">
                  @endif
              </td>
              <td style="width:320px">
                  <a href="{{ route('product',[$p->id]) }}" style="padding: 0" class="btn-link">{{ $p->title }}</a>
                  <div style="color:#666; font-size: 13px"> 
                  @if($p->cost_type == 'Fijo')
                  Clasificado
                  @else
                  Publicación {{ $p->cost_type }}
                  @endif
                  <span style="color: #999; font-size: 11px"> | #{{ $p->id }}</span></div>
                  @if($p->status == 'Revision')
                  <div style="color:#999; font-size: 10px">Esta publicación se encuentra en revisión porque probablemente no cumple con nuestras políticas de publicación</div>
                  @endif
                  @if($p->status == 'Canceled')
                  <div style="color:#999; font-size: 10px">Anulamos esta publicación porque no cumple con nuestras políticas de publicación</div>
                  @endif
              </td>
              <td style="color: #666">
                  <div style="color:#666"><span style="color:#337ab7;font-weight: bold">@if($p->price == '0') A convenir @else S/{{ number_format($p->price, 2, ',', '.') }} @endif</span>@if($tipo_producto == 'producto') x {{ $p->available_quantity }}  @if($p->available_quantity == 1)disponible @else disponibles @endif @endif</div>
                  <div style="color:#333; font-size: 12px">{{ $p->visits }}  @if($p->visits == 1)visita @else visitas @endif</div>
                  @if($p->status != 'Finished')
                  <div style="color:#333; font-size: 12px">  Finaliza en {{ $p->getTimeToEnd() }}</div>
                  @endif
              </td>
              <td>
                <div style="text-align: center">
                    <div class="btn-group" style="margin-bottom: 0px;">
                         @if($p->status == 'Active' or $p->status == 'Paused')
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-gear"></i> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu menu-publications">
                          
                          @if($p->status == 'Active')
                          <li><a href="{{ route('pause',['id' => $p->id]) }}">Pausar</a></li>
                            @if($p->cost_type != 'Fijo' && $p->cost_type != 'Premium')
                            @if($conf->activate_plans)
                            <li><a href="{{ route('plan',['id' => $p->id]) }}">Aumentar exposición</a></li>
                            @endif
                            @endif
                          @else
                             @if(!$p->lack_stock)
                                <li><a href="{{ route('activate',['id' => $p->id]) }}">Activar</a></li>
                             @else
                              <li><a href="{{ route('stock',['id' => $p->id]) }}">Agregar stock</a></li>                                
                             @endif
                          @endif
                          
                          @if(!$p->lack_stock)
                            <li><a href="{{ route('modify-publication',['id' => $p->id]) }}">Modificar</a></li>
                            @if($tipo_producto == 'producto')
                            <li><a href="{{ route('shipping_cost',['id' => $p->id]) }}">Costos de envío</a></li>
                            @endif
                            <li><a href="{{ route('change-category',['id' => $p->id]) }}">Cambiar categoría</a></li>
                            @endif
                          
                          <li><a href="#" data-toggle="modal" data-target="#finish-modal" data-pub="{{ $p->id }}" class="finish-btn">Finalizar</a></li>
                          
                        </ul>
                         @endif
                         
                        @if($p->status == 'Finished')
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-gear"></i> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu menu-publications">
                          <li><a href="{{ route('republish-publication',['id' => $p->id]) }}">Republicar</a></li>
                          </ul>
                        @endif
                  </div>
                </div>
              </td>
          </tr>
          @endforeach
      </tbody>
  </table>
</div>
<div class="col-xs-12">
    <div class="text-center">
        {{ $publications->appends($query)->links() }}
    </div>
</div>
@else
<div class="row">
    <div class="col-xs-12">
      <div class="alert alert-warning" role="alert">
          <strong>No tienes ninguna publicación {{ $status_txt }}.</strong>
      </div>
    </div>
</div>
@endif
</section>


<form method="get" action="{{ route('finish') }}">
<div class="modal fade" id="finish-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Finalizar Publicación</h4>
      </div>
      <div class="modal-body">
	  ¿Seguro que deseas finalizar esta publicación?
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Finalizar</button>
        <input type="hidden" name="id" value="" id="pub-id">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
</form>

@endsection
@section('scripts')
    @parent
    <script>
        $(document).ready(function() {
            $(".finish-btn").click(function() {
                $("#pub-id").val($(this).attr('data-pub'));
            });
            
            $('#sort-box').change(function() {
                if($(this).val() == '1') {
                   $(location).attr('href', "{!! route('publications', array_merge($query, ['sort' => '1'])) !!}");
                }
                if($(this).val() == '2') {
                   $(location).attr('href', "{!! route('publications', array_merge($query, ['sort' => '2'])) !!}");
                }
                if($(this).val() == '3') {
                   $(location).attr('href', "{!! route('publications', array_merge($query, ['sort' => '3'])) !!}");
                }
                if($(this).val() == '4') {
                   $(location).attr('href', "{!! route('publications', array_merge($query, ['sort' => '4'])) !!}");
                }
                if($(this).val() == '5') {
                   $(location).attr('href', "{!! route('publications', array_merge($query, ['sort' => '5'])) !!}");
                }
                if($(this).val() == '6') {
                   $(location).attr('href', "{!! route('publications', array_merge($query, ['sort' => '6'])) !!}");
                }
                if($(this).val() == '7') {
                   $(location).attr('href', "{!! route('publications', array_merge($query, ['sort' => '7'])) !!}");
                }
                if($(this).val() == '8') {
                   $(location).attr('href', "{!! route('publications', array_merge($query, ['sort' => '8'])) !!}");
                }
            });
        });
	  
	  
    </script>
@endsection

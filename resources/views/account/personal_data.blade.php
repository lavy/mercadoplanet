@extends('layouts.myaccount')

@section('title', 'Datos personales')

@section('myaccountcontent')

@if (Session::has('result'))
    @if (Session::get('result'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @else
        <div class="alert alert-danger">{{ Session::get('message') }}</div>
    @endif
@endif

<div><h3>Datos personales</h3></div>

<fieldset>
    <div class="pd-header"><span class="title-pd">Datos de cuenta</span></div>
    <hr />
    <div class="pd-item">
        <label>Usuario: </label>
        <span>{{ Auth::user()->username }}</span>
        <span class="pd-link"><a href="javascript:void(0)" data-toggle="modal" data-target="#mod-username-modal">Modificar</a></span>
    </div>
    <div class="pd-item">
        <label>Contraseña: </label>
        <span>********</span>
        <span class="pd-link"><a href="javascript:void(0)" data-toggle="modal" data-target="#mod-password-modal">Modificar</a></span>
    </div>
</fieldset>

<fieldset>
    <div class="pd-header"><span class="title-pd">Datos personales</span></div>
    <hr />
    <div class="pd-item">
        <label>Nombre y apellido: </label>
        <span>{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</span>
    </div>
    @if (Auth::user()->document != '')
    <div class="pd-item">
        <label>Documento: </label>
        <span>{{ Auth::user()->document_type }} {{ Auth::user()->document }}</span>
    </div>
    @endif
    <div class="pd-item">
        <label>E-mail: </label>
        <span>{{ Auth::user()->email }}</span>
        <span class="pd-link"><a href="javascript:void(0)" data-toggle="modal" data-target="#mod-email-modal">Modificar</a></span>
    </div>
    <div class="pd-item">
        <label>Teléfono: </label>
        <span>{{ Auth::user()->phone_number }}</span>
        <span class="pd-link"><a href="javascript:void(0)" data-toggle="modal" data-target="#mod-phone-modal">Modificar</a></span>
    </div>
</fieldset>

@if (Auth::user()->document == '')
<div class="row">
    <div class="col-xs-12 register-col">
        <span class="register-text">Aún no has completado tu registro</span> <a href="{{ url('completereg') }}" class="btn btn-primary">Completar</a>
    </div>
</div>
@else
<fieldset>
    <div class="pd-header"><span class="title-pd">Domicilios</span></div>
    @foreach($addresses as $address)
    <hr />
    <div>
    <span>{{ $address->address_type }} {{ $address->address_text }}</span>
    @if($address->default_for_purchases)
    <span class="label label-default">Compras</span>
    @endif
    @if($address->default_for_sales)
    <span class="label label-default">Ventas</span>
    @endif
    <div class="address-btn-group btn-group">
        <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle"><i class="fa fa-gear"></i> <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <li><a href="{{ url('/edit_address', $address->id) }}">Modificar</a></li>
            @if(!$address->default_for_purchases)
            <li><a href="{{ url('/use_address_for_purchases', $address->id) }}">Usar para compras</a></li>
            @endif
            @if(!$address->default_for_sales)
            <li><a href="{{ url('/use_address_for_sales', $address->id) }}">Usar para ventas</a></li>
            <li><a href="{{ url('/remove_address', $address->id) }}">Eliminar</a></li>
            @endif
        </ul>

    </div>
    <br />
    <span class="address-second">
        Nro. {{ $address->number }}
        @if(!empty($address->other_data))
        , {{ $address->other_data }}
        @endif
        @if(!empty($address->reference))
        , {{ $address->reference }}
        @endif
        <br />
    </span>
    <span class="address-second">{{ $address->district->name }}, {{ $address->district->province->name }}, {{ $address->district->province->department->name }}</span>
    @if($address->default_for_sales)
    <div class="checkbox">
        <label>
            <input type="checkbox" value="1" id="send_with_email" name="send_with_email" @if($address->send_with_email) checked="checked" @endif> 
            Quiero que mis compradores reciban mi domicilio junto con mis datos de contacto.
        </label>
    </div>
    @endif
    </div>
    @endforeach
    <div class="add-address"><a href="{{ url('/newaddress') }}">Agregar domicilio</a></div>
</fieldset>

@endif

<!-- Modificar nombre de usuario -->
<div class="modal fade" id="mod-username-modal" tabindex="-1" role="dialog" aria-labelledby="mod-username-modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="msg-form" method="post" action="{{ url('/modusername') }}" role="form">
                {{ csrf_field() }}
                <div class="modal-header mod-modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="mod-username-modal-label">Modificar nombre de usuario</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group sandbox">
                        <div class="form-group">
                            <label>Nuevo nombre de usuario{!! required() !!}</label>
                            <input id="username" name="username" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Contraseña{!! required() !!}</label>
                            <input id="mod-username-password" name="password" type="password" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="cancel-msg" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modificar contraseña -->
<div class="modal fade" id="mod-password-modal" tabindex="-1" role="dialog" aria-labelledby="mod-password-modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="msg-form" method="post" action="{{ url('/modpassword') }}" role="form">
                {{ csrf_field() }}
                <div class="modal-header mod-modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="mod-password-modal-label">Modificar contraseña</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group sandbox">
                        <div class="form-group">
                            <label>Contraseña actual{!! required() !!}</label>
                            <input id="mod-password-password" name="old_password" type="password" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Contraseña nueva{!! required() !!}</label>
                            <input id="new-password" name="password" type="password" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Confirme contraseña nueva{!! required() !!}</label>
                            <input id="password-confirm" name="password_confirmation" type="password" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="cancel-msg" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modificar email-->
<div class="modal fade" id="mod-email-modal" tabindex="-1" role="dialog" aria-labelledby="mod-email-modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="msg-form" method="post" action="{{ url('/modemail') }}" role="form">
                {{ csrf_field() }}
                <div class="modal-header mod-modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="mod-email-modal-label">Modificar e-mail</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group sandbox">
                        <div class="form-group">
                            <label>E-mail{!! required() !!}</label>
                            <input id="email" name="email" type="email" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Confirme e-mail{!! required() !!}</label>
                            <input id="email-confirm" name="email_confirmation" type="email" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Contraseña{!! required() !!}</label>
                            <input id="mod-email-password" name="password" type="password" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="cancel-msg" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modificar número de teléfono -->
<div class="modal fade" id="mod-phone-modal" tabindex="-1" role="dialog" aria-labelledby="mod-phone-modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="msg-form" method="post" action="{{ url('/modphone') }}" role="form">
                {{ csrf_field() }}
                <div class="modal-header mod-modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="mod-phone-modal-label">Modificar número de teléfono</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group sandbox">
                        <div class="form-group">
                            <label>Nuevo número de teléfono{!! required() !!}</label>
                            <input id="phone-number" name="phone_number" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Contraseña{!! required() !!}</label>
                            <input id="mod-phone-number-password" name="password" type="password" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="cancel-msg" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    @parent
    
    <script>
        $(document).ready(function() {
            $('#mod-username-modal').on('show.bs.modal', function (event) {
                $("#username").val('');
                $("#mod-username-password").val('');
            });
            $('#mod-password-modal').on('show.bs.modal', function (event) {
                $("#mod-password-password").val('');
                $("#new-password").val('');
                $("#password-confirm").val('');
            });
            $('#mod-names-modal').on('show.bs.modal', function (event) {
                $("#first-name").val('');
                $("#last-name").val('');
                $("#mod-names-password").val('');
            });
            $('#mod-document-modal').on('show.bs.modal', function (event) {
                $("#document_type_1").removeAttr('selected');
                $("#document_type_2").removeAttr('selected');
                $("#document").val('');
                $("#mod-document-password").val('');
            });
            $('#mod-email-modal').on('show.bs.modal', function (event) {
                $("#email").val('');
                $("#email-confirm").val('');
                $("#mod-email-password").val('');
            });
            $('#mod-phone-modal').on('show.bs.modal', function (event) {
                $("#phone-number").val('');
                $("#mod-phone-number-password").val('');
            });
            
            $("#send_with_email").click(function() {
                
                var to_send = this.checked ? 1 : 0;
                
                $("#send_with_email").attr('disabled', 'disabled');
                
                $.ajax({
                    url: "{{ url('/set_send_with_email') }}",
                    type: "POST",
                    data: {
                        value: to_send,
                        _token: "{{ csrf_token() }}", 
                    },
                    success: function(res) {
                        if (!res.result) {
                            if (to_send) {
                                $("#send_with_email").removeAttr('checked');
                            } else {
                                $("#send_with_email").attr('checked', 'checked');
                            }
                        }
                        
                        $("#send_with_email").removeAttr('disabled');
                    }
                });
            });
        });
    </script>
    
@endsection

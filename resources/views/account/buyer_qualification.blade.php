@extends('layouts.myaccount')

@section('title', 'Detalles de la compra')

@section('myaccountcontent')
<link href="/css/jquery.filer.css" rel="stylesheet">
<h3>Calificar</h3>
<form name="qualification-form" method="post" action="{{ route('buyer_qualification_save') }}">
<input type="hidden" name="id" value="{{ $id }}">
{{ csrf_field() }}
<div class="row">
    <div class="col-md-8">
	<br>
	<strong>¿Recibiste el producto que esperabas?</strong>
        @if ($errors->has('concreted'))
        <br>
            <span class="text-danger">
                {{ $errors->first('concreted') }}
            </span>
        @endif
	    <div class="radio">
		<label>
		    <input type="radio" name="concreted" id="concreted1" value="1" @if(old('concreted') == '1') checked @endif>
		    Si, tengo el producto y está bien.
		</label>
	    </div>
	    <div class="radio">
		<label>
		    <input type="radio" name="concreted" id="concreted2" value="0"@if(old('concreted') == '0') checked @endif>
		    No, decidí no comprarlo.
		</label>
	  </div>
	    <div class="radio">
	    <label>
	      <input type="radio" name="concreted" id="concreted3" value="-1"@if(old('concreted') == '-1') checked @endif>
	      No, tuve un problema.
	    </label>
	  </div>
        <div id="concreted3-type" @if(old('concreted') != '-1') style="display: none" @endif>
        <label>¿Qué pasó?</label>
        @if ($errors->has('no_concreted_type'))
        <br>
            <span class="text-danger">
                {{ $errors->first('no_concreted_type') }}
            </span>
        @endif
        <select class="form-control" name="no_concreted_type">
            <option value="">Elegir</option>
            <option @if(old('no_concreted_type') == 'Lo que compré no se corresponde con lo que recibí') selected @endif>Lo que compré no se corresponde con lo que recibí</option>
            <option @if(old('no_concreted_type') == 'Recibí un producto defectuoso') selected @endif>Recibí un producto defectuoso</option>
            <option @if(old('no_concreted_type') == 'Aún no recibo el producto') selected @endif>Aún no recibo el producto</option>
            <option @if(old('no_concreted_type') == 'El vendedor se quedó sin inventario') selected @endif>El vendedor se quedó sin inventario</option>
            <option @if(old('no_concreted_type') == 'No obtuve respuesta del vendedor') selected @endif>No obtuve respuesta del vendedor</option>
            <option @if(old('no_concreted_type') == 'Otro') selected @endif>Otro</option>
        </select>
        </div>
	<br>
	<strong>¿Recomendarías a este vendedor?</strong>
            @if ($errors->has('recommended'))
            <br>
                <span class="text-danger">
                    {{ $errors->first('recommended') }}
                </span>
            @endif
	    <div class="radio">
		<label>
		    <input type="radio" name="recommended" value="Sí" @if(old('recommended') == 'Sí') checked @endif>
		    Si.
		</label>
	    </div>
	    <div class="radio">
		<label>
		    <input type="radio" name="recommended" value="No estoy seguro" @if(old('recommended') == 'No estoy seguro') checked @endif>
		    No estoy seguro.
		</label>
	  </div>
	    <div class="radio">
	    <label>
	      <input type="radio" name="recommended" value="No" @if(old('recommended') == 'No') checked @endif>
	      No.
	    </label>
	  </div>
	<br>
	<strong>Comparte lo que opinas del vendedor.</strong>
        @if ($errors->has('opinion'))
        <br>
            <span class="text-danger">
                {{ $errors->first('opinion') }}
            </span>
        @endif
        <textarea class="form-control" name="opinion" id="opinion" rows="3" style="max-width: 300px; margin-top: 10px">{{ old('opinion') }}</textarea>
        <div style="font-size: 12px;">Quedan <span id="num-chars">200</span> caracteres.</div>
	<br>
        <button class="btn btn-primary" id="btn-submit"  @if(!strlen(old('opinion'))) disabled="true" @endif>Calificar</button> <a href="{{ url('purchases') }}" class="btn btn-link">Cancelar</a>
    </div>
    <div class="col-md-4">
	<div class="product-container">
	<div>
	    @if(count($publication->images))
	    <img src="{{ url('/photo/160x160?url='.urlencode($publication->images[0]->name)) }}">
	    @else
	    <img src="{{ url('/photo/160x160?url=nophoto.jpg') }}">
	    @endif
	</div>
	    <div style="margin-top: 8px">
	    <a href="{{ route('product',[$publication->id]) }}" style="padding: 0" class="btn-link">{{ $publication->title }}</a>
	</div>
	<div style="color:#333;    margin: 10px 0;font-size: 18px;"><strong>S/{{ number_format($publication->price, 2, ',', '.') }}</strong></div>
	<div>
	    Vendedor:
	    <br>
	    {{ $publication->user->getFullName() }}
	    <br>
	    <a href="{{ route('reputationseller', ['user_id' => $publication->user->id]) }}" class="btn-link">{{ strtoupper($publication->user->username)}} ({{ $publication->user->getPoints() }})</a>
	</div>
	</div>
    </div>
</div>
</form>
@endsection
@section('scripts')
    @parent
    <script>
	$(document).ready(function(){
          
	$("[name=concreted]").click(function() {
          if($("#concreted3").is(':checked')) {
            $("#concreted3-type").show();
          } else {
            $("#concreted3-type").hide();          
          }          
        });
        
	var maxChars = 200;
	$("#opinion").keyup(controlChars);


	function controlChars(e) {
	  var rest = maxChars - $(this).val().length;
	  if(rest < 0) rest = 0;
	  if($(this).val().length > 0) $("#btn-submit").prop('disabled', false);
	  else $("#btn-submit").prop('disabled', true);
	  $("#num-chars").text(rest);
		    if (e.which < 0x20) {
			    // e.which < 0x20, then it's not a printable character
			    // e.which === 0 - Not a character
			    return;     // Do nothing
		    }
		    if ($(this).val().length == maxChars) {
			    e.preventDefault();
		    } else if ($(this).val().length > maxChars) {
			    // Maximum exceeded
			    $(this).val($(this).val().substring(0, maxChars));
		    }
	    }
	});
    </script>
@endsection
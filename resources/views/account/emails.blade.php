@extends('layouts.myaccount')

@section('title', 'Configuración de E-mails')

@section('myaccountcontent')
@if (Session::has('message'))
    <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
<div style="padding-bottom: 20pt"><h3>E-mails</h3></div>
<div class="table-responsive">
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/update_emails') }}">
     {{ csrf_field() }}
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>¿Qué e-mails quieres recibir?</th>
                <th>Sí</th>
                <th>No</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Vendiste un producto</td>
                @if ($emails->sold_product)
                <td><input class="rad" type="radio" id="sold_product_yes" value="1" name="sold_product" checked="checked" /></td>
                <td><input class="rad" type="radio" id="sold_product_no" value="0" name="sold_product" /></td> 
                @else
                <td><input class="rad" type="radio" id="sold_product_yes" value="1" name="sold_product" /></td>
                <td><input class="rad" type="radio" id="sold_product_no" value="0" name="sold_product" checked="checked" /></td> 
                @endif
            </tr>
            <tr>
                <td>Hicieron una pregunta en tu publicación</td>
                @if ($emails->somebody_asked_you)
                <td><input class="rad" type="radio" id="somebody_asked_you_yes" value="1" name="somebody_asked_you" checked="checked" /></td>
                <td><input class="rad" type="radio" id="somebody_asked_you_no" value="0" name="somebody_asked_you" /></td> 
                @else
                <td><input class="rad" type="radio" id="somebody_asked_you_yes" value="1" name="somebody_asked_you" /></td>
                <td><input class="rad" type="radio" id="somebody_asked_you_no" value="0" name="somebody_asked_you" checked="checked" /></td> 
                @endif
            </tr>
            <tr>
                <td>Finalizó tu publicación</td>
                @if ($emails->your_publication_finished)
                <td><input class="rad" type="radio" id="your_publication_finished_yes" value="1" name="your_publication_finished" checked="checked" /></td>
                <td><input class="rad" type="radio" id="your_publication_finished_no" value="0" name="your_publication_finished" /></td> 
                @else
                <td><input class="rad" type="radio" id="your_publication_finished_yes" value="1" name="your_publication_finished" /></td>
                <td><input class="rad" type="radio" id="your_publication_finished_no" value="0" name="your_publication_finished" checked="checked" /></td> 
                @endif
            </tr>
            <tr>
                <td>Tu publicación se republicó automáticamente</td>
                @if ($emails->automatic_republishing)
                <td><input class="rad" type="radio" id="automatic_republishing_yes" value="1" name="automatic_republishing" checked="checked" /></td>
                <td><input class="rad" type="radio" id="automatic_republishing_no" value="0" name="automatic_republishing" /></td> 
                @else
                <td><input class="rad" type="radio" id="automatic_republishing_yes" value="1" name="automatic_republishing" /></td>
                <td><input class="rad" type="radio" id="automatic_republishing_no" value="0" name="automatic_republishing" checked="checked" /></td> 
                @endif
            </tr>
            <tr>
                <td>Compraste un producto</td>
                @if ($emails->bought_product)
                <td><input class="rad" type="radio" id="bought_product_yes" value="1" name="bought_product" checked="checked" /></td>
                <td><input class="rad" type="radio" id="bought_product_no" value="0" name="bought_product" /></td> 
                @else
                <td><input class="rad" type="radio" id="bought_product_yes" value="1" name="bought_product" /></td>
                <td><input class="rad" type="radio" id="bought_product_no" value="0" name="bought_product" checked="checked" /></td> 
                @endif
            </tr>
            <tr>
                <td>Se usó tu auto-oferta</td>
                @if ($emails->used_auto_offer)
                <td><input class="rad" type="radio" id="used_auto_offer_yes" value="1" name="used_auto_offer" checked="checked" /></td>
                <td><input class="rad" type="radio" id="used_auto_offer_no" value="0" name="used_auto_offer" /></td> 
                @else
                <td><input class="rad" type="radio" id="used_auto_offer_yes" value="1" name="used_auto_offer" /></td>
                <td><input class="rad" type="radio" id="used_auto_offer_no" value="0" name="used_auto_offer" checked="checked" /></td> 
                @endif
            </tr>
            <tr>
                <td>Tu publicación está por finalizar</td>
                @if ($emails->publication_almost_finish)
                <td><input class="rad" type="radio" id="publication_almost_finish_yes" value="1" name="publication_almost_finish" checked="checked" /></td>
                <td><input class="rad" type="radio" id="publication_almost_finish_no" value="0" name="publication_almost_finish" /></td> 
                @else
                <td><input class="rad" type="radio" id="publication_almost_finish_yes" value="1" name="publication_almost_finish" /></td>
                <td><input class="rad" type="radio" id="publication_almost_finish_no" value="0" name="publication_almost_finish" checked="checked" /></td> 
                @endif
            </tr>
            <tr>
                <td>Ofertas y promociones de mercadoplanet</td>
                @if ($emails->offers_and_promotions)
                <td><input class="rad" type="radio" id="offers_and_promotions_yes" value="1" name="offers_and_promotions" checked="checked" /></td>
                <td><input class="rad" type="radio" id="offers_and_promotions_no" value="0" name="offers_and_promotions" /></td> 
                @else
                <td><input class="rad" type="radio" id="offers_and_promotions_yes" value="1" name="offers_and_promotions" /></td>
                <td><input class="rad" type="radio" id="offers_and_promotions_no" value="0" name="offers_and_promotions" checked="checked" /></td> 
                @endif
            </tr>
            <tr>
                <td>Cambios en los términos y condiciones</td>
                @if ($emails->terms_and_conditions_changes)
                <td><input class="rad" type="radio" id="terms_and_conditions_changes_yes" value="1" name="terms_and_conditions_changes" checked="checked" /></td>
                <td><input class="rad" type="radio" id="terms_and_conditions_changes_no" value="0" name="terms_and_conditions_changes" /></td> 
                @else
                <td><input class="rad" type="radio" id="terms_and_conditions_changes_yes" value="1" name="terms_and_conditions_changes" /></td>
                <td><input class="rad" type="radio" id="terms_and_conditions_changes_no" value="0" name="terms_and_conditions_changes" checked="checked" /></td> 
                @endif
            </tr>
        </tbody>
    </table
    <div class="form-group">
        <div style="float:left">
            <button id="btn_save" type="submit" class="btn btn-primary" disabled="disabled">Guardar</button>
        </div>
        <div id="for_cancel" style="float:left"></div>
    </form>
</div>

@endsection

@section('scripts')
    @parent
    
    <script>
        $(document).ready(function() {
            $(".rad").change(function() { 
                $("#btn_save").removeAttr("disabled");     
                $("#for_cancel").html('<button id="btn_cancel" type="reset" class="btn btn-link btn-block">Cancelar</button>');
                
                $("#btn_cancel").click(function() {
                   //$("#for_cancel").html('');
                });
            });
        });
    </script>
    
@endsection
@extends('layouts.app')

@section('title', 'Republicar producto')

@section('content')

<section class="mainContent clearfix userProfile">
       <div class="container">
         <div class="row">
           <div class="col-xs-12">
             <div class="innerWrapper">
               <div class="row">
                 <div class="col-md-12" style="text-align: center">
                   <h4>Republicar</h4>
                 </div>
               </div>
                <?php $tipo_producto = $publication->getProductType(); ?>
                 <form method="post" name="mainForm" action="{{ route('republish-publication-save') }}" onsubmit="return submitForm()" class="form-horizontal">
                 {{ csrf_field() }}
                 
                  <input type="hidden" id="id" name="id" value="{{ $publication->id }}">
                                    
               <div class="row">
                 <div class="col-md-1">
                 @if (count($publication->images) > 0)
                 <img src="{{ url('/photo/150x150?url='.urlencode($publication->images[0]->name)) }}" alt="{{ $publication->title }}" class="img-thumbnail">
                    @else
                    <img src="{{ url('/photo/150x150?url=nophoto.jpg') }}">
                    @endif
                 </div>
                 <div class="col-md-11">
               
               
               
                
                 <div class="form-group">
                   <label for="title" class="col-sm-2 control-label">Título: *</label>
                   <div class="col-sm-6">
                     <input type="text" class="form-control" maxlength="60" name="title" id="title" value="{{ $publication->title }}" required  @if(old('title'))value="{{old('title')}}"@else @if($publication->title)value="{{$publication->title}}"@endif @endif>
                     @if ($errors->has('title'))
                         <span class="text-danger">
                             {{ $errors->first('title') }}
                         </span>
                     @endif
                   </div>
                 </div> 
                 <div class="row">
                   <div class="col-sm-2"></div>
                   <div class="col-sm-10">
                     <div class="alert alert-warning" role="alert">Podrás modificar la descripción y/o forma de entrega una vez que republiques tu producto.</div>
                   </div>
                 </div>
               
                
                 
                 @if($tipo_producto === 'producto')
               <div class="form-group">
                   <label for="title" class="col-sm-3 control-label">Cantidad: *</label>
                   <div class="col-sm-6 input-group">
                    <input type="text" class="form-control" maxlength="4" style="width: 70px" name="cantidad" id="cantidad" value="1"  >
                     @if ($errors->has('cantidad'))
                        <span class="text-danger" style="line-height: 30px; margin-left: 10px">
                             {{ $errors->first('cantidad') }}
                         </span>
                     @endif
                   </div>
                 </div>
                 @else
                 <input type="hidden" name="cantidad" value="1">
                 @endif
               <div class="form-group">
                   
                   <label for="precio" class="col-sm-3 control-label">@if($tipo_producto === 'servicio')<input type="radio" name="convenir" id="convenir1" value="0" style="margin-right: 5px" @if($publication->price != '0') checked @endif>@endif
                    Precio: *</label>
                   <div class="col-sm-6 input-group">
                    <div class="input-group-addon">S/.</div>
                     <input type="text" class="form-control" maxlength="12" style="width: 130px" name="precio" id="precio" value="{{ number_format($publication->price,2,',','.') }}">
                     @if ($errors->has('precio'))
                            <span class="text-danger" style="line-height: 30px; margin-left: 10px">
                             {{ $errors->first('precio') }}
                         </span>
                     @endif
                   </div>
                   @if($tipo_producto === 'servicio')
                    <label for="convenir2" class="col-sm-3 control-label" style="line-height: 30px;margin-left: 12px;"><input type="radio" name="convenir" id="convenir2" value="1" style="margin-right: 5px" @if($publication->price == '0') checked @endif>A convenir</label>
                    @endif
                     
                 </div>
                 
                 </div>
               </div>            
                  
               @if($tipo_producto === 'producto')
               <div class="row">
                 <div class="col-md-12">
                   <h3 style="text-transform: none;"><span>Selecciona el plan de publicación para tu producto</span></h3>
                 </div>
               </div>
               
               <div class="alert alert-info" role="alert">En cualquiera de los planes tu publicación tendrá una vigencia de {{ config('app.global.publication_lifetime_days') }} días</div>

                 <div class="row">
                     <div class="col-md-3">
                         <a href="javascript: void(0)" class="target-anchor" data-plan="Premium" data-price="S/. 10,00">
                         <div class="plan-target">
                             <i class="fa fa-check" style="display: none"></i>
                             <h3>Premium</h3>
                             <div class="plan-price">
                                 <div class="plan-price-value">S/. 10,00</div>
                                 <div class="plan-price-spec">Costo por publicación</div>
                             </div>
                             <div class="plan-spec">
                                 <ul>
                                     <li>Recibe un <strong>100%</strong> más de visitas.</li>
                                 </ul>
                             </div>
                         </div>
                         </a>
                     </div>
                     <div class="col-md-3">
                         <a href="javascript: void(0)" class="target-anchor" data-plan="Oro" data-price="S/. 6,00">
                         <div class="plan-target">
                             <i class="fa fa-check" style="display: none"></i>
                             <h3>Oro</h3>
                             <div class="plan-price">
                                 <div class="plan-price-value">S/. 6,00</div>
                                 <div class="plan-price-spec">Costo por publicación</div>
                             </div>
                             <div class="plan-spec">
                                 <ul>
                                     <li>Recibe un <strong>70%</strong> más de visitas.</li>
                                 </ul>
                             </div>
                         </div>
                         </a>
                     </div>
                     <div class="col-md-3">
                         <a href="javascript: void(0)" class="target-anchor" data-plan="Plata" data-price="S/. 4,00">
                         <div class="plan-target">
                             <i class="fa fa-check" style="display: none"></i>
                             <h3>Plata</h3>
                             <div class="plan-price">
                                 <div class="plan-price-value">S/. 4,00</div>
                                 <div class="plan-price-spec">Costo por publicación</div>
                             </div>
                             <div class="plan-spec">
                                 <ul>
                                     <li>Recibe un <strong>50%</strong> más de visitas.</li>
                                 </ul>
                             </div>
                         </div>
                         </a>
                     </div>
                     <div class="col-md-3">
                         <a href="javascript: void(0)" class="target-anchor" data-plan="Bronce" data-price="S/. 2,00">
                         <div class="plan-target">
                             <i class="fa fa-check" style="display: none"></i>
                             <h3>Bronce</h3>
                             <div class="plan-price">
                                 <div class="plan-price-value">S/. 2,00</div>
                                 <div class="plan-price-spec">Costo por publicación</div>
                             </div>
                             <div class="plan-spec">
                                 <ul>
                                     <li>Recibe un <strong>20%</strong> más de visitas.</li>
                                 </ul>
                                
                             </div>
                         </div>
                         </a>
                     </div>
                     <div class="col-md-3">
                         <a href="javascript: void(0)" class="target-anchor" data-plan="Gratis" data-price="Gratis">
                         <div class="plan-target target-active">
                             <i class="fa fa-check"></i>
                             <h3>Gratis</h3>
                             <div class="plan-price">
                                 <div class="plan-price-value">S/. 0,00</div>
                                 <div class="plan-price-spec">Costo por publicación</div>
                             </div>
                             <div class="plan-spec">
                                 <ul>
                                     <li>Exposición al final de las listas.</li>
                                 </ul>
                                
                             </div>
                         </div>
                         </a>
                     </div>
                 </div>
               @else
                 <div class="row">
                     <div class="col-md-3">
                         <a href="javascript: void(0)" class="target-anchor" data-plan="Clasificado" data-price="S/. 20">
                         <div class="plan-target target-active">
                             <i class="fa fa-check"></i>
                             <h3>Clasificado</h3>
                             <div class="plan-price">
                                 <div class="plan-price-value">S/. 20,00</div>
                                 <div class="plan-price-spec">Costo por publicación</div>
                             </div>
                             <div class="plan-spec">
                                 <ul>
                                     <li>Clasificados</li>
                                 </ul>
                                
                             </div>
                         </div>
                         </a>
                     </div>
                 </div>
               
               @endif
               @if($tipo_producto === 'producto')
                <input type="hidden" name="plan" id="plan" value="Gratis">
                @else
                <input type="hidden" name="plan" id="plan" value="Fijo">
                @endif
               <br><br>
               
                 <a class="btn btn-link" href="{{ route('publications') }}">Cancelar</a> <button class="btn btn-primary">Republicar</button>
                 <br><br>
               </form>
             </div>
           </div>
            <div class="col-xs-12">Tu publicación debe cumplir con las Políticas de {{ config('app.name') }}</div>
         </div>
       </div>
     </section>

@endsection

@section('scripts')
	@parent
  <script src="/js/jquery.number.min.js"></script>
  
  <script>
    $(document).ready(function() {
        
      $('#precio').number(true, 2, ',', '.' );
      @if($tipo_producto === 'producto')     
        $(".target-anchor").click(function() {
            $('.plan-target').removeClass('target-active');
            $('.plan-target .fa-check').hide();
            $(this).find('.plan-target').addClass('target-active');
            $('.plan-target .fa-check').hide();
            $('.target-active .fa-check').show();
            $("#plan").val($(this).attr('data-plan'));
        });
        @endif
      
      
      
    });
    
    
    </script>
@endsection

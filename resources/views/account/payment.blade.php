@extends('layouts.myaccount')

@section('title', 'Pago de factura')

@section('myaccountcontent')
<link href="/css/jquery.filer.css" rel="stylesheet">
<h3>Reportar pago</h3>
<form name="payment-form" method="post" action="{{ route('payment_save') }}" enctype="multipart/form-data">
<input type="hidden" name="id" value="{{ $factura->id }}">
{{ csrf_field() }}
<div class="row">
    <div class="col-md-8">
        
        @if($factura->payment_status == 'Rechazado')
        <div class="label label-danger">Pago rechazado</div>
        @endif
        @if($factura->payment_status == 'Aprobado')
        <div class="label label-success">Pago aprobado</div>
        @endif
        @if($factura->payment_status == 'Por revisar')
        <div class="label label-warning">Pago por revisar</div>
        @endif
        
	<br>
	<br>
        <img src="{{ asset('img/payment_methods/BCP.png') }}" width="80px" height="50px">
        <input type="radio" name="payment" value="banco_credito">
        <img src="{{ asset('img/payment_methods/bbva-continental.png') }}" width="80px" height="50px">
        <input type="radio" name="payment" value="bbva_continental">
        <img src="{{ asset('img/payment_methods/interbank.png') }}" width="80px" height="50px">
        <input type="radio" name="payment" value="interbank">
        <img src="{{ asset('img/payment_methods/bitcoin.png') }}" width="80px" height="50px">
        <input type="radio" name="payment" value="bitcoin">

        <div id="info-payment"></div>
    <br>
    <br>
	<strong>¿Describe tu pago?</strong>
        @if ($errors->has('description'))
        <br>
            <span class="text-danger">
                {{ $errors->first('description') }}
            </span>
        @endif
        <textarea class="form-control" name="description" id="description" rows="3" style="max-width: 300px; margin-top: 10px">{{ $factura->payment_description }}</textarea>
        <div style="font-size: 12px;">Quedan <span id="num-chars">200</span> caracteres.</div>
	<br>
        <br>
        <strong>Anexa soportes de tus recibos de pago</strong>
        @if($factura->image1)
        <div><a href="{{ route('download-payments', ['file' => $factura->image1, 'name' => $factura->name1]) }}" class="btn-link"><i class="fa fa-file"></i> {{ $factura->name1 }}</a></div>
        @endif 
        @if($factura->image2)
        <div><a href="{{ route('download-payments', ['file' => $factura->image2, 'name' => $factura->name2]) }}" class="btn-link"><i class="fa fa-file"></i> {{ $factura->name2 }}</a></div>
        @endif
        @if($factura->image3)
        <div><a href="{{ route('download-payments', ['file' => $factura->image3, 'name' => $factura->name3]) }}" class="btn-link"><i class="fa fa-file"></i> {{ $factura->name3 }}</a></div>
        @endif
        <br>
        <div class="col-md-10">
	     <input type="file" name="files[]" id="filer_input" multiple="multiple">
	</div>
        <div class="col-md-12">
            <button class="btn btn-primary" id="btn-submit"  @if(!strlen($factura->payment_description)) disabled="true" @endif>Guardar</button> <a href="{{ url('billing') }}" class="btn btn-link">Cancelar</a>
        </div>
    </div>
    <div class="col-md-4">
	<div class="product-container">
        <div style="margin-top: 8px">
            <b>Número de factura:</b><br>{{ $factura->id }}<br>
            <b>Fecha de emisión:</b><br>{{ date('d-m-Y', strtotime($factura->emitted_at)) }}<br>
            <b>Monto:</b><br>{{ number_format($factura->getTotal(),2,',','.') }}
	</div>
	</div>
    </div>
</div>
</form>
@endsection
@section('scripts')
    @parent
    <script src="/js/jquery.filer.min.js" type="text/javascript"></script>
    <script>
	$(document).ready(function(){

	    $('input[type=radio][name=payment]').change(function () {
	        $('#info-payment').empty();
            $.ajax({
                method: "GET",
                url: "{{url('/getInfoPayment')}}",
                data: { payment: this.value, _token: '{{ csrf_token() }}'}
            }).done(function( msg ) {
                $('#info-payment').append("<div>"+msg[0].description+"</div>");
            });
        });

        $('#filer_input').filer({
                limit: 3,
                maxSize: 3,
                extensions: ["jpg", "png", "gif", "pdf"],
                showThumbs: true,
                addMore: true,
                allowDuplicates: false
        });
            
	var maxChars = 200;
	$("#description").keyup(controlChars);


	function controlChars(e) {
	  var rest = maxChars - $(this).val().length;
	  if(rest < 0) rest = 0;
	  if($(this).val().length > 0) $("#btn-submit").prop('disabled', false);
	  else $("#btn-submit").prop('disabled', true);
	  $("#num-chars").text(rest);
		    if (e.which < 0x20) {
			    // e.which < 0x20, then it's not a printable character
			    // e.which === 0 - Not a character
			    return;     // Do nothing
		    }
		    if ($(this).val().length == maxChars) {
			    e.preventDefault();
		    } else if ($(this).val().length > maxChars) {
			    // Maximum exceeded
			    $(this).val($(this).val().substring(0, maxChars));
		    }
	    }
	});
    </script>
@endsection
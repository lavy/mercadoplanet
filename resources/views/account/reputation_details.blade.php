<div class="row">
    <div class="col-md-4 col-md-offset-4" style="text-align: center;margin-bottom: 15px"><i class="fa fa-map-marker"></i> {{ $user->location() }}</div>
    @if($user->seller_type != 'Normal')
    <?php $image_lider = ($user->seller_type=='Líder' ? 'lider.png': 'lider-gold.png'); ?>
    <div>
        <div class="col-md-4 col-md-offset-4" style="text-align: center;margin-bottom: 10px" >
            <img src="/img/assets/{{ $image_lider }}" style="position: relative;left: -40px;top: -3px;">
            <span style="text-align: center;margin-bottom: 15px;background-color: #f3f3f3;padding: 10px;border-radius: 5px; margin-left: -40px;">
                Este es un Vendedor <strong>@if($user->seller_type=='Líder Gold') Oro @else Plata @endif</strong>
            </span>
        </div>
    </div>
    @endif
    <div class="col-md-4 col-md-offset-4" style="text-align: center;">
        {!! compute_stars($reputation['value']) !!}
    </div>
</div>
    @if($reputation['value'] !== -1)
    <div class="arrow-box" style="top: 20px;padding: 20px 15px 10px 15px;">
        <div class="row">
            <div class="col-md-3">
                <div style="font-size: 35px;font-weight: bold; text-align: center; margin-bottom: 15px">{{ $reputation['recommended'] }}%</div>
                <div style="font-size: 14px;text-align: center">de sus compradores lo recomiendan</div>
            </div>
            <div class="col-md-3">
                <div style="font-size: 35px;font-weight: bold; text-align: center; margin-bottom: 15px">{{ $reputation['sales'] }}</div>
                <div style="font-size: 14px;text-align: center">ventas concretadas</div>
            </div>
            <div class="col-md-3">
                @if($reputation['level'] === 5)
                <div style="text-align: center; margin-bottom: 15px"><strong>Vendedor destacado</strong><br>se encuentra entre los mejores del sitio.</div>
                @endif
                @if($reputation['level'] === 4)
                <div style="text-align: center; margin-bottom: 15px"><strong>Vendedor destacado</strong><br>por sus buenas calificaciones.</div>
                @endif
                @if($reputation['level'] === 3)
                <div style="font-weight: bold; text-align: center; margin-bottom: 15px">Buen vendedor.</div>
                @endif
                @if($reputation['level'] === 2)
                <div style="text-align: center; margin-bottom: 15px">Este vendedor <strong>ha tenido problemas con algunas de sus ventas.</strong></div>
                @endif
                @if($reputation['level'] === 1)
                <div style="font-weight: bold; text-align: center; margin-bottom: 15px"><span class="fa fa-user"></span><br>Este vendedor no tiene buena reputación en el sitio.</div>
                @endif
            </div>
            <div class="col-md-3">
                <div style="text-align: center; margin-bottom: 15px"><strong>{{ $reputation['time'] }}</strong><br>vendiendo en MercadoPlanet.</div>
            </div>
        </div>
    </div>
    @else   
<div class="row">
    <div class="col-md-4 col-md-offset-4" style="text-align: center;margin-top: 15px">        
        El vendedor <strong>aún no tiene suficientes ventas</strong> para calcular su reputación.
    </div>
</div>
    @endif
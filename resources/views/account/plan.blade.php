@extends('layouts.app')

@section('title', 'Inicio')

@section('content')
<form id="msg-form" method="post" action="{{ url('/save-plan') }}" role="form">
<input type="hidden" name="plan" id="plan" value="">
<input type="hidden" name="id" id="id" value="{{ $publication->id }}">
{{ csrf_field() }}
<section class="mainContent clearfix userProfile">
       <div class="container">
        <?php $tipo_producto = $publication->getProductType(); ?>
         <div class="row">
           <div class="col-xs-12">
             <div class="innerWrapper">
            
               @if($tipo_producto === 'producto')
               
               <div class="row">
                 <div class="col-md-12">
                   <h3 style="text-transform: none;"><span>Aumenta la exposición de tu producto para mejores resultados en las búsquedas</span></h3>
                 </div>
               </div>
               <div class="row">
                 <div class="col-md-12">
                   {{ $publication->title }}
                 </div>
               </div>
               <br>
               
               <div class="alert alert-info" role="alert">En cualquiera de los planes la vigencia de tu publicación se extenderá por {{ config('app.global.publication_lifetime_days') }} días a partir de la fecha</div>

                 <div class="row">
                     <div class="col-md-3">
                         <a href="javascript: void(0)" class="target-anchor" data-plan="Premium" data-price="S/. 10,00">
                         <div class="plan-target">
                             <i class="fa fa-check" style="display: none"></i>
                             <h3>Premium</h3>
                             <div class="plan-price">
                                 <div class="plan-price-value">S/. 10,00</div>
                                 <div class="plan-price-spec">Costo por publicación</div>
                             </div>
                             <div class="plan-spec">
                                 <ul>
                                     <li>Recibe un <strong>100%</strong> más de visitas.</li>
                                 </ul>
                             </div>
                         </div>
                         </a>
                     </div>
                     
                     @if($publication->cost_type == 'Plata' || $publication->cost_type == 'Bronce' || $publication->cost_type == 'Gratis')
                     <div class="col-md-3">
                         <a href="javascript: void(0)" class="target-anchor" data-plan="Oro" data-price="S/. 6,00">
                         <div class="plan-target">
                             <i class="fa fa-check" style="display: none"></i>
                             <h3>Oro</h3>
                             <div class="plan-price">
                                 <div class="plan-price-value">S/. 6,00</div>
                                 <div class="plan-price-spec">Costo por publicación</div>
                             </div>
                             <div class="plan-spec">
                                 <ul>
                                     <li>Recibe un <strong>70%</strong> más de visitas.</li>
                                 </ul>
                             </div>
                         </div>
                         </a>
                     </div>
                     @endif
                     @if($publication->cost_type == 'Bronce' || $publication->cost_type == 'Gratis')
                     <div class="col-md-3">
                         <a href="javascript: void(0)" class="target-anchor" data-plan="Plata" data-price="S/. 4,00">
                         <div class="plan-target">
                             <i class="fa fa-check" style="display: none"></i>
                             <h3>Plata</h3>
                             <div class="plan-price">
                                 <div class="plan-price-value">S/. 4,00</div>
                                 <div class="plan-price-spec">Costo por publicación</div>
                             </div>
                             <div class="plan-spec">
                                 <ul>
                                     <li>Recibe un <strong>50%</strong> más de visitas.</li>
                                 </ul>
                             </div>
                         </div>
                         </a>
                     </div>
                     @endif
                     @if($publication->cost_type == 'Gratis')
                     <div class="col-md-3">
                         <a href="javascript: void(0)" class="target-anchor" data-plan="Bronce" data-price="S/. 2,00">
                         <div class="plan-target">
                             <i class="fa fa-check" style="display: none"></i>
                             <h3>Bronce</h3>
                             <div class="plan-price">
                                 <div class="plan-price-value">S/. 2,00</div>
                                 <div class="plan-price-spec">Costo por publicación</div>
                             </div>
                             <div class="plan-spec">
                                 <ul>
                                     <li>Recibe un <strong>20%</strong> más de visitas.</li>
                                 </ul>
                                
                             </div>
                         </div>
                         </a>
                     </div>
                     @endif
                 </div>
               @else
                 <div class="row">
                     <div class="col-md-3">
                         <a href="javascript: void(0)" class="target-anchor" data-plan="Clasificado" data-price="S/. 20">
                         <div class="plan-target target-active">
                             <i class="fa fa-check"></i>
                             <h3>Clasificado</h3>
                             <div class="plan-price">
                                 <div class="plan-price-value">S/. 20,00</div>
                                 <div class="plan-price-spec">Costo por publicación</div>
                             </div>
                             <div class="plan-spec">
                                 <ul>
                                     <li>Clasificados</li>
                                 </ul>
                                
                             </div>
                         </div>
                         </a>
                     </div>
                 </div>
               
               @endif
               
               <br><br>
               <a class="btn btn-link" href="{{ route('publications') }}">Volver</a> <button type="submit" id="submit-button" class="btn btn-primary" disabled>Confirmar</a>
                 <br><br>
             </div>
           </div>
            <div class="col-xs-12">Tu publicación debe cumplir con las Políticas de {{ config('app.name') }}</div>
         </div>
       </div>
     </section>
</form>
@endsection

@section('scripts')
	@parent

  <script src="/js/jquery.number.min.js"></script>
  <script>
    $(document).ready(function() {
        $('#precio').number(true, 2, ',', '.' );
        @if($tipo_producto === 'producto')     
        $(".target-anchor").click(function() {
            $('.plan-target').removeClass('target-active');
            $('.plan-target .fa-check').hide();
            $(this).find('.plan-target').addClass('target-active');
            $('.plan-target .fa-check').hide();
            $('.target-active .fa-check').show();
            $("#plan").val($(this).attr('data-plan'));
            $("#modal-price").text($(this).attr('data-price'));
            if($("#plan").val() != '') {
                $("#submit-button").removeAttr('disabled');
            }
        });
        @endif
    });  
    
  
  </script>
@endsection

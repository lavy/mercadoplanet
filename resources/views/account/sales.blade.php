@extends('layouts.myaccount')

@section('title', 'Compras')

@section('myaccountcontent')
@if (Session::has('message'))
<div class="row">
    <div class="col-xs-12">
      <div class="alert  {{ Session::get('alert-class', 'alert-info') }}" role="alert">
          <strong>{{ Session::get('message') }}</strong>
      </div>
    </div>
</div>
@endif
<?php $sales = ($status == 'open' ? $saleso : $salesc); ?>
<h3>Ventas</h3><section class="mainContent clearfix">
    <div class="row  userProfile">
      <div class="col-md-9">
        <div class="btn-group" role="group" id="pub-labels">
            <a href="{{ route('sales', array_merge($query, ['status' => 'open'])) }}" class="btn btn-default @if($status == 'open') active @endif"><i class="fa fa-play-circle" aria-hidden="true"></i>Abiertas ({{ $num_open }})</a>
            <a href="{{ route('sales', array_merge($query, ['status' => 'closed'])) }}" class="btn btn-default @if($status == 'closed') active @endif"><i class="fa fa-close" aria-hidden="true"></i>Cerradas ({{ $num_closed }})</a>
        </div>
      </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-9">
            <div class="form-inline">
                <input type="text" name="search" id="search" value="{{ $search }}" class="form-control" placeholder="Buscar por comprador o artículo" style="width:300px">
                <button class="btn btn-primary" id="button-search" style="padding: 0px 10px;height: 32px;margin-left: 10px;margin-right: 10px;">Buscar</button>
            </div> 
        </div>
        <div class="col-md-3">
            <div class="filterArea pub-filter">
                <select name="sort-box" id="sort-box" class="select-drop">
                    <option value="1" {{ ($sort=='1' ? 'selected':'') }}>Más recientes</option>
                    <option value="2" {{ ($sort=='2' ? 'selected':'') }}>Menos recientes</option>
                </select>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-2" style="line-height: 30px">
        Cobros:
        </div>
        <div class="col-md-3 filterArea pub-filter" style="height: 35px;">
            <select name="charge-box" id="charge-box" class="select-drop">
                <option value="1" {{ ($charge=='1' ? 'selected':'') }}>A cobrar</option>
                <option value="2" {{ ($charge=='2' ? 'selected':'') }}>Cobrados</option>
                <option value="3" {{ ($charge=='3' ? 'selected':'') }}>Todos</option>
            </select>            
        </div>
        <div class="col-md-2" style="line-height: 30px">
        Calificaciones:
        </div>
        <div class="col-md-3 filterArea pub-filter" style="height: 35px;">
            <select name="qualification-box" id="qualification-box" class="select-drop">
                <option value="1" {{ ($qualification=='1' ? 'selected':'') }}>Sin calificar</option>
                <option value="2" {{ ($qualification=='2' ? 'selected':'') }}>Concretadas</option>
                <option value="3" {{ ($qualification=='3' ? 'selected':'') }}>Canceladas</option>
                <option value="4" {{ ($qualification=='4' ? 'selected':'') }}>Contradictorias</option>
                <option value="5" {{ ($qualification=='5' ? 'selected':'') }}>Aún no me califican</option>
                <option value="6" {{ ($qualification=='6' ? 'selected':'') }}>Todos</option>
            </select>
        </div>
    </div>
    <br>
@if(count($sales))
<div class="table-responsive" style="min-height: 250px">
  <table class="table table-hover table-favorites">
      <tbody>
          @foreach($sales as $pur)
          <?php $p = $pur->publication; ?>
          <tr>
              <td>
                  <strong>{{ $pur->buyer->getFullName() }}</strong><br>
                  <div style="color:#666">{{ $pur->buyer->email }}</div>
                  <div>{{ $pur->buyer->phone_number }}</div>
                  <div><a href="{{ route('sale_detail', ['id' => $pur->id, 'go_messages' => 'true']) }}" class="btn-link">Enviar mensaje</a></div>
              </td>
              <td style="width: 40px">
                  @if(count($p->images))
                  <img src="{{ url('/photo/40x40?url='.urlencode($p->images[0]->name)) }}">
                  @else
                  <img src="{{ url('/photo/40x40?url=nophoto.jpg') }}">
                  @endif
              </td>
              <td style="width:320px">
                  <a href="{{ route('product',[$p->id]) }}" style="padding: 0" class="btn-link">{{ $p->title }}</a>
                  <div style="color:#999"><span style="color:#337ab7;font-weight: bold">S/{{ number_format($p->price, 2, ',', '.') }}</span> x {{ $pur->quantity }} @if($pur->quantity == 1)unidad @else unidades @endif</div>
              </td>
              <td style="width: 170px;color: #666">  
                @if($pur->seller_calification !== 'No calificado' && (time() - strtotime($pur->created_at)) < (config('app.global.days_to_qualify') * 24 * 3600))
                <div>
                    @if(($pur->seller_concreted and $pur->buyer_concreted) or ($pur->seller_concreted and $pur->buyer_calification == 'No calificado'))
                    <i class="fa fa-check" style="color:green; font-size: 16px"></i> Concretado
                    @elseif((!$pur->seller_concreted and !$pur->buyer_concreted) or (!$pur->seller_concreted and $pur->buyer_calification == 'No calificado'))
                    <i class="fa fa-remove" style="color:red; font-size: 16px"></i> Cancelado
                    @else
                    <i class="fa fa-ban" style="color:red; font-size: 16px"></i> Contradicción
                    @endif
                </div>
                @endif
                @if($pur->seller_calification === 'Positivo')
		<a href="" data-toggle="modal" data-target="#qualification-modal" class="btn-link" data-sale="{{ $pur->id }}">
                <i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i> 
                Calificaste positivo
		</a>
                @elseif($pur->seller_calification === 'Negativo')
		<a href="" data-toggle="modal" data-target="#qualification-modal" class="btn-link" data-sale="{{ $pur->id }}">
                <i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i> 
                Calificaste negativo
		</a>
                @elseif($pur->seller_calification === 'Neutral')
		<a href="" data-toggle="modal" data-target="#qualification-modal" class="btn-link" data-sale="{{ $pur->id }}">
                <i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i> 
                Calificaste neutral
		</a>
                @elseif(($pur->seller_calification === 'No calificado' && (time() - strtotime($pur->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600))
                    or ($pur->seller_calification === 'No calificado' && $pur->seller_qualification_modified)
                )
                No calificaste 
		@else
		<a href="{{ route('seller_qualification', ['id' => $pur->id]) }}" class="btn-link"><i class="fa fa-clock-o" style="font-size: 16px;color:#666"></i> {{ $pur->getTimeToQualify() }} dias para calificar</a>
                @endif
                
                @if($pur->seller_calification != 'No calificado')
                @if($pur->buyer_calification == 'No calificado')
                @if((time() - strtotime($pur->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600)
                or ($pur->buyer_calification === 'No calificado' && $pur->buyer_qualification_modified))
                <div>No te calificaron</div>
                @else
                <div>{{ $pur->getTimeToQualify() }} para que califique</div>
                @endif
                @else
                @if($pur->buyer_calification === 'Positivo')
                <div><i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i>
                Te calificaron positivo</div>
                @elseif($pur->buyer_calification === 'Negativo')
                <div><i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i>
                Te calificaron  negativo</div>
                @elseif($pur->buyer_calification === 'Neutral')
                <div><i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i>
                Te calificaron neutral</div>
                @endif
                @endif
                @endif
                @if($pur->charged) <div><i  class="fa fa-money" style="color: #47bac1; font-size: 16px"></i> cobrado</div> @endif
              </td>
              <td>
                  <div style="text-align: center">                      
                        <a href="{{ route('sale_detail', ['id' => $pur->id]) }}" class="btn-link">Ver detalle</a>
                        
                    <div class="btn-group" style="margin-bottom: 0px;">
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-gear"></i> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu menu-publications">
                          <li><a href="{{ route('sale_detail', ['id' => $pur->id, 'go_messages' => 'true']) }}">Enviar mensaje</a></li>
                          <li><a href="#" class="add-note-button" data-id="{{ $pur->id }}">Agregar nota</a></li>
                          
                          <li><a href="{{ route('charged', ['id' => $pur->id]) }}">
                                  @if($pur->charged)
                                  Marcar no cobrado
                                  @else
                                  Marcar cobrado
                                  @endif
                              </a>
                          </li>
                        </ul>
                  </div>
                  </div>
              </td>
          </tr>
          <tr class="row-notes" style="display: none" id="add-note-container-{{ $pur->id }}">
              <td colspan="5">
                  <div>
                    <textarea class="form-control note" style="width: 50%" name="note" id="note-{{ $pur->id }}" data-id="{{ $pur->id }}" placeholder="Escribe una nota..."></textarea>
                    <div style="font-size: 12px;">Quedan <span id="num-chars-{{ $pur->id }}">250</span> caracteres.</div>
                    <button type="submit" class="btn btn-primary btn-note" style="padding: 0px 5px; height: 35px" id="btn-note-{{ $pur->id }}" data-id="{{ $pur->id }}" disabled="true">Guardar</button><img src="/img/assets/ajax-loader.gif" id="spinner2" style="display: none">
                    <a href="#" class="btn-link btn-add-cancel" data-id="{{ $pur->id }}">Cancelar</a>
                  </div>
              </td>
          </tr>
          <?php $note = $pur->sellerNotes()->orderBy('created_at', 'desc')->first(); ?>
          <tr class="row-notes" id="row-notes-{{ $pur->id }}" >
              <td colspan="3">
                  @if(count($note))
                    <div style="float:left"><i class="fa fa-pencil-square-o" style="font-size: 16px; color: #999"></i></div>
                    <div style="float:left; width: 60%; margin-left: 5px;" class="note-description">{{ $note->description }}</div>
                    <div style="float:left; width: 60%; margin-left: 5px;display: none"  class="note-edit-description">
                        <textarea class="form-control" data-note-id="{{ $note->id }}">{{ $note->description }}</textarea>
                        <div style="font-size: 12px;">Quedan <span class="num-chars3">{{ 250 - strlen($note->description) }}</span> caracteres.</div>
                        <button class="btn-link btn-save" data-id="{{ $pur->id }}">Guardar</button>
                        <button class="btn-link btn-cancel">Cancelar</button>
                    </div>
                    <div style="float:left; width: 15%; display: none" class="note-options"><button class="btn-link btn-mod-option">Modificar</button></div>
                    <div style="float:left; width: 15%; display: none" class="note-options"><button class="btn-link btn-del-option" data-id="{{ $pur->id }}">Eliminar</button></div>
                  @endif
              </td>
          </tr>
          @endforeach
      </tbody>
  </table>
</div>
<div class="col-xs-12">
    <div class="text-center">
        {{ $sales->appends($query)->links() }}
    </div>
</div>
@else
<div class="row">
    <div class="col-xs-12">
      <div class="alert alert-warning" role="alert">
          <strong>No tienes ninguna venta.</strong>
      </div>
    </div>
</div>
@endif
</section>

<div class="modal fade" id="qualification-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Calificación</h4>
      </div>
      <div class="modal-body">
	  <img src="/img/assets/ajax-loader.gif" id="spinner">
	  <div class="modal-container"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

@endsection
@section('scripts')
    @parent
    <script src="/js/moment.js"></script>
    <script>
        $(document).ready(function() {
          moment.locale('es');
          $('.moment-ends').each(function(i, obj) {
              var date = $(this).attr('data-date');
              $(this).text(moment(date, "YYYY-MM-DD H:m:s").fromNow());
          });
          
          $(document).on({
                mouseenter: function () {
                    $(this).find('.note-options').show();         
                },
                mouseleave: function () {
                    $(this).find('.note-options').hide();         
                }
            }, '.row-notes');
            
            $(document).on('click', '.btn-mod-option', function() {
                $(this).parent().parent().find('.note-edit-description').show();
                $(this).parent().parent().find('.note-description').hide();
                var id = $(this).parent().parent().find('.btn-del-option').attr('data-id');
                $('#add-note-container-'+id).hide();
            });
            
            $(document).on('click', '.btn-cancel', function() {
                $(this).parent().parent().find('.note-edit-description').hide();
                $(this).parent().parent().find('.note-description').show();
            });
            
            $(document).on('click', '.btn-del-option', function() {
                var row = $(this).parent().parent().parent();
                var note_id = $(this).parent().parent().find('textarea').attr('data-note-id');
                id = $(this).attr('data-id');
                $.ajax({
                    method: "POST",
                    url: "{{url('/delete-note')}}",
                    cache: false,
                    data: { id: id, note_id: note_id, _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
		    var output = '';
                    if(msg.is_ok) {
                        var resp = JSON.parse(msg.value);
                        if(resp.id != '') {;
                            $('#row-notes-'+id+' .note-description').text(resp.description);
                            $('#row-notes-'+id+' .btn-del-option').attr('data-id', id);
                            $('#row-notes-'+id+' textarea').text(resp.description);
                            $('#row-notes-'+id+' textarea').attr('data-note-id', resp.id);
                        } else {
                            row.find('td').html('');
                        }
                        
                    }
                });
            });
	  
          $(".add-note-button").click(function(e) {
              e.preventDefault();
              var id = $(this).attr('data-id');
              $('#add-note-container-'+id).show();
              $('#row-notes-'+id).find('.note-edit-description').hide();
              $('#row-notes-'+id).find('.note-description').show();
          });
	  
          $(".btn-add-cancel").click(function(e) {
              e.preventDefault();
              var id = $(this).attr('data-id');
              $('#add-note-container-'+id).hide();
          });
          
	  $('#qualification-modal').on('show.bs.modal', function (event) {
	    var button = $(event.relatedTarget);
	    var recipient = button.data('sale');
	    var modal = $(this);
	    $("#spinner").show();
	    modal.find('.modal-container').html('');
	    $.ajax({
                    method: "POST",
                    url: "{{url('/qualification-seller')}}",
                    cache: false,
                    data: { id: recipient, _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
		    var output = '';
                    if(msg.is_ok) {
			var resp = JSON.parse(msg.value);
			if(resp.seller_qualification == 'Positivo') {
			    output += '<div><i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i> Calificaste positivo</div>';
			}
			if(resp.seller_qualification == 'Neutral') {
			    output += '<div><i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i> Calificaste neutral</div>';
			}
			if(resp.seller_qualification == 'Negativo') {
			    output += '<div><i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i> Calificaste negativo</div>';
			}
			
			if(resp.seller_qualification != 'No calificado') {
			    output += '<div style="margin-top: 5px">"'+resp.seller_comment+'"</div>';
                            if(resp.buyer_reply !== '' && resp.buyer_reply) {
                                output += '<div style="margin-top: 5px;margin-left: 15px">Replica: "'+resp.buyer_reply+'"</div>';
                            }
			    if(resp.can_modify == 'true') {
				output += '<a href="{{ route('seller_qualification_edit') }}?id='+recipient+'" class="btn-link">Modificar</a>';
			    }
			}
			if(resp.buyer_qualification != 'No calificado') {
			    output += '<hr>';
			    if(resp.buyer_qualification == 'Positivo') {
				output += '<div><i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i> Te calificó positivo</div>';
			    }
			    if(resp.buyer_qualification == 'Neutral') {
				output += '<div><i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i> Te calificó neutral</div>';
			    }
			    if(resp.buyer_qualification == 'Negativo') {
				output += '<div><i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i> Te calificó negativo</div>';
			    }
			    output += '<div style="margin-top: 5px">"'+resp.buyer_comment+'"</div>';
                            
                            if(resp.seller_reply !== '' && resp.seller_reply) {
                                output += '<div style="margin-top: 5px;margin-left: 15px">Replica: "'+resp.seller_reply+'"</div>';
                            }
                            
                            if(resp.can_modify == 'true') {
				output += '<a href="{{ route('seller_reply_edit') }}?id='+recipient+'" class="btn-link">Replicar</a>';
			    }
			}
			$("#spinner").hide();
                    }
		    
		    modal.find('.modal-container').html(output);
                });
	    
	    //modal.find('.modal-body').text('New message to ' + recipient);
	    });
            
            $('#sort-box').change(function() {
                if($(this).val() == '1') {
                   $(location).attr('href', "{!! route('sales', array_merge($query, ['sort' => '1'])) !!}");
                }
                if($(this).val() == '2') {
                   $(location).attr('href', "{!! route('sales', array_merge($query, ['sort' => '2'])) !!}");
                }
            });
            
            $('#charge-box').change(function() {
                if($(this).val() == '1') {
                   $(location).attr('href', "{!! route('sales', array_merge($query, ['charge' => '1'])) !!}");
                }
                if($(this).val() == '2') {
                   $(location).attr('href', "{!! route('sales', array_merge($query, ['charge' => '2'])) !!}");
                }
                if($(this).val() == '3') {
                   $(location).attr('href', "{!! route('sales', array_merge($query, ['charge' => '3'])) !!}");
                }
            });
            
            $('#qualification-box').change(function() {
                if($(this).val() == '1') {
                   $(location).attr('href', "{!! route('sales', array_merge($query, ['qualification' => '1'])) !!}");
                }
                if($(this).val() == '2') {
                   $(location).attr('href', "{!! route('sales', array_merge($query, ['qualification' => '2'])) !!}");
                }
                if($(this).val() == '3') {
                   $(location).attr('href', "{!! route('sales', array_merge($query, ['qualification' => '3'])) !!}");
                }
                if($(this).val() == '4') {
                   $(location).attr('href', "{!! route('sales', array_merge($query, ['qualification' => '4'])) !!}");
                }
                if($(this).val() == '5') {
                   $(location).attr('href', "{!! route('sales', array_merge($query, ['qualification' => '5'])) !!}");
                }
                if($(this).val() == '6') {
                   $(location).attr('href', "{!! route('sales', array_merge($query, ['qualification' => '6'])) !!}");
                }
            });
            
            $('#button-search').click(function() {
                $(location).attr('href', "{!! route('sales') !!}?search="+$("#search").val());
            });
            
            $(".btn-note").click(function() {
                id = $(this).attr('data-id');
                var value = $("#note-"+id).val();           
                $("#spinner2").show();
                $.ajax({
                    method: "POST",
                    url: "{{url('/save-note')}}",
                    cache: false,
                    data: { id: id, note: value, _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
		    var output = '';
                    if(msg.is_ok) {
			$("#note-"+id).val('');
                        $("#btn-note"+ id).prop('disabled', true);
                        $("#num-chars-"+id).text(maxChars2);
                        var resp = JSON.parse(msg.value);
                        $('#add-note-container-'+id).hide();
                        if($('#row-notes-'+id+' textarea').text()!='') {                        
                            $('#row-notes-'+id+' .note-description').text(resp.description);
                            $('#row-notes-'+id+' .btn-del-option').attr('data-id', id);
                            $('#row-notes-'+id+' textarea').text(resp.description);
                            $('#row-notes-'+id+' textarea').attr('data-note-id', resp.id);
                        } else {
                            var output = '<div style="float:left"><i class="fa fa-pencil-square-o" style="font-size: 16px; color: #999"></i></div>';
                            output += '<div style="float:left; width: 60%; margin-left: 5px;" class="note-description">'+resp.description+'</div>';
                            output += '<div style="float:left; width: 60%; margin-left: 5px;display: none"  class="note-edit-description">';
                            output += '<textarea class="form-control" data-note-id="'+resp.id+'">'+resp.description+'</textarea>';
                            output += '<div style="font-size: 12px;">Quedan <span class="num-chars3">'+(250-resp.description.length)+'</span> caracteres.</div>';
                            output += '<button class="btn-link btn-save" data-id="'+id+'">Guardar</button>';
                            output += '<button class="btn-link btn-cancel">Cancelar</button>';
                            output += '</div>';
                            output += '<div style="float:left; width: 15%; display: none" class="note-options"><button class="btn-link btn-mod-option">Modificar</button></div>';
                            output += '<div style="float:left; width: 15%; display: none" class="note-options"><button class="btn-link btn-del-option" data-id="'+id+'">Eliminar</button></div>';
                            $('#row-notes-'+id+' td').html(output);
                        }
                    }
                    $("#spinner2").hide();
                });
            });
            
            
            
            $(document).on('click', '.btn-save', function() {
                $('.btn-save').prop('disabled', true);
                id = $(this).attr('data-id');
                var note_id = $(this).parent().find('textarea').attr('data-note-id');
                var note_value = $(this).parent().find('textarea').val();
                var textarea = $(this).parent().parent().find('.note-edit-description');
                var row = $(this).parent().parent().find('.note-description');
                $.ajax({
                    method: "POST",
                    url: "{{url('/update-note')}}",
                    cache: false,
                    data: { id: id, note_id: note_id, note: note_value, _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
		    var output = '';
                    if(msg.is_ok) {
                        var resp = JSON.parse(msg.value);
                        row.text(resp.description);
                        textarea.hide();
                        row.show();
                        $('.btn-save').prop('disabled', false);

                    }
                    $("#spinner2").hide();
                });
            });
            
            var maxChars2 = 250;
            $(".note").keyup(controlChars2);
            $(document).on('keyup', ".note-edit-description textarea", controlChars3);
            
            
            
            function controlChars2(e) {
                var rest = maxChars2 - $(this).val().length;
                var id = $(this).attr('data-id');
                if(rest < 0) rest = 0;
                if($(this).val().length > 0) $("#btn-note-"+id).prop('disabled', false);
                else $("#btn-note-"+id).prop('disabled', true);
                $("#num-chars-"+id).text(rest);
                if (e.which < 0x20) {
                        // e.which < 0x20, then it's not a printable character
                        // e.which === 0 - Not a character
                        return;     // Do nothing
                }
                if ($(this).val().length == maxChars2) {
                        e.preventDefault();
                } else if ($(this).val().length > maxChars2) {
                        // Maximum exceeded
                        $(this).val($(this).val().substring(0, maxChars2));
                }
            }
            
            function controlChars3(e) {
                var rest = maxChars2 - $(this).val().length;
                if(rest < 0) rest = 0;
                if($(this).val().length > 0) $(this).parent().find('.btn-save').prop('disabled', false);
                else $(this).parent().find('.btn-save').prop('disabled', true);
                $(this).parent().find('.num-chars3').text(rest);
                if (e.which < 0x20) {
                        // e.which < 0x20, then it's not a printable character
                        // e.which === 0 - Not a character
                        return;     // Do nothing
                }
                if ($(this).val().length == maxChars2) {
                        e.preventDefault();
                } else if ($(this).val().length > maxChars2) {
                        // Maximum exceeded
                        $(this).val($(this).val().substring(0, maxChars2));
                }
            }
        });
        
          
    </script>
@endsection
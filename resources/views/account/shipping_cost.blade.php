@extends('layouts.myaccount')

@section('title', 'Detalles de la compra')

@section('myaccountcontent')
<link href="/css/jquery.filer.css" rel="stylesheet">
<h3>Modificar costos de envío</h3>
<form name="shipping-form" method="post" action="{{ route('shipping_cost_save') }}">
<input type="hidden" name="id" value="{{ $id }}">
{{ csrf_field() }}
<div class="row">
    <div class="col-md-8">
        
	<br>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="personal_pickup" id="personal_pickup" value="1" @if($publication->personal_pickup == '1') checked @endif>
                Ofrecer retiro en persona
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="make_shipments" id="make_shipments" value="1" @if($make_shipments == '1') checked @endif>
                Se realizan envíos
            </label>
        </div>
        <div id="shipment-cnt" style="display: none">
            <div id="free_shipment_cnt" style="margin-left: 20px;">
                <div class="radio">
                    <label>
                        <input type="radio" class="free-shipment" name="free_shipment" id="free_shipment1" value="1"  @if($publication->free_shipment == '1') checked @endif>
                        Gratis a todo el país
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" class="free-shipment" name="free_shipment" id="free_shipment0" value="0"  @if($publication->free_shipment != '1' and $make_shipments) checked @endif>
                        Cargar costos de envío (opcional)
                    </label>
                </div>
            </div>
            <div id="shipping-costs-parent" style="display: none">
                <div id="shipping-costs-cnt">
                    @foreach($publication->shippingCosts as $sc)
                    <div class="form-inline cost-line">
                        <input class="form-control" name="shipping_cost_name[]" maxlength="100" size="50" value="{{ $sc->name }}"> S/.
                        <input class="form-control cost" name="shipping_cost[]" maxlength="10" size="10" value="{{ number_format($sc->cost,2,',','.') }}">
                        <button class="btn delete-cost-line" style="padding: 5px"><i class="fa fa-trash"></i></button>
                    </div>
                    @endforeach
                    <div class="form-inline cost-line">
                        <input class="form-control" name="shipping_cost_name[]" maxlength="100" size="50"> S/.
                        <input class="form-control cost" name="shipping_cost[]" maxlength="10" size="10">
                        <button class="btn delete-cost-line" style="padding: 5px"><i class="fa fa-trash"></i></button>
                    </div>
                </div>
                <div id="shipping-costs-add"><a href="javascript: void(0)" id="add-shipping-btn" class="btn-link">Agregar otro</a></div>
            </div>
        </div>
        <button class="btn btn-primary" id="btn-submit"  >Guardar</button> <a href="{{ url('publications') }}" class="btn btn-link">Cancelar</a>
    </div>
    <div class="col-md-4">
	<div class="product-container">
	<div>
	    @if(count($publication->images))
	    <img src="{{ url('/photo/160x160?url='.urlencode($publication->images[0]->name)) }}">
	    @else
	    <img src="{{ url('/photo/160x160?url=nophoto.jpg') }}">
	    @endif
	</div>
	    <div style="margin-top: 8px">
	    <a href="{{ route('product',[$publication->id]) }}" style="padding: 0" class="btn-link">{{ $publication->title }}</a>
	</div>
	<div style="color:#333;    margin: 10px 0;font-size: 18px;"><strong>S/{{ number_format($publication->price, 2, ',', '.') }}</strong></div>
	
	</div>
    </div>
</div>
</form>
@endsection
@section('scripts')
    @parent
    <script src="/js/jquery.number.min.js"></script>
    <script>
	$(document).ready(function(){
            
        $('.cost').number(true, 2, ',', '.' );
        
	$("[name=concreted]").click(function() {
          if($("#concreted3").is(':checked')) {
            $("#concreted3-type").show();
          } else {
            $("#concreted3-type").hide();          
          }          
        });
        
	var maxChars = 200;
	$("#opinion").keyup(controlChars);


	function controlChars(e) {
	  var rest = maxChars - $(this).val().length;
	  if(rest < 0) rest = 0;
	  if($(this).val().length > 0) $("#btn-submit").prop('disabled', false);
	  else $("#btn-submit").prop('disabled', true);
	  $("#num-chars").text(rest);
		    if (e.which < 0x20) {
			    // e.which < 0x20, then it's not a printable character
			    // e.which === 0 - Not a character
			    return;     // Do nothing
		    }
		    if ($(this).val().length == maxChars) {
			    e.preventDefault();
		    } else if ($(this).val().length > maxChars) {
			    // Maximum exceeded
			    $(this).val($(this).val().substring(0, maxChars));
		    }
	    }
            
            $("#add-shipping-btn").click(function() {
                var output = '<div class="form-inline cost-line">\n\
                <input class="form-control" name="shipping_cost_name[]" maxlength="100" size="50"> S/.\n\
                <input class="form-control" name="shipping_cost[]" maxlength="10" size="10">\n\
                <button class="btn delete-cost-line" style="padding: 5px"><i class="fa fa-trash"></i></button>\n\
                </div>';
    
                $("#shipping-costs-cnt").append(output);
            });
            
            $(document).on('click', ".delete-cost-line", function(e) {
                e.preventDefault();
                var elem = $(this).parent();
                var lines = elem.parent().find('.cost-line');
                if(lines.length == 1) return;
                elem.remove();
            });
            
            $("#make_shipments").click(function() {
                if($(this).is(':checked')) {
                    $('#shipment-cnt').show();
                } else {
                    $('#shipment-cnt').hide();
                }
            });
            
            $(".free-shipment").click(function() {
                if($("#free_shipment0").is(':checked')) {
                    $('#shipping-costs-parent').show();
                } else {
                    $('#shipping-costs-parent').hide();
                }
            });
            
            @if($make_shipments)
                $("#shipment-cnt").show();
            @endif
            @if($publication->free_shipment != '1' and $make_shipments)
                $("#shipping-costs-parent").show();
            @endif
	});
    </script>
@endsection
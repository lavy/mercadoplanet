@extends('layouts.myaccount')

@section('title', 'Facturación')

@section('myaccountcontent')
@if (Session::has('message'))
<div class="row">
    <div class="col-xs-12">
      <div class="alert  {{ Session::get('alert-class', 'alert-info') }}" role="alert">
          <strong>{{ Session::get('message') }}</strong>
      </div>
    </div>
</div>
@endif
<h3>Facturación</h3>
@if(count($pendiente))
<div class="row">
    <div class="col-xs-12">
      <div class="alert  alert-info" role="alert">
          <strong>Próxima fecha de corte: </strong> {{ date('d-m-Y', strtotime($pendiente->emit_on)) }}
      </div>
    </div>
</div>
@endif
@if(count($facturas) or count($pendiente))
<div class="table-responsive div-publications">
  <table class="table table-hover table-publications">
      <thead>
          <tr>
              <th># Factura</th>
              <th>Fecha de emisión</th>
              <th>Monto</th>
              <th>Estado</th>
              <th>&nbsp;</th>
          </tr>
      </thead>
      <tbody>
          @if(count($pendiente))
           <tr>
              <td>
               {{ $pendiente->id }}
              </td>
              <td>
               No emitida
              </td>
              <td>
               {{ number_format($pendiente->getTotal(),2,',','.') }}
              </td>
              <td>
               Próxima a facturar
              </td>
              <td>                  
                <div style="text-align: center">                      
                    <div class="btn-group" style="margin-bottom: 0px;">
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-gear"></i> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu menu-publications">
                          <li><a href="{{ route('invoice_detail', ['id' => $pendiente->id]) }}">Ver detalle</a></li>
                        </ul>
                    </div>
                </div>
              </td>
          </tr>
          @endif
          @foreach($facturas as $f)
          <tr>
              <td>
               {{ $f->id }}
              </td>
              <td>
               {{ date('d-m-Y', strtotime($f->emitted_at)) }}
              </td>
              <td>
               {{ number_format($f->getTotal(),2,',','.') }}
              </td>
              <td>
               @if($f->status == 'Emitted')
               <div>Pendiente de pago</div>
               @elseif($f->status == 'Paid')
               <div>Pagada</div>
               @elseif($f->status == 'Canceled')
               <div>Anulada</div>
               @endif
               @if($f->payment_status == 'Rechazado')
                <div class="label label-danger">Pago rechazado</div>
                @endif
                @if($f->payment_status == 'Aprobado')
                <div class="label label-success">Pago aprobado</div>
                @endif
                @if($f->payment_status == 'Por revisar')
                <div class="label label-warning">Pago por revisar</div>
                @endif
              </td>
              <td>                  
                <div style="text-align: center">                      
                    <div class="btn-group" style="margin-bottom: 0px;">
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-gear"></i> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu menu-publications">
                          <li><a href="{{ route('invoice_detail', ['id' => $f->id]) }}">Ver detalle</a></li>
                          @if($f->status == 'Emitted')
                          <li><a href="{{ route('payment', ['id' => $f->id]) }}">Pagar{{--Notificar pago--}}</a></li>
                          @endif
                        </ul>
                    </div>
                </div>
              </td>
          </tr>
          @endforeach
      </tbody>
  </table>
</div>
<div class="col-xs-12">
    <div class="text-center">
        {{ $facturas/*->appends($query)*/->links() }}
    </div>
</div>
@else
<div class="row">
    <div class="col-xs-12">
      <div class="alert alert-warning" role="alert">
          <strong>No tienes ninguna factura.</strong>
      </div>
    </div>
</div>
@endif



@endsection
@section('scripts')
    @parent
    <script>
        $(document).ready(function() {
        });
	  
	  
    </script>
@endsection

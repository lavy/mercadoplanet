@extends('layouts.myaccount')

@section('title', 'Compras')

@section('myaccountcontent')
@if (Session::has('message'))
<div class="row">
    <div class="col-xs-12">
      <div class="alert  {{ Session::get('alert-class', 'alert-info') }}" role="alert">
          <strong>{{ Session::get('message') }}</strong>
      </div>
    </div>
</div>
@endif
<h3>Compras</h3>
@if(count($purchases))
<div class="table-responsive">
  <table class="table table-hover table-favorites">
      <tbody>
          @foreach($purchases as $pur)
          <?php $p = $pur->publication; ?>
          <tr>
              <td>
                  <strong>{{ $p->user->getFullName() }}</strong><br>
                  <div style="color:#666">{{ $p->user->email }}</div>
                  <div>{{ $p->user->phone_number }}</div>
              </td>
              <td style="width: 40px">
                  @if(count($p->images))
                  <img src="{{ url('/photo/40x40?url='.urlencode($p->images[0]->name)) }}">
                  @else
                  <img src="{{ url('/photo/40x40?url=nophoto.jpg') }}">
                  @endif
              </td>
              <td style="width:320px">
                  <a href="{{ route('product',[$p->id]) }}" style="padding: 0" class="btn-link">{{ $p->title }}</a>
                  <div style="color:#999"><span style="color:#337ab7;font-weight: bold">S/{{ number_format($pur->price, 2, ',', '.') }}</span> x {{ $pur->quantity }} @if($pur->quantity == 1)unidad @else unidades @endif</div>
              </td>
              <td style="width: 170px;color: #666">  
                <div>
                @if($pur->buyer_calification === 'Positivo')
		<a href="" data-toggle="modal" data-target="#qualification-modal" class="btn-link" data-purchase="{{ $pur->id }}">
                <i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i> 
                Calificaste positivo
		</a>
                @elseif($pur->buyer_calification === 'Negativo')
		<a href="" data-toggle="modal" data-target="#qualification-modal" class="btn-link" data-purchase="{{ $pur->id }}">
                <i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i> 
                Calificaste negativo
		</a>
                @elseif($pur->buyer_calification === 'Neutral')
		<a href="" data-toggle="modal" data-target="#qualification-modal" class="btn-link" data-purchase="{{ $pur->id }}">
                <i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i> 
                Calificaste neutral
		</a>
                @elseif(($pur->buyer_calification === 'No calificado' && (time() - strtotime($pur->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600))
                    or ($pur->buyer_calification === 'No calificado' && $pur->buyer_qualification_modified)
                )
                No calificaste 
		@else
		<a href="{{ route('buyer_qualification', ['id' => $pur->id]) }}" class="btn-link"><i class="fa fa-clock-o" style="font-size: 16px;color:#666"></i> {{ $pur->getTimeToQualify() }} dias para calificar</a>
                @endif
                </div>
                
                @if($pur->buyer_calification != 'No calificado')
                @if($pur->seller_calification == 'No calificado')
                @if((time() - strtotime($pur->created_at)) > (config('app.global.days_to_qualify') * 24 * 3600)
                || ($pur->seller_calification === 'No calificado' && $pur->seller_qualification_modified))
                <div>No te calificaron</div>
                @else
                <div>{{ $pur->getTimeToQualify() }} para que califique</div>
                @endif
                @else
                @if($pur->seller_calification === 'Positivo')
                <div><i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i>
                Te calificaron positivo</div>
                @elseif($pur->seller_calification === 'Negativo')
                <div><i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i>
                Te calificaron  negativo</div>
                @elseif($pur->seller_calification === 'Neutral')
                <div><i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i>
                Te calificaron neutral</div>
                @endif
                @endif
                @endif
              </td>
              <td>
                  <div style="text-align: center">                      
                        <a href="{{ route('purchase_detail', ['id' => $pur->id]) }}" class="btn-link">Ver detalle</a>
                  </div>
              </td>
          </tr>
          @endforeach
      </tbody>
  </table>
</div>
<div class="col-xs-12">
    <div class="text-center">
        {{ $purchases/*->appends($query)*/->links() }}
    </div>
</div>
@else
<div class="row">
    <div class="col-xs-12">
      <div class="alert alert-warning" role="alert">
          <strong>No tienes ninguna compra por ahora.</strong>
      </div>
    </div>
</div>
@endif

<div class="modal fade" id="qualification-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Calificación</h4>
      </div>
      <div class="modal-body">
	  <img src="/img/assets/ajax-loader.gif" id="spinner">
	  <div class="modal-container"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

@endsection
@section('scripts')
    @parent
    <script src="/js/moment.js"></script>
    <script>
        $(document).ready(function() {
          moment.locale('es');
          $('.moment-ends').each(function(i, obj) {
              var date = $(this).attr('data-date');
              $(this).text(moment(date, "YYYY-MM-DD H:m:s").fromNow());
          });
	  
	  $('#qualification-modal').on('show.bs.modal', function (event) {
	    var button = $(event.relatedTarget);
	    var recipient = button.data('purchase');
	    var modal = $(this);
	    $("#spinner").show();
	    modal.find('.modal-container').html('');
	    $.ajax({
                    method: "POST",
                    url: "{{url('/qualification')}}",
                    cache: false,
                    data: { id: recipient, _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
		    var output = '';
                    if(msg.is_ok) {
			var resp = JSON.parse(msg.value);
			if(resp.buyer_qualification == 'Positivo') {
			    output += '<div><i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i> Calificaste positivo</div>';
			}
			if(resp.buyer_qualification == 'Neutral') {
			    output += '<div><i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i> Calificaste neutral</div>';
			}
			if(resp.buyer_qualification == 'Negativo') {
			    output += '<div><i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i> Calificaste negativo</div>';
			}
			
			if(resp.buyer_qualification != 'No calificado') {
			    output += '<div style="margin-top: 5px">"'+resp.buyer_comment+'"</div>';
                            if(resp.seller_reply !== '' && resp.seller_reply) {
                                output += '<div style="margin-top: 5px;margin-left: 15px">Replica: "'+resp.seller_reply+'"</div>';
                            }
			    if(resp.can_modify == 'true') {
				output += '<a href="{{ route('buyer_qualification_edit') }}?id='+recipient+'" class="btn-link">Modificar</a>';
			    }
			}
			if(resp.seller_qualification != 'No calificado') {
			    output += '<hr>';
			    if(resp.seller_qualification == 'Positivo') {
				output += '<div><i class="fa fa-plus-circle fa-plus-circle-active" style="font-size: 16px"></i> Te calificó positivo</div>';
			    }
			    if(resp.seller_qualification == 'Neutral') {
				output += '<div><i class="fa fa-dot-circle-o fa-dot-circle-active" style="font-size: 16px"></i> Te calificó neutral</div>';
			    }
			    if(resp.seller_qualification == 'Negativo') {
				output += '<div><i class="fa fa-minus-circle fa-minus-circle-active" style="font-size: 16px"></i> Te calificó negativo</div>';
			    }
			    output += '<div style="margin-top: 5px">"'+resp.seller_comment+'"</div>';
                            
                            if(resp.buyer_reply !== '' && resp.buyer_reply) {
                                output += '<div style="margin-top: 5px;margin-left: 15px">Replica: "'+resp.buyer_reply+'"</div>';
                            }
                            
                            if(resp.can_modify == 'true') {
				output += '<a href="{{ route('buyer_reply_edit') }}?id='+recipient+'" class="btn-link">Replicar</a>';
			    }
			}
			$("#spinner").hide();
                    }
		    
		    modal.find('.modal-container').html(output);
                });
	    
	    //modal.find('.modal-body').text('New message to ' + recipient);
	    });

        });
        
          
          function delete_favorite(id) {
                $.ajax({
                    method: "POST",
                    url: "{{url('/favorite')}}",
                    cache: false,
                    data: { id: id, _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
                    if(msg.is_ok) {
                      location.reload();
                    }
                });
            }
    </script>
@endsection
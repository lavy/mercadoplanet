@extends('layouts.myaccount')

@section('title', 'Favoritos')

@section('myaccountcontent')
<h3>Favoritos</h3>
@if(count($favorites))
<div class="table-responsive">
  <table class="table table-hover table-favorites">
      <tbody>
          @foreach($favorites as $f)
          <tr>
              <td style="width: 120px">
                  @if(count($f->images))
                  <img src="{{ url('/photo/100x100?url='.urlencode($f->images[0]->name)) }}">
                  @else
                  <img src="{{ url('/photo/100x100?url=nophoto.jpg') }}">
                  @endif
              </td>
              <td><a href="{{ route('product',[$f->id]) }}" class="btn btn-link">{{ $f->title }}</a></td>
              <td><span class="price">S/{{ number_format($f->price, 2, ',', '.') }}</span></td>
              <td>
                  <div>
                    @if($f->status === 'Paused')
                    Publicación pausada
                    @endif
                    @if($f->status === 'Finished')
                    Publicación finalizada
                    @endif
                    @if($f->status === 'Active')
                    Finaliza <span class="moment-ends" data-date="{{ $f->ends_at }}"></span>
                    @endif
                  </div>
                  <div>                      
                      <div class="btn-group">
                        @if($f->status === 'Active' and $f->getProductType() === 'producto')
                        <a href="{{ route('product',['id' => $f->id, 'go_buy' => 'true']) }}" class="btn btn-primary">Comprar</a>
                        @else
                        <a href="{{ route('product',['id' => $f->id]) }}" class="btn btn-primary">Publicación</a>
                        @endif
                        
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-list"></i> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                          @if($f->status == 'Active')
                          <li><a href="{{ route('product',['id' => $f->id, 'go_questions' => 'true']) }}">Preguntar</a></li>
                          @endif
                          <li><a href="javascript: void(0);" onclick="javascript: delete_favorite({{ $f->id }})">Eliminar</a></li>
                        </ul>
                      </div>
                  </div>
              </td>
          </tr>
          @endforeach
      </tbody>
  </table>
</div>
@else
<div class="row">
    <div class="col-xs-12">
      <div class="alert alert-warning" role="alert">
          <strong>No tienes ninguna publicación en tus favoritos.</strong>
      </div>
    </div>
</div>
@endif
@endsection
@section('scripts')
    @parent
    <script src="/js/moment.js"></script>
    <script>
        $(document).ready(function() {
          moment.locale('es');
          $('.moment-ends').each(function(i, obj) {
              var date = $(this).attr('data-date');
              $(this).text(moment(date, "YYYY-MM-DD H:m:s").fromNow());
          });

        });
        
          
          function delete_favorite(id) {
                $.ajax({
                    method: "POST",
                    url: "{{url('/favorite')}}",
                    cache: false,
                    data: { id: id, _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
                    if(msg.is_ok) {
                      location.reload();
                    }
                });
            }
    </script>
@endsection
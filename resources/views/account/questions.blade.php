@extends('layouts.myaccount')

@section('title', 'Favoritos')

@section('myaccountcontent')@if (Session::has('message'))
<div class="row">
    <div class="col-xs-12">
      <div class="alert  {{ Session::get('alert-class', 'alert-info') }}" role="alert">
          <strong>{{ Session::get('message') }}</strong>
      </div>
    </div>
</div>
@endif
<h3>Preguntas</h3>
<div class="alert alert-info">Responde la pregunta en detalle. Una vez que la envíes desaparecerá del listado y no podrás editarla.</div>
@if(count($pubs))
    @foreach($pubs as $px)
    <?php $question = DB::table('questions')->find($px->id); ?>  
    <div id="question-{{ $question->id }}" class="question-container">
    <div class="row" style="margin-top: 10px;margin-top: 30px;background-color: #f6f6f6;">
        <?php $p = Auth::user()->publications()->find($px->publication_id); ?>
        <div class="col-md-8" style="padding: 10px 15px;">
          @if(count($p->images))
          <img src="{{ url('/photo/50x50?url='.urlencode($p->images[0]->name)) }}" style="margin-bottom: 10px;">
          @else
          <img src="{{ url('/photo/50x50?url=nophoto.jpg') }}" style="margin-bottom: 10px;">
          @endif
          <a href="{{ route('product',[$p->id]) }}" class="btn btn-link">{{ $p->title }}</a>
          <span>S/{{ number_format($p->price, 2, ',', '.') }}</span>
          <span class="ends_at">
          @if($p->status === 'Paused')
          Publicación pausada
          @endif
          @if($p->status === 'Finished')
          Publicación finalizada
          @endif
          @if($p->status === 'Active')
          Finaliza en {{  $p->getTimeToEnd()}}</span>
          @endif
          </span>
        </div>
        <div class="col-md-4" style="text-align: right;line-height: 80px;">
            
        </div>
    </div>  
    <div class="row" style="line-height: 20px;padding: 10px;">
        <div class="col-md-8 user-question"><i class="fa fa-comment"></i>
          @if ($question->banned_question == 0)
          {{ $question->question }}
          @else
            <span class="label label-warning lb-lg">Esta pregunta fue eliminada por infringir las normas del sitio.</span>
          @endif
        </div>
        <div class="col-md-4" style="text-align: right">
            hace <span>{{ \App\Question::humanizeTime(time() - strtotime($question->created_at))  }}</span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <textarea class="form-control answer" data-question-id="{{ $question->id }}"></textarea>
            <div style="float:right; margin-left: 10px" class="question-controls">Quedan <span class="num-chars">500</span> caracteres.</div>
            <a href="#" class="btn-link delete-question-link" data-question='{{ $question->id }}' data-toggle="modal" data-target="#delete-question-modal"><div style="float:right; margin-left: 10px" ><i class="fa fa-trash-o"></i> Eliminar pregunta</div></a>
            @if($p->category->getHomeTopLevelCategory()->id < 4)
            <?php $buyer = \App\User::find($question->asker_id); ?>
            <div style="float:right; margin-left: 10px" >({{ $buyer->getFullName() }} - {{ $buyer->phone_number }} - {{ $buyer->email }})</div>
            @endif
            <button class="btn btn-primary submit-answer" style="margin-top: 10px;" disabled>Responder</button>
        </div>
    </div>    
    @if (!empty($question->answer))
    <div class="row" style="line-height: 20px; color: #666;padding: 0px 10px 10px 10px;">
      <div class="col-xs-12 user-answer">
        <i class="fa fa-comments"></i>
        @if ($question->banned_answer == 0)
          {{ $question->answer }} 
        @else
          <span class="label label-warning lb-lg">Esta respuesta fue eliminada por infringir las normas del sitio.</span>
        @endif
      </div>
    </div>
    @endif
    </div>
    @endforeach
@else
<div class="row">
    <div class="col-xs-12">
      <div class="alert alert-warning" role="alert">
          <strong>No tienes preguntas pendientes por responder.</strong>
      </div>
    </div>
</div>
@endif
<form method="post" action="{{ url('/delete_question') }}">
<div class="modal fade" id="delete-question-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Eliminar pregunta</h4>
      </div>
      <div class="modal-body">
            {{ csrf_field() }}
            <input type="hidden" name="question_id" id="question_id" value="">
            <div class="checkbox">
                <label>
                    <input type="checkbox" checked="true" disabled>
                    Eliminar pregunta
                </label>
            </div>
            <br>
            Ademas puedes:
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="no_questions" value="1">
                    Bloquear al comprador para que no realice más preguntas
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="no_offers" value="1">
                    Bloquear al comprador para que no pueda ofertar
                </label>
            </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Confirmar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
</form>

@endsection
@section('scripts')
    @parent
    <script>
        $(document).ready(function() {
          $(".submit-answer").click(function() {
              var btn = $(this);
              var txtarea = btn.parent().find('textarea');
              var question_id = txtarea.attr('data-question-id');
              answer_question(question_id, txtarea.val());
          });
          var maxChars = 500;
            $(".answer").keyup(controlChars);


            function controlChars(e) {
              var submit_button = $(this).closest( ".question-container" ).find('.submit-answer');
              var num_chars = $(this).closest( ".question-container" ).find('.num-chars');
              var rest = maxChars - $(this).val().length;
              if(rest < 0) rest = 0;
              if($(this).val().length > 0) submit_button.prop('disabled', false);
              else submit_button.prop('disabled', true);
              num_chars.text(rest);
                        if (e.which < 0x20) {
                                // e.which < 0x20, then it's not a printable character
                                // e.which === 0 - Not a character
                                return;     // Do nothing
                        }
                        if ($(this).val().length == maxChars) {
                                e.preventDefault();
                        } else if ($(this).val().length > maxChars) {
                                // Maximum exceeded
                                $(this).val($(this).val().substring(0, maxChars));
                        }
                }
                
            $('#delete-question-modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var recipient = button.data('question');
                var modal = $(this);
                $('#question_id').val(recipient);
            });
                
        });
        
          
          function delete_questions(id) {
                $.ajax({
                    method: "POST",
                    url: "{{url('/delete_questions')}}",
                    cache: false,
                    data: { id: id, _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
                    if(msg.is_ok) {
                      location.reload();
                    }
                });
            } 
          
          
          function answer_question(id, answer) {
                $.ajax({
                    method: "POST",
                    url: "{{url('/answer-questions')}}",
                    cache: false,
                    data: { id: id, answer: answer, _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
                    if(msg.is_ok) {
                      $("#question-"+id).fadeOut( function() { $(this).remove(); });
                    }
                });
            }
    </script>
@endsection
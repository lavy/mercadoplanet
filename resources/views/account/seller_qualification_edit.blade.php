@extends('layouts.myaccount')

@section('title', 'Detalles de la compra')

@section('myaccountcontent')
<h3>Calificar</h3>
<form name="qualification-form" method="post" action="{{ route('seller_qualification_edit_save') }}">
<input type="hidden" name="id" value="{{ $id }}">
{{ csrf_field() }}
<div class="row">
    <div class="col-md-8">
        <div class="alert alert-info">
            Sólo puedes modificar tu calificación en una sola oportunidad.
        </div>
	<br>
	<strong>¿Ya entregaste el producto?</strong>
        @if ($errors->has('concreted'))
        <br>
            <span class="text-danger">
                {{ $errors->first('concreted') }}
            </span>
        @endif
	    <div class="radio">
		<label>
		    <input type="radio" name="concreted" id="concreted1" value="1" @if($purchase->seller_concreted == '1') checked @endif>
		    Si, ya lo entregué.
		</label>
	    </div>
	    <div class="radio">
	    <label>
	      <input type="radio" name="concreted" id="concreted3" value="-1"@if($purchase->seller_concreted == '0' && !empty($purchase->seller_concreted_problem)) checked @endif>
	      No, no lo entregaré.
	    </label>
	  </div>
        <div id="concreted3-type" @if(!($purchase->seller_concreted == '0' && !empty($purchase->seller_concreted_problem))) style="display: none" @endif>
        <label>¿Qué pasó?</label>
        @if ($errors->has('no_concreted_type'))
        <br>
            <span class="text-danger">
                {{ $errors->first('no_concreted_type') }}
            </span>
        @endif
        <select class="form-control" name="no_concreted_type">
            <option value="">Elegir</option>
            <option @if($purchase->seller_concreted_problem == 'El comprador se arrepintió') selected @endif>El comprador se arrepintió</option>
            <option @if($purchase->seller_concreted_problem == 'Me quedé sin inventario') selected @endif>Me quedé sin inventario</option>
            <option @if($purchase->seller_concreted_problem == 'Decidí no venderlo') selected @endif>Decidí no venderlo</option>
        </select>
        </div>
	<br>
	<strong>¿Recomendarías a este comprador?</strong>
            @if ($errors->has('recommended'))
            <br>
                <span class="text-danger">
                    {{ $errors->first('recommended') }}
                </span>
            @endif
	    <div class="radio">
		<label>
		    <input type="radio" name="recommended" id="recommended1" value="Sí" @if($purchase->seller_recommends == 'Sí') checked @endif>
		    Si.
		</label>
	    </div>
	    <div class="radio">
		<label>
		    <input type="radio" name="recommended" id="recommended2" value="No estoy seguro" @if($purchase->seller_recommends == 'No estoy seguro') checked @else @if($purchase->seller_concreted == '1') disabled @endif @endif>
		    No estoy seguro.
		</label>
	  </div>
	    <div class="radio">
	    <label>
	      <input type="radio" name="recommended" id="recommended3" value="No" @if($purchase->seller_recommends == 'No') checked @else disabled @endif>
	      No.
	    </label>
	  </div>
	<br>
	<strong>Comparte lo que opinas del vendedor.</strong>
        @if ($errors->has('opinion'))
        <br>
            <span class="text-danger">
                {{ $errors->first('opinion') }}
            </span>
        @endif
        <textarea class="form-control" name="opinion" id="opinion" rows="3" style="max-width: 300px; margin-top: 10px">{{ $purchase->seller_comment }}</textarea>
        <div style="font-size: 12px;">Quedan <span id="num-chars">200</span> caracteres.</div>
	<br>
        <button class="btn btn-primary" id="btn-submit"  @if(!strlen($purchase->seller_comment))) disabled="true" @endif>Calificar</button> <a href="{{ url('sales') }}" class="btn btn-link">Cancelar</a>
    </div>
    <div class="col-md-4">
	<div class="product-container">
	<div>
	    @if(count($publication->images))
	    <img src="{{ url('/photo/160x160?url='.urlencode($publication->images[0]->name)) }}">
	    @else
	    <img src="{{ url('/photo/160x160?url=nophoto.jpg') }}">
	    @endif
	</div>
	    <div style="margin-top: 8px">
	    <a href="{{ route('product',[$publication->id]) }}" style="padding: 0" class="btn-link">{{ $publication->title }}</a>
	</div>
	<div style="color:#333;    margin: 10px 0;font-size: 18px;"><strong>S/{{ number_format($publication->price, 2, ',', '.') }}</strong></div>
	<div>
	    Comprador:
	    <br>
	    {{ $purchase->buyer->getFullName() }}
	    <br>
	    <a href="{{ route('reputationseller', ['user_id' => $purchase->buyer->id]) }}" class="btn-link">{{ strtoupper($purchase->buyer->username)}} ({{ $purchase->buyer->getPoints() }})</a>
	</div>
	</div>
    </div>
</div>
</form>
@endsection
@section('scripts')
    @parent
    <script>
	$(document).ready(function(){
          
	$("[name=concreted]").click(function() {
          if($("#concreted3").is(':checked')) {
            $("#concreted3-type").show();
            $("#recommended2").prop('disabled', false);
          } else {
            $("#concreted3-type").hide();          
          }   
          if($("#concreted3").is(':checked')) {
            $("#recommended2").prop('disabled', false);
          }       
        });
        
	var maxChars = 200;
	$("#opinion").keyup(controlChars);


	function controlChars(e) {
	  var rest = maxChars - $(this).val().length;
	  if(rest < 0) rest = 0;
	  if($(this).val().length > 0) $("#btn-submit").prop('disabled', false);
	  else $("#btn-submit").prop('disabled', true);
	  $("#num-chars").text(rest);
		    if (e.which < 0x20) {
			    // e.which < 0x20, then it's not a printable character
			    // e.which === 0 - Not a character
			    return;     // Do nothing
		    }
		    if ($(this).val().length == maxChars) {
			    e.preventDefault();
		    } else if ($(this).val().length > maxChars) {
			    // Maximum exceeded
			    $(this).val($(this).val().substring(0, maxChars));
		    }
	    }
	});
    </script>
@endsection
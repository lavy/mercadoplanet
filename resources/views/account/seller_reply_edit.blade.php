@extends('layouts.myaccount')

@section('title', 'Detalles de la compra')

@section('myaccountcontent')
<h3>Calificar</h3>
<form name="qualification-form" method="post" action="{{ route('seller_reply_edit_save') }}">
<input type="hidden" name="id" value="{{ $id }}">
{{ csrf_field() }}
<div class="row">
    <div class="col-md-8">
	<br>
        <strong>Comentario recibido</strong>
        <br>"{{ $purchase->buyer_comment }}"
	<br>
        <br>
        <strong>Escribe tu réplica</strong>
        @if ($errors->has('replica'))
        <br>
            <span class="text-danger">
                {{ $errors->first('replica') }}
            </span>
        @endif
        <textarea class="form-control" name="replica" id="replica" rows="3" style="max-width: 300px; margin-top: 10px">{{ $purchase->seller_reply }}</textarea>
        <div style="font-size: 12px;">Quedan <span id="num-chars">200</span> caracteres.</div>
	<br>
        <button class="btn btn-primary" id="btn-submit"  @if(!strlen($purchase->seller_reply))) disabled="true" @endif>Enviar</button> <a href="{{ url('sales') }}" class="btn btn-link">Cancelar</a>
    </div>
    <div class="col-md-4">
	<div class="product-container">
	<div>
	    @if(count($publication->images))
	    <img src="{{ url('/photo/160x160?url='.urlencode($publication->images[0]->name)) }}">
	    @else
	    <img src="{{ url('/photo/160x160?url=nophoto.jpg') }}">
	    @endif
	</div>
	    <div style="margin-top: 8px">
	    <a href="{{ route('product',[$publication->id]) }}" style="padding: 0" class="btn-link">{{ $publication->title }}</a>
	</div>
	<div style="color:#333;    margin: 10px 0;font-size: 18px;"><strong>S/{{ number_format($publication->price, 2, ',', '.') }}</strong></div>
	<div>
	    Comprador:
	    <br>
	    {{ $purchase->buyer->getFullName() }}
	    <br>
	    <a href="{{ route('reputationbuyer', ['user_id' => $purchase->buyer->id]) }}" class="btn-link">{{ strtoupper($purchase->buyer->username)}} ({{ $purchase->buyer->getPoints() }})</a>
	</div>
	</div>
    </div>
</div>
</form>
@endsection
@section('scripts')
    @parent
    <script>
	$(document).ready(function(){
            
	var maxChars = 200;
	$("#replica").keyup(controlChars);


	function controlChars(e) {
	  var rest = maxChars - $(this).val().length;
	  if(rest < 0) rest = 0;
	  if($(this).val().length > 0) $("#btn-submit").prop('disabled', false);
	  else $("#btn-submit").prop('disabled', true);
	  $("#num-chars").text(rest);
		    if (e.which < 0x20) {
			    // e.which < 0x20, then it's not a printable character
			    // e.which === 0 - Not a character
			    return;     // Do nothing
		    }
		    if ($(this).val().length == maxChars) {
			    e.preventDefault();
		    } else if ($(this).val().length > maxChars) {
			    // Maximum exceeded
			    $(this).val($(this).val().substring(0, maxChars));
		    }
	    }
	});
    </script>
@endsection
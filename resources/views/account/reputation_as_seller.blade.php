@extends('layouts.reputation_as')

@section('title', 'Reputación como vendedor')

@section('another-link')
<a href="{{ route('reputationbuyer', ['user_id' => $user->id]) }}">Ver perfil como comprador</a>
@endsection

@section('reputation_info')

<div class="reputation-content-info col-md-12">
	<div><h3>Reputación como vendedor</h3></div>
	<div class="reputation-another-link"><a href="#">¿Cómo funciona el sistema de reputación?</a></div>
</div>
@include('account.reputation_details')
@endsection

@section('calification-title', 'Calificaciones como vendedor')

@section('qualifications')

    <div class="row" style="margin-bottom: 30px; margin-top: -30px">
        <div class="col-sm-2  col-sm-offset-3 qualification-text">{{ $num_positives }} <a href="{{ route('reputationseller', ['user_id' => $user->id, 'type' => 'positive']) }}"><i class="fa fa-plus-circle @if($num_positives) fa-plus-circle-active @endif"></i></a></div>
        <div class="col-sm-2 qualification-text">{{ $num_neutrals }} <a href="{{ route('reputationseller', ['user_id' => $user->id, 'type' => 'neutral']) }}"><i class="fa fa-dot-circle-o @if($num_neutrals) fa-dot-circle-active @endif"></i></a></div>
        <div class="col-sm-2 qualification-text">{{ $num_negatives }} <a href="{{ route('reputationseller', ['user_id' => $user->id, 'type' => 'negative']) }}"><i class="fa fa-minus-circle @if($num_negatives) fa-minus-circle-active @endif"></i></a></div>
    </div>
	<br>
        @foreach($qualifications as $q)
	<div class="row">
            <div class="col-xs-6">
                <div class="popover-qualification">
                    <div class="popover left">
                      <div class="arrow"></div>
                      <div class="popover-content">
                            @if($q->buyer_calification === 'Positivo')
                            <i class="fa fa-plus-circle fa-plus-circle-active"></i> 
                            @endif
                            @if($q->buyer_calification === 'Negativo')
                            <i class="fa fa-minus-circle fa-minus-circle-active"></i> 
                            @endif
                            @if($q->buyer_calification === 'Neutral')
                            <i class="fa fa-dot-circle-o fa-dot-circle-active"></i> 
                            @endif
                             {{ $q->buyer_comment }}
                             <small style="float:right; margin-top: 10px">{{ date("d/m/Y", strtotime($q->created_at)) }}</small>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="comment-user"><i class="fa fa-user"></i> {{ strtoupper($q->buyer->username) }} ({{ $q->buyer->getPoints() }})</div>
                <div class="comment-product"><i class="fa fa-shopping-cart"></i> 
                    <span class="comment-title">{{ $q->publication->title }}</span> <span>S/{{ number_format($q->publication->price, 2 , ',', '.') }}</span>
                </div>
            </div>
	</div>
        @endforeach
        
	<div class="col-xs-12">
            <div class="text-center">
                {{ $qualifications->appends($query)->links() }}
            </div>
        </div>
@endsection
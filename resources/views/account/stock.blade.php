@extends('layouts.myaccount')

@section('title', 'Agregar stock')

@section('myaccountcontent')
<link href="/css/jquery.filer.css" rel="stylesheet">
<h3>Agregar stock</h3>
<form name="shipping-form" method="post" action="{{ route('stock_save') }}">
<input type="hidden" name="id" value="{{ $id }}">
{{ csrf_field() }}
<div class="row">
    <div class="col-md-8">
        
	<br>
        <div> 
            @if ($errors->has('cantidad'))
                <span class="text-danger" style="line-height: 30px; margin-left: 10px">
                    {{ $errors->first('cantidad') }}
                </span>
            @endif
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-3 control-label">Cantidad: *</label>
            <div class="col-sm-6 input-group">
             <input type="text" class="form-control" maxlength="4" style="width: 70px" name="cantidad" id="cantidad" value="1"  >
            </div>
          </div>
        <button class="btn btn-primary" id="btn-submit"  >Agregar</button> <a href="{{ url('publications') }}" class="btn btn-link">Cancelar</a>
    </div>
    <div class="col-md-4">
	<div class="product-container">
	<div>
	    @if(count($publication->images))
	    <img src="{{ url('/photo/160x160?url='.urlencode($publication->images[0]->name)) }}">
	    @else
	    <img src="{{ url('/photo/160x160?url=nophoto.jpg') }}">
	    @endif
	</div>
	    <div style="margin-top: 8px">
	    <a href="{{ route('product',[$publication->id]) }}" style="padding: 0" class="btn-link">{{ $publication->title }}</a>
	</div>
	<div style="color:#333;    margin: 10px 0;font-size: 18px;"><strong>S/{{ number_format($publication->price, 2, ',', '.') }}</strong></div>
	
	</div>
    </div>
</div>
</form>
@endsection
@section('scripts')
    @parent
    <script src="/js/jquery.number.min.js"></script>
    <script>
	
    </script>
@endsection
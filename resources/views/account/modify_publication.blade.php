@extends('layouts.app')

@section('title', 'Inicio')

@section('content')

<section class="mainContent clearfix userProfile">
       <div class="container">
         <div class="row">
           <div class="col-xs-12">
             <div class="innerWrapper">
               <div class="row">
                 <div class="col-md-12" style="text-align: center">
                   <h4>Modificar Publicacíon</h4>
                 </div>
               </div>
                <?php $tipo_producto = $publication->getProductType(); ?>
               <div class="row">
                 <div class="col-md-12"><h3 style="text-transform: none;"><span>Ingresa fotos de tu {{ $tipo_producto }}</span></h3></div>
                 <div class="col-md-12">
                     <small>Añade imágenes que muestren tu {{ $tipo_producto }} en detalle, no incluyas logos, banners ni textos promocionales.</small>
                 </div>
                  <div class="col-md-12">
                    <div class="file-uploader">				
                      <div class="file-uploader-left" style="text-align: left;">
                        <div id="multiupload">
                          <form class="b-upload b-upload_multi" action="#" method="POST" enctype="multipart/form-data">
                            <div class="js-files b-upload__files" id="sortable">
                              <div style="display: none">
                                <div class="js-file-tpl b-thumb" data-id="<%=uid%>" title="<%-name%>, <%-sizeText%>">
                                  <div data-fileapi="file.remove" class="b-thumb__del">✖</div>
                                  <div class="b-thumb__preview">
                                    <div class="b-thumb__preview__pic"></div>
                                  </div>
                                  <div class="b-thumb__progress progress progress-small"><div class="bar"></div></div>
                                </div>
                              </div>
                            </div>
                            <div class="btnx">
                              <input type="file" name="filedata" id="file" />
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>


                  <div id="popup" class="popup" style="display: none;">
                          <div class="popup__body"><div class="js-img"></div></div>
                          <div style="margin: 0 0 5px; text-align: center;">
                                  <div class="js-upload btn btn_browse btn_browse_small">Upload</div>
                          </div>
                  </div>
                  <div class="col-xs-12">
                    <div class="alert alert-warning" role="alert"><strong>Tamaño máximo de imagen:</strong> 4MB. <strong>Dimensiones mínimas:</strong> 500x500 (px). <strong>Dimensiones máximas:</strong> 2000x2000 (px).</div>
                  </div>
               </div>
                 <form method="post" name="mainForm" action="{{ route('modify-publication-save') }}" onsubmit="return submitForm()" class="form-horizontal">
                 {{ csrf_field() }}
                 
                  <input type="hidden" id="id" name="id" value="{{ $publication->id }}">
                  <input type="hidden" id="pictures_info" name="pictures_info" value="">
                  <input type="hidden" id="tipo_producto" name="tipo_producto" value="{{ $tipo_producto }}">
                  
               @if($tipo_producto === 'vehículo' || $tipo_producto === 'vehículo de colección' || $tipo_producto === 'moto' || $tipo_producto === 'lancha' || $tipo_producto === 'vehiculo' || $tipo_producto === 'casa' || $tipo_producto === 'departamento' || $tipo_producto === 'local comercial' || $tipo_producto === 'oficina' || $tipo_producto === 'terreno' || $tipo_producto === 'inmueble' || $tipo_producto === 'servicio') 
               <div class="row">
                 @if($tipo_producto === 'servicio')
                 <div class="col-md-12"><h3 style="text-transform: none;"><span>Tu ubicación</span></h3></div>
                 @else
                 <div class="col-md-12"><h3 style="text-transform: none;"><span>Ubicación de tu {{ $tipo_producto }}</span></h3></div>
                 @endif
                 <div class="col-md-12"><small>Datos Obligatorios *</small></div>
               </div>
               <br><br>          
                <div class="form-group{{ $errors->has('departamento') ? ' has-error' : '' }}">                    
                    <label for="title" class="col-sm-2 control-label">Departamento: *</label>
                   <div class="col-sm-6">
                        <select class="form-control" id="department" name="departamento">
                            <option value="">Seleccione</option>
                            @foreach($departments as $department)
                            <option value="{{$department->id}}" @if(old('departamento', $departamento) == $department->id) selected="selected" @endif>{{$department->name}}</option>
                            @endforeach
                        </select>
                    </div>                    
                </div>
                        
                <div class="form-group{{ $errors->has('provincia') ? ' has-error' : '' }}">      
                    <label for="title" class="col-sm-2 control-label">Provincia: *</label>
                   <div class="col-sm-6">
                        <select class="form-control" id="province" name="provincia">
                        <option value="">Seleccione</option>   
                        @foreach($provinces as $province)
                        <option value="{{$province->id}}" @if(old('provincia', $provincia) == $province->id) selected="selected" @endif>{{$province->name}}</option>
                        @endforeach
                        </select>
                    </div>                    
                </div>
                        
                <div class="form-group{{ $errors->has('distrito') ? ' has-error' : '' }}">    
                    <label for="title" class="col-sm-2 control-label">Distrito: *</label>
                   <div class="col-sm-6">
                        <select class="form-control" id="district" name="distrito">
                        <option value="">Seleccione</option>
                        @foreach($districts as $district)
                        <option value="{{$district->id}}" @if(old('distrito', $distrito) == $district->id) selected="selected" @endif>{{$district->name}}</option>
                        @endforeach
                        </select>
                    </div>                    
                </div>
               @endif
               
               @if($tipo_producto === 'casa' || $tipo_producto === 'departamento' || $tipo_producto === 'local comercial' || $tipo_producto === 'oficina' || $tipo_producto === 'terreno' || $tipo_producto === 'inmueble' || $tipo_producto === 'servicio')   
                <div class="form-group {{ $errors->has('calle') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Calle *</label>
                   <div class="col-sm-2">
                       <input type="text" name="calle" maxlength="50" class="form-control"  @if(old('calle'))value="{{old('calle')}}"@else @if($publication->address_street)value="{{$publication->address_street}}"@endif @endif>
                    </div>                          
                </div>  
                <div class="form-group {{ $errors->has('numero') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Número *</label>
                   <div class="col-sm-2">
                       <input type="text" name="numero" maxlength="50" class="form-control"  @if(old('numero'))value="{{old('numero')}}"@else @if($publication->address_number)value="{{$publication->address_number}}"@endif @endif>
                    </div>                          
                </div>               
               @endif
               
               
               
               
               @if($tipo_producto === 'vehículo' || $tipo_producto === 'vehículo de colección' || $tipo_producto === 'moto' || $tipo_producto === 'lancha' || $tipo_producto === 'vehiculo' || $tipo_producto === 'casa' || $tipo_producto === 'departamento' || $tipo_producto === 'local comercial' || $tipo_producto === 'oficina' || $tipo_producto === 'terreno' || $tipo_producto === 'inmueble' || $tipo_producto === 'servicio')  
               <div class="row">
                 <div class="col-md-12"><h3 style="text-transform: none;"><span>Teléfono de contacto</span></h3></div>
               </div>
                        
                <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">    
                    <label for="title" class="col-sm-2 control-label">Teléfono:</label>
                   <div class="col-sm-6">
                       <input id="number" type="text" class="form-control" maxlength="20" name="telefono" style="width:200px;float:left" placeholder="Ej. 011 2355-4558" @if(old('telefono'))value="{{old('telefono')}}"@else @if($publication->contact_phone)value="{{$publication->contact_phone}}"@endif @endif>
                       <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" data-original-title="Si no indicas un teléfono de contacto se publicará el registrado en tu cuenta." style="color: #999;font-size: 18px; line-height: 30px;margin-left: 10px;"></i>
                    </div>                          
                </div>         
               @endif
               
               
               
               <div class="row">
                 <div class="col-md-12"><h3 style="text-transform: none;"><span>Describe tu {{ $tipo_producto }}</span></h3></div>
                 <div class="col-md-12"><small>Datos Obligatorios *</small></div>
               </div>
               <br>
               
               
               
               @if($tipo_producto === 'casa' || $tipo_producto === 'departamento' || $tipo_producto === 'local comercial' || $tipo_producto === 'oficina')                                       
                <div class="form-group {{ $errors->has('nambientes') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Ambientes *</label>
                   <div class="col-sm-2">
                       <select name="nambientes" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('nambientes') and old('nambientes') == '1') selected @else  @if(!old('nambientes') and $publication->property_environments and $publication->property_environments == '1') selected @endif @endif>1</option>
                           <option @if(old('nambientes') and old('nambientes') == '2') selected @else  @if(!old('nambientes') and $publication->property_environments and $publication->property_environments == '2') selected @endif @endif>2</option>
                           <option @if(old('nambientes') and old('nambientes') == '3') selected @else  @if(!old('nambientes') and $publication->property_environments and $publication->property_environments == '3') selected @endif @endif>3</option>
                           <option @if(old('nambientes') and old('nambientes') == '4') selected @else  @if(!old('nambientes') and $publication->property_environments and $publication->property_environments == '4') selected @endif @endif>4</option>
                           <option @if(old('nambientes') and old('nambientes') == 'Más de 4') selected @else  @if(!old('nambientes') and $publication->property_environments and $publication->property_environments == 'Más de 4') selected @endif @endif>Más de 4</option>
                       </select>
                    </div>                          
                </div> 
               @endif
               @if($tipo_producto === 'casa' || $tipo_producto === 'departamento' || $tipo_producto === 'local comercial' || $tipo_producto === 'oficina' || $tipo_producto === 'inmueble')                                       
                <div class="form-group {{ $errors->has('años') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Antigüedad *</label>
                   <div class="col-sm-2">
                       <input type="text" name="años" class="form-control"  @if(old('años'))value="{{old('años')}}"@else @if($publication->property_age)value="{{$publication->property_age}}"@endif @endif>
                    </div>                 
                </div> 
               @endif
               @if($tipo_producto === 'casa' || $tipo_producto === 'departamento' || $tipo_producto === 'local comercial' || $tipo_producto === 'oficina' || $tipo_producto === 'inmueble')                                       
                <div class="form-group {{ $errors->has('construido') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Area construida (m²) *</label>
                   <div class="col-sm-2">
                       <input type="text" name="construido" class="form-control"  @if(old('construido'))value="{{old('construido')}}"@else @if($publication->property_construction_area)value="{{$publication->property_construction_area}}"@endif @endif>
                    </div>                 
                </div> 
               @endif
               @if($tipo_producto === 'casa' || $tipo_producto === 'departamento' || $tipo_producto === 'local comercial' || $tipo_producto === 'oficina' || $tipo_producto === 'terreno' || $tipo_producto === 'inmueble')                                       
                <div class="form-group {{ $errors->has('terreno') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Area de terreno (m²) *</label>
                   <div class="col-sm-2">
                       <input type="text" name="terreno" class="form-control"  @if(old('terreno'))value="{{old('terreno')}}"@else @if($publication->property_land_area)value="{{$publication->property_land_area}}"@endif @endif>
                    </div>                 
                </div> 
               @endif
               @if($tipo_producto === 'casa' || $tipo_producto === 'departamento' || $tipo_producto === 'local comercial' || $tipo_producto === 'oficina')                                       
                <div class="form-group {{ $errors->has('baños') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Baños *</label>
                   <div class="col-sm-2">
                       <select name="baños" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('baños') and old('baños') == '1') selected @else  @if(!old('baños') and $publication->property_bathrooms and $publication->property_bathrooms == '1') selected @endif @endif>1</option>
                           <option @if(old('baños') and old('baños') == '2') selected @else  @if(!old('baños') and $publication->property_bathrooms and $publication->property_bathrooms == '2') selected @endif @endif>2</option>
                           <option @if(old('baños') and old('baños') == '3') selected @else  @if(!old('baños') and $publication->property_bathrooms and $publication->property_bathrooms == '3') selected @endif @endif>3</option>
                           <option @if(old('baños') and old('baños') == '4') selected @else  @if(!old('baños') and $publication->property_bathrooms and $publication->property_bathrooms == '4') selected @endif @endif>4</option>
                           <option @if(old('baños') and old('baños') == 'Más de 4') selected @else  @if(!old('baños') and $publication->property_bathrooms and $publication->property_bathrooms == 'Más de 4') selected @endif @endif>Más de 4</option>
                       </select>
                    </div>                          
                </div> 
               @endif
               @if($tipo_producto === 'casa' || $tipo_producto === 'departamento')                                       
                <div class="form-group {{ $errors->has('dormitorios') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Habitaciones *</label>
                   <div class="col-sm-2">
                       <select name="dormitorios" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('dormitorios') and old('dormitorios') == '1') selected @else  @if(!old('dormitorios') and $publication->property_bedrooms and $publication->property_bedrooms == '1') selected @endif @endif>1</option>
                           <option @if(old('dormitorios') and old('dormitorios') == '2') selected @else  @if(!old('dormitorios') and $publication->property_bedrooms and $publication->property_bedrooms == '2') selected @endif @endif>2</option>
                           <option @if(old('dormitorios') and old('dormitorios') == '3') selected @else  @if(!old('dormitorios') and $publication->property_bedrooms and $publication->property_bedrooms == '3') selected @endif @endif>3</option>
                           <option @if(old('dormitorios') and old('dormitorios') == '4') selected @else  @if(!old('dormitorios') and $publication->property_bedrooms and $publication->property_bedrooms == '4') selected @endif @endif>4</option>
                           <option @if(old('dormitorios') and old('dormitorios') == 'Más de 4') selected @else  @if(!old('dormitorios') and $publication->property_bedrooms and $publication->property_bedrooms == 'Más de 4') selected @endif @endif>Más de 4</option>
                       </select>
                    </div>                          
                </div> 
               @endif
                     
               @if($tipo_producto === 'inmueble')                                       
                <div class="form-group {{ $errors->has('tipoinmueble') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Tipo de inmueble *</label>
                   <div class="col-sm-2">
                       <input type="text" name="tipoinmueble" class="form-control"  @if(old('tipoinmueble'))value="{{old('tipoinmueble')}}"@else @if($publication->vehicle_type)value="{{$publication->vehicle_type}}"@endif @endif>
                    </div>                 
                </div> 
               @endif  
               
               @if($tipo_producto === 'terreno')  
               <br>
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Acceso</label>
                   <div class="col-sm-2">
                       <select name="acceso" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('acceso') and old('acceso') == 'Arena') selected @else  @if(!old('acceso') and $publication->parameters()->where('name', 'Acceso')->first() and $publication->parameters()->where('name', 'Acceso')->first()->value == 'Arena') selected @endif @endif>Arena</option>
                           <option @if(old('acceso') and old('acceso') == 'Asfalto') selected @else  @if(!old('acceso') and $publication->parameters()->where('name', 'Acceso')->first() and $publication->parameters()->where('name', 'Acceso')->first()->value == 'Asfalto') selected @endif @endif>Asfalto</option>
                           <option @if(old('acceso') and old('acceso') == 'Ripio') selected @else  @if(!old('acceso') and $publication->parameters()->where('name', 'Acceso')->first() and $publication->parameters()->where('name', 'Acceso')->first()->value == 'Ripio') selected @endif @endif>Ripio</option>
                           <option @if(old('acceso') and old('acceso') == 'Tierra') selected @else  @if(!old('acceso') and $publication->parameters()->where('name', 'Acceso')->first() and $publication->parameters()->where('name', 'Acceso')->first()->value == 'Tierra') selected @endif @endif>Tierra</option>
                           <option @if(old('acceso') and old('acceso') == 'Otro acceso') selected @else  @if(!old('acceso') and $publication->parameters()->where('name', 'Acceso')->first() and $publication->parameters()->where('name', 'Acceso')->first()->value == 'Otro acceso') selected @endif @endif>Otro acceso</option>
                       </select>
                    </div>                          
                </div>
               @endif      
               @if($tipo_producto === 'terreno')                                       
                <div class="form-group {{ $errors->has('construido') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Area construida (m²)</label>
                   <div class="col-sm-2">
                       <input type="text" name="construido" class="form-control"  @if(old('construido'))value="{{old('construido')}}"@else @if($publication->property_construction_area)value="{{$publication->property_construction_area}}"@endif @endif>
                    </div>                 
                </div> 
               @endif               
               @if($tipo_producto === 'terreno')  
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Uso</label>
                   <div class="col-sm-2">
                       <select name="uso" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('uso') and old('uso') == 'Residencial') selected @else  @if(!old('uso') and $publication->parameters()->where('name', 'Uso')->first() and $publication->parameters()->where('name', 'Uso')->first()->value == 'Residencial') selected @endif @endif>Residencial</option>
                           <option @if(old('uso') and old('uso') == 'Comercial') selected @else  @if(!old('uso') and $publication->parameters()->where('name', 'Uso')->first() and $publication->parameters()->where('name', 'Uso')->first()->value == 'Comercial') selected @endif @endif>Comercial</option>
                           <option @if(old('uso') and old('uso') == 'Industrial') selected @else  @if(!old('uso') and $publication->parameters()->where('name', 'Uso')->first() and $publication->parameters()->where('name', 'Uso')->first()->value == 'Industrial') selected @endif @endif>Industrial</option>
                       </select>
                    </div>                          
                </div>
               @endif  
               
               @if($tipo_producto === 'casa' || $tipo_producto === 'local comercial' || $tipo_producto === 'oficina')  
               <br>
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Cantidad de pisos</label>
                   <div class="col-sm-2">
                       <select name="pisos" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('pisos') and old('pisos') == '1') selected @else  @if(!old('pisos') and $publication->parameters()->where('name', 'Cantidad de pisos')->first() and $publication->parameters()->where('name', 'Cantidad de pisos')->first()->value == '1') selected @endif @endif>1</option>
                           <option @if(old('pisos') and old('pisos') == '2') selected @else  @if(!old('pisos') and $publication->parameters()->where('name', 'Cantidad de pisos')->first() and $publication->parameters()->where('name', 'Cantidad de pisos')->first()->value == '2') selected @endif @endif>2</option>
                           <option @if(old('pisos') and old('pisos') == '3') selected @else  @if(!old('pisos') and $publication->parameters()->where('name', 'Cantidad de pisos')->first() and $publication->parameters()->where('name', 'Cantidad de pisos')->first()->value == '3') selected @endif @endif>3</option>
                           <option @if(old('pisos') and old('pisos') == 'Más de 3') selected @else  @if(!old('pisos') and $publication->parameters()->where('name', 'Cantidad de pisos')->first() and $publication->parameters()->where('name', 'Cantidad de pisos')->first()->value == 'Más de 3') selected @endif @endif>Más de 3</option>
                       </select>
                    </div>                          
                </div>
               @endif              
               @if($tipo_producto === 'departamento') 
               <br>
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Amoblado</label>
                   <div class="col-sm-2">
                       <select name="amoblado" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('amoblado') and old('amoblado') == 'Sí') selected @else  @if(!old('amoblado') and $publication->parameters()->where('name', 'Amoblado')->first() and $publication->parameters()->where('name', 'Amoblado')->first()->value == 'Sí') selected @endif @endif>Sí</option>
                           <option @if(old('amoblado') and old('amoblado') == 'No') selected @else  @if(!old('amoblado') and $publication->parameters()->where('name', 'Amoblado')->first() and $publication->parameters()->where('name', 'Amoblado')->first()->value == 'No') selected @endif @endif>No</option>
                       </select>
                    </div>                          
                </div>
               @endif   
               
               @if($tipo_producto === 'departamento' || $tipo_producto === 'local comercial' || $tipo_producto === 'oficina')  
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Ascensores</label>
                   <div class="col-sm-2">
                       <select name="ascensores" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('ascensores') and old('ascensores') == '1') selected @else  @if(!old('ascensores') and $publication->parameters()->where('name', 'Ascensores')->first() and $publication->parameters()->where('name', 'Ascensores')->first()->value == '1') selected @endif @endif>1</option>
                           <option @if(old('ascensores') and old('ascensores') == '2') selected @else  @if(!old('ascensores') and $publication->parameters()->where('name', 'Ascensores')->first() and $publication->parameters()->where('name', 'Ascensores')->first()->value == '2') selected @endif @endif>2</option>
                           <option @if(old('ascensores') and old('ascensores') == '3') selected @else  @if(!old('ascensores') and $publication->parameters()->where('name', 'Ascensores')->first() and $publication->parameters()->where('name', 'Ascensores')->first()->value == '3') selected @endif @endif>3</option>
                           <option @if(old('ascensores') and old('ascensores') == '4') selected @else  @if(!old('ascensores') and $publication->parameters()->where('name', 'Ascensores')->first() and $publication->parameters()->where('name', 'Ascensores')->first()->value == '4') selected @endif @endif>4</option>
                           <option @if(old('ascensores') and old('ascensores') == 'Más de 4') selected @else  @if(!old('ascensores') and $publication->parameters()->where('name', 'Ascensores')->first() and $publication->parameters()->where('name', 'Ascensores')->first()->value == 'Más de 4') selected @endif @endif>Más de 4</option>
                       </select>
                    </div>                          
                </div>
               @endif       
               
               @if($tipo_producto === 'departamento')  
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Cant.de pisos del edificio</label>
                   <div class="col-sm-2">
                       <select name="pisos" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('pisos') and old('pisos') == '1') selected @else  @if(!old('pisos') and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first() and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first()->value == '1') selected @endif @endif>1</option>
                           <option @if(old('pisos') and old('pisos') == '2') selected @else  @if(!old('pisos') and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first() and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first()->value == '2') selected @endif @endif>2</option>
                           <option @if(old('pisos') and old('pisos') == '3') selected @else  @if(!old('pisos') and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first() and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first()->value == '3') selected @endif @endif>3</option>
                           <option @if(old('pisos') and old('pisos') == '4') selected @else  @if(!old('pisos') and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first() and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first()->value == '4') selected @endif @endif>4</option>
                           <option @if(old('pisos') and old('pisos') == '5') selected @else  @if(!old('pisos') and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first() and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first()->value == '5') selected @endif @endif>5</option>
                           <option @if(old('pisos') and old('pisos') == '6') selected @else  @if(!old('pisos') and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first() and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first()->value == '6') selected @endif @endif>6</option>
                           <option @if(old('pisos') and old('pisos') == '7') selected @else  @if(!old('pisos') and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first() and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first()->value == '7') selected @endif @endif>7</option>
                           <option @if(old('pisos') and old('pisos') == '8') selected @else  @if(!old('pisos') and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first() and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first()->value == '8') selected @endif @endif>8</option>
                           <option @if(old('pisos') and old('pisos') == '9') selected @else  @if(!old('pisos') and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first() and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first()->value == '9') selected @endif @endif>9</option>
                           <option @if(old('pisos') and old('pisos') == '10') selected @else  @if(!old('pisos') and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first() and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first()->value == '10') selected @endif @endif>10</option>
                           <option @if(old('pisos') and old('pisos') == 'Más de 10') selected @else  @if(!old('pisos') and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first() and $publication->parameters()->where('name', 'Cant.de pisos del edificio')->first()->value == 'Más de 10') selected @endif @endif>Más de 10</option>
                       </select>
                    </div>                          
                </div>
               @endif                  
               @if($tipo_producto === 'casa' || $tipo_producto === 'departamento' || $tipo_producto === 'local comercial' || $tipo_producto === 'oficina' || $tipo_producto === 'inmueble') 
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Condición</label>
                   <div class="col-sm-2">
                       <select name="estado" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('estado') and old('estado') == 'A refaccionar') selected @else  @if(!old('estado') and $publication->parameters()->where('name', 'Condición')->first() and $publication->parameters()->where('name', 'Condición')->first()->value == 'A refaccionar') selected @endif @endif>A refaccionar</option>
                           <option @if(old('estado') and old('estado') == 'Regular') selected @else  @if(!old('estado') and $publication->parameters()->where('name', 'Condición')->first() and $publication->parameters()->where('name', 'Condición')->first()->value == 'Regular') selected @endif @endif>Regular</option>
                           <option @if(old('estado') and old('estado') == 'Bueno') selected @else  @if(!old('estado') and $publication->parameters()->where('name', 'Condición')->first() and $publication->parameters()->where('name', 'Condición')->first()->value == 'Bueno') selected @endif @endif>Bueno</option>
                           <option @if(old('estado') and old('estado') == 'Muy Bueno') selected @else  @if(!old('estado') and $publication->parameters()->where('name', 'Condición')->first() and $publication->parameters()->where('name', 'Condición')->first()->value == 'Muy Bueno') selected @endif @endif>Muy Bueno</option>
                           <option @if(old('estado') and old('estado') == 'Excelente') selected @else  @if(!old('estado') and $publication->parameters()->where('name', 'Condición')->first() and $publication->parameters()->where('name', 'Condición')->first()->value == 'Excelente') selected @endif @endif>Excelente</option>
                       </select>
                    </div>                          
                </div>
               @endif              
               @if($tipo_producto === 'casa' || $tipo_producto === 'departamento' || $tipo_producto === 'local comercial' || $tipo_producto === 'oficina') 
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Garaje</label>
                   <div class="col-sm-2">
                       <select name="garaje" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('garaje') and old('garaje') == 'Sí') selected @else  @if(!old('garaje') and $publication->parameters()->where('name', 'Garaje')->first() and $publication->parameters()->where('name', 'Garaje')->first()->value == 'Sí') selected @endif @endif>Sí</option>
                           <option @if(old('garaje') and old('garaje') == 'No') selected @else  @if(!old('garaje') and $publication->parameters()->where('name', 'Garaje')->first() and $publication->parameters()->where('name', 'Garaje')->first()->value == 'No') selected @endif @endif>No</option>
                       </select>
                    </div>                          
                </div>
               @endif             
               @if($tipo_producto === 'casa' || $tipo_producto === 'departamento' || $tipo_producto === 'local comercial' || $tipo_producto === 'oficina' || $tipo_producto === 'terreno' || $tipo_producto === 'inmueble')
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Horario de contacto</label>
                   <div class="col-sm-2">
                       <input type="text" name="horario" class="form-control" @if(old('horario'))value="{{old('horario')}}"@else @if($publication->vehicle_schedule)value="{{$publication->vehicle_schedule}}"@endif @endif>
                    </div>                          
                </div>  
               <br>
               @endif            
               @if($tipo_producto === 'casa')
                <h3>Comodidades</h3>
                <div class="row">
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Aire acondicionado" @if(old('Comodidades', $comodidades) and in_array('Aire acondicionado',old('Comodidades', $comodidades))) checked @endif> Aire acondicionado</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Alarma de seguridad" @if(old('Comodidades', $comodidades) and in_array('Alarma de seguridad',old('Comodidades', $comodidades))) checked @endif> Alarma de seguridad</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="BBQ" @if(old('Comodidades', $comodidades) and in_array('BBQ',old('Comodidades', $comodidades))) checked @endif> BBQ</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Calefacción por ambiente" @if(old('Comodidades', $comodidades) and in_array('Calefacción por ambiente',old('Comodidades', $comodidades))) checked @endif> Calefacción por ambiente</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Cancha de fútbol" @if(old('Comodidades', $comodidades) and in_array('Cancha de fútbol',old('Comodidades', $comodidades))) checked @endif> Cancha de fútbol</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Cancha de tenis" @if(old('Comodidades', $comodidades) and in_array('Cancha de tenis',old('Comodidades', $comodidades))) checked @endif> Cancha de tenis</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Conexión a internet" @if(old('Comodidades', $comodidades) and in_array('Conexión a internet',old('Comodidades', $comodidades))) checked @endif> Conexión a internet</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Gimnasio" @if(old('Comodidades', $comodidades) and in_array('Gimnasio',old('Comodidades', $comodidades))) checked @endif> Gimnasio</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Jacuzzi" @if(old('Comodidades', $comodidades) and in_array('Jacuzzi',old('Comodidades', $comodidades))) checked @endif> Jacuzzi</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Jardín" @if(old('Comodidades', $comodidades) and in_array('Jardín',old('Comodidades', $comodidades))) checked @endif> Jardín</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Linea telefónica" @if(old('Comodidades', $comodidades) and in_array('Linea telefónica',old('Comodidades', $comodidades))) checked @endif> Linea telefónica</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Patio" @if(old('Comodidades', $comodidades) and in_array('Patio',old('Comodidades', $comodidades))) checked @endif> Patio</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Piscina" @if(old('Comodidades', $comodidades) and in_array('Piscina',old('Comodidades', $comodidades))) checked @endif> Piscina</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Seguridad" @if(old('Comodidades', $comodidades) and in_array('Seguridad',old('Comodidades', $comodidades))) checked @endif> Seguridad</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="TV/Cable" @if(old('Comodidades', $comodidades) and in_array('TV/Cable',old('Comodidades', $comodidades))) checked @endif> TV/Cable</label></div>
                </div>
                <br> 
                <h3>Ambientes</h3>
                <div class="row">
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Amoblado" @if(old('Ambientes', $ambientes) and in_array('Amoblado',old('Ambientes', $ambientes))) checked @endif> Amoblado</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Área de Servicio" @if(old('Ambientes', $ambientes) and in_array('Área de Servicio',old('Ambientes', $ambientes))) checked @endif> Área de Servicio</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Biblioteca" @if(old('Ambientes', $ambientes) and in_array('Biblioteca',old('Ambientes', $ambientes))) checked @endif> Biblioteca</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Cocina" @if(old('Ambientes', $ambientes) and in_array('Cocina',old('Ambientes', $ambientes))) checked @endif> Cocina</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Comedor" @if(old('Ambientes', $ambientes) and in_array('Comedor',old('Ambientes', $ambientes))) checked @endif> Comedor</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Depósito" @if(old('Ambientes', $ambientes) and in_array('Depósito',old('Ambientes', $ambientes))) checked @endif> Depósito</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Dormitorio en suite" @if(old('Ambientes', $ambientes) and in_array('Dormitorio en suite',old('Ambientes', $ambientes))) checked @endif> Dormitorio en suite</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Escritorio" @if(old('Ambientes', $ambientes) and in_array('Escritorio',old('Ambientes', $ambientes))) checked @endif> Escritorio</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Lavandería" @if(old('Ambientes', $ambientes) and in_array('Lavandería',old('Ambientes', $ambientes))) checked @endif> Lavandería</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Living" @if(old('Ambientes', $ambientes) and in_array('Living',old('Ambientes', $ambientes))) checked @endif> Living</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Sala" @if(old('Ambientes', $ambientes) and in_array('Sala',old('Ambientes', $ambientes))) checked @endif> Sala</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Terraza" @if(old('Ambientes', $ambientes) and in_array('Terraza',old('Ambientes', $ambientes))) checked @endif> Terraza</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Toilette" @if(old('Ambientes', $ambientes) and in_array('Toilette',old('Ambientes', $ambientes))) checked @endif> Toilette</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Vestidor" @if(old('Ambientes', $ambientes) and in_array('Vestidor',old('Ambientes', $ambientes))) checked @endif> Vestidor</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Walk-in closet" @if(old('Ambientes', $ambientes) and in_array('Walk-in closet',old('Ambientes', $ambientes))) checked @endif> Walk-in closet</label></div>
                </div>
               @endif           
               @if($tipo_producto === 'departamento')
                <h3>Comodidades</h3>
                <div class="row">
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Aire acondicionado" @if(old('Comodidades', $comodidades) and in_array('Aire acondicionado',old('Comodidades', $comodidades))) checked @endif> Aire acondicionado</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Aire acondicionado por ambiente" @if(old('Comodidades', $comodidades) and in_array('Aire acondicionado por ambiente',old('Comodidades', $comodidades))) checked @endif> Aire acondicionado por ambiente</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="BBQ" @if(old('Comodidades', $comodidades) and in_array('BBQ',old('Comodidades', $comodidades))) checked @endif> BBQ</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Calefacción" @if(old('Comodidades', $comodidades) and in_array('Calefacción',old('Comodidades', $comodidades))) checked @endif> Calefacción</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Calefacción por ambiente" @if(old('Comodidades', $comodidades) and in_array('Calefacción por ambiente',old('Comodidades', $comodidades))) checked @endif> Calefacción por ambiente</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Cancha de tenis" @if(old('Comodidades', $comodidades) and in_array('Cancha de tenis',old('Comodidades', $comodidades))) checked @endif> Cancha de tenis</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Conexión a internet" @if(old('Comodidades', $comodidades) and in_array('Conexión a internet',old('Comodidades', $comodidades))) checked @endif> Conexión a internet</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Estacionamiento de cortesía" @if(old('Comodidades', $comodidades) and in_array('Estacionamiento de cortesía',old('Comodidades', $comodidades))) checked @endif> Estacionamiento de cortesía</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Gimnasio" @if(old('Comodidades', $comodidades) and in_array('Gimnasio',old('Comodidades', $comodidades))) checked @endif> Gimnasio</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Jacuzzi" @if(old('Comodidades', $comodidades) and in_array('Jacuzzi',old('Comodidades', $comodidades))) checked @endif> Jacuzzi</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Jardín" @if(old('Comodidades', $comodidades) and in_array('Jardín',old('Comodidades', $comodidades))) checked @endif> Jardín</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Juegos infantiles" @if(old('Comodidades', $comodidades) and in_array('Juegos infantiles',old('Comodidades', $comodidades))) checked @endif> Juegos infantiles</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Linea telefónica" @if(old('Comodidades', $comodidades) and in_array('Linea telefónica',old('Comodidades', $comodidades))) checked @endif> Linea telefónica</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Patio" @if(old('Comodidades', $comodidades) and in_array('Patio',old('Comodidades', $comodidades))) checked @endif> Patio</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Piscina" @if(old('Comodidades', $comodidades) and in_array('Piscina',old('Comodidades', $comodidades))) checked @endif> Piscina</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Salida de emergencia" @if(old('Comodidades', $comodidades) and in_array('Salida de emergencia',old('Comodidades', $comodidades))) checked @endif> Salida de emergencia</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Sauna" @if(old('Comodidades', $comodidades) and in_array('Sauna',old('Comodidades', $comodidades))) checked @endif> Sauna</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Seguridad" @if(old('Comodidades', $comodidades) and in_array('Seguridad',old('Comodidades', $comodidades))) checked @endif> Seguridad</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Sistema contra incendio" @if(old('Comodidades', $comodidades) and in_array('Sistema contra incendio',old('Comodidades', $comodidades))) checked @endif> Sistema contra incendio</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Comodidades[]" value="Spa" @if(old('Comodidades', $comodidades) and in_array('Spa',old('Comodidades', $comodidades))) checked @endif> Spa</label></div>
                </div>
                <br> 
                <h3>Ambientes</h3>
                <div class="row">
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Area de servicio" @if(old('Ambientes', $ambientes) and in_array('Area de servicio',old('Ambientes', $ambientes))) checked @endif> Area de servicio</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Baño de visita" @if(old('Ambientes', $ambientes) and in_array('Baño de visita',old('Ambientes', $ambientes))) checked @endif> Baño de visita</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Baulera / Altillo" @if(old('Ambientes', $ambientes) and in_array('Baulera / Altillo',old('Ambientes', $ambientes))) checked @endif> Baulera / Altillo</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Biblioteca" @if(old('Ambientes', $ambientes) and in_array('Biblioteca',old('Ambientes', $ambientes))) checked @endif> Biblioteca</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Cocina" @if(old('Ambientes', $ambientes) and in_array('Cocina',old('Ambientes', $ambientes))) checked @endif> Cocina</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Comedor" @if(old('Ambientes', $ambientes) and in_array('Comedor',old('Ambientes', $ambientes))) checked @endif> Comedor</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Dormitorio en suite" @if(old('Ambientes', $ambientes) and in_array('Dormitorio en suite',old('Ambientes', $ambientes))) checked @endif> Dormitorio en suite</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Escritorio" @if(old('Ambientes', $ambientes) and in_array('Escritorio',old('Ambientes', $ambientes))) checked @endif> Escritorio</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Lavandería" @if(old('Ambientes', $ambientes) and in_array('Lavandería',old('Ambientes', $ambientes))) checked @endif> Lavandería</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Living" @if(old('Ambientes', $ambientes) and in_array('Living',old('Ambientes', $ambientes))) checked @endif> Living</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Sala" @if(old('Ambientes', $ambientes) and in_array('Sala',old('Ambientes', $ambientes))) checked @endif> Sala</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Terraza" @if(old('Ambientes', $ambientes) and in_array('Terraza',old('Ambientes', $ambientes))) checked @endif> Terraza</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Toilette" @if(old('Ambientes', $ambientes) and in_array('Toilette',old('Ambientes', $ambientes))) checked @endif> Toilette</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Vestidor" @if(old('Ambientes', $ambientes) and in_array('Vestidor',old('Ambientes', $ambientes))) checked @endif> Vestidor</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Ambientes[]" value="Walk-in closet" @if(old('Ambientes', $ambientes) and in_array('Walk-in closet',old('Ambientes', $ambientes))) checked @endif> Walk-in closet</label></div>
                </div>
               @endif         
               @if($tipo_producto === 'local comercial')
                <h3>Características Adicionales</h3>
                <div class="row">
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Aire acondicionado" @if(old('Caracteristicas', $caracteristicas) and in_array('Aire acondicionado',old('Caracteristicas', $caracteristicas))) checked @endif> Aire acondicionado</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Alarma" @if(old('Caracteristicas', $caracteristicas) and in_array('Alarma',old('Caracteristicas', $caracteristicas))) checked @endif> Alarma</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Calefacción" @if(old('Caracteristicas', $caracteristicas) and in_array('Calefacción',old('Caracteristicas', $caracteristicas))) checked @endif> Calefacción</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Cocina" @if(old('Caracteristicas', $caracteristicas) and in_array('Cocina',old('Caracteristicas', $caracteristicas))) checked @endif> Cocina</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Conexión a internet" @if(old('Caracteristicas', $caracteristicas) and in_array('Conexión a internet',old('Caracteristicas', $caracteristicas))) checked @endif> Conexión a internet</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Depósito" @if(old('Caracteristicas', $caracteristicas) and in_array('Depósito',old('Caracteristicas', $caracteristicas))) checked @endif> Depósito</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Gas Natural" @if(old('Caracteristicas', $caracteristicas) and in_array('Gas Natural',old('Caracteristicas', $caracteristicas))) checked @endif> Gas Natural</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Instalación de agua" @if(old('Caracteristicas', $caracteristicas) and in_array('Instalación de agua',old('Caracteristicas', $caracteristicas))) checked @endif> Instalación de agua</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Linea telefónica" @if(old('Caracteristicas', $caracteristicas) and in_array('Linea telefónica',old('Caracteristicas', $caracteristicas))) checked @endif> Linea telefónica</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Luz eléctrica" @if(old('Caracteristicas', $caracteristicas) and in_array('Luz eléctrica',old('Caracteristicas', $caracteristicas))) checked @endif> Luz eléctrica</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Salida de emergencia" @if(old('Caracteristicas', $caracteristicas) and in_array('Salida de emergencia',old('Caracteristicas', $caracteristicas))) checked @endif> Salida de emergencia</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Seguridad" @if(old('Caracteristicas', $caracteristicas) and in_array('Seguridad',old('Caracteristicas', $caracteristicas))) checked @endif> Seguridad</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Sistema contra incendio" @if(old('Caracteristicas', $caracteristicas) and in_array('Sistema contra incendio',old('Caracteristicas', $caracteristicas))) checked @endif> Sistema contra incendio</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="TV/Cable" @if(old('Caracteristicas', $caracteristicas) and in_array('TV/Cable',old('Caracteristicas', $caracteristicas))) checked @endif> TV/Cable</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Vestuario" @if(old('Caracteristicas', $caracteristicas) and in_array('Vestuario',old('Caracteristicas', $caracteristicas))) checked @endif> Vestuario</label></div>
                </div>
               @endif         
               @if($tipo_producto === 'oficina')
                <h3>Características Adicionales</h3>
                <div class="row">
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Aire acondicionado" @if(old('Caracteristicas', $caracteristicas) and in_array('Aire acondicionado',old('Caracteristicas', $caracteristicas))) checked @endif> Aire acondicionado</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Alarma de seguridad" @if(old('Caracteristicas', $caracteristicas) and in_array('Alarma de seguridad',old('Caracteristicas', $caracteristicas))) checked @endif> Alarma de seguridad</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Calefacción" @if(old('Caracteristicas', $caracteristicas) and in_array('Calefacción',old('Caracteristicas', $caracteristicas))) checked @endif> Calefacción</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Cocina" @if(old('Caracteristicas', $caracteristicas) and in_array('Cocina',old('Caracteristicas', $caracteristicas))) checked @endif> Cocina</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Comedor" @if(old('Caracteristicas', $caracteristicas) and in_array('Comedor',old('Caracteristicas', $caracteristicas))) checked @endif> Comedor</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Conexión a internet" @if(old('Caracteristicas', $caracteristicas) and in_array('Conexión a internet',old('Caracteristicas', $caracteristicas))) checked @endif> Conexión a internet</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Depósito" @if(old('Caracteristicas', $caracteristicas) and in_array('Depósito',old('Caracteristicas', $caracteristicas))) checked @endif> Depósito</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Gas Natural" @if(old('Caracteristicas', $caracteristicas) and in_array('Gas Natural',old('Caracteristicas', $caracteristicas))) checked @endif> Gas Natural</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Instalación de agua" @if(old('Caracteristicas', $caracteristicas) and in_array('Instalación de agua',old('Caracteristicas', $caracteristicas))) checked @endif> Instalación de agua</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Jardín" @if(old('Caracteristicas', $caracteristicas) and in_array('Jardín',old('Caracteristicas', $caracteristicas))) checked @endif> Jardín</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Linea telefónica" @if(old('Caracteristicas', $caracteristicas) and in_array('Linea telefónica',old('Caracteristicas', $caracteristicas))) checked @endif> Linea telefónica</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Luz eléctrica" @if(old('Caracteristicas', $caracteristicas) and in_array('Luz eléctrica',old('Caracteristicas', $caracteristicas))) checked @endif> Luz eléctrica</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Oficinas independientes" @if(old('Caracteristicas', $caracteristicas) and in_array('Oficinas independientes',old('Caracteristicas', $caracteristicas))) checked @endif> Oficinas independientes</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Recepción" @if(old('Caracteristicas', $caracteristicas) and in_array('Recepción',old('Caracteristicas', $caracteristicas))) checked @endif> Recepción</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Sala de juntas" @if(old('Caracteristicas', $caracteristicas) and in_array('Sala de juntas',old('Caracteristicas', $caracteristicas))) checked @endif> Sala de juntas</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Salida de emergencia" @if(old('Caracteristicas', $caracteristicas) and in_array('Salida de emergencia',old('Caracteristicas', $caracteristicas))) checked @endif> Salida de emergencia</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Seguridad" @if(old('Caracteristicas', $caracteristicas) and in_array('Seguridad',old('Caracteristicas', $caracteristicas))) checked @endif> Seguridad</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Sistema contra incendio" @if(old('Caracteristicas', $caracteristicas) and in_array('Sistema contra incendio',old('Caracteristicas', $caracteristicas))) checked @endif> Sistema contra incendio</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Terraza" @if(old('Caracteristicas', $caracteristicas) and in_array('Terraza',old('Caracteristicas', $caracteristicas))) checked @endif> Terraza</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="TV/Cable" @if(old('Caracteristicas', $caracteristicas) and in_array('TV/Cable',old('Caracteristicas', $caracteristicas))) checked @endif> TV/Cable</label></div>
                </div>
               @endif        
               @if($tipo_producto === 'terreno')
                <h3>Características Adicionales</h3>
                <div class="row">
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Gas Natural" @if(old('Caracteristicas', $caracteristicas) and in_array('Gas Natural',old('Caracteristicas', $caracteristicas))) checked @endif> Gas Natural</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Instalación de agua" @if(old('Caracteristicas', $caracteristicas) and in_array('Instalación de agua',old('Caracteristicas', $caracteristicas))) checked @endif> Instalación de agua</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Linea telefónica" @if(old('Caracteristicas', $caracteristicas) and in_array('Linea telefónica',old('Caracteristicas', $caracteristicas))) checked @endif> Linea telefónica</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Caracteristicas[]" value="Luz eléctrica" @if(old('Caracteristicas', $caracteristicas) and in_array('Luz eléctrica',old('Caracteristicas', $caracteristicas))) checked @endif> Luz eléctrica</label></div>
                </div>
               @endif
               @if($tipo_producto === 'servicio')
                <h3>Zonas de Cobertura</h3>
                <div class="row">
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Amazonas" @if(old('Zonas', $zonas) and in_array('Amazonas',old('Zonas', $zonas))) checked @endif> Amazonas</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Ancash" @if(old('Zonas', $zonas) and in_array('Ancash',old('Zonas', $zonas))) checked @endif> Ancash</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Apurimac" @if(old('Zonas', $zonas) and in_array('Apurimac',old('Zonas', $zonas))) checked @endif> Apurimac</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Arequipa" @if(old('Zonas', $zonas) and in_array('Arequipa',old('Zonas', $zonas))) checked @endif> Arequipa</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Ayacucho" @if(old('Zonas', $zonas) and in_array('Ayacucho',old('Zonas', $zonas))) checked @endif> Ayacucho</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Cajamarca" @if(old('Zonas', $zonas) and in_array('Cajamarca',old('Zonas', $zonas))) checked @endif> Cajamarca</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Callao" @if(old('Zonas', $zonas) and in_array('Callao',old('Zonas', $zonas))) checked @endif> Callao</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Cusco" @if(old('Zonas', $zonas) and in_array('Cusco',old('Zonas', $zonas))) checked @endif> Cusco</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Huancavelica" @if(old('Zonas', $zonas) and in_array('Huancavelica',old('Zonas', $zonas))) checked @endif> Huancavelica</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Huanuco" @if(old('Zonas', $zonas) and in_array('Huanuco',old('Zonas', $zonas))) checked @endif> Huanuco</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Ica" @if(old('Zonas', $zonas) and in_array('Ica',old('Zonas', $zonas))) checked @endif> Ica</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Junin" @if(old('Zonas', $zonas) and in_array('Junin',old('Zonas', $zonas))) checked @endif> Junin</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="La Libertad" @if(old('Zonas', $zonas) and in_array('La Libertad',old('Zonas', $zonas))) checked @endif> La Libertad</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Lambayeque" @if(old('Zonas', $zonas) and in_array('Lambayeque',old('Zonas', $zonas))) checked @endif> Lambayeque</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Lima" @if(old('Zonas', $zonas) and in_array('Lima',old('Zonas', $zonas))) checked @endif> Lima</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Loreto" @if(old('Zonas', $zonas) and in_array('Loreto',old('Zonas', $zonas))) checked @endif> Loreto</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Madre De Dios" @if(old('Zonas', $zonas) and in_array('Madre De Dios',old('Zonas', $zonas))) checked @endif> Madre De Dios</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Moquegua" @if(old('Zonas', $zonas) and in_array('Moquegua',old('Zonas', $zonas))) checked @endif> Moquegua</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Pasco" @if(old('Zonas', $zonas) and in_array('Pasco',old('Zonas', $zonas))) checked @endif> Pasco</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Piura" @if(old('Zonas', $zonas) and in_array('Piura',old('Zonas', $zonas))) checked @endif> Piura</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Puno" @if(old('Zonas', $zonas) and in_array('Puno',old('Zonas', $zonas))) checked @endif> Puno</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="San Martin" @if(old('Zonas', $zonas) and in_array('San Martin',old('Zonas', $zonas))) checked @endif> San Martin</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Tacna" @if(old('Zonas', $zonas) and in_array('Tacna',old('Zonas', $zonas))) checked @endif> Tacna</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Tumbes" @if(old('Zonas', $zonas) and in_array('Tumbes',old('Zonas', $zonas))) checked @endif> Tumbes</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Zonas[]" value="Ucayali" @if(old('Zonas', $zonas) and in_array('Ucayali',old('Zonas', $zonas))) checked @endif> Ucayali</label></div>
                </div>
                <br>
               @endif
               
               
               
               @if($tipo_producto === 'vehículo' || $tipo_producto === 'vehiculo')  
                <div class="form-group {{ $errors->has('año') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Año *</label>
                   <div class="col-sm-2">
                       <select name="año" class="form-control">
                           <option value="">Elegir</option>
                           @for($i=2016; $i>=1980; $i--)
                           <option @if(old('año') and old('año') == $i) selected @else  @if(!old('año') and $publication->vehicle_year and $publication->vehicle_year == $i) selected @endif @endif>{{ $i }}</option>
                           @endfor
                          
                       </select>
                    </div>        
                </div>  
               @endif
               
               @if($tipo_producto === 'vehículo de colección')  
                <div class="form-group {{ $errors->has('año') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Año *</label>
                   <div class="col-sm-2">
                       <select name="año" class="form-control">
                           <option value="">Elegir</option>
                           @for($i=1979; $i>=1900; $i--)
                           <option @if(old('año') and old('año') == $i) selected @else  @if(!old('año') and $publication->vehicle_year and $publication->vehicle_year == $i) selected @endif @endif>{{ $i }}</option>
                           @endfor
                          
                       </select>
                    </div>        
                </div>  
               @endif
               
               @if($tipo_producto === 'lancha')  
                <div class="form-group {{ $errors->has('año') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Año *</label>
                   <div class="col-sm-2">
                       <select name="año" class="form-control">
                           <option value="">Elegir</option>
                           @for($i=2016; $i>=1973; $i--)
                           <option @if(old('año') and old('año') == $i) selected @else  @if(!old('año') and $publication->vehicle_year and $publication->vehicle_year == $i) selected @endif @endif>{{ $i }}</option>
                           @endfor
                          
                       </select>
                    </div>        
                </div>   
                <div class="form-group {{ $errors->has('marca') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Marca *</label>
                   <div class="col-sm-2">
                       <input type="text" name="marca" class="form-control"  @if(old('marca'))value="{{old('marca')}}"@else @if($publication->vehicle_brand)value="{{$publication->vehicle_brand}}"@endif @endif>
                    </div>                          
                </div>
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Tipo de embarcación</label>
                   <div class="col-sm-2">
                       <select name="tipo" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('tipo') and old('tipo') == 'Botes') selected @else  @if(!old('tipo') and $publication->vehicle_type and $publication->vehicle_type == 'Botes') selected @endif @endif>Botes</option>
                          <option @if(old('tipo') and old('tipo') == 'Canoas') selected @else  @if(!old('tipo') and $publication->vehicle_type and $publication->vehicle_type == 'Canoas') selected @endif @endif>Canoas</option>
                          <option @if(old('tipo') and old('tipo') == 'Catamaranes') selected @else  @if(!old('tipo') and $publication->vehicle_type and $publication->vehicle_type == 'Catamaranes') selected @endif @endif>Catamaranes</option>
                          <option @if(old('tipo') and old('tipo') == 'Cruceros') selected @else  @if(!old('tipo') and $publication->vehicle_type and $publication->vehicle_type == 'Cruceros') selected @endif @endif>Cruceros</option>
                          <option @if(old('tipo') and old('tipo') == 'Gomones') selected @else  @if(!old('tipo') and $publication->vehicle_type and $publication->vehicle_type == 'Gomones') selected @endif @endif>Gomones</option>
                          <option @if(old('tipo') and old('tipo') == 'Jet Ski') selected @else  @if(!old('tipo') and $publication->vehicle_type and $publication->vehicle_type == 'Jet Ski') selected @endif @endif>Jet Ski</option>
                          <option @if(old('tipo') and old('tipo') == 'Kayaks') selected @else  @if(!old('tipo') and $publication->vehicle_type and $publication->vehicle_type == 'Kayaks') selected @endif @endif>Kayaks</option>
                          <option @if(old('tipo') and old('tipo') == 'Lanchas') selected @else  @if(!old('tipo') and $publication->vehicle_type and $publication->vehicle_type == 'Lanchas') selected @endif @endif>Lanchas</option>
                          <option @if(old('tipo') and old('tipo') == 'Otra Embarcación') selected @else  @if(!old('tipo') and $publication->vehicle_type and $publication->vehicle_type == 'Otra Embarcación') selected @endif @endif>Otra Embarcación</option>
                          <option @if(old('tipo') and old('tipo') == 'Veleros') selected @else  @if(!old('tipo') and $publication->vehicle_type and $publication->vehicle_type == 'Veleros') selected @endif @endif>Veleros</option>
                          <option @if(old('tipo') and old('tipo') == 'Waverunner') selected @else  @if(!old('tipo') and $publication->vehicle_type and $publication->vehicle_type == 'Waverunner') selected @endif @endif>Waverunner</option>
                          <option @if(old('tipo') and old('tipo') == 'Yates') selected @else  @if(!old('tipo') and $publication->vehicle_type and $publication->vehicle_type == 'Yates') selected @endif @endif>Yates</option>
                       </select>
                    </div>                          
                </div>
               <br>
               
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Horario de contacto</label>
                   <div class="col-sm-2">
                       <input type="text" name="horario" class="form-control" @if(old('horario'))value="{{old('horario')}}"@else @if($publication->vehicle_schedule)value="{{$publication->vehicle_schedule}}"@endif @endif>
                    </div>                          
                </div>  
               <br>
               
               @endif
               @if($tipo_producto === 'vehiculo') 
                <div class="form-group {{ $errors->has('marca') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Marca *</label>
                   <div class="col-sm-2">
                       <input type="text" name="marca" class="form-control"  @if(old('marca'))value="{{old('marca')}}"@else @if($publication->vehicle_brand)value="{{$publication->vehicle_brand}}"@endif @endif>
                    </div>                          
                </div>
                <div class="form-group {{ $errors->has('modelo') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Modelo *</label>
                   <div class="col-sm-2">
                       <input type="text" name="modelo" class="form-control"  @if(old('modelo'))value="{{old('modelo')}}"@else @if($publication->vehicle_model)value="{{$publication->vehicle_model}}"@endif @endif>
                    </div>                          
                </div>                                
                <div class="form-group {{ $errors->has('kilometros') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Kilómetros *</label>
                   <div class="col-sm-2">
                       <input type="text" name="kilometros" class="form-control" @if(old('kilometros'))value="{{old('kilometros')}}"@else @if($publication->vehicle_kilometers)value="{{$publication->vehicle_kilometers}}"@endif @endif>
                    </div>                          
                </div>  
               <br>
                                                    
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Combustible</label>
                   <div class="col-sm-2">
                       <select name="combustible" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('combustible') and old('combustible') == 'Diesel') selected @else  @if(!old('combustible') and $publication->vehicle_fuel and $publication->vehicle_fuel == 'Diesel') selected @endif @endif>Diesel</option>
                            <option @if(old('combustible') and old('combustible') == 'Gas') selected @else  @if(!old('combustible') and $publication->vehicle_fuel and $publication->vehicle_fuel == 'Gas') selected @endif @endif>Gas</option>
                            <option @if(old('combustible') and old('combustible') == 'Gasolina') selected @else  @if(!old('combustible') and $publication->vehicle_fuel and $publication->vehicle_fuel == 'Gasolina') selected @endif @endif>Gasolina</option>
                       </select>
                    </div>                          
                </div>      
               @endif
               @if($tipo_producto === 'moto')  
                <div class="form-group {{ $errors->has('año') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Año *</label>
                   <div class="col-sm-2">
                       <select name="año" class="form-control">
                           <option value="">Elegir</option>
                           @for($i=2016; $i>=1973; $i--)
                           <option @if(old('año') and old('año') == $i) selected @else  @if(!old('año') and $publication->vehicle_year and $publication->vehicle_year == $i) selected @endif @endif>{{ $i }}</option>
                           @endfor
                          
                       </select>
                    </div>        
                </div>                              
                <div class="form-group {{ $errors->has('kilometros') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Kilómetros *</label>
                   <div class="col-sm-2">
                       <input type="text" name="kilometros" class="form-control" @if(old('kilometros'))value="{{old('kilometros')}}"@else @if($publication->vehicle_kilometers)value="{{$publication->vehicle_kilometers}}"@endif @endif>
                    </div>                          
                </div> 
                <div class="form-group {{ $errors->has('modelo') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Modelo *</label>
                   <div class="col-sm-2">
                       <input type="text" name="modelo" class="form-control"  @if(old('modelo'))value="{{old('modelo')}}"@else @if($publication->vehicle_model)value="{{$publication->vehicle_model}}"@endif @endif>
                    </div>                          
                </div>
                <div class="form-group {{ $errors->has('version') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Versión *</label>
                   <div class="col-sm-2">
                       <input type="text" name="version" class="form-control"  @if(old('version'))value="{{old('version')}}"@else @if($publication->vehicle_version)value="{{$publication->vehicle_version}}"@endif @endif>
                    </div>                          
                </div> 
               <br>
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Alarma</label>
                   <div class="col-sm-2">
                       <select name="alarma" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('alarma') and old('alarma') == 'Sí') selected @else  @if(!old('alarma') and $publication->parameters()->where('name', 'Alarma')->first() and $publication->parameters()->where('name', 'Alarma')->first()->value == 'Sí') selected @endif @endif>Sí</option>
                            <option @if(old('alarma') and old('alarma') == 'No') selected @else  @if(!old('alarma') and $publication->parameters()->where('name', 'Alarma')->first() and $publication->parameters()->where('name', 'Alarma')->first()->value == 'No') selected @endif @endif>No</option>
                       </select>
                    </div>                          
                </div>
               
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Color</label>
                   <div class="col-sm-2">
                       <select name="color" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('color') and old('color') == 'Amarillo') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Amarillo') selected @endif @endif>Amarillo</option>
                            <option @if(old('color') and old('color') == 'Anaranjado') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Anaranjado') selected @endif @endif>Anaranjado</option>
                            <option @if(old('color') and old('color') == 'Azul') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Azul') selected @endif @endif>Azul</option>
                            <option @if(old('color') and old('color') == 'Beige') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Beige') selected @endif @endif>Beige</option>
                            <option @if(old('color') and old('color') == 'Blanco') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Blanco') selected @endif @endif>Blanco</option>
                            <option @if(old('color') and old('color') == 'Crema') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Crema') selected @endif @endif>Crema</option>
                            <option @if(old('color') and old('color') == 'Dorado') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Dorado') selected @endif @endif>Dorado</option>
                            <option @if(old('color') and old('color') == 'Gris') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Gris') selected @endif @endif>Gris</option>
                            <option @if(old('color') and old('color') == 'Marrón') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Marrón') selected @endif @endif>Marrón</option>
                            <option @if(old('color') and old('color') == 'Morado') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Morado') selected @endif @endif>Morado</option>
                            <option @if(old('color') and old('color') == 'Negro') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Negro') selected @endif @endif>Negro</option>
                            <option @if(old('color') and old('color') == 'Plateado') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Plateado') selected @endif @endif>Plateado</option>
                            <option @if(old('color') and old('color') == 'Rojo') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Rojo') selected @endif @endif>Rojo</option>
                            <option @if(old('color') and old('color') == 'Verde') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Verde') selected @endif @endif>Verde</option>
                            <option @if(old('color') and old('color') == 'Vino tinto') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Vino tinto') selected @endif @endif>Vino tinto</option>
                            <option @if(old('color') and old('color') == 'Otro') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Otro') selected @endif @endif>Otro</option>
                       </select>
                    </div>                          
                </div>
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Frenos</label>
                   <div class="col-sm-2">
                       <select name="frenos" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('frenos') and old('frenos') == 'ABS') selected @else  @if(!old('frenos') and $publication->parameters()->where('name', 'Frenos')->first() and $publication->parameters()->where('name', 'Frenos')->first()->value == 'ABS') selected @endif @endif>ABS</option>
                            <option @if(old('frenos') and old('frenos') == 'Disco Delantero') selected @else  @if(!old('frenos') and $publication->parameters()->where('name', 'Frenos')->first() and $publication->parameters()->where('name', 'Frenos')->first()->value == 'Disco Delantero') selected @endif @endif>Disco Delantero</option>
                            <option @if(old('frenos') and old('frenos') == 'Disco Trasero') selected @else  @if(!old('frenos') and $publication->parameters()->where('name', 'Frenos')->first() and $publication->parameters()->where('name', 'Frenos')->first()->value == 'Disco Trasero') selected @endif @endif>Disco Trasero</option>
                            <option @if(old('frenos') and old('frenos') == 'No Aplica') selected @else  @if(!old('frenos') and $publication->parameters()->where('name', 'Frenos')->first() and $publication->parameters()->where('name', 'Frenos')->first()->value == 'No Aplica') selected @endif @endif>No Aplica</option>
                       </select>
                    </div>                          
                </div>                     
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Sistema de Arranque</label>
                   <div class="col-sm-2">
                       <select name="arranque" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('arranque') and old('arranque') == 'A pedal') selected @else  @if(!old('arranque') and $publication->parameters()->where('name', 'Sistema de Arranque')->first() and $publication->parameters()->where('name', 'Sistema de Arranque')->first()->value == 'A pedal') selected @endif @endif>A pedal</option>
                           <option @if(old('arranque') and old('arranque') == 'Eléctrico') selected @else  @if(!old('arranque') and $publication->parameters()->where('name', 'Sistema de Arranque')->first() and $publication->parameters()->where('name', 'Sistema de Arranque')->first()->value == 'Eléctrico') selected @endif @endif>Eléctrico</option>
                           <option @if(old('arranque') and old('arranque') == 'Eléctrico y Pedal') selected @else  @if(!old('arranque') and $publication->parameters()->where('name', 'Sistema de Arranque')->first() and $publication->parameters()->where('name', 'Sistema de Arranque')->first()->value == 'Eléctrico y Pedal') selected @endif @endif>Eléctrico y Pedal</option>
                           <option @if(old('arranque') and old('arranque') == 'Otro') selected @else  @if(!old('arranque') and $publication->parameters()->where('name', 'Sistema de Arranque')->first() and $publication->parameters()->where('name', 'Sistema de Arranque')->first()->value == 'Otro') selected @endif @endif>Otro</option>
                       </select>
                    </div>                          
                </div>   
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Tipo de motor</label>
                   <div class="col-sm-2">
                       <select name="motor" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('motor') and old('motor') == '2 tiempos') selected @else  @if(!old('motor') and $publication->parameters()->where('name', 'Tipo de motor')->first() and $publication->parameters()->where('name', 'Tipo de motor')->first()->value == '2 tiempos') selected @endif @endif>2 tiempos</option>
                           <option @if(old('motor') and old('motor') == '4 tiempos') selected @else  @if(!old('motor') and $publication->parameters()->where('name', 'Tipo de motor')->first() and $publication->parameters()->where('name', 'Tipo de motor')->first()->value == '4 tiempos') selected @endif @endif>4 tiempos</option>
                       </select>
                    </div>                          
                </div>                                              
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Único dueño</label>
                   <div class="col-sm-2">
                       <select name="dueño" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('dueño') and old('dueño') == 'Sí') selected @else  @if(!old('dueño') and $publication->vehicle_owner and $publication->vehicle_owner == 'Sí') selected @endif @endif>Sí</option>
                            <option @if(old('dueño') and old('dueño') == 'No') selected @else  @if(!old('dueño') and $publication->vehicle_owner and $publication->vehicle_owner == 'No') selected @endif @endif>No</option>
                       </select>
                    </div>                          
                </div>                                    
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Horario de contacto</label>
                   <div class="col-sm-2">
                       <input type="text" name="horario" class="form-control" @if(old('horario'))value="{{old('horario')}}"@else @if($publication->vehicle_schedule)value="{{$publication->vehicle_schedule}}"@endif @endif>
                    </div>                          
                </div>  
               <br><br> 
               @endif
               
               
                @if($tipo_producto === 'vehículo' || $tipo_producto === 'vehículo de colección' || $tipo_producto === 'vehiculo')
                @if( $tipo_producto !== 'vehiculo')
                <div class="form-group {{ $errors->has('puertas') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Cant. de puertas *</label>
                   <div class="col-sm-2">
                       <select name="puertas" class="form-control">
                           <option value="">Elegir</option>
                           @for($i=2; $i<=5; $i++)
                           <option @if(old('puertas') and old('puertas') == $i) selected @else  @if(!old('puertas') and $publication->vehicle_doors and $publication->vehicle_doors == $i) selected @endif @endif>{{ $i }}</option>
                           @endfor
                       </select>
                    </div>                          
                </div>                             
                <div class="form-group {{ $errors->has('kilometros') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Kilómetros *</label>
                   <div class="col-sm-2">
                       <input type="text" name="kilometros" class="form-control" @if(old('kilometros'))value="{{old('kilometros')}}"@else @if($publication->vehicle_kilometers)value="{{$publication->vehicle_kilometers}}"@endif @endif>
                    </div>                          
                </div>
                @if($tipo_producto === 'vehículo')
                <div class="form-group {{ $errors->has('version') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Versión *</label>
                   <div class="col-sm-2">
                       <input type="text" name="version" class="form-control"  @if(old('version'))value="{{old('version')}}"@else @if($publication->vehicle_version)value="{{$publication->vehicle_version}}"@endif @endif>
                    </div>                          
                </div>
               @endif 
                @if($tipo_producto === 'vehículo de colección')
                <div class="form-group {{ $errors->has('marca') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Marca *</label>
                   <div class="col-sm-2">
                       <select name="marca" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('marca') and old('marca') == 'BMW') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'BMW') selected @endif @endif>BMW</option>
                            <option @if(old('marca') and old('marca') == 'Cadillac') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Cadillac') selected @endif @endif>Cadillac</option>
                            <option @if(old('marca') and old('marca') == 'Chevrolet') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Chevrolet') selected @endif @endif>Chevrolet</option>
                            <option @if(old('marca') and old('marca') == 'Citroën') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Citroën') selected @endif @endif>Citroën</option>
                            <option @if(old('marca') and old('marca') == 'Daihatsu') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Daihatsu') selected @endif @endif>Daihatsu</option>
                            <option @if(old('marca') and old('marca') == 'Dodge') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Dodge') selected @endif @endif>Dodge</option>
                            <option @if(old('marca') and old('marca') == 'Fiat') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Fiat') selected @endif @endif>Fiat</option>
                            <option @if(old('marca') and old('marca') == 'Ford') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Ford') selected @endif @endif>Ford</option>
                            <option @if(old('marca') and old('marca') == 'Ika') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Ika') selected @endif @endif>Ika</option>
                            <option @if(old('marca') and old('marca') == 'Jeep') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Jeep') selected @endif @endif>Jeep</option>
                            <option @if(old('marca') and old('marca') == 'Lotus') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Lotus') selected @endif @endif>Lotus</option>
                            <option @if(old('marca') and old('marca') == 'Mercedes Benz') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Mercedes Benz') selected @endif @endif>Mercedes Benz</option>
                            <option @if(old('marca') and old('marca') == 'Mini Cooper') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Mini Cooper') selected @endif @endif>Mini Cooper</option>
                            <option @if(old('marca') and old('marca') == 'Mitsubishi') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Mitsubishi') selected @endif @endif>Mitsubishi</option>
                            <option @if(old('marca') and old('marca') == 'Morris') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Morris') selected @endif @endif>Morris</option>
                            <option @if(old('marca') and old('marca') == 'Opel') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Opel') selected @endif @endif>Opel</option>
                            <option @if(old('marca') and old('marca') == 'Peugeot') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Peugeot') selected @endif @endif>Peugeot</option>
                            <option @if(old('marca') and old('marca') == 'Porsche') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Porsche') selected @endif @endif>Porsche</option>
                            <option @if(old('marca') and old('marca') == 'Renault') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Renault') selected @endif @endif>Renault</option>
                            <option @if(old('marca') and old('marca') == 'Toyota') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Toyota') selected @endif @endif>Toyota</option>
                            <option @if(old('marca') and old('marca') == 'Volkswagen') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Volkswagen') selected @endif @endif>Volkswagen</option>
                            <option @if(old('marca') and old('marca') == 'Volvo') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Volvo') selected @endif @endif>Volvo</option>
                            <option @if(old('marca') and old('marca') == 'Otra Marca') selected @else  @if(!old('marca') and $publication->vehicle_brand and $publication->vehicle_brand == 'Otra Marca') selected @endif @endif>Otra Marca</option>
                       </select>
                    </div>                          
                </div>
                <div class="form-group {{ $errors->has('modelo') ? ' has-error' : '' }}">    
                    <label  class="col-sm-2 control-label">Modelo *</label>
                   <div class="col-sm-2">
                       <input type="text" name="modelo" maxlength="30" class="form-control"  @if(old('modelo'))value="{{old('modelo')}}"@else @if($publication->vehicle_model)value="{{$publication->vehicle_model}}"@endif @endif>
                    </div>                          
                </div>
               @endif  
               <br>
               <br>
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Color</label>
                   <div class="col-sm-2">
                       <select name="color" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('color') and old('color') == 'Amarillo') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Amarillo') selected @endif @endif>Amarillo</option>
                            <option @if(old('color') and old('color') == 'Anaranjado') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Anaranjado') selected @endif @endif>Anaranjado</option>
                            <option @if(old('color') and old('color') == 'Azul') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Azul') selected @endif @endif>Azul</option>
                            <option @if(old('color') and old('color') == 'Beige') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Beige') selected @endif @endif>Beige</option>
                            <option @if(old('color') and old('color') == 'Blanco') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Blanco') selected @endif @endif>Blanco</option>
                            <option @if(old('color') and old('color') == 'Crema') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Crema') selected @endif @endif>Crema</option>
                            <option @if(old('color') and old('color') == 'Dorado') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Dorado') selected @endif @endif>Dorado</option>
                            <option @if(old('color') and old('color') == 'Gris') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Gris') selected @endif @endif>Gris</option>
                            <option @if(old('color') and old('color') == 'Marrón') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Marrón') selected @endif @endif>Marrón</option>
                            <option @if(old('color') and old('color') == 'Morado') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Morado') selected @endif @endif>Morado</option>
                            <option @if(old('color') and old('color') == 'Negro') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Negro') selected @endif @endif>Negro</option>
                            <option @if(old('color') and old('color') == 'Plateado') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Plateado') selected @endif @endif>Plateado</option>
                            <option @if(old('color') and old('color') == 'Rojo') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Rojo') selected @endif @endif>Rojo</option>
                            <option @if(old('color') and old('color') == 'Verde') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Verde') selected @endif @endif>Verde</option>
                            <option @if(old('color') and old('color') == 'Vino tinto') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Vino tinto') selected @endif @endif>Vino tinto</option>
                            <option @if(old('color') and old('color') == 'Otro') selected @else  @if(!old('color') and $publication->vehicle_color and $publication->vehicle_color == 'Otro') selected @endif @endif>Otro</option>
                       </select>
                    </div>                          
                </div>                                      
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Combustible</label>
                   <div class="col-sm-2">
                       <select name="combustible" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('combustible') and old('combustible') == 'Diesel') selected @else  @if(!old('combustible') and $publication->vehicle_fuel and $publication->vehicle_fuel == 'Diesel') selected @endif @endif>Diesel</option>
                            <option @if(old('combustible') and old('combustible') == 'Gas') selected @else  @if(!old('combustible') and $publication->vehicle_fuel and $publication->vehicle_fuel == 'Gas') selected @endif @endif>Gas</option>
                            <option @if(old('combustible') and old('combustible') == 'Gasolina') selected @else  @if(!old('combustible') and $publication->vehicle_fuel and $publication->vehicle_fuel == 'Gasolina') selected @endif @endif>Gasolina</option>
                       </select>
                    </div>                          
                </div>                                       
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Dirección</label>
                   <div class="col-sm-2">
                       <select name="direccion" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('direccion') and old('direccion') == 'Mecánica') selected @else  @if(!old('direccion') and $publication->vehicle_steering and $publication->vehicle_steering == 'Mecánica') selected @endif @endif>Mecánica</option>
                            <option @if(old('direccion') and old('direccion') == 'Hidráulica') selected @else  @if(!old('direccion') and $publication->vehicle_steering and $publication->vehicle_steering == 'Hidráulica') selected @endif @endif>Hidráulica</option>
                            <option @if(old('direccion') and old('direccion') == 'Asistida') selected @else  @if(!old('direccion') and $publication->vehicle_steering and $publication->vehicle_steering == 'Asistida') selected @endif @endif>Asistida</option>
                       </select>
                    </div>                          
                </div>                                        
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Transmisión</label>
                   <div class="col-sm-2">
                       <select name="transmision" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('transmision') and old('transmision') == 'Automática') selected @else  @if(!old('transmision') and $publication->vehicle_transmission and $publication->vehicle_transmission == 'Automática') selected @endif @endif>Automática</option>
                            <option @if(old('transmision') and old('transmision') == 'Mecánica') selected @else  @if(!old('transmision') and $publication->vehicle_transmission and $publication->vehicle_transmission == 'Mecánica') selected @endif @endif>Mecánica</option>
                       </select>
                    </div>                          
                </div>                                         
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Único dueño</label>
                   <div class="col-sm-2">
                       <select name="dueño" class="form-control">
                           <option value="">Elegir</option>
                           <option @if(old('dueño') and old('dueño') == 'Sí') selected @else  @if(!old('dueño') and $publication->vehicle_owner and $publication->vehicle_owner == 'Sí') selected @endif @endif>Sí</option>
                            <option @if(old('dueño') and old('dueño') == 'No') selected @else  @if(!old('dueño') and $publication->vehicle_owner and $publication->vehicle_owner == 'No') selected @endif @endif>No</option>
                       </select>
                    </div>                          
                </div> 
               @endif
               
                <div class="form-group">    
                    <label  class="col-sm-2 control-label">Horario de contacto</label>
                   <div class="col-sm-2">
                       <input type="text" name="horario" class="form-control" @if(old('horario'))value="{{old('horario')}}"@else @if($publication->vehicle_schedule)value="{{$publication->vehicle_schedule}}"@endif @endif>
                    </div>                          
                </div>  
               <h3>Seguridad</h3>
               <div class="row">
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Seguridad[]" value="3° Luz de stop" @if(old('Seguridad', $seguridad) and in_array('3° Luz de stop',old('Seguridad', $seguridad))) checked @endif> 3° Luz de stop</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Seguridad[]" value="ABS" @if(old('Seguridad', $seguridad) and in_array('ABS',old('Seguridad', $seguridad))) checked @endif> ABS</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Seguridad[]" value="Airbag conductor" @if(old('Seguridad', $seguridad) and in_array('Airbag conductor',old('Seguridad', $seguridad))) checked @endif> Airbag conductor</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Seguridad[]" value="Airbag de cortina" @if(old('Seguridad', $seguridad) and in_array('Airbag de cortina',old('Seguridad', $seguridad))) checked @endif> Airbag de cortina</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Seguridad[]" value="Airbag laterales" @if(old('Seguridad', $seguridad) and in_array('Airbag laterales',old('Seguridad', $seguridad))) checked @endif> Airbag laterales</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Seguridad[]" value="Airbag pasajero" @if(old('Seguridad', $seguridad) and in_array('Airbag pasajero',old('Seguridad', $seguridad))) checked @endif> Airbag pasajero</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Seguridad[]" value="Alarma" @if(old('Seguridad', $seguridad) and in_array('Alarma',old('Seguridad', $seguridad))) checked @endif> Alarma</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Seguridad[]" value="Apoya cabeza traseros" @if(old('Seguridad', $seguridad) and in_array('Apoya cabeza traseros',old('Seguridad', $seguridad))) checked @endif> Apoya cabeza traseros</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Seguridad[]" value="Blindado" @if(old('Seguridad', $seguridad) and in_array('Blindado',old('Seguridad', $seguridad))) checked @endif> Blindado</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Seguridad[]" value="Control de estabilidad" @if(old('Seguridad', $seguridad) and in_array('Control de estabilidad',old('Seguridad', $seguridad))) checked @endif> Control de estabilidad</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Seguridad[]" value="Control de tracción" @if(old('Seguridad', $seguridad) and in_array('Control de tracción',old('Seguridad', $seguridad))) checked @endif> Control de tracción</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Seguridad[]" value="Doble tracción" @if(old('Seguridad', $seguridad) and in_array('Doble tracción',old('Seguridad', $seguridad))) checked @endif> Doble tracción</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Seguridad[]" value="Faros antiniebla delanteros" @if(old('Seguridad', $seguridad) and in_array('Faros antiniebla delanteros',old('Seguridad', $seguridad))) checked @endif> Faros antiniebla delanteros</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Seguridad[]" value="Faros antinieblas traseros" @if(old('Seguridad', $seguridad) and in_array('Faros antinieblas traseros',old('Seguridad', $seguridad))) checked @endif> Faros antinieblas traseros</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Seguridad[]" value="Faros de xenón" @if(old('Seguridad', $seguridad) and in_array('Faros de xenón',old('Seguridad', $seguridad))) checked @endif> Faros de xenón</label></div>                
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Seguridad[]" value="Inmovilizador de motor" @if(old('Seguridad', $seguridad) and in_array('Inmovilizador de motor',old('Seguridad', $seguridad))) checked @endif> Inmovilizador de motor</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Seguridad[]" value="Isofix" @if(old('Seguridad', $seguridad) and in_array('Isofix',old('Seguridad', $seguridad))) checked @endif> Isofix</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Seguridad[]" value="Repartidor electrónico de fuerza de frenado" @if(old('Seguridad', $seguridad) and in_array('Repartidor electrónico de fuerza de frenado',old('Seguridad', $seguridad))) checked @endif> Repartidor electrónico de fuerza de frenado</label></div>
               </div>
               <br>
               <h3>Confort</h3>
               <div class="row">
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="Aire acondicionado" @if(old('Confort', $confort) and in_array('Aire acondicionado',old('Confort', $confort))) checked @endif> Aire acondicionado</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="Alarma de luces encendidas" @if(old('Confort', $confort) and in_array('Alarma de luces encendidas',old('Confort', $confort))) checked @endif> Alarma de luces encendidas</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="Alza lunas" @if(old('Confort', $confort) and in_array('Alza lunas',old('Confort', $confort))) checked @endif> Alza lunas</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="Apertura remota de baúl" @if(old('Confort', $confort) and in_array('Apertura remota de baúl',old('Confort', $confort))) checked @endif> Apertura remota de baúl</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="Asiento conductor regulable en altura" @if(old('Confort', $confort) and in_array('Asiento conductor regulable en altura',old('Confort', $confort))) checked @endif> Asiento conductor regulable en altura</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="Asiento trasero rebatible" @if(old('Confort', $confort) and in_array('Asiento trasero rebatible',old('Confort', $confort))) checked @endif> Asiento trasero rebatible</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="Asientos eléctricos" @if(old('Confort', $confort) and in_array('Asientos eléctricos',old('Confort', $confort))) checked @endif> Asientos eléctricos</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="Cierre centralizado de puertas" @if(old('Confort', $confort) and in_array('Cierre centralizado de puertas',old('Confort', $confort))) checked @endif> Cierre centralizado de puertas</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="Climatizador automático" @if(old('Confort', $confort) and in_array('Climatizador automático',old('Confort', $confort))) checked @endif> Climatizador automático</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="Computadora de abordo" @if(old('Confort', $confort) and in_array('Computadora de abordo',old('Confort', $confort))) checked @endif> Computadora de abordo</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="Control de velocidad de crucero" @if(old('Confort', $confort) and in_array('Control de velocidad de crucero',old('Confort', $confort))) checked @endif> Control de velocidad de crucero</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="Espejos eléctricos" @if(old('Confort', $confort) and in_array('Espejos eléctricos',old('Confort', $confort))) checked @endif> Espejos eléctricos</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="Faros regulables desde el interior" @if(old('Confort', $confort) and in_array('Faros regulables desde el interior',old('Confort', $confort))) checked @endif> Faros regulables desde el interior</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="GPS" @if(old('Confort', $confort) and in_array('GPS',old('Confort', $confort))) checked @endif> GPS</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="Limpiaparab. posterior" @if(old('Confort', $confort) and in_array('Limpiaparab. posterior',old('Confort', $confort))) checked @endif> Limpiaparab. posterior</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="Sensor de estacionamiento" @if(old('Confort', $confort) and in_array('Sensor de estacionamiento',old('Confort', $confort))) checked @endif> Sensor de estacionamiento</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="Sensor de lluvia" @if(old('Confort', $confort) and in_array('Sensor de lluvia',old('Confort', $confort))) checked @endif> Sensor de lluvia</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="Sensor de luz" @if(old('Confort', $confort) and in_array('Sensor de luz',old('Confort', $confort))) checked @endif> Sensor de luz</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="Sunroof" @if(old('Confort', $confort) and in_array('Sunroof',old('Confort', $confort))) checked @endif> Sunroof</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Confort[]" value="Tapizado de cuero" @if(old('Confort', $confort) and in_array('Tapizado de cuero',old('Confort', $confort))) checked @endif> Tapizado de cuero</label></div>
               </div>
               <br>
               <h3>Sonido</h3>
               <div class="row">
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Sonido[]" value="AM/FM" @if(old('Sonido', $sonido) and in_array('AM/FM',old('Sonido', $sonido))) checked @endif> AM/FM</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Sonido[]" value="Bluetooth" @if(old('Sonido', $sonido) and in_array('Bluetooth',old('Sonido', $sonido))) checked @endif> Bluetooth</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Sonido[]" value="Cargador de CD" @if(old('Sonido', $sonido) and in_array('Cargador de CD',old('Sonido', $sonido))) checked @endif> Cargador de CD</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Sonido[]" value="CD" @if(old('Sonido', $sonido) and in_array('CD',old('Sonido', $sonido))) checked @endif> CD</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Sonido[]" value="Comando satelital de stereo" @if(old('Sonido', $sonido) and in_array('Comando satelital de stereo',old('Sonido', $sonido))) checked @endif> Comando satelital de stereo</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Sonido[]" value="DVD" @if(old('Sonido', $sonido) and in_array('DVD',old('Sonido', $sonido))) checked @endif> DVD</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Sonido[]" value="Entrada auxiliar" @if(old('Sonido', $sonido) and in_array('Entrada auxiliar',old('Sonido', $sonido))) checked @endif> Entrada auxiliar</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Sonido[]" value="Entrada USB" @if(old('Sonido', $sonido) and in_array('Entrada USB',old('Sonido', $sonido))) checked @endif> Entrada USB</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Sonido[]" value="MP3" @if(old('Sonido', $sonido) and in_array('MP3',old('Sonido', $sonido))) checked @endif> MP3</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Sonido[]" value="Pasacassette" @if(old('Sonido', $sonido) and in_array('Pasacassette',old('Sonido', $sonido))) checked @endif> Pasacassette</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Sonido[]" value="Tarjeta SD" @if(old('Sonido', $sonido) and in_array('Tarjeta SD',old('Sonido', $sonido))) checked @endif> Tarjeta SD</label></div>
               </div>
               <br>
               <h3>Exterior</h3>
               <div class="row">
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Exterior[]" value="Limpia/lava luneta" @if(old('Exterior', $exterior) and in_array('Limpia/lava luneta',old('Exterior', $exterior))) checked @endif> Limpia/lava luneta</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Exterior[]" value="Llantas de aleación" @if(old('Exterior', $exterior) and in_array('Llantas de aleación',old('Exterior', $exterior))) checked @endif> Llantas de aleación</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Exterior[]" value="Paragolpes pintados" @if(old('Exterior', $exterior) and in_array('Paragolpes pintados',old('Exterior', $exterior))) checked @endif> Paragolpes pintados</label></div>
                <div class="col-sm-3"><label class="parameters"><input type="checkbox" name="Exterior[]" value="Vidrios polarizados" @if(old('Exterior', $exterior) and in_array('Vidrios polarizados',old('Exterior', $exterior))) checked @endif> Vidrios polarizados</label></div>
               </div>
               <br><br>
                 <div class="form-group">
                   <label for="title" class="col-sm-2 control-label">Título: *</label>
                   <div class="col-sm-6">
                     <input type="text" class="form-control" maxlength="60" name="title" id="title"  required placeholder='Ej.: Volkswagen Golf Motor 2.0 2006 Azul 5 puertas' @if(old('title'))value="{{old('title')}}"@else @if($publication->title)value="{{$publication->title}}"@endif @endif @if($cannot_modify_title) readonly @endif>
                     @if ($errors->has('title'))
                         <span class="text-danger">
                             {{ $errors->first('title') }}
                         </span>
                     @endif
                   </div>
                 </div> 
               @endif
               
               @if($tipo_producto === 'producto') 
                 <div class="form-group">
                   <label for="title" class="col-sm-2 control-label">Título: *</label>
                   <div class="col-sm-6">
                     <input type="text" class="form-control" maxlength="60" name="title" id="title" value="{{ $publication->title }}" required placeholder='Ej: Laptop HP Pavilion 5200 14", 16GB Ram, 1TB HD' @if(old('title'))value="{{old('title')}}"@else @if($publication->title)value="{{$publication->title}}"@endif @endif @if($cannot_modify_title) readonly @endif>
                     @if ($errors->has('title'))
                         <span class="text-danger">
                             {{ $errors->first('title') }}
                         </span>
                     @endif
                   </div>
                 </div> 
                 <div class="row">
                   <div class="col-sm-2"></div>
                   <div class="col-sm-10">
                     <div class="alert alert-warning" role="alert"><strong>No incluyas datos de contacto en tu publicación</strong>. Si lo haces tu publicación será eliminada y podrás ser suspendido. El comprador recibirá tus datos luego de comprar.</div>
                   </div>
                 </div>
               @endif
               
               @if($tipo_producto === 'moto') 
                 <div class="form-group">
                   <label for="title" class="col-sm-2 control-label">Título: *</label>
                   <div class="col-sm-6">
                     <input type="text" class="form-control" maxlength="60" name="title" id="title" value="{{ $publication->title }}" required placeholder='Ej: Yamaha Bws 100cc Negra' @if(old('title'))value="{{old('title')}}"@else @if($publication->title)value="{{$publication->title}}"@endif @endif @if($cannot_modify_title) readonly @endif>
                     @if ($errors->has('title'))
                         <span class="text-danger">
                             {{ $errors->first('title') }}
                         </span>
                     @endif
                   </div>
                 </div> 
               @endif
               
               @if($tipo_producto === 'lancha') 
                 <div class="form-group">
                   <label for="title" class="col-sm-2 control-label">Título: *</label>
                   <div class="col-sm-6">
                     <input type="text" class="form-control" maxlength="60" name="title" id="title" value="{{ $publication->title }}" required placeholder='Ej: Sport con camarote 2016' @if(old('title'))value="{{old('title')}}"@else @if($publication->title)value="{{$publication->title}}"@endif @endif @if($cannot_modify_title) readonly @endif>
                     @if ($errors->has('title'))
                         <span class="text-danger">
                             {{ $errors->first('title') }}
                         </span>
                     @endif
                   </div>
                 </div> 
               @endif
               
               @if($tipo_producto === 'casa' || $tipo_producto === 'departamento' || $tipo_producto === 'local comercial' || $tipo_producto === 'oficina' || $tipo_producto === 'terreno' || $tipo_producto === 'inmueble') 
                <br>
                <br>
                 <div class="form-group">
                   <label for="title" class="col-sm-2 control-label">Título: *</label>
                   <div class="col-sm-6">
                     <input type="text" class="form-control" maxlength="60" name="title" id="title" value="{{ $publication->title }}" required placeholder='Ej: Casa de dos pisos, 5 ambientes, 3 baños y 2 estacionamientos' @if(old('title'))value="{{old('title')}}"@else @if($publication->title)value="{{$publication->title}}"@endif @endif @if($cannot_modify_title) readonly @endif>
                     @if ($errors->has('title'))
                         <span class="text-danger">
                             {{ $errors->first('title') }}
                         </span>
                     @endif
                   </div>
                 </div> 
               @endif
               
               @if($tipo_producto === 'servicio') 
                 <div class="form-group">
                   <label for="title" class="col-sm-2 control-label">Título: *</label>
                   <div class="col-sm-6">
                     <input type="text" class="form-control" maxlength="60" name="title" id="title" value="{{ $publication->title }}" required placeholder='Ej: Servicio de manicure y pedicure a domicilio' @if(old('title'))value="{{old('title')}}"@else @if($publication->title)value="{{$publication->title}}"@endif @endif @if($cannot_modify_title) readonly @endif>
                     @if ($errors->has('title'))
                         <span class="text-danger">
                             {{ $errors->first('title') }}
                         </span>
                     @endif
                   </div>
                 </div> 
               @endif
               
                 <div class="form-group">
                   <label for="description" class="col-sm-2 control-label">Descripción:</label>
                   <div class="col-sm-10">
                     <textarea name="description_html" id="description_html" class="form-control">@if(old('description_html')){{old('description_html')}}@else @if($publication->description_html){{$publication->description_html}}@endif @endif</textarea>
                   </div>
                 </div>
               
                 @if($tipo_producto === 'producto')  
                 <br><br>
                 <div class="row">
                   <div class="col-md-12"><h3 style="text-transform: none;"><span>Indica la condición de tu producto</span></h3></div>
                 </div>
                 <div class="form-group">
                   <label for="description" class="col-sm-2 control-label"></label>
                   <div class="col-sm-10">
                     <div class="radio">
                      <label>
                        <input type="radio" name="condition" id="condition-nuevo" value="Nuevo" @if($publication->condition === 'Nuevo') checked @endif>
                        Nuevo
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="condition" id="condition-usado" value="Usado" @if($publication->condition === 'Usado') checked @endif>
                        Usado
                      </label>
                    </div>
                    @if ($errors->has('condition'))
                        <span class="text-danger">
                            {{ $errors->first('condition') }}
                        </span>
                    @endif
                   </div>
                 </div>
                 @else
                 <input type="hidden" name="condition" value="Usado">
                 @endif
                 @if($tipo_producto === 'producto')
               <div class="row">
                 <div class="col-md-12">
                   <h3 style="text-transform: none;"><span>Ingresa la cantidad disponible de tu producto y el precio de venta</span></h3>
                 </div>
               </div>
               @else
               <div class="row">
                 <div class="col-md-12">
                   <h3 style="text-transform: none;"><span>Ingresa el precio de venta de tu {{ $tipo_producto }}</span></h3>
                 </div>
               </div>               
               @endif
               <form method="post" action="{{ route('sell-save-price') }}" class="form-horizontal">
                 {{ csrf_field() }}
                 
                 @if($tipo_producto === 'producto')
               <div class="form-group">
                   <label for="title" class="col-sm-3 control-label">Cantidad: *</label>
                   <div class="col-sm-6 input-group">
                    <input type="text" class="form-control" maxlength="4" style="width: 70px" name="cantidad" id="cantidad" value="{{ $publication->available_quantity }}"  >
                     @if ($errors->has('cantidad'))
                        <span class="text-danger" style="line-height: 30px; margin-left: 10px">
                             {{ $errors->first('cantidad') }}
                         </span>
                     @endif
                   </div>
                 </div>
                 @else
                 <input type="hidden" name="cantidad" value="1">
                 @endif
               <div class="form-group">
                   
                   <label for="precio" class="col-sm-3 control-label">@if($tipo_producto === 'servicio')<input type="radio" name="convenir" id="convenir1" value="0" style="margin-right: 5px" @if($publication->price != '0') checked @endif>@endif
                    Precio: *</label>
                   <div class="col-sm-6 input-group">
                    <div class="input-group-addon">S/.</div>
                     <input type="text" class="form-control" maxlength="12" style="width: 130px" name="precio" id="precio" value="{{ number_format($publication->price,2,',','.') }}">
                     @if ($errors->has('precio'))
                            <span class="text-danger" style="line-height: 30px; margin-left: 10px">
                             {{ $errors->first('precio') }}
                         </span>
                     @endif
                   </div>
                   @if($tipo_producto === 'servicio')
                    <label for="convenir2" class="col-sm-3 control-label" style="line-height: 30px;margin-left: 12px;"><input type="radio" name="convenir" id="convenir2" value="1" style="margin-right: 5px" @if($publication->price == '0') checked @endif>A convenir</label>
                    @endif
                     
                 </div>
               <br><br>
                 <a class="btn btn-link" href="{{ route('sell-category', [$publication->topLevelCategory()->id]) }}">Volver</a> <button class="btn btn-primary">Modificar</button>
                 <br><br>
               </form>
             </div>
           </div>
            <div class="col-xs-12">Tu publicación debe cumplir con las Políticas de {{ config('app.name') }}</div>
         </div>
       </div>
     </section>

@endsection

@section('scripts')
	@parent
  <script src="/js/tinymce/tinymce.min.js"></script>
  <script src="/js/FileAPI.min.js"></script>
  <script src="/js/FileAPI.exif.js"></script>
  <script src="/js/jquery.fileapi.js"></script>
  <script src="/js/jquery.number.min.js"></script>
  
  <script>
    $(document).ready(function() {
        
      $('#precio').number(true, 2, ',', '.' );
      
      tinymce.init({
        selector: 'textarea',
        menubar: false,
        language: 'es',
        height: 400,
        plugins: ['advlist autolink lists link image charmap print preview hr anchor pagebreak',
          'searchreplace wordcount visualblocks visualchars code fullscreen',
          'insertdatetime media nonbreaking save table contextmenu directionality',
          'emoticons template paste textcolor colorpicker textpattern'
        ],
        toolbar1: "fontselect |  fontsizeselect | backcolor forecolor | bold italic underline | alignleft aligncenter alignright alignjustify | numlist bullist | outdent indent | link unlink",
        toolbar2: " newdocument preview | undo redo | code | image hr",
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        image_list: "{{ route('sell-list-images', ['id' => $publication->id]) }}"
      });
                
                
      $('#multiupload').fileapi({
        url: '{{ route('sell-save-image') }}',
        onFilePrepare: function (evt, uiEvt){
          var file = uiEvt.file;
          uiEvt.options.data.uid = FileAPI.uid(file);
        },
        onFileComplete: function (evt, uiEvt){
          uiEvt.file.$el[0].setAttribute('data-image-id', uiEvt.file.$el.attr('data-fileapi-id'));
          refillThumbsBg(8);
          reloadPicturesInfo();
        },
        data: { _token: '{{ csrf_token() }}', id: '{{ $publication->id }}' },
        multiple: true,
        
        @if(count($images))
        files: [
        @foreach($images as $img)
        {
            src: "{{ url('uploads/images/products')."/".$img->name }}"
        },
        @endforeach
        ],
        @endif
        
        autoUpload: true,
        accept: 'image/*',
        maxSize: FileAPI.MB*4, // max file size
        duplicate: false,
        maxFiles:8,
        imageOriginal: true,
        imageSize: { minWidth: 320, minHeight: 240 /*, maxWidth: 2000, maxHeight: 2000*/ },
        imageTransform: {
          // resize by max side
          maxWidth: 800,
          maxHeight: 600
        },

        onFileRemoveCompleted: function (evt, file){
          evt.preventDefault();
	  $.post('{{ route('sell-remove-image') }}', {  _token: '{{ csrf_token() }}', uid: file.$el.attr('data-image-id'), id: '{{ $publication->id }}'}, function() {
//            alert( "success" );
          })
          .done(function() {
//            alert( "second success" );
          })
          .fail(function() {
            alert( "Error al elimianr la imagen." );
          })
          .always(function() {
            evt.widget.remove(file);
            refillThumbsBg(8);
            reloadPicturesInfo();
          });
        },
        
        elements: {
          ctrl: { upload: '.js-upload' },
          empty: { show: '.b-upload__hint' },
          emptyQueue: { hide: '.js-upload' },
          list: '.js-files',
          file: {
            tpl: '.js-file-tpl',
            preview: {
              el: '.b-thumb__preview',
              width: 80,
              height: 80
            },
            upload: { show: '.progress', hide: '.b-thumb__rotate' },
            complete: { hide: '.progress', show: '.b-thumb__del'},
            progress: '.progress .bar'
          }
        }
      });
      
      $( "#sortable" ).sortable({
        placeholder: "thumb-placeholder",
        update: function( event, ui ) { reloadPicturesInfo();}
      });

      addPicturesIds();
      reloadPicturesInfo();
      refillThumbsBg(8);
      
      
           
            $("#department").change(function (){
                $("#province").empty();
                $("#province").append($("<option>", {
                    value: '',
                    text: 'Seleccione'
                }));
                $("#district").empty();
                $("#district").append($("<option>", {
                    value: '',
                    text: 'Seleccione'
                }));
                      
                if ($("#department").val() == '') {
                    return;
                }
                    
                $.ajax({
                    url: "{{ url('/searchprovinces') }}",
                    type: "POST",
                    data: {
                        id: $("#department").val(),
                        _token: "{{ csrf_token() }}", 
                    },
                    success: function(res) {
                 
                        for (var i = 0; i < res.length; ++i) {
                            $("#province").append($("<option>", {
                                value: res[i].id,
                                text: res[i].name
                            }));
                        }
                    }
                });
            });
            
            $("#province").change(function (){
                $("#district").empty();
                $("#district").append($("<option>", {
                    value: '',
                    text: 'Seleccione'
                }));
                      
                if ($("#province").val() == '') {
                    return;
                }
                    
                $.ajax({
                    url: "{{ url('/searchdistricts') }}",
                    type: "POST",
                    data: {
                        id: $("#province").val(),
                        _token: "{{ csrf_token() }}", 
                    },
                    success: function(res) {
                            
                        for (var i = 0; i < res.length; ++i) {
                            $("#district").append($("<option>", {
                                value: res[i].id,
                                text: res[i].name
                            }));
                        }
                    }
                });
            });
      
      
    });
    
    
    function refillThumbsBg(max) {
      $(".thumb-bg").remove();
      max = max || 8;
      elems = $(".js-file-tpl").length;
      for(var i = elems; i < max; i++) {
        $(".js-files").append('<div class="thumb-bg"><div class="b-thumb__preview"><div class="b-thumb__preview__pic"></div></div></div>');
      }
      $(".thumb-bg").click(function() {
        $("#file").click();
      });
    }

    function addPicturesIds() {
      var picturesIds = [
      @foreach($images as $img)
       "{{ $img->fileapi_id }}",
      @endforeach
      ];
      $(".b-upload__files .js-file-tpl").each(function(index) {
        $(this).attr('data-image-id', picturesIds[index]);
      });
    }
    
    function reloadPicturesInfo() {
      var valuePictureInfo = [];
      $(".b-upload__files .js-file-tpl").each(function(index) {
        var fileapiId = $(this).attr('data-fileapi-id');
        var imageId = $(this).attr('data-image-id');
        var element = {position: index, fileapiid: fileapiId, imageid: imageId };
        valuePictureInfo.push(element);
      });
    
      $("#pictures_info").val(JSON.stringify(valuePictureInfo));
    }
    
    // Returns text statistics for the specified editor by id
    function getStats(id) {
        var body = tinymce.get(id).getBody(), text = tinymce.trim(body.innerHTML);
        
        return {
            chars: text.length,
            words: text.split(/[\w\u2019\'-]+/).length
        };
    }
    function submitForm() {
       // Check if the user has entered less than 100 words
        if (getStats('description_html').chars > 65000) {
            alert("El contenido de la descripción es muy largo, intente reducirlo.");
            return false;
        }

        // Submit the form
        document.mainForm.submit();
    }
    </script>
@endsection

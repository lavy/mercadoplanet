@extends('layouts.myaccount')

@section('title', 'Reputación')

@section('myaccountcontent')

<div class="col-md-7">
<h3>Reputación</h3>
<div class="row">
    <?php $user = Auth::user(); ?>
    @if($user->seller_type != 'Normal')
    <?php $image_lider = ($user->seller_type=='Líder' ? 'lider.png': 'lider-gold.png'); ?>
    <div>
    
        <div class="col-md-12" style="text-align: center;margin-bottom: 10px">
            <img src="/img/assets/{{ $image_lider }}" style="position: relative;left: -40px;top: -3px;">
            <span style="text-align: center;margin-bottom: 15px;background-color: #f3f3f3;padding: 10px;border-radius: 5px; margin-left: -40px;">
                Este es un Vendedor <strong>@if($user->seller_type=='Líder Gold') Oro @else Plata @endif</strong>
            </span>
        </div>
    </div>
    @endif
    <div class="col-md-12" style="text-align: center">
        {!! compute_stars($reputation['value']) !!}
    </div>
    @if($reputation['value'] !== -1)
	<div class="row">
		<div class="col-md-12">
			<div class="arrow-box" style="top: 20px;padding: 10px 15px 10px 15px; margin-bottom: 40px;">
				<div class="row">
					<div class="col-md-4">
						<div style="font-size: 35px;font-weight: bold; text-align: center; margin-bottom: 15px">{{ $reputation['recommended'] }}%</div>
						<div style="font-size: 14px;text-align: center">de tus compradores te recomiendan</div>
					</div>
					<div class="col-md-4">
						<div style="font-size: 35px;font-weight: bold; text-align: center; margin-bottom: 15px">{{ $reputation['sales'] }}</div>
						<div style="font-size: 14px;text-align: center">ventas concretadas</div>
					</div>
					<div class="col-md-4">
						<div style="text-align: center; margin-bottom: 15px"><strong>{{ $reputation['time'] }}</strong><br>vendiendo en MercadoPlanet.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="text-align: center"><h4>Calificaciones</h4></div>
	</div>
	<div class="row" style="margin-bottom: 20px;">
		<div class="col-md-4 qualification-text">{{ $num_positives }} <i class="fa fa-plus-circle @if($num_positives) fa-plus-circle-active @endif"></i></div>
		<div class="col-md-4 qualification-text">{{ $num_neutral }} <i class="fa fa-dot-circle-o @if($num_neutral) fa-dot-circle-active @endif"></i></div>
		<div class="col-md-4 qualification-text">{{ $num_negatives }} <i class="fa fa-minus-circle @if($num_negatives) fa-minus-circle-active @endif"></i></div>
	</div>
    @else
	<div class="row">
		<div class="col-md-12" style="margin-top: 15px;text-align: center">        
			<strong>Aún no tienes suficientes ventas</strong> para calcular tu reputación.
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="margin-top: 15px;text-align: center">        
			<small>Te faltan {{ $reputation['missing_sales'] }} ventas para empezar a tener reputación en el sitio.</small>
		</div>
    </div>
    @endif
</div>
</div>
<div class="col-md-5">
	<div class="list-group">
		<a href="{{ route('reputationseller', ['user_id' => Auth::user()->id]) }}" class="list-group-item">Comentarios de tus compradores</a>
		<a href="{{ route('reputationbuyer', ['user_id' => Auth::user()->id]) }}" class="list-group-item">Tu reputación como comprador</a>
	</div>
</div>

@endsection

@section('scripts')
    @parent
    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
@endsection

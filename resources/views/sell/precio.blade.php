@extends('layouts.app')

@section('title', 'Inicio')

@section('content')

<section class="mainContent clearfix userProfile">
       <div class="container">
         <div class="row">
           <div class="col-xs-12">
             <div class="btn-group" role="group" id="pub-labels">
               <a href="javascript:void(0)" class="btn btn-default" disabled><i class="fa fa-gift" aria-hidden="true"></i>Elige que quieres publicar</a>
               <a href="javascript:void(0)" class="btn btn-default" disabled><i class="fa fa-list" aria-hidden="true"></i>Describe tu producto</a>
               <a href="javascript:void(0)" class="btn btn-default" active><i class="fa fa-money" aria-hidden="true"></i>Ingresa precio, cantidad y costo de envio</a>
               <a href="javascript:void(0)" class="btn btn-default" disabled><i class="fa fa-cubes" aria-hidden="true"></i>Elige tu plan y Confirma tu publicación</a>
             </div>
           </div>
         </div>
        <?php $tipo_producto = $publication->getProductType(); ?>
         <div class="row">
           <div class="col-xs-12">
             <div class="innerWrapper">
               @if($tipo_producto === 'producto')
               <div class="row">
                 <div class="col-md-12">
                   <h3 style="text-transform: none;"><span>Ingresa la cantidad disponible de tu producto y el precio de venta</span></h3>
                 </div>
               </div>
               @else
               <div class="row">
                 <div class="col-md-12">
                   <h3 style="text-transform: none;"><span>Ingresa el precio de venta de tu {{ $tipo_producto }}</span></h3>
                 </div>
               </div>               
               @endif
               <form method="post" action="{{ route('sell-save-price') }}" class="form-horizontal">
                 {{ csrf_field() }}
                 
                 @if($tipo_producto === 'producto')
               <div class="form-group">
                   <label for="title" class="col-sm-3 control-label">Cantidad: *</label>
                   <div class="col-sm-6 input-group">
                    <input type="text" class="form-control" maxlength="4" style="width: 70px" name="cantidad" id="cantidad" value="{{ $publication->quantity }}"  >
                     @if ($errors->has('cantidad'))
                        <span class="text-danger" style="line-height: 30px; margin-left: 10px">
                             {{ $errors->first('cantidad') }}
                         </span>
                     @endif
                   </div>
                 </div>
                 @else
                 <input type="hidden" name="cantidad" value="1">
                 @endif

                   <div class="form-group">
                       <label for="currency" class="col-sm-3 control-label">
                           Moneda: *</label>
                       <div class="col-sm-6 input-group">
                           <select name="currency" id="currency" class="form-control">
                               @foreach($currencies as $currency)
                                    <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                               @endforeach
                           </select>
                           @if ($errors->has('currency'))
                               <span class="text-danger" style="line-height: 30px; margin-left: 10px">
                             {{ $errors->first('currency') }}
                         </span>
                           @endif
                       </div>
                   </div>

               <div class="form-group">
                   
                   <label for="precio" class="col-sm-3 control-label">@if($tipo_producto === 'servicio')<input type="radio" name="convenir" id="convenir1" value="0" style="margin-right: 5px" @if($publication->price != '0') checked @endif>@endif
                    Precio: *</label>
                   <div class="col-sm-6 input-group">
                    <div class="input-group-addon">S/.</div>
                     {{--<input type="text" class="form-control" maxlength="12" style="width: 130px" name="precio" id="precio" value="{{ number_format($publication->price,2,',','.') }}">--}}
                     <input type="text" class="form-control" maxlength="12" style="width: 130px" name="precio" id="precio" value="{{ number_format($publication->price,2,',','.') }}">
                     @if ($errors->has('precio'))
                            <span class="text-danger" style="line-height: 30px; margin-left: 10px">
                             {{ $errors->first('precio') }}
                         </span>
                     @endif
                   </div>
                   @if($tipo_producto === 'servicio')
                    <label for="convenir2" class="col-sm-3 control-label" style="line-height: 30px;margin-left: 12px;"><input type="radio" name="convenir" id="convenir2" value="1" style="margin-right: 5px" @if($publication->price == '0') checked @endif>A convenir</label>
                    @endif
                     
                 </div>
                   <div class="form-group">

                       <label for="costo_envio" class="col-sm-3 control-label">
                           Costo Envio: *</label>
                       <div class="col-sm-6 input-group">
                           <input type="text" class="form-control" maxlength="12" style="width: 130px" name="costo_envio" id="costo_envio" value="{{ number_format($publication->price,2,',','.') }}">
                           @if ($errors->has('costo_envio'))
                               <span class="text-danger" style="line-height: 30px; margin-left: 10px">
                             {{ $errors->first('costo_envio') }}
                         </span>
                           @endif
                       </div>
                   </div>
               <br><br>
                 <a class="btn btn-link" href="{{ route('sell-description') }}">Volver</a> <button class="btn btn-primary">Continuar</button>
                 <br><br>
               </form>
             </div>
           </div>
            <div class="col-xs-12">Tu publicación debe cumplir con las Políticas de {{ config('app.name') }}</div>
         </div>
       </div>
     </section>

@endsection

@section('scripts')
	@parent

  <script src="/js/jquery.number.min.js"></script>
  <script>
      $('#precio').click(function () {
         $('#precio').val('');
      });

      $('#precio').blur(function () {
          $('#precio').number(true, 2, ',', '.' );
      });

      $('#costo_envio').click(function () {
          $('#costo_envio').val('');
      });

      $('#costo_envio').blur(function () {
          $('#costo_envio').number(true, 2, ',', '.' );
      });
  
  </script>
@endsection

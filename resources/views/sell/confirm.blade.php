@extends('layouts.app')

@section('title', 'Inicio')

@section('content')

<section class="mainContent clearfix userProfile">
       <div class="container">
         <div class="row">
           <div class="col-xs-12">
             <div class="btn-group" role="group" id="pub-labels">
               <a href="javascript:void(0)" class="btn btn-default" disabled><i class="fa fa-gift" aria-hidden="true"></i>Elige que quieres publicar</a>
               <a href="javascript:void(0)" class="btn btn-default" disabled><i class="fa fa-list" aria-hidden="true"></i>Describe tu producto</a>
               <a href="javascript:void(0)" class="btn btn-default" disabled><i class="fa fa-money" aria-hidden="true"></i>Ingresa precio, cantidad y costo de envio</a>
               <a href="javascript:void(0)" class="btn btn-default" active><i class="fa fa-cubes" aria-hidden="true"></i>Elige tu plan y Confirma tu publicación</a>
             </div>
           </div>
         </div>
        <?php $tipo_producto = $publication->getProductType(); ?>
         <div class="row">
           <div class="col-xs-12">
             <div class="innerWrapper">
               @if($tipo_producto === 'producto')
               <div class="row">
                 <div class="col-md-12">
                   <h3 style="text-transform: none;"><span>Selecciona el plan de publicación para tu producto</span></h3>
                 </div>
               </div>
               
               <div class="alert alert-info" role="alert">En cualquiera de los planes tu publicación tendrá una vigencia de {{ config('app.global.publication_lifetime_days') }} días</div>

                 <div class="row">
                     @if($conf->activate_plans)
                     <div class="col-md-3">
                         <a href="javascript: void(0)" class="target-anchor" data-plan="Premium" data-price="S/. 10,00">
                         <div class="plan-target">
                             <i class="fa fa-check" style="display: none"></i>
                             <h3>Premium</h3>
                             <div class="plan-price">
                                 <div class="plan-price-value">S/. 10,00</div>
                                 <div class="plan-price-spec">Costo por publicación</div>
                             </div>
                             <div class="plan-spec">
                                 <ul>
                                     <li>Recibe un <strong>100%</strong> más de visitas.</li>
                                 </ul>
                             </div>
                         </div>
                         </a>
                     </div>
                     <div class="col-md-3">
                         <a href="javascript: void(0)" class="target-anchor" data-plan="Oro" data-price="S/. 6,00">
                         <div class="plan-target">
                             <i class="fa fa-check" style="display: none"></i>
                             <h3>Oro</h3>
                             <div class="plan-price">
                                 <div class="plan-price-value">S/. 6,00</div>
                                 <div class="plan-price-spec">Costo por publicación</div>
                             </div>
                             <div class="plan-spec">
                                 <ul>
                                     <li>Recibe un <strong>70%</strong> más de visitas.</li>
                                 </ul>
                             </div>
                         </div>
                         </a>
                     </div>
                     <div class="col-md-3">
                         <a href="javascript: void(0)" class="target-anchor" data-plan="Plata" data-price="S/. 4,00">
                         <div class="plan-target">
                             <i class="fa fa-check" style="display: none"></i>
                             <h3>Plata</h3>
                             <div class="plan-price">
                                 <div class="plan-price-value">S/. 4,00</div>
                                 <div class="plan-price-spec">Costo por publicación</div>
                             </div>
                             <div class="plan-spec">
                                 <ul>
                                     <li>Recibe un <strong>50%</strong> más de visitas.</li>
                                 </ul>
                             </div>
                         </div>
                         </a>
                     </div>
                     <div class="col-md-3">
                         <a href="javascript: void(0)" class="target-anchor" data-plan="Bronce" data-price="S/. 2,00">
                         <div class="plan-target">
                             <i class="fa fa-check" style="display: none"></i>
                             <h3>Bronce</h3>
                             <div class="plan-price">
                                 <div class="plan-price-value">S/. 2,00</div>
                                 <div class="plan-price-spec">Costo por publicación</div>
                             </div>
                             <div class="plan-spec">
                                 <ul>
                                     <li>Recibe un <strong>20%</strong> más de visitas.</li>
                                 </ul>
                                
                             </div>
                         </div>
                         </a>
                     </div>
                     @endif
                     <div class="col-md-3">
                         <a href="javascript: void(0)" class="target-anchor" data-plan="Gratis" data-price="Gratis">
                         <div class="plan-target target-active">
                             <i class="fa fa-check"></i>
                             <h3>Gratis</h3>
                             <div class="plan-price">
                                 <div class="plan-price-value">S/. 0,00</div>
                                 <div class="plan-price-spec">Costo por publicación</div>
                             </div>
                             <div class="plan-spec">
                                 <ul>
                                    @if($conf->activate_plans)
                                     <li>Exposición al final de las listas.</li>
                                     @endif
                                 </ul>
                                
                             </div>
                         </div>
                         </a>
                     </div>
                 </div>
               @else
                 <div class="row">
                     <div class="col-md-3">
                         <a href="javascript: void(0)" class="target-anchor" data-plan="Clasificado" data-price="S/. 20">
                         <div class="plan-target target-active">
                             <i class="fa fa-check"></i>
                             <h3>Clasificado</h3>
                             <div class="plan-price">
                                 <div class="plan-price-value">S/. 20,00</div>
                                 <div class="plan-price-spec">Costo por publicación</div>
                             </div>
                             <div class="plan-spec">
                                 <ul>
                                     <li>Clasificados</li>
                                 </ul>
                                
                             </div>
                         </div>
                         </a>
                     </div>
                 </div>
               
               @endif
               
               <br><br>
               <a class="btn btn-link" href="{{ route('sell-price') }}">Volver</a> <a href="javascript:void(0)" data-toggle="modal" data-target="#mod-confirm-modal" class="btn btn-primary">Continuar</a>
                 <br><br>
             </div>
           </div>
            <div class="col-xs-12">Tu publicación debe cumplir con las Políticas de {{ config('app.name') }}</div>
         </div>
       </div>
     </section>


      <div class="modal fade" id="mod-confirm-modal" tabindex="-1" role="dialog" aria-labelledby="mod-confirm-modal-label">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <form id="msg-form" method="post" action="{{ url('/sell/save-confirm') }}" role="form">
                      {{ csrf_field() }}
                      <div class="modal-header mod-modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="mod-phone-modal-label">{{ Auth::user()->first_name }}, Confirma tu publicación</h4>
                      </div>
                      <div class="modal-body">
                          <div class="form-group sandbox">
                            <div class="row">
                              <div class="col-xs-12" id="message-modal"></div>
                            </div>
                            <div class="row">
                              <div class="col-xs-8">
                                @if($tipo_producto === 'producto')
                                <div>Cantidad: <label><span id="show-quantity">1</span></label></div>
                                @endif
                                <div>E-mail: <span style="color:#999;font-family: sans-serif;">{{ Auth::user()->email }}</span> <a href="{{ url('logout') }}" class="btn btn-link">Soy otro usuario</a></div>
                                @if($tipo_producto !== 'producto')
                                <div style="font-weight: bold;">Costo por publicación: <label><span style="font-weight: bold; font-size: 20px;color: #47bac1;" id="modal-price">S/. 20,00</span></label></div>
                                @endif
                                <br><br>
                                <div style="text-align:center" id="confirm-modal" style="font-size: 18px;"><button class="btn btn-primary" id="confirm-button">Confirmar Publicación</button></div>
                                @if($tipo_producto === 'producto')
                                <input type="hidden" name="plan" id="plan" value="Gratis">
                                @else
                                <input type="hidden" name="plan" id="plan" value="Fijo">
                                @endif
                              </div>
                              <div class="col-xs-4">
                                <div class="productImage clearfix">
                                  @if (count($publication->images) > 0)
                                  <img src="{{ url('/photo/120x120?url='.urlencode($publication->images[0]->name)) }}" alt="{{ $publication->title }}">
                                  @else
                                  <img src="{{ url('/photo/120x120?url=nophoto.jpg') }}">
                                  @endif
                                </div>
                                <div class="productCaption clearfix">
                                <h5>{{ $publication->title }}</h5>
                                <h3>
                                @if($tipo_producto === 'servicio' && $publication->price == '0')
                                A convenir
                                @else
                                S/ {{ number_format($publication->price,2,',','.') }}
                                @endif
                                </h3>
                                </div></div>
                            </div>
                          </div>
                      </div>
                      <div class="modal-footer">
                          <button id="cancel-msg" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                      </div>
                  </form>
              </div>
          </div>
      </div>

@endsection

@section('scripts')
	@parent

  <script src="/js/jquery.number.min.js"></script>
  <script>
    $(document).ready(function() {
        $('#precio').number(true, 2, ',', '.' );
        @if($tipo_producto === 'producto')     
        $(".target-anchor").click(function() {
            $('.plan-target').removeClass('target-active');
            $('.plan-target .fa-check').hide();
            $(this).find('.plan-target').addClass('target-active');
            $('.plan-target .fa-check').hide();
            $('.target-active .fa-check').show();
            $("#plan").val($(this).attr('data-plan'));
            $("#modal-price").text($(this).attr('data-price'));
        });
        @endif
    });  
    
  
  </script>
@endsection

@extends('layouts.app')

@section('title', 'Inicio')

@section('content')

<section class="mainContent clearfix userProfile">
       <div class="container">
         <div class="row">
           <div class="col-xs-12">
             <div class="btn-group" role="group" id="pub-labels">
               <a href="javascript:void(0)" class="btn btn-default active"><i class="fa fa-gift" aria-hidden="true"></i>Elige que quieres publicar</a>
               <a href="javascript:void(0)" class="btn btn-default" disabled><i class="fa fa-list" aria-hidden="true"></i>Describe tu producto</a>
               <a href="javascript:void(0)" class="btn btn-default" disabled><i class="fa fa-money" aria-hidden="true"></i>Ingresa precio, cantidad y costo de envio</a>
               <a href="javascript:void(0)" class="btn btn-default" disabled><i class="fa fa-cubes" aria-hidden="true"></i>Elige tu plan y Confirma tu publicación</a>
             </div>
           </div>
         </div>
         <div class="row">
           <div class="col-xs-12">
             <div class="innerWrapper">
               <ul class="list-inline" style="text-align:center">
                 <li><a href="{{ route('sell-category', [1]) }}" class="btn btn-default btn-lg"><img src="/img/assets/vehicles.png"><br>Vehículos</a></li>
                 <li><a href="{{ route('sell-category', [2]) }}" class="btn btn-default btn-lg"><img src="/img/assets/house.png"><br>Inmuebles</a></li>
                 <li><a href="{{ route('sell-category', [3]) }}" class="btn btn-default btn-lg"><img src="/img/assets/services.png"><br>Servicios</a></li>
                 <li><a href="{{ route('sell-category', [4]) }}" class="btn btn-default btn-lg"><img src="/img/assets/products.png"><br>Productos</a></li>
               </ul>
             </div>
           </div>
            <div class="col-xs-12">Tu publicación debe cumplir con las Políticas de {{ config('app.name') }}</div>
         </div>
       </div>
     </section>

@endsection

@section('scripts')
	@parent
	<script>
		$(document).ready(function() {
			$('.register-link').click(function() {
				location.href = '{{ url('/register') }}';
			});

            $(".btn-favorite").click(function() {
                var anchor = $(this);
                $.ajax({
                    method: "POST",
                    url: "{{url('/favorite')}}",
                    cache: false,
                    data: { id: anchor.attr('data-pub-id'), _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
                    if(msg.is_ok) {
                        if(msg.value) {
                            $(".pub-"+anchor.attr('data-pub-id')).addClass('btn-favorite-marked');
                        } else {
                            $(".pub-"+anchor.attr('data-pub-id')).removeClass('btn-favorite-marked');
                        }
                    }
                });
            });
		});
	</script>
@endsection

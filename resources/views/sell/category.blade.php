@extends('layouts.app')
@section('title', 'Inicio')
@section('content')

<section class="mainContent clearfix userProfile">
       <div class="container">
         <div class="row">
           <div class="col-xs-12">
             <div class="btn-group" role="group" id="pub-labels">
               <a href="javascript:void(0)" class="btn btn-default active"><i class="fa fa-gift" aria-hidden="true"></i>Elige que quieres publicar</a>
               <a href="javascript:void(0)" class="btn btn-default" disabled><i class="fa fa-list" aria-hidden="true"></i>Describe tu producto</a>
               <a href="javascript:void(0)" class="btn btn-default" disabled><i class="fa fa-money" aria-hidden="true"></i>Ingresa precio, cantidad y costo de envio</a>
               <a href="javascript:void(0)" class="btn btn-default" disabled><i class="fa fa-cubes" aria-hidden="true"></i>Elige tu plan y Confirma tu publicación</a>
             </div>
           </div>
         </div>
         <div class="row">
           <div class="col-xs-12">
             <div class="innerWrapper">
               <form method="post" action="{{ route('sell-save-category') }}">
                 {{ csrf_field() }}
               <div class="row">
                 <div class="col-md-12" style="text-align: center">
                   <h4>Selecciona la categoría</h4>
                 </div>
                 <div class="col-md-2">
                   <input type="hidden" name="complete" id="complete" value="0">
                 </div>
               </div>
                 <div class="row" id="select-category">
                   <div class="col-sm-3 category-column">
                     <div class="category-type">
                       <div class="category-name">
						 @if(count($category_path) and $category_path[0]->id === $category->id) 
                         <?php $show_his = true; $category = $category_path[0];array_shift($category_path);?>
                         @endif
                         @if($category->id === 1)<img src="/img/assets/vehicles.png"><br>{{ $category->name }} @endif
                         @if($category->id === 2)<img src="/img/assets/house.png"><br>{{ $category->name }} @endif
                         @if($category->id === 3)<img src="/img/assets/services.png"><br>{{ $category->name }} @endif
                         @if($category->id === 4)<img src="/img/assets/products.png"><br>{{ $category->name }} y Otros @endif
                       </div>
                     </div>
                   </div>
                    @if(count($category_path) and @$show_his)
                    @foreach($category_path as $index => $cp)
                   <div class="col-sm-3 category-column" id="category-column-{{ $index+1 }}">
                     <select size="18" class="category-list" name="category_id_{{ $index+1 }}">
                       @foreach ($cp->parentCategory->children as $sc)
                       <option value="{{ $sc->id }}" data-content="true" @if($sc->id === $cp->id) selected="true" @endif class="category-item" data-column="{{ $index+1 }}">{{ $sc->name }}</option>
                       @endforeach
                     </select>
                   </div>                    
                    @endforeach
                    
                    @foreach(end($category_path)->groupFeatures()->orderBy('id')->get() as $index => $gfts)
                   <div class="col-sm-3 category-column" id="category-column-{{ count($category_path)+$index+1 }}">
                     <select size="18" class="category-list" name="feature_id_{{ count($category_path)+$index+1 }}">
                       @foreach ($gfts->features as $key => $ft)
                       <option value="{{ $ft->id }}" data-content="true" @if($ft->id === $features[$index]->id) selected="true" @endif class="category-item" data-offset="{{ $index+1 }}" data-category-id="{{ $cp->id }}" data-column="{{ count($category_path)+$index+1 }}" data-offset="">{{ $ft->name }}</option>
                       @endforeach
                     </select>
                   </div>                                        
                    @endforeach
                    <div class="col-sm-3 category-column" style="height: 301px" id="category-column-6">
                        <div style="height: 301px; border: 1px solid #aaa; border-radius: 5px; text-align: center;">
                            <i class="fa fa-check-circle" style="font-size: 70px; color: #5cb85c; margin-top: 50px;"></i>
                            <div style="margin-top: 5px;font-weight: bold;">¡Completado!</div>
                            <div style="margin-top: 20px"><button class="btn btn-primary">Continuar</button></div>
                        </div>
                    </div>
                    @else
                   <div class="col-sm-3 category-column" id="category-column-1">
                     <select size="18" class="category-list" name="category_id_1">
                       @foreach ($subcategories as $sc)
                       <option value="{{ $sc->id }}" data-content="false" class="category-item" data-column="1">{{ $sc->name }}</option>
                       @endforeach
                     </select>
                   </div>
                    @endif
                 </div>
               </form>
             </div>
           </div>
            <div class="col-xs-12">Tu publicación debe cumplir con las Políticas de {{ config('app.name') }}</div>
         </div>
       </div>
     </section>

@endsection

@section('scripts')
	@parent
	<script>
		$(document).ready(function() {

            $(document).on('click', ".category-item", function() {
                var anchor = $(this);
                var dataColumn = anchor.attr('data-column');
                var nextColumn = parseInt(dataColumn)+1;

                if(anchor.attr('data-content') == 'false'){
                    anchor.attr('data-content','true');

                    /* delete columns with a greater value */
                    for(var i = nextColumn; i < 100; i++) {
                      $("#category-column-"+i).remove();
                    }

                    $.ajax({
                        method: "POST",
                        url: "{{url('/get_subcategory')}}",
                        cache: false,
                        data: { id: anchor.val(), offset: anchor.attr('data-offset'), category_id: anchor.attr('data-category-id'), _token: '{{ csrf_token() }}'}
                      })
                    .done(function( msg ) {
                        if(msg.is_ok) {
                          anchor.attr('data-content','false');
                          if(msg.value === 'categories' && msg.subcategories.length > 0) {
                              var output = '<div class="col-sm-3 category-column" id="category-column-'+nextColumn+'">';
                              output += '<select size="18" class="category-list" name="category_id_'+nextColumn+'">';
                              for(i in msg.subcategories) {
                                output += '<option value="'+msg.subcategories[i].id+'" class="category-item" data-content="false" data-column="'+nextColumn+'">'+msg.subcategories[i].name+'</option>';
                              }
                              output += '</select></div>';
                              $("#select-category").append(output);
                            } else {
                                if(msg.value === 'features' && msg.features.length > 0) {
                                    var output = '<div class="col-sm-3 category-column feature-column" id="category-column-'+nextColumn+'">';
                                    output += '<select size="18" class="category-list" name="feature_id_'+nextColumn+'">';
                                    for(i in msg.features) {
                                      output += '<option value="'+msg.features[i].id+'" class="category-item" data-content="false"  data-offset="'+msg.offset+'" data-category-id="'+msg.category_id+'" data-column="'+nextColumn+'">'+msg.features[i].name+'</option>';
                                    }
                                    output += '</select></div>';
                                    $("#select-category").append(output);
                                  }
                            }
                        }
                        if(msg.value === 'complete') {
                          $("#complete").val('1');

                          var output = '<div class="col-sm-3 category-column" style="height: 301px" id="category-column-'+nextColumn+'">';
                          output += '<div style="height: 301px; border: 1px solid #aaa; border-radius: 5px; text-align: center;"><i class="fa fa-check-circle" style="font-size: 70px; color: #5cb85c; margin-top: 50px;"></i>';
                          output += '<div style="margin-top: 5px;font-weight: bold;">¡Completado!</div>';
                          output += '<div style="margin-top: 20px"><button class="btn btn-primary">Continuar</button></div>';
                          output += '</div></div>';
                          $("#select-category").append(output);
                        }
                    });
                }
            });

		});
	</script>
@endsection

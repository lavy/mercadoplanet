@extends('layouts.app')

@section('title', 'Ayuda')

@section('content')
<style>
#lhc_status_container {
  display: none;
}
</style>
<section class="mainContent clearfix">
<div class="container">

    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('help') }}" class="btn-link">Ayuda</a></li>
                @foreach($breadcrumb as $key => $value)
                @if($article->id == $key)
                <li class="breadcrumb-item active">{{ $value }}</li>
                @else
                <li class="breadcrumb-item"><a href="{{ route('help', ['id' => $key]) }}" class="btn-link">{{ $value }}</a></li>
                @endif
                @endforeach
            </ol>
        @if($article)
        <h3>{{ $article->title }}</h3>
        @else
        <h3 style="float:left">¿En que podemos ayudarte?</h3>
        <button class="btn btn-primary btn-chat" id="button-chat" onclick="return contact()" style="float:right">Estamos en Línea!</button>
        <div style="clear: both"></div>
        @endif
        </div>
    </div>
    @if(count($categories))
    <div class="row">
        <div class="col-md-12">
            <ul class="ul-help">
                @foreach($categories as $c)
                <a href="{{ route('help', ['id' => $c->id]) }}"><li><i class="fa fa-chevron-right"></i> {{ $c->title }}</li></a>
                @endforeach
            </ul>
        </div>
    </div>
    @endif

    @if($article and $article->final)
    {!! $article->content !!}
    @endif

    @if($article)
    <div class="row" style="margin-top: 20px">
        <div class="col-md-12">
            <a href="{{ route('help', ['id' => $article->parent_id]) }}"><button class="btn btn-primary">Volver</button></a>
        </div>
    </div>
    @endif
</div>
</section>
@endsection
@section('scripts')
    @parent
    <script type="text/javascript">
    var LHCChatOptions = {};
    LHCChatOptions.opt = {widget_height:340,widget_width:300,popup_height:520,popup_width:500};
    (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    var referrer = (document.referrer) ? encodeURIComponent(document.referrer.substr(document.referrer.indexOf('://')+1)) : '';
    var location  = (document.location) ? encodeURIComponent(window.location.href.substring(window.location.protocol.length)) : '';
    po.src = '//chat.mercadoplanet.com/chat/index.php/esp/chat/getstatus/(click)/internal/(position)/bottom_right/(ma)/br/(hide_offline)/true/(top)/350/(units)/pixels/(leaveamessage)/true/(theme)/1?r='+referrer+'&l='+location;
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
    })();window.onload = function() {
      if(!document.getElementById('lhc_status_container') && document.getElementById('button-chat')) {
        document.getElementById('button-chat').style.display='none';
      }
    }

      function contact() {
        if(document.getElementById('lhc_status_container'))
          lh_inst.lh_openchatWindow();
      }
    </script>
@endsection

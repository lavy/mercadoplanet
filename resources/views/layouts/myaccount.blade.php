@extends('layouts.app')

@section('content')

<section class="mainContent clearfix ">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="panel-body panel-body-summary">
                    <div class="collapse navbar-collapse navbar-ex1-collapse navbar-side-collapse">
                        <ul class="nav navbar-nav side-nav">
                            <li><a href="{{ url('/summary') }}">Resumen</a></li>
                            <li><a href="{{ url('/billing') }}">Facturación</a></li>
                            <li><a href="{{ url('/myreputation') }}">Reputación</a></li>
                            <li>
                                <a href="javascript:;" data-toggle="collapse" data-target="#purchases">Compras <i class="fa fa-plus"></i></a>
                                <ul id="purchases" class="collapse collapseItem">
                                    <li><a href="{{ url('/favorites') }}"><i class="fa fa-caret-right" aria-hidden="true"></i>Favoritos</a></li>
                                    <li><a href="{{ url('/myquestions') }}"><i class="fa fa-caret-right" aria-hidden="true"></i>Preguntas</a></li>
                                    <li><a href="{{ url('/purchases') }}"><i class="fa fa-caret-right" aria-hidden="true"></i>Compras</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:;" data-toggle="collapse" data-target="#sales">Ventas <i class="fa fa-plus"></i></a>
                                <ul id="sales" class="collapse collapseItem">
                                    <li><a href="{{ url('/publications') }}"><i class="fa fa-caret-right" aria-hidden="true"></i>Publicaciones</a></li>
                                    <li><a href="{{ url('/questions') }}"><i class="fa fa-caret-right" aria-hidden="true"></i>Preguntas</a></li>
                                    <li><a href="{{ url('/sales') }}"><i class="fa fa-caret-right" aria-hidden="true"></i>Ventas</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:;" data-toggle="collapse" data-target="#configuration">Configuración <i class="fa fa-plus"></i></a>
                                <ul id="configuration" class="collapse collapseItem">
                                    <li><a href="{{ url('/profile') }}"><i class="fa fa-caret-right" aria-hidden="true"></i>Datos personales</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>      
            </div>
            <div class="col-md-9">
            @yield('myaccountcontent')
            </div>
        </div>
    </div>
</section>

@endsection
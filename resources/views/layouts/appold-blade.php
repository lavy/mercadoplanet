<!DOCTYPE html>
<html lang="en">
<head>

    <!-- SITE TITTLE -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MercadoPlanet - @yield('title')</title>

    <!-- PLUGINS CSS STYLE -->

    <link href="/plugins/jquery-ui/jquery-ui.css" rel="stylesheet">
    <link href="/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/plugins/selectbox/select_option1.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/plugins/rs-plugin/css/settings.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/plugins/owl-carousel/owl.carousel.css" media="screen">


    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://kit.fontawesome.com/d2e845508d.js" crossorigin="anonymous"></script>
    <!-- CUSTOM CSS -->
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/colors/default.css" id="option_color">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

</head>

<body>

@section ('header')
<div class="main-wrapper">

    <!-- HEADER -->
    <div class="header clearfix">

        <!-- NAVBAR -->
        <nav class="navbar navbar-main navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ url('img/logo.png') }}" alt=""></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        @if (Auth::check())
                        <li class="dropdown">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> {{ Auth::user()->username }} <i class="fa fa-user fa-big"></i></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="{{url('/summary')}}">Mi cuenta</a></li>
                                <li><a href="{{url('/logout')}}">Salir</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <?php
                            $favorites = Auth::user()->favorites()
                                ->where(function($query) {
                                    $query->where('publications.status', 'Active')
                                        ->orWhere('publications.status', 'Paused')
                                        ->orWhere('publications.status', 'Finished');
                                })
                                ->whereRaw('publications.created_at > (NOW() - INTERVAL 90 DAY)')
                                ->orderBy('id', 'desc')->limit(5)->get();
                            ?>
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="300" data-close-others="true" aria-expanded="false"><i class="fa @if(count($favorites)) fa-heart @else fa-heart-o @endif fa-big">{{ count($favorites) }}</i></a>
                            @if(count($favorites))
                            <ul class="dropdown-menu dropdown-menu-right" style="width:300px">
                                @foreach($favorites as $f)
                                <li><a href="{{ route('product',[$f->id]) }}" style="border-bottom: 1px solid #efefef;">
                                        <div>
                                            <div style="float: left">
                                                @if(count($f->images))
                                                <img src="{{ url('/photo/50x50?url='.urlencode($f->images[0]->name)) }}">
                                                @else
                                                <img src="{{ url('/photo/50x50?url=nophoto.jpg') }}">
                                                @endif
                                            </div>
                                            <div style="float: left; margin-left: 10px;max-width: 185px;">
                                                <div style="text-overflow: ellipsis;white-space: nowrap;overflow: hidden;">{{ $f->title }}</div>
                                                <div>S/{{ number_format($f->price, 2, ',', '.') }}</div>
                                            </div>

                                        </div>
                                    </a></li>
                                @endforeach
                                <li style="text-align: center"><a href="{{ url('/favorites') }}">Ver todos</a></li>
                            </ul>
                            @endif


                        <li class="dropdown">
                            <?php
                            $notifications = Auth::user()->notifications()
                                ->orderBy('id', 'desc')->limit(15)->get();
                            ?>
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="300" data-close-others="true" aria-expanded="false"><i class="fa @if(count($notifications) and (Auth::user()->latest_notification_view == null or newNotifications($notifications[0]->created_at))) fa-bell @else fa-bell-o @endif fa-big">{{ count($notifications) }}</i></a>
                            @if(count($notifications))
                            <ul class="dropdown-menu dropdown-menu-right" id="dropdown-favorites" style="width: 325px;max-height: 380px;overflow-y: auto;">
                                @foreach($notifications as $n)
                                <?php
                                $f = \App\Publication::find($n->publication_id);
                                ?>
                                <li>
                                    @if($n->type == 'sale' or $n->type == 'message-seller')
                                    <a href="{{ route('sale_detail',['id' => $n->transaction_id]) }}" style="border-bottom: 1px solid #efefef;">
                                        @endif
                                        @if($n->type == 'message-buyer')
                                        <a href="{{ route('purchase_detail',['id' => $n->transaction_id]) }}" style="border-bottom: 1px solid #efefef;">
                                            @endif
                                            @if($n->type == 'question')
                                            <a href="{{ route('questions') }}" style="border-bottom: 1px solid #efefef;">
                                                @endif
                                                @if($n->type == 'response')
                                                <a href="{{ route('myquestions') }}" style="border-bottom: 1px solid #efefef;">
                                                    @endif
                                                    <div>
                                                        <div style="float: left">
                                                            @if(!empty($f) && count($f->images))
                                                            <img src="{{ url('/photo/50x50?url='.urlencode($f->images[0]->name)) }}">
                                                            @else
                                                            <img src="{{ url('/photo/50x50?url=nophoto.jpg') }}">
                                                            @endif
                                                        </div>
                                                        <div style="float: left; margin-left: 10px;max-width: 185px;">
                                                            <div style="white-space: pre-wrap;font-size: 13px;text-transform: none;">{{ $n->body }}</div>
                                                        </div>
                                                        <div style="clear: both"></div>
                                                        <div style="text-align: right;text-decoration: none;color: #aaa;text-transform: none;font-size: 0.8em;">hace <?php echo humanizeTime($n->created_at); ?></div>

                                                    </div>
                                                </a></li>
                                @endforeach
                            </ul>
                            @endif


                            @else
                        <li><a href=" {{url('/register')}}">Regístrate</a></li>
                        <li><a href=" {{url('/login')}}">Ingresa</a></li>
                        @endif
                        <li><a href="{{ route('sell') }}">Vender <i class="fa fa-shopping-cart fa-big"></i></a></li>
                        <li><a href="/help/"><i class="fa fa-question-circle fa-big"></i></a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->

                <div id="main-search-box">
                <span class="input-group">
                    <input class="form-control search-input" id="main-search" placeholder="Encuentra lo que quieras..." aria-describedby="basic-addon2" value="@yield('search')">
                  <span class="input-group-addon search-button" id="main-search-button">Buscar</span>
                </span>
                </div>
            </div>
        </nav>

    </div>
    @show

    @yield('content')

    @section('footer')
    <!-- LIGHT SECTION -->
    <section class="lightSection clearfix">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="owl-carousel partnersLogoSlider">
                        <div class="slide">
                            <div class="partnersLogo clearfix">
                                <img src="/img/home/partners/partner-01.png" alt="partner-img">
                            </div>
                        </div>
                        <div class="slide">
                            <div class="partnersLogo clearfix">
                                <img src="/img/home/partners/partner-02.png" alt="partner-img">
                            </div>
                        </div>
                        <div class="slide">
                            <div class="partnersLogo clearfix">
                                <img src="/img/home/partners/partner-03.png" alt="partner-img">
                            </div>
                        </div>
                        <div class="slide">
                            <div class="partnersLogo clearfix">
                                <img src="/img/home/partners/partner-04.png" alt="partner-img">
                            </div>
                        </div>
                        <div class="slide">
                            <div class="partnersLogo clearfix">
                                <img src="/img/home/partners/partner-05.png" alt="partner-img">
                            </div>
                        </div>
                        <div class="slide">
                            <div class="partnersLogo clearfix">
                                <img src="/img/home/partners/partner-01.png" alt="partner-img">
                            </div>
                        </div>
                        <div class="slide">
                            <div class="partnersLogo clearfix">
                                <img src="/img/home/partners/partner-02.png" alt="partner-img">
                            </div>
                        </div>
                        <div class="slide">
                            <div class="partnersLogo clearfix">
                                <img src="/img/home/partners/partner-03.png" alt="partner-img">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- FOOTER -->
    <div class="footer clearfix">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-xs-12">
                    <div class="footerLink">
                        <h5>Ayuda</h5>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('help?id=1') }}">Comprar</a></li>
                            <li><a href="{{ url('help?id=2') }}">Vender</a></li>
                            <li><a href="{{ url('help?id=3') }}">Cuenta</a></li>
                            <li><a href="{{ url('help?id=4') }}">Seguridad</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="footerLink">
                        <h5>Acerca de</h5>
                        <ul class="list-unstyled">
                            <li><a href="#">MercadoPlanet</a></li>
                            <li><a href="#">Términos y Condiciones</a></li>
                            <li><a href="{{ url('/privacy')}}">Políticas  de Privacidad</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="footerLink">
                        <h5>Mi cuenta</h5>
                        <ul class="list-unstyled">
                            @if(!Auth::check())
                            <li><a href="{{ url('/register') }}">Regístrate</a></li>
                            <li><a href="{{ url('/login') }}">Ingresar</a></li>
                            @endif
                            <li><a href="{{ url('/sell') }}">Vender</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="footerLink">
                        <h5>Contacto</h5>
                        <ul class="list-unstyled">
                            <li><a href="mailto:contacto@mercadoplanet.com">contacto@mercadoplanet.com</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- COPY RIGHT -->
    <div class="copyRight clearfix">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <p>&copy; 2016 Copyright MercadoPlanet</p>
                </div>
            </div>
        </div>
    </div>
</div>

@show

@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="/plugins/jquery-ui/jquery-ui.js"></script>
<script src="/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="/plugins/owl-carousel/owl.carousel.js"></script>
<script src="/plugins/selectbox/jquery.selectbox-0.1.3.min.js"></script>
<script src="/plugins/countdown/jquery.syotimer.js"></script>
<script src="/options/optionswitcher.js"></script>
<script src="/js/custom.js"></script>
<script>
    $(document).ready(function() {
        $('[data-toggle="required-field-tooltip"]').tooltip();

        $("#main-search-button").click(function() {
            if($("#main-search").val()!='') {
                window.location="{!! route('list') !!}?search="+$("#main-search").val();
            }
        });
        $('#main-search').keypress(function(e){
            if(e.which == 13){//Enter key pressed
                if($("#main-search").val()!='') {
                    window.location="{!! route('list') !!}?search="+$("#main-search").val();
                }
            }
        });

        $("#dropdown-favorites").mouseenter(function() {
            $.ajax({
                method: "GET",
                url: "{{url('/update_latest_notification_view')}}",
                cache: false
            });
        });
    });
</script>
@show

</body>
</html>
<?php
function humanizeTime($date) {
    $diff = time() - strtotime($date);
    if ($diff/60 < 60 ) {
        $m = ceil($diff/60);
        return $m == 1 ? "1 minuto" : $m." minutos";
    } elseif ($diff/3600 < 24) {
        $h = ceil($diff/3600);
        return $h == 1 ? "1 hora" : $h." horas";
    } else {
        $d = ceil($diff/(3600*24));
        return $d == 1 ? "1 día" : $d." días";
    }

}
function newNotifications($date) {
    return strtotime(Auth::user()->latest_notification_view) < strtotime($date);
}
?>

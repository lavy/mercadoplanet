@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row reputation-header">
    <div class="col-md-6">
        <div class="reputation-username">
            {{ $user->username }}
        </div>
        @if($address != null)
        <div class="reputation-address">
            <i class="fa fa-map-marker"></i> Ubicado en {{ $user->location() }}
        </div>
        @endif
        <div class="reputation-punctuation">
            {{ $total_points }} @if($total_points == 1) punto obtenido @else puntos obtenidos @endif de sus compras y ventas. 
        </div>
        <div class="reputation-another-link">
        @yield('another-link')
        </div>
    </div>
    <div class="col-md-6"></div>
    </div>
<div class="row reputation-content">
	
    @yield('reputation_info')
	<div class="row">
		<div class="calification-title col-md-12" style="text-align: center"><h4>@yield('calification-title')</h4></div>
	</div>    
    <br>
    @yield('qualifications')
        <br><br>
</div>
</div>

@endsection

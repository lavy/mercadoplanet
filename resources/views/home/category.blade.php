@extends('layouts.app')

@section('title', $category->name)

@section('content')


      <!-- MAIN CONTENT SECTION -->
      <section class="mainContent clearfix">
        <div class="container">
          <div class="row featuredProducts">
            <div class="col-lg-6">


                <div class="row categories-nav">
                    <h4>{{ $category->name }}</h4>

                    <?php $num1 = ceil(count($children_categories)/3); ?>
                    <?php $num2 = round(count($children_categories)/3); ?>
                    <?php $num3 = floor(count($children_categories)/3); ?>
                    <?php $count = 1; ?>
                    @foreach ($children_categories as $cat)
                    @if ($count == 1 or $count == $num1+1 or $count == $num1+$num2+1)
                    <div class="col-xs-4">
                    @endif
                        <h5><a id="cat-{{ $cat->id }}" href="{{ route('list', ['category' => $cat->id]) }}">{{ $cat->name }}</a></h5>
                        <ul>
                            @if (count($cat->children) > 0)
                            @foreach ($cat->children as $cat2)
                            <li><a id="cat-{{ $cat2->id }}" href="{{ route('list', ['category' => $cat2->id]) }}">{{ $cat2->name }}</a></li>
                            @endforeach
                            @else
                            @if (count($cat->groupFeatures) > 0)
                            @foreach ($cat->groupFeatures[0]->features as $param)
                            <li><a id="param-{{ $param->id }}" href="{{ route('list', ['category' => $cat->id, 'feature1' => $param->id]) }}">{{ $param->name }}</a></li>
                            @endforeach
                            @endif
                            @endif
                        </ul>
                    @if ($count == $num1 or $count == $num1+$num2 or $count == count($children_categories))
                    </div>
                    @endif
                    <?php $count++; ?>
                    @endforeach
                </div>

            </div>

            <div class="col-lg-6">

                @if (count($suggestions) > 0)
                <div class="row">
                    <div class="col-lg-12">
                      <div class="page-header">
                        <h4>Recomendados</h4>
                      </div>
                      <div class="owl-carousel categoryProductsSlider">
                        @foreach ($suggestions as $pub)
                        <div class="slide">
                          <div class="productImage clearfix">
                            @if (count($pub->images) > 0)
                            <img src="{{ url('/photo/120x120?url='.urlencode($pub->images[0]->name)) }}" alt="{{ $pub->title }}">
                            @else
                            <img src="{{ url('/photo/120x120?url=nophoto.jpg') }}">
                            @endif
                            <div class="productMasking">
                              <ul class="list-inline btn-group" role="group">
                                <li><a href="{{ ($authenticated? 'javascript:void(0)' : url('favorite', [$pub->id])) }}" class="btn btn-default btn-favorite pub-{{ $pub->id }} {{ ($pub->isFavorite() ? 'btn-favorite-marked':'') }}" data-pub-id="{{ $pub->id }}"><i class="fa fa-heart"></i></a></li>
                              </ul>
                              <a href="{{ route('product',[$pub->id]) }}"><p class="title">{{ $pub->title }}</p></a>
                            </div>
                          </div>
                          <div class="productCaption clearfix">
                            @if($pub->price == '0')
                            <a href="{{ route('product',[$pub->id]) }}"><h3 style="text-transform: none;font-size: 14px;">Precio a convenir</h3></a>
                            @else
                            <a href="{{ route('product',[$pub->id]) }}"><h3>S/{{ number_format($pub->price, 2, ',', '.') }}</h3></a>
                            @endif
                          </div>
                        </div>
                        @endforeach
                      </div>
                    </div>
                </div>
                @endif

                
                
                @if (count($bestsellers) > 0 && $category->id != 1 && $category->id != 2 && $category->id != 3)
                <div class="row">
                    <div class="col-lg-12">
                      <div class="page-header">
                        <h4>Más Vendidos</h4>
                      </div>
                      <div class="owl-carousel categoryProductsSlider">
                        @foreach ($bestsellers as $pub)
                        <div class="slide">
                          <div class="productImage clearfix">
                            @if (count($pub->images) > 0)
                            <img src="{{ url('/photo/120x120?url='.urlencode($pub->images[0]->name)) }}" alt="{{ $pub->title }}">
                            @else
                            <img src="{{ url('/photo/120x120?url=nophoto.jpg') }}">
                            @endif
                            <div class="productMasking">
                              <ul class="list-inline btn-group" role="group">
                                <li><a href="{{ ($authenticated? 'javascript:void(0)' : url('favorite', [$pub->id])) }}" class="btn btn-default btn-favorite pub-{{ $pub->id }} {{ ($pub->isFavorite() ? 'btn-favorite-marked':'') }}" data-pub-id="{{ $pub->id }}"><i class="fa fa-heart"></i></a></li>
                              </ul>
                              <a href="{{ route('product',[$pub->id]) }}"><p class="title">{{ $pub->title }}</p></a>
                            </div>
                          </div>
                          <div class="productCaption clearfix">
                            @if($pub->price == '0')
                            <a href="{{ route('product',[$pub->id]) }}"><h3 style="text-transform: none;font-size: 14px;">Precio a convenir</h3></a>
                            @else
                            <a href="{{ route('product',[$pub->id]) }}"><h3>S/{{ number_format($pub->price, 2, ',', '.') }}</h3></a>
                            @endif
                          </div>
                        </div>
                        @endforeach
                      </div>
                    </div>
                </div>
                @endif

                @if (count($latest_pub) > 0)
                <div class="row">
                    <div class="col-lg-12">
                      <div class="page-header">
                        <h4>Últimos Publicados</h4>
                      </div>
                      <div class="owl-carousel categoryProductsSlider">
                        @foreach ($latest_pub as $pub)
                        <div class="slide">
                          <div class="productImage clearfix">
                            @if (count($pub->images) > 0)
                            <img src="{{ url('/photo/120x120?url='.urlencode($pub->images[0]->name)) }}" alt="{{ $pub->title }}">
                            @else
                            <img src="{{ url('/photo/120x120?url=nophoto.jpg') }}">
                            @endif
                            <div class="productMasking">
                              <ul class="list-inline btn-group" role="group">
                                <li><a href="{{ ($authenticated? 'javascript:void(0)' : url('favorite', [$pub->id])) }}" class="btn btn-default btn-favorite pub-{{ $pub->id }} {{ ($pub->isFavorite() ? 'btn-favorite-marked':'') }}" data-pub-id="{{ $pub->id }}"><i class="fa fa-heart"></i></a></li>
                              </ul>
                              <a href="{{ route('product',[$pub->id]) }}"><p class="title">{{ $pub->title }}</p></a>
                            </div>
                          </div>
                          <div class="productCaption clearfix">
                            @if($pub->price == '0')
                            <a href="{{ route('product',[$pub->id]) }}"><h3 style="text-transform: none;font-size: 14px;">Precio a convenir</h3></a>
                            @else
                            <a href="{{ route('product',[$pub->id]) }}"><h3>S/{{ number_format($pub->price, 2, ',', '.') }}</h3></a>
                            @endif
                          </div>
                        </div>
                        @endforeach
                      </div>
                    </div>
                </div>
                @endif
            </div>
          </div>
        </div>
      </section>

@endsection

@section('scripts')
	@parent
  <script>
    $(document).ready(function() {

      $(".btn-favorite").click(function() {
          var anchor = $(this);
          $.ajax({
              method: "POST",
              url: "{{url('/favorite')}}",
              cache: false,
              data: { id: anchor.attr('data-pub-id'), _token: '{{ csrf_token() }}'}
            })
          .done(function( msg ) {
              if(msg.is_ok) {
                  if(msg.value) {
                      $(".pub-"+anchor.attr('data-pub-id')).addClass('btn-favorite-marked');
                  } else {
                      $(".pub-"+anchor.attr('data-pub-id')).removeClass('btn-favorite-marked');
                  }
              }
          });
      });

    });
  </script>
@endsection

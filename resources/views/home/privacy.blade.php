@extends('layouts.app')

@section('title', 'Políticas de privacidad')

@section('content')


      <!-- MAIN CONTENT SECTION -->
      <section class="mainContent clearfix">
        <div class="container">
            <h3>Políticas de privacidad</h3>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                La privacidad de datos de los usuarios es muy importante para <strong>MercadoPlanet</strong>, por lo cual se toman precauciones para salvaguardar  sus datos. Estas políticas de privacidad describen la información que <strong>MercadoPlanet</strong> recoge sobre sus usuarios y visitantes y lo que puede hacerse con dicha información. Mediante el uso del sitio web, el usuario reconoce y conviene en que <strong>MercadoPlanet</strong> puede a su sola discreción conservar por tiempo indeterminado o divulgar el contenido del usuario, así como también su información, ya sea direcciones de email, direcciones ip, datos de tiempo y hora, y demás información del usuario, sobre sus actividades o acceso y uso del sitio, siempre que considere que es necesario por ley o creyendo de buena fe que dicha conservación o divulgación es razonablemente necesaria para lo siguiente: cumplir con un proceso legal; exigir el cumplimiento de estas condiciones; responder a reclamos sobre la violación de los derechos de terceros en virtud del contenido; responder reclamos de que información de contacto (por ejemplo, número telefónico, domicilio, calle) de un tercero fue anunciada o transmitida sin su consentimiento o como una forma de acoso; proteger los derechos, la propiedad, o la seguridad personal de <strong>MercadoPlanet</strong>, sus usuarios o del público en general.
            </p>
            <br>
            <h4>Datos que recabamos</h4>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                <strong>MercadoPlanet</strong> solo recolecta datos personales que el usuario nos envía en forma voluntaria y con una finalidad específica. El usuario puede brindarnos datos como forma de registración para algún servicio, o al realizar llamados o consultas por teléfono o cualquier otro medio de comunicación. Los datos serán usados para que el usuario de internet, que publique información en el sitio de <strong>MercadoPlanet</strong>, pueda ser contactado por el interesado en adquirir bienes o servicios relacionados a la información publicada. Asimismo, los datos podrán ser usados para que <strong>MercadoPlanet</strong> contacte a los usuarios a fin de brindarle información sobre cambios y/o novedades y/o requerirles información sobre la experiencia en el uso del sitio web. Esos datos no serán usados con otra finalidad distinta que aquella para la que fueron recolectados en cada oportunidad, excepto en el caso que sea requerido de otro modo por la ley. Si el usuario ha dado permiso sus datos podrán ser compartidos con otras empresas subsidiarias o afiliadas de <strong>MercadoPlanet</strong>. No se requiere que el usuario proporcione información personal como condición para usar este sitio, a menos que sea necesario para proveerle información adicional que el usuario solicite. Cuando el usuario utiliza este sitio, almacenamos datos en nuestros servidores para posibilitar la conexión y por cuestiones de seguridad. Estos datos pueden incluir el nombre de su proveedor de servicio de internet, el sitio web que el usuario ha usado para vincularse a este sitio, los sitios web de <strong>MercadoPlanet</strong> que el usuario ha visitado, los sitios web que el usuario visita desde nuestro este sitio, su dirección ip (internet protocol), la fecha y duración de sus visitas a este sitio. Estos datos podrían conducir posiblemente a su identificación, pero generalmente no lo hacemos. Utilizamos los datos periódicamente para fines estadísticos, pero mantenemos el anonimato de cada usuario individual de manera tal que la persona no pueda ser identificada. Los datos personales se reúnen exclusivamente si el usuario nos los proporciona, a través del registro, del llenado de formularios o de correos electrónicos, como parte de un pedido de mayor información, de consultas o solicitudes acerca de este sitio y situaciones similares en las que el usuario haya elegido proveernos esa información. Mantendremos el control de y la responsabilidad del uso de cualquier dato personal que el usuario nos proporcione y solo los usaremos para contactarlo por vía electrónico o telefónica. Algunos o todos estos datos pueden ser almacenados o procesados en ordenadores ubicados en otras jurisdicciones, cuyas leyes de protección de la información pueden diferir de la jurisdicción del domicilio del usuario. Los datos del usuario podrán ser compartidos con terceras partes, así como con otras empresas subsidiarias o afiliadas de <strong>MercadoPlanet</strong>. Si la totalidad de los activos de <strong>MercadoPlanet</strong> son vendidos o fusionados con otra compañía, la sociedad adquirente podrá acceder a la información personal del usuario sin su consentimiento. Si una parte del negocio de <strong>MercadoPlanet</strong> es vendido, la entidad adquirente deberá tener acceso a la información personal del usuario sin su consentimiento. En caso de que el usuario utilice los medios de comunicación de mensajes provistos por <strong>MercadoPlanet</strong>, <strong>MercadoPlanet</strong> podrá almacenar y acceder a dichos mensajes, hacer un seguimiento y remitir notificaciones relacionadas a dicho mensaje.
            </p>
            <br>
            <h4>Acceso, supresión y rectificación de datos personales</h4>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                Los usuarios podrán acceder, cancelar y actualizar sus datos personales, incluyendo su dirección de e-mail. Los usuarios garantizan y responden, en cualquier caso, de la veracidad, exactitud, vigencia y autenticidad de la información personal facilitada, y se comprometen a mantenerla debidamente actualizada.
            Una vez registrado en <strong>MercadoPlanet</strong>, el usuario podrá revisar y cambiar la información que nos ha enviado durante el proceso de registración incluyendo:
            </p>
            <ul style='text-align: justify; font-size: 14px; line-height: 25px'>
                <li style="list-style-type: disc;margin-left: 17px;">El apodo y dirección de correo electrónico. Sin perjuicio de los cambios que realice, <strong>MercadoPlanet</strong> conservará la información personal anterior por motivos de seguridad y control del fraude.</li>
                <li style="list-style-type: disc;margin-left: 17px;">Los datos de la registración como: compañía, domicilio, ciudad, región, código postal, número principal de teléfono, número secundario de teléfono, correo electrónico, etc.</li>
                <li style="list-style-type: disc;margin-left: 17px;">La contraseña.</li>
            </ul>
            <br>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
            <strong>MercadoPlanet</strong> podrá modificar en cualquier momento los términos y condiciones de estas políticas de privacidad y confidencialidad y/o las prácticas de envío de e-mails. Si decidimos introducir algún cambio material a nuestras políticas de privacidad, te notificaremos publicando una versión actualizada de las políticas en esta sección o mediante el envío de un e-mail o informándolo en la página principal u otras secciones del sitio para mantenerte actualizado de los cambios realizados.
            </p>
            <br>
            
            
            <h4>Uso que hacemos de la información</h4>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                Para suministrar un excelente servicio y para que los usuarios puedan realizar operaciones en forma ágil y segura, <strong>MercadoPlanet</strong> requiere cierta información de carácter personal, incluyendo dirección de e-mail. La recolección de información nos permite ofrecer a los usuarios servicios y funcionalidades que se adecuen mejor a sus necesidades y personalizar nuestros servicios para hacer que sus experiencias con <strong>MercadoPlanet</strong> sean lo más cómodas posible. La información personal que recabamos tiene las siguientes finalidades:
            </p>
            <ul style='text-align: justify; font-size: 14px; line-height: 25px'>
                <li style="list-style-type: disc;margin-left: 17px;">Ayudar al comprador y vendedor a entrar en contacto directo en la oportunidad que corresponda según la modalidad de la compra. En este caso, <strong>MercadoPlanet</strong> suministrará a los interesados sus datos personales (nombre, teléfonos, localidad y correo electrónico), a través de correo electrónico o a través del sitio. La información así conocida por el comprador o el vendedor, sólo podrá ser utilizada a efectos de concluir la operación originada en <strong>MercadoPlanet</strong> y no deberá ser empleada por ninguno de ellos con fines publicitarios o promocionales u otras actividades no relacionadas con <strong>MercadoPlanet</strong>, salvo la expresa autorización del usuario.</li>
                <li style="list-style-type: disc;margin-left: 17px;">Mejorar nuestras iniciativas comerciales y promocionales y analizar las páginas visitadas las búsquedas realizadas por los usuarios, para mejorar nuestra oferta de contenidos y artículos, personalizar dichos contenidos, su presentación y servicios.</li>
            </ul>
            <br>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
            Enviar información o mensajes por e-mail sobre nuevos servicios, mostrar publicidad o promociones, banners, de interés para nuestros usuarios, noticias sobre <strong>MercadoPlanet</strong>, además de la información expresamente autorizada en la sección de preferencias. Si el usuario lo prefiere, puede solicitar que lo excluyan de las listas para el envío de información promocional o publicitaria. Por favor aprende a hacerlo la sección "cambio de preferencias de e-mail" abajo.
            </p>
            <br>
            
            
            <h4>Confidencialidad de la información</h4>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                Una vez registrado en su sitio web, <strong>MercadoPlanet</strong> no venderá, alquilará o compartirá la información personal excepto en las formas establecidas en estas políticas. Sin perjuicio de ello, el usuario consiente en forma expresa que <strong>MercadoPlanet</strong> transfiera total o parcialmente la información personal a cualquiera de las sociedades controladas, controlantes y/o vinculadas con <strong>MercadoPlanet</strong>, a cualquier título y en el momento, forma y condiciones que estime pertinentes. Haremos todo lo que esté a nuestro alcance para proteger la privacidad de la información. Puede suceder que en virtud de órdenes judiciales, o de regulaciones legales, nos veamos compelidos a revelar información a las autoridades o terceras partes bajo ciertas circunstancias, o bien en casos que terceras partes puedan interceptar o acceder a cierta información o transmisiones de datos en cuyo caso <strong>MercadoPlanet</strong> no responderá por la información que sea revelada.
            </p>
            <br>
            
            
            <h4>Apodo</h4>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                Para interactuar dentro del sitio, los usuarios deben utilizar un seudónimo que los identificará. Los usuarios no tendrán acceso a la información personal de los otros usuarios, salvo que hayan realizado con ellos alguna operación a través de nuestra plataforma, luego de la cual ambas partes recibirán la información de la otra.
            </p>
            <br>
            
            
            <h4>Clave personal</h4>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                Para acceder a los servicios reservados únicamente para los usuarios debidamente registrados los usuarios dispondrán de una clave personal. Con ella podrán comprar, vender, ofertar, calificar, entre otras actividades. Esta clave deben mantenerla bajo absoluta confidencialidad y, en ningún caso, deberán revelarla o compartirla con otras personas.
                El usuario será responsable de todos los actos que tengan lugar mediante el uso de su apodo y clave, lo que incluye hacerse cargo del pago de las tarifas que eventualmente se devenguen o por los perjuicios que puedan sufrir otros usuarios por tal motivo. Si por cualquier razón un usuario creyera que alguien puede conocer su clave, deberá modificarla ingresando desde el menú de navegación en <a href="{{ url('/profile') }}" class="btn-link">mi cuenta > configuración > datos personales</a>.
            </p>
            <br>
            
            
            <h4>Menores de edad</h4>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                En vista de la importancia de proteger la privacidad de los menores de edad, nosotros no recopilamos, procesamos o utilizamos en este sitio ninguna información relacionada con algún individuo que sepamos que es menor de edad sin el previo y verificable consentimiento de su representante legal. Tal representante legal tiene el derecho, a su solicitud, de ver la información que facilitó el menor de edad y/o solicitar que la misma sea borrada o corregida.
            </p>
            <br>
            
            
            <h4>El uso de la información por otros usuarios</h4>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                Para facilitar la interacción entre todos los miembros de la comunidad de <strong>MercadoPlanet</strong>, nuestro servicio permite un acceso limitado a ciertos datos de contacto del resto de usuarios, tales como nombre de usuario, teléfonos, ciudad y dirección de correo electrónico.
Los usuarios sólo podrán utilizar la información personal de otros usuarios obtenida en el sitio para: (a) comunicaciones relacionadas con <strong>MercadoPlanet</strong> que no constituyan comunicaciones comerciales no solicitadas, (b) utilizar servicios ofrecidos en <strong>MercadoPlanet</strong> (por ejemplo: depósito, seguros, envío o transporte y reclamaciones sobre fraude), y (c) cualquier otra finalidad a la que el usuario correspondiente consienta expresamente una vez le hayan sido comunicadas previamente la información legalmente requerida.
Bajo ninguna circunstancia, se debe comunicar información personal o dirección de correo electrónico de otro usuario a ningún tercero sin nuestro consentimiento y el del usuario afectado. No se debe agregar a la agenda de direcciones de correo electrónico (física o electrónica) los datos de ningún usuario de <strong>MercadoPlanet</strong>, ni siquiera los datos de quienes hayan adquirido algún artículo ofrecido en <strong>MercadoPlanet</strong>, sin que medie el consentimiento expreso de tal usuario.
            </p>
            <br>
            
            
            <h4>Cookies</h4>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                El usuario y el visitante del sitio web de <strong>MercadoPlanet</strong> conoce y acepta que <strong>MercadoPlanet</strong> podrá utilizar un sistema de seguimiento mediante la utilización de cookies (las "cookies"). Las cookies son pequeños archivos que se instalan en el disco rígido, con una duración limitada en el tiempo que ayudan a personalizar los servicios. También ofrecemos ciertas funcionalidades que sólo están disponibles mediante el empleo de cookies. Las cookies se utilizan con el fin de conocer los intereses, el comportamiento y la demografía de quienes visitan o son usuarios de nuestro sitio web y de esa forma, comprender mejor sus necesidades e intereses y darles un mejor servicio o proveerle información relacionada. También usaremos la información obtenida por intermedio de las cookies para analizar las páginas navegadas por el visitante o usuario, las búsquedas realizadas, mejorar nuestras iniciativas comerciales y promocionales, mostrar publicidad o promociones, banners de interés, noticias sobre <strong>MercadoPlanet</strong> , perfeccionar nuestra oferta de contenidos y artículos, personalizar dichos contenidos, presentación y servicios; también podremos utilizar cookies para promover y hacer cumplir las reglas y seguridad del sitio. <strong>MercadoPlanet</strong> podrá agregar cookies en los e-mails que envíe para medir la efectividad de las promociones.
            </p>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                Utilizamos adicionalmente las cookies para que el usuario no tenga que introducir su clave tan frecuentemente durante una sesión de navegación, también para contabilizar y corroborar las registraciones, la actividad del usuario y otros conceptos para acuerdos comerciales, siempre teniendo como objetivo de la instalación de las cookies, el beneficio del usuario que la recibe, y no será usado con otros fines ajenos a <strong>MercadoPlanet</strong>.
            </p>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                Se establece que la instalación, permanencia y existencia de las cookies en el computador del usuario depende de su exclusiva voluntad y puede ser eliminada de su computador cuando el usuario así lo desee. Para saber como quitar las cookies del sistema es necesario revisar la sección ayuda (help) del navegador.
            </p>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                Adicionalmente, se pueden encontrar cookies u otros sistemas similares instalados por terceros en ciertas páginas de nuestro sitio web. Por ejemplo, al navegar por una página creada por un usuario, puede que exista una cookie emplazada en tal página.
            </p>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                Se aclara expresamente que estas políticas cubre la utilización de cookies por este sitio y no la utilización de cookies por parte de anunciantes. Nosotros no controlamos el uso de cookies por terceros.
            </p>
            <br>
            
            
            <h4>Inclusión de información personal en las categorías especiales</h4>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                Dentro de la categoría autos, motos y otros vehículos e inmuebles y propiedades se permite la inclusión del teléfono del vendedor en la publicación. En la categoría servicios el vendedor deberá incluir en la publicación su dirección de correo electrónico y su teléfono, y será optativa la publicación de su domicilio y de la url de su sitio web.
            </p>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                El vendedor sólo podrá ingresar los datos mencionados, según corresponda, dentro del campo específico al momento de publicar el artículo. Sin embargo, en ningún caso deberá incluirlo en la descripción o en el título de su publicación, ni en ningún otro lugar fuera del campo específico.
            </p>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                Tampoco podrá el vendedor incluir ningún otro dato personal dentro o fuera del campo indicado. <strong>MercadoPlanet</strong> no se hace responsable del uso que el vendedor, el comprador, cualquier usuario u otra persona pudieran hacer de la información publicada.
            </p>
            <br>
            
            
            <h4>Derechos de propiedad</h4>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                El servicio se encuentra protegido dentro del máximo alcance permitido por las leyes y los tratados internacionales respecto a los derechos de autor. El contenido que se muestra en el servicio o a través del servicio, se encuentra protegido por las leyes de derecho de autor como trabajo colectivo y/o compilación, conforme a las leyes de derecho de autor y a los convenios internacionales al respecto. Queda prohibido reproducir, modificar, crear trabajos derivados o bien redistribuir los sitios o el trabajo colectivo. Queda prohibido copiar o reproducir los sitios o partes de estos en otro servidor o ubicación para su subsiguiente reproducción o redistribución. El usuario se obliga además a no reproducir, duplicar ni copiar contenido del servicio incluyendo los avisos, la estructura y organización del sitio y el código informático. Asimismo, conviene en someterse a todas las notificaciones de derechos de autor que se publican en el servicio. El usuario no puede descompilar o desensamblar o aplicar ingeniería inversa o de otra manera intentar descubrir los códigos fuente que pudieran encontrarse en el servicio. El usuario se obliga a no reproducir, duplicar, copiar, vender, revender ni explotar con fines comerciales ningún aspecto del servicio. <strong>MercadoPlanet</strong> es una marca registrada ante la oficina de patentes y marcas de los estados unidos. Si bien <strong>MercadoPlanet</strong> no reclama titularidad sobre el contenido publicado por sus usuarios, al anunciar contenido en las áreas públicas del servicio, el usuario automáticamente otorga, manifiesta y garantiza que él cuenta con el derecho de otorgar a favor de <strong>MercadoPlanet</strong> una licencia irrevocable, vitalicia, no-exclusiva, totalmente paga y de alcance mundial para usar, copiar, ejecutar, desplegar y distribuir dicho contenido y para preparar trabajos derivados a partir de este o bien incorporarlo en otros trabajos, y otorgar y autorizar sub-licencias (a través de múltiples escalafones) de lo que antecede. Necesitamos contar con estos derechos para albergar y desplegar el contenido de usuario.
            </p>
            <br>
            
            
            <h4>Almacenamiento de datos. </h4>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                Los datos personales que el usuario pudiera haber brindado a <strong>MercadoPlanet</strong> serán almacenados durante el tiempo requerido para la finalidad para la cual pudieran haber sido solicitados por <strong>MercadoPlanet</strong> o brindados por el usuario. Esos datos se utilizarán únicamente con el propósito de proporcionarle la información o evacuar la consulta que el usuario haya requerido o para otros propósitos para los cuales el usuario dio su consentimiento, salvo que la ley estipule otra cosa.
            </p>
            <br>
            
            
            <h4>Seguridad</h4>
            <p style='text-align: justify; font-size: 14px; line-height: 25px'>
                <strong>MercadoPlanet</strong> es consciente de la importancia que tienen para el usuario la confidencialidad y seguridad de sus datos personales generados por el uso de este sitio. Para ello, adoptamos precauciones de seguridad técnicas y de organización a fin de proteger sus datos personales de manipulación, pérdida, destrucción o acceso por personas no autorizadas. Nuestros procedimientos de seguridad experimentan revisiones continuas basadas en el desarrollo de nuevas tecnologías.
            </p>
            
        </div>
      </section>
@endsection

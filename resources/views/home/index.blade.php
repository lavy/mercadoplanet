@extends('layouts.app')

@section('title', 'Inicio')

@section('content')


      <div class="row">
        <br>
        <div class="col-md-12 col-md-1 col-md-offset-1">
        <a href="{{url('category/1')}}" class="btn btn-info btn-lg" style="border-radius: 50%;"><i class="fas fa-car"></i></a>
          <div>
            Vehiculos
          </div>
        </div>
        <div class="col-md-12 col-md-1">
          <a href="{{url('category/2')}}" class="btn btn-info btn-lg" style="border-radius: 50%;"><i class="fas fa-home"></i></a>
          <div>
            Inmuebles
          </div>
        </div>
        <div class="col-md-12 col-md-1">
          <a href="{{url('category/3')}}" class="btn btn-info btn-lg" style="border-radius: 50%;"><i class="fas fa-shopping-bag"></i></a>
          <div>
            Servicios
          </div>
        </div>
        <div class="col-md-12 col-md-1">
          <a href="{{url('category/5')}}" class="btn btn-info btn-lg" style="border-radius: 50%;"><i class="fas fa-tachometer-alt"></i></a>
          <div>
            Accesorios para Vehiculos
          </div>
        </div>
        <div class="col-md-12 col-md-1">
          <a href="{{url('category/587')}}" class="btn btn-info btn-lg" style="border-radius: 50%;"><i class="fas fa-desktop"></i></a>
          <div>
            Computación
          </div>
        </div>
        <div class="col-md-12 col-md-1">
          <a href="{{url('category/1057')}}" class="btn btn-info btn-lg" style="border-radius: 50%;"><i class="fas fa-soccer-ball-o"></i></a>
          <div>
            Deportes y Fitness
          </div>
        </div>
        <div class="col-md-12 col-md-1">
          <a href="{{url('category/402')}}" class="btn btn-info btn-lg" style="border-radius: 50%;"><i class="fas fa-mobile-alt"></i></a>
          <div>
            Celulares y Telefonos
          </div>
        </div>
        <div class="col-md-12 col-md-1">
          <a href="{{url('category/1939')}}" class="btn btn-info btn-lg" style="border-radius: 50%;"><i class="fas fa-headphones"></i></a>
          <div>
            Musica y Peliculas
          </div>
        </div>
        <div class="col-md-12 col-md-1">
          <a href="{{url('category/2162')}}" class="btn btn-info btn-lg" style="border-radius: 50%;"><i class="fas fa-tshirt"></i></a>
          <div>
            Ropa y Accesorios
          </div>
        </div>
        <div class="col-md-12 col-md-1">
          <a href="{{url('login')}}" class="btn btn-info btn-lg" style="border-radius: 50%;"><i class="fas fa-plus"></i></a>
          <div>
            Mas Categorias
          </div>
        </div>
      </div>
      <!-- BANNER -->
      <div class="bannercontainer">
        <div class="fullscreenbanner-container">
          <div class="fullscreenbanner">
            <ul>
            @if (count($latest_visited) > 0)
            @foreach ($latest_visited as $pub)
              <li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="700" data-title="Slide 1">
                <img src="img/home/banner-slider/slider-bg.jpg" alt="slidebg1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                <div class="slider-caption container">
                  <div class="tp-caption rs-caption-1 sft start"
                    data-hoffset="0"
                    data-x="370"
                    data-y="54"
                    data-speed="800"
                    data-start="1500"
                    data-easing="Back.easeInOut"
                    data-endspeed="300">
                    <img src="/img/home/banner-slider/cat-{{ $pub->getTopLevelCategory()->id }}.png" alt="slider-image">
                  </div>

                  <div class="tp-caption rs-caption-2 sft"
                    data-hoffset="0"
                    data-y="90"
                    data-speed="800"
                    data-start="2000"
                    data-easing="Back.easeInOut"
                    data-endspeed="300">
                    {{ $pub->getTopLevelCategory()->name }}
                  </div>

                  <div class="tp-caption rs-caption-3 sft"
                    data-hoffset="0"
                    data-y="150"
                    data-speed="1000"
                    data-start="3000"
                    data-easing="Power4.easeOut"
                    data-endspeed="300"
                    data-endeasing="Power1.easeIn"
                    data-captionhidden="off">
                    <small>Últimos Visitados</small>
                  </div>
                  <div class="tp-caption rs-caption-4 sft"
                    data-hoffset="0"
                    data-y="200"
                    data-speed="800"
                    data-start="3500"
                    data-easing="Power4.easeOut"
                    data-endspeed="300"
                    data-endeasing="Power1.easeIn"
                    data-captionhidden="off">

                    <div style="width:180px">
                        <div class="slide">
                          <div class="productImage clearfix">
                            @if (count($pub->images) > 0)
                            <img src="{{ url('/photo/120x120?url='.urlencode($pub->images[0]->name)) }}" alt="{{ $pub->title }}">
                            @else
                            <img src="{{ url('/photo/120x120?url=nophoto.jpg') }}">
                            @endif
                          </div>
                          <div class="productCaption clearfix">
                            <a href="{{ route('product',[$pub->id]) }}"><h5>{{ $pub->title }}</h5></a>
                            @if($pub->price == '0')
                            <a href="{{ route('product',[$pub->id]) }}"><h3 style="text-transform: none;font-size: 16px;">Precio a convenir</h3></a>
                            @else
                            <a href="{{ route('product',[$pub->id]) }}"><h3>S/{{ number_format($pub->price, 2, ',', '.') }}</h3></a>
                            @endif
                          </div>
                        </div>
                    </div>

                  </div>
                </div>
              </li>
            @endforeach
            @else
              <li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="700" data-title="Slide 1">
                <img src="img/home/banner-slider/slider-bg.jpg" alt="slidebg1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                <div class="slider-caption container">
                  <div class="tp-caption rs-caption-1 sft start"
                    data-hoffset="0"
                    data-x="0"
                    data-y="54"
                    data-speed="800"
                    data-start="1500"
                    data-easing="Back.easeInOut"
                    data-endspeed="300">
                    <img src="img/home/banner-slider/main.png" alt="slider-image">
                  </div>
                </div>
              </li>
			  @endif
            </ul>
          </div>
        </div>
      </div>
      <!-- MAIN CONTENT SECTION -->
      <section class="mainContent clearfix">
        <div class="container">
        @if (count($latest_pub) > 0)
          <div class="row featuredProducts">
            <div class="col-xs-12">
              <div class="page-header">
                <h4>Ultimos Publicados en <span>{{ $category->parentCategory->name }} - {{ $category->name }}</span></h4>
              </div>
            </div>
            <div class="col-xs-12">
              <div class="owl-carousel featuredProductsSlider">

                @foreach ($latest_pub as $pub)
                <div class="slide">
                  <div class="productImage clearfix">
                @if (count($pub->images) > 0)
                <img src="{{ url('/photo/120x120?url='.urlencode($pub->images[0]->name)) }}" alt="{{ $pub->title }}">
                @else
                <img src="{{ url('/photo/120x120?url=nophoto.jpg') }}">
                @endif
                <div class="productMasking">
                  <ul class="list-inline btn-group" role="group">
                    <li><a href="{{ ($authenticated? 'javascript:void(0)' : url('favorite', [$pub->id])) }}" class="btn btn-default btn-favorite pub-{{ $pub->id }} {{ ($pub->isFavorite() ? 'btn-favorite-marked':'') }}" data-pub-id="{{ $pub->id }}"><i class="fa fa-heart"></i></a></li>
                  </ul>
                  <a href="{{ route('product',[$pub->id]) }}"><p class="title">{{ $pub->title }}</p></a>
                </div>
              </div>
              <div class="productCaption clearfix">
                @if($pub->price == '0')
                <a href="{{ route('product',[$pub->id]) }}"><h3 style="text-transform: none;font-size: 16px;">Precio a convenir</h3></a>
                @else
                <a href="{{ route('product',[$pub->id]) }}"><h3>S/{{ number_format($pub->price, 2, ',', '.') }}</h3></a>
                @endif
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    @endif


    @if (count($suggestions) > 0)
      <div class="row featuredProducts">
        <div class="col-xs-12">
          <div class="page-header">
                <h4>Recomendaciones en <span>{{ $category->parentCategory->name }} - {{ $category->name }}</span></h4>
            </div>
            </div>
            <div class="col-xs-12">
              <div class="owl-carousel featuredProductsSlider">

                @foreach ($suggestions as $pub)
                <div class="slide">
                  <div class="productImage clearfix">
                    @if (count($pub->images) > 0)
                    <img src="{{ url('/photo/120x120?url='.urlencode($pub->images[0]->name)) }}" alt="{{ $pub->title }}">
                    @else
                    <img src="{{ url('/photo/120x120?url=nophoto.jpg') }}">
                    @endif
                    <div class="productMasking">
                      <ul class="list-inline btn-group" role="group">
                        <li><a href="{{ ($authenticated? 'javascript:void(0)' : url('favorite', [$pub->id])) }}" class="btn btn-default btn-favorite pub-{{ $pub->id }} {{ ($pub->isFavorite() ? 'btn-favorite-marked':'') }}" data-pub-id="{{ $pub->id }}"><i class="fa fa-heart"></i></a></li>
                      </ul>
                      <a href="{{ route('product',[$pub->id]) }}"><p class="title">{{ $pub->title }}</p></a>
                    </div>
                  </div>
                  <div class="productCaption clearfix">
                    @if($pub->price == '0')
                    <a href="{{ route('product',[$pub->id]) }}"><h3 style="text-transform: none;font-size: 16px;">Precio a convenir</h3></a>
                    @else
                    <a href="{{ route('product',[$pub->id]) }}"><h3>S/{{ number_format($pub->price, 2, ',', '.') }}</h3></a>
                    @endif
                  </div>
                </div>
				@endforeach
              </div>
            </div>
          </div>
        @endif

        @if (count($bestsellers) > 0)
          <div class="row featuredProducts">
            <div class="col-xs-12">
              <div class="page-header">
                <h4>Los más vendidos en <span>{{ $toplevel->name }}</span></h4>
              </div>
            </div>
            <div class="col-xs-12">
              <div class="owl-carousel featuredProductsSlider">

                @foreach ($bestsellers as $pub)
                <div class="slide">
                  <div class="productImage clearfix">
                    @if (count($pub->images) > 0)
                    <img src="{{ url('/photo/120x120?url='.urlencode($pub->images[0]->name)) }}" alt="{{ $pub->title }}">
                    @else
                    <img src="{{ url('/photo/120x120?url=nophoto.jpg') }}">
                    @endif
                    <div class="productMasking">
                      <ul class="list-inline btn-group" role="group">
                        <li><a href="{{ ($authenticated? 'javascript:void(0)' : url('favorite', [$pub->id])) }}" class="btn btn-default btn-favorite pub-{{ $pub->id }} {{ ($pub->isFavorite() ? 'btn-favorite-marked':'') }}" data-pub-id="{{ $pub->id }}"><i class="fa fa-heart"></i></a></li>
                      </ul>
                      <a href="{{ route('product',[$pub->id]) }}"><p class="title">{{ $pub->title }}</p></a>
                    </div>
                  </div>
                  <div class="productCaption clearfix">
                    @if($pub->price == '0')
                    <a href="{{ route('product',[$pub->id]) }}"><h3 style="text-transform: none;font-size: 16px;">Precio a convenir</h3></a>
                    @else
                    <a href="{{ route('product',[$pub->id]) }}"><h3>S/{{ number_format($pub->price, 2, ',', '.') }}</h3></a>
                    @endif
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        @endif

          @if (!Auth::check())
          <div class="row">
              <div class="col-xs-12 register-col">
                  <span class="register-text">Si aún no tienes cuenta</span> <button class="btn btn-primary register-link">Registrate Gratis</button>
              </div>
          </div>
          @endif




		  @if (count($latest_pub2) > 0)
          <div class="row featuredProducts">
            <div class="col-xs-12">
              <div class="page-header">
                <h4>Ultimos Publicados en <span>{{ $category2->parentCategory->name }} - {{ $category2->name }}</span></h4>
              </div>
            </div>
            <div class="col-xs-12">
              <div class="owl-carousel featuredProductsSlider">

				@foreach ($latest_pub2 as $pub)
                <div class="slide">
                  <div class="productImage clearfix">
                    @if (count($pub->images) > 0)
                    <img src="{{ url('/photo/120x120?url='.urlencode($pub->images[0]->name)) }}" alt="{{ $pub->title }}">
                    @else
                    <img src="{{ url('/photo/120x120?url=nophoto.jpg') }}">
                    @endif
                    <div class="productMasking">
                      <ul class="list-inline btn-group" role="group">
                        <li><a href="{{ ($authenticated? 'javascript:void(0)' : url('favorite', [$pub->id])) }}" class="btn btn-default btn-favorite pub-{{ $pub->id }} {{ ($pub->isFavorite() ? 'btn-favorite-marked':'') }}" data-pub-id="{{ $pub->id }}"><i class="fa fa-heart"></i></a></li>
                      </ul>
                      <a href="{{ route('product',[$pub->id]) }}"><p class="title">{{ $pub->title }}</p></a>
                    </div>
                  </div>
                  <div class="productCaption clearfix">
                    @if($pub->price == '0')
                    <a href="{{ route('product',[$pub->id]) }}"><h3 style="text-transform: none;font-size: 16px;">Precio a convenir</h3></a>
                    @else
                    <a href="{{ route('product',[$pub->id]) }}"><h3>S/{{ number_format($pub->price, 2, ',', '.') }}</h3></a>
                    @endif
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        @endif


        @if (count($suggestions2) > 0)
          <div class="row featuredProducts">
            <div class="col-xs-12">
              <div class="page-header">
				  <h4>Recomendaciones en <span>{{ $category2->parentCategory->name }} - {{ $category2->name }}</span></h4>
              </div>
            </div>
            <div class="col-xs-12">
              <div class="owl-carousel featuredProductsSlider">

                @foreach ($suggestions2 as $pub)
                <div class="slide">
                  <div class="productImage clearfix">
                    @if (count($pub->images) > 0)
                    <img src="{{ url('/photo/120x120?url='.urlencode($pub->images[0]->name)) }}" alt="{{ $pub->title }}">
                    @else
                    <img src="{{ url('/photo/120x120?url=nophoto.jpg') }}">
                    @endif
                    <div class="productMasking">
                      <ul class="list-inline btn-group" role="group">
                        <li><a href="{{ ($authenticated? 'javascript:void(0)' : url('favorite', [$pub->id])) }}" class="btn btn-default btn-favorite pub-{{ $pub->id }} {{ ($pub->isFavorite() ? 'btn-favorite-marked':'') }}" data-pub-id="{{ $pub->id }}"><i class="fa fa-heart"></i></a></li>
                      </ul>
                      <a href="{{ route('product',[$pub->id]) }}"><p class="title">{{ $pub->title }}</p></a>
                    </div>
                  </div>
                  <div class="productCaption clearfix">
                    @if($pub->price == '0')
                    <a href="{{ route('product',[$pub->id]) }}"><h3 style="text-transform: none;font-size: 16px;">Precio a convenir</h3></a>
                    @else
                    <a href="{{ route('product',[$pub->id]) }}"><h3>S/{{ number_format($pub->price, 2, ',', '.') }}</h3></a>
                    @endif
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        @endif

        @if (count($bestsellers2) > 0)
          <div class="row featuredProducts">
            <div class="col-xs-12">
              <div class="page-header">
                <h4>Los más vendidos en <span>{{ $toplevel2->name }}</span></h4>
              </div>
            </div>
            <div class="col-xs-12">
              <div class="owl-carousel featuredProductsSlider">

                @foreach ($bestsellers2 as $pub)
                <div class="slide">
                  <div class="productImage clearfix">
                    @if (count($pub->images) > 0)
                    <img src="{{ url('/photo/180x180?url='.urlencode($pub->images[0]->name)) }}" alt="{{ $pub->title }}">
                    @else
                    <img src="{{ url('/photo/180x180?url=nophoto.jpg') }}">
                    @endif
                    <div class="productMasking">
                      <ul class="list-inline btn-group" role="group">
                        <li><a href="{{ ($authenticated? 'javascript:void(0)' : url('favorite', [$pub->id])) }}" class="btn btn-default btn-favorite pub-{{ $pub->id }} {{ ($pub->isFavorite() ? 'btn-favorite-marked':'') }}" data-pub-id="{{ $pub->id }}"><i class="fa fa-heart"></i></a></li>
                      </ul>
                      <a href="{{ route('product',[$pub->id]) }}"><p class="title">{{ $pub->title }}</p></a>
                    </div>
                  </div>
                  <div class="productCaption clearfix">
                    @if($pub->price == '0')
                    <a href="{{ route('product',[$pub->id]) }}"><h3 style="text-transform: none;font-size: 16px;">Precio a convenir</h3></a>
                    @else
                    <a href="{{ route('product',[$pub->id]) }}"><h3>S/{{ number_format($pub->price, 2, ',', '.') }}</h3></a>
                    @endif
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        @endif


          <br>
        @if (count($featured) > 0)
          <div class="row featuredProducts">
            <div class="col-xs-12">
              <div class="page-header">
                <h4>Publicaciones destacadas</h4>
              </div>
            </div>
            <div class="col-xs-12">
              <div class="owl-carousel featuredProductsSlider">
                @foreach ($featured as $pub)
                <div class="slide">
                  <div class="productImage clearfix">
                    @if (count($pub->images) > 0)
                    <img src="{{ url('/photo/180x180?url='.urlencode($pub->images[0]->name)) }}" alt="{{ $pub->title }}">
                    @else
                    <img src="{{ url('/photo/180x180?url=nophoto.jpg') }}">
                    @endif
                    <div class="productMasking">
                      <ul class="list-inline btn-group" role="group">
                        <li><a href="{{ ($authenticated? 'javascript:void(0)' : url('favorite', [$pub->id])) }}" class="btn btn-default btn-favorite pub-{{ $pub->id }} {{ ($pub->isFavorite() ? 'btn-favorite-marked':'') }}" data-pub-id="{{ $pub->id }}"><i class="fa fa-heart"></i></a></li>
                      </ul>
                      <a href="{{ route('product',[$pub->id]) }}"><p class="title">{{ $pub->title }}</p></a>
                    </div>
                  </div>
                  <div class="productCaption clearfix">
                    @if($pub->price == '0')
                    <a href="{{ route('product',[$pub->id]) }}"><h3 style="text-transform: none;font-size: 16px;">Precio a convenir</h3></a>
                    @else
                    <a href="{{ route('product',[$pub->id]) }}"><h3>S/{{ number_format($pub->price, 2, ',', '.') }}</h3></a>
                    @endif
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        @endif
        @if (count($featured) > 0)
          <div class="row featuredProducts">
            <div class="col-xs-12">
              <div class="page-header">
                <h4>Productos que te puedan gustar</h4>
              </div>
            </div>
            <div class="col-xs-12">
              <div class="owl-carousel featuredProductsSlider">
                @foreach ($featured as $pub)
                  <div class="slide">
                    <div class="productImage clearfix">
                      @if (count($pub->images) > 0)
                        <img src="{{ url('/photo/180x180?url='.urlencode($pub->images[0]->name)) }}" alt="{{ $pub->title }}">
                      @else
                        <img src="{{ url('/photo/180x180?url=nophoto.jpg') }}">
                      @endif
                      <div class="productMasking">
                        <ul class="list-inline btn-group" role="group">
                          <li><a href="{{ ($authenticated? 'javascript:void(0)' : url('favorite', [$pub->id])) }}" class="btn btn-default btn-favorite pub-{{ $pub->id }} {{ ($pub->isFavorite() ? 'btn-favorite-marked':'') }}" data-pub-id="{{ $pub->id }}"><i class="fa fa-heart"></i></a></li>
                        </ul>
                        <a href="{{ route('product',[$pub->id]) }}"><p class="title">{{ $pub->title }}</p></a>
                      </div>
                    </div>
                    <div class="productCaption clearfix">
                      @if($pub->price == '0')
                        <a href="{{ route('product',[$pub->id]) }}"><h3 style="text-transform: none;font-size: 16px;">Precio a convenir</h3></a>
                      @else
                        <a href="{{ route('product',[$pub->id]) }}"><h3>S/{{ number_format($pub->price, 2, ',', '.') }}</h3></a>
                      @endif
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          </div>
          @endif
        </div>
      </section>


      <!-- LIGHT SECTION -->
      <section class="lightSection clearfix">

          <div class="container">
              <div class="row">

                  <div class="col-xs-12">


                      @if (count($latest_visited) > 0)
                          <div class="row featuredProducts">
                              <div class="col-xs-12">
                                  <div class="page-header">
                                      <h4>Tu Historial</h4>
                                  </div>
                              </div>
                              <div class="col-xs-12">
                                  <div class="owl-carousel featuredProductsSlider">
                                      @foreach ($latest_visited as $pub)
                                          <div class="slide">
                                              <div class="productImage clearfix">
                                                  @if (count($pub->images) > 0)
                                                      <img src="{{ url('/photo/180x180?url='.urlencode($pub->images[0]->name)) }}" alt="{{ $pub->title }}">
                                                  @else
                                                      <img src="{{ url('/photo/180x180?url=nophoto.jpg') }}">
                                                  @endif
                                                  <div class="productMasking">
                                                      <ul class="list-inline btn-group" role="group">
                                                          <li><a href="{{ ($authenticated? 'javascript:void(0)' : url('favorite', [$pub->id])) }}" class="btn btn-default btn-favorite pub-{{ $pub->id }} {{ ($pub->isFavorite() ? 'btn-favorite-marked':'') }}" data-pub-id="{{ $pub->id }}"><i class="fa fa-heart"></i></a></li>
                                                      </ul>
                                                      <a href="{{ route('product',[$pub->id]) }}"><p class="title">{{ $pub->title }}</p></a>
                                                  </div>
                                              </div>
                                              <div class="productCaption clearfix">
                                                  @if($pub->price == '0')
                                                      <a href="{{ route('product',[$pub->id]) }}"><h3 style="text-transform: none;font-size: 16px;">Precio a convenir</h3></a>
                                                  @else
                                                      <a href="{{ route('product',[$pub->id]) }}"><h3>S/{{ number_format($pub->price, 2, ',', '.') }}</h3></a>
                                                  @endif
                                              </div>
                                          </div>
                                      @endforeach
                                  </div>
                              </div>
                          </div>
                      @endif


                      <div class="owl-carousel partnersLogoSlider">
                          <div class="slide">
                              <div class="partnersLogo clearfix">
                                  <img src="/img/home/partners/partner-01.png" alt="partner-img">
                              </div>
                          </div>
                          <div class="slide">
                              <div class="partnersLogo clearfix">
                                  <img src="/img/home/partners/partner-02.png" alt="partner-img">
                              </div>
                          </div>
                          <div class="slide">
                              <div class="partnersLogo clearfix">
                                  <img src="/img/home/partners/partner-03.png" alt="partner-img">
                              </div>
                          </div>
                          <div class="slide">
                              <div class="partnersLogo clearfix">
                                  <img src="/img/home/partners/partner-04.png" alt="partner-img">
                              </div>
                          </div>
                          <div class="slide">
                              <div class="partnersLogo clearfix">
                                  <img src="/img/home/partners/partner-05.png" alt="partner-img">
                              </div>
                          </div>
                          <div class="slide">
                              <div class="partnersLogo clearfix">
                                  <img src="/img/home/partners/partner-01.png" alt="partner-img">
                              </div>
                          </div>
                          <div class="slide">
                              <div class="partnersLogo clearfix">
                                  <img src="/img/home/partners/partner-02.png" alt="partner-img">
                              </div>
                          </div>
                          <div class="slide">
                              <div class="partnersLogo clearfix">
                                  <img src="/img/home/partners/partner-03.png" alt="partner-img">
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>

      <!-- FOOTER -->
      <div class="footer clearfix">
          <div class="container">
              <div class="row">
                  <div class="col-sm-3 col-xs-12">
                      <div class="footerLink">
                          <h5>Ayuda</h5>
                          <ul class="list-unstyled">
                              <li><a href="{{ url('help?id=1') }}">Comprar</a></li>
                              <li><a href="{{ url('help?id=2') }}">Vender</a></li>
                              <li><a href="{{ url('help?id=3') }}">Cuenta</a></li>
                              <li><a href="{{ url('help?id=4') }}">Seguridad</a></li>
                          </ul>
                      </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                      <div class="footerLink">
                          <h5>Acerca de</h5>
                          <ul class="list-unstyled">
                              <li><a href="#">MercadoPlanet</a></li>
                              <li><a href="#">Términos y Condiciones</a></li>
                              <li><a href="{{ url('/privacy')}}">Políticas  de Privacidad</a></li>
                          </ul>
                      </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                      <div class="footerLink">
                          <h5>Mi cuenta</h5>
                          <ul class="list-unstyled">
                              @if(!Auth::check())
                                  <li><a href="{{ url('/register') }}">Regístrate</a></li>
                                  <li><a href="{{ url('/login') }}">Ingresar</a></li>
                              @endif
                              <li><a href="{{ url('/sell') }}">Vender</a></li>
                          </ul>
                      </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                      <div class="footerLink">
                          <h5>Contacto</h5>
                          <ul class="list-unstyled">
                              <li><a href="mailto:contacto@mercadoplanet.com">contacto@mercadoplanet.com</a></li>
                          </ul>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <!-- COPY RIGHT -->
      <div class="copyRight clearfix">
          <div class="container">
              <div class="row">
                  <div class="col-xs-12">
                      <p>&copy; 2016 Copyright MercadoPlanet</p>
                  </div>
              </div>
          </div>
      </div>
      </div>



@endsection

@section('scripts')
	@parent
	<script>
		$(document).ready(function() {
			$('.register-link').click(function() {
				location.href = '{{ url('/register') }}';
			});

            $(".btn-favorite").click(function() {
                var anchor = $(this);
                $.ajax({
                    method: "POST",
                    url: "{{url('/favorite')}}",
                    cache: false,
                    data: { id: anchor.attr('data-pub-id'), _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
                    if(msg.is_ok) {
                        if(msg.value) {
                            $(".pub-"+anchor.attr('data-pub-id')).addClass('btn-favorite-marked');
                        } else {
                            $(".pub-"+anchor.attr('data-pub-id')).removeClass('btn-favorite-marked');
                        }
                    }
                });
            });
		});
	</script>
@endsection

@extends('layouts.app')

@section('title', 'Listado')
@section('search', $search)

@section('content')


      <!-- LIGHT SECTION -->
      <section class="lightSection clearfix pageHeader">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <ol class="breadcrumb">
                  <li><a href="{{ route('home') }}">Inicio</a></li>
                @foreach ($breadcrumb as $bc)
                <li>
                  <a href="{{ route('list', array_merge($query, ['category' => $bc->id, 'search' => null])) }}">{{ $bc->name }}</a>
                </li>
                @endforeach
              </ol>
            </div>
          </div>
        </div>
      </section>

      <!-- MAIN CONTENT SECTION -->
      <section class="mainContent clearfix productsContent">
        <div class="container">
          @if(count($pubs))
          <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12 sideBar">
              @if (count($filters))
              <strong>Filtrado por:</strong>
              @foreach ($filters as $filter => $value)
              <div class="alert alert-filter alert-dismissible" role="alert">
                  <a href="{{ route('list', array_merge($query, [$filter => null])) }}"><button type="button" class="close"  aria-label="Close"><span aria-hidden="true">&times;</span></button></a>
                {{ ucwords(strtolower($value->name)) }}
              </div>
              @endforeach
              @endif

              @if (count($sub_cats) > 0)
              <div class="panel panel-default">
                <div class="panel-heading">Categorias</div>
                <div class="panel-body">
                  <div class="collapse navbar-collapse navbar-ex1-collapse navbar-side-collapse">
                    <ul class="nav navbar-nav side-nav">
                      <li>
                        <ul id="marca" class="collapseItem">
                          @foreach($sub_cats as $cat)
                          <li><a href="{{ route('list', array_merge($query, ['category' => $cat->id])) }}"><i class="fa fa-caret-right" aria-hidden="true"></i>{{ $cat->name }} <span>({{ $cat->num }})</span></a></li>
                          @endforeach
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              @endif
              @if (count($group_features) > 0)
              <?php $count = 0; ?>
              @foreach($group_features as $gf)
              <?php $count++; ?>
              @if (!array_key_exists ('feature'.$count , $query))
              <div class="panel panel-default">
                <div class="panel-heading">{{ $gf->name }}</div>
                <div class="panel-body">
                  <div class="collapse navbar-collapse navbar-ex1-collapse navbar-side-collapse">
                    <ul class="nav navbar-nav side-nav">
                      <li>
                        <ul id="marca" class="collapseItem">
                          @foreach(${'count_features'.$count} as $f)
                          <li><a href="{{ route('list', array_merge($query, ['feature'.$count => $f->id])) }}"><i class="fa fa-caret-right" aria-hidden="true"></i>{{ $f->name }} <span>({{ $f->num }})</span></a></li>
                          @endforeach
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              @endif
              @endforeach
              @endif
              @if (!array_key_exists ('condition' , $query) and $count_conditions['new'] > 0 and $count_conditions['used'] > 0)
              <div class="panel panel-default">
                <div class="panel-heading">Estado</div>
                <div class="panel-body">
                  <div class="collapse navbar-collapse navbar-ex1-collapse navbar-side-collapse">
                    <ul class="nav navbar-nav side-nav">
                      <li>
                        <ul id="marca" class="collapseItem">
                          <li><a href="{{ route('list', array_merge($query, ['condition' => 'new'])) }}"><i class="fa fa-caret-right" aria-hidden="true"></i>Nuevo <span>({{ $count_conditions['new'] }})</span></a></li>
                          <li><a href="{{ route('list', array_merge($query, ['condition' => 'used'])) }}"><i class="fa fa-caret-right" aria-hidden="true"></i>Usado <span>({{ $count_conditions['used'] }})</span></a></li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              @endif
              @if (!array_key_exists ('location' , $query) and count($count_locations) > 0)
              <div class="panel panel-default">
                <div class="panel-heading">Ubicación</div>
                <div class="panel-body">
                  <div class="collapse navbar-collapse navbar-ex1-collapse navbar-side-collapse">
                    <ul class="nav navbar-nav side-nav">
                      <li>
                        <ul id="marca" class="collapseItem">
                          @foreach($count_locations as $loc)
                          <li><a href="{{ route('list', array_merge($query, ['location' => $loc->id])) }}"><i class="fa fa-caret-right" aria-hidden="true"></i>{{ ucwords(strtolower($loc->name)) }} <span>({{ $loc->num }})</span></a></li>
                          @endforeach
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              @endif
              <div class="panel panel-default priceRange">
                <div class="panel-heading">Rango de precios</div>
                <div class="panel-body clearfix">
                    <div id="pricerange">
                        <input type="text" id="price-min" placeholder="Mín." class="form-control form-control-price" value="{{ $min }}">
                    -
                    <input type="text" id="price-max" placeholder="Máx." class="form-control form-control-price" value="{{ $max }}">
                    </div>
                    <button class="btn btn-default" id="price-filter-button">Filtrar</button>
                </div>
              </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
              <div class="row filterArea">
                <div class="col-xs-6">
                    <select name="sort-box" id="sort-box" class="select-drop">
                    <option value="relevance" {{ ($sort=='relevance' ? 'selected':'') }}>Más relevante</option>
                    <option value="lowtohigh" {{ ($sort=='lowtohigh' ? 'selected':'') }}>Menor precio</option>
                    <option value="hightolow" {{ ($sort=='hightolow' ? 'selected':'') }}>Mayor precio</option>
                  </select>
                </div>
                <div class="col-xs-6">
                  <div class="btn-group pull-right" role="group">
                    <button type="button" class="btn btn-default {{ ($view!='grid' ? 'active':'') }}" onclick="window.location.href='{{ route('list', array_merge($query, ['view' => 'list'])) }}'"><i class="fa fa-th-list" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-default {{ ($view=='grid' ? 'active':'') }}" onclick="window.location.href='{{ route('list', array_merge($query, ['view' => 'grid'])) }}'"><i class="fa fa-th" aria-hidden="true"></i></button>
                  </div>
                </div>
              </div>
              @if ($view != 'grid')
              <div class="row productListSingle">
                @foreach ($pubs as $pub)
                <div class="col-xs-12">
                  <div class="media">
                    <div class="media-left">
                    @if (count($pub->images) > 0)
                    <img src="{{ url('/photo/150x150?url='.urlencode($pub->images[0]->name)) }}" alt="{{ $pub->title }}">
                    @else
                    <img src="{{ url('/photo/150x150?url=nophoto.jpg') }}">
                    @endif
                    </div>
                    <div class="media-body">
                      @if($pub->topLevelCategory()->id == 4)
                      <h4 class="media-heading"><a href="{{ route('product',[$pub->id]) }}">{{ $pub->title }}</a></h4>
                      <p>Artículo {{ $pub->condition }} / {{ $pub->getSales() }} vendidos / {{ $pub->location() }}
                          
                      </p>
                      @endif
                      @if($pub->topLevelCategory()->id == 1)
                      <h4 class="media-heading">
                        <a href="{{ route('product',[$pub->id]) }}">{{ $pub->category->parentCategory->name }}  {{ $pub->category->name }} {{ $pub->title }}</a>
                      </h4>
                      <p>{{ $pub->vehicle_year }} / @if($pub->vehicle_kilometers) {{ number_format($pub->vehicle_kilometers,0,',','.') }} Km / @endif {{ $pub->vehicleFullLocation() }}
                      </p>
                      @endif
                      @if($pub->topLevelCategory()->id == 2)
                      <h4 class="media-heading">
                        <a href="{{ route('product',[$pub->id]) }}">{{ $pub->category->parentCategory->name }} en {{ $pub->category->name }} {{ $pub->title }}</a>
                      </h4>
                      <p>{{ $pub->category->parentCategory->name }} en {{ $pub->category->name }} / {{ $pub->vehicleFullLocation() }} /
                        @if($pub->property_construction_area){{ $pub->property_construction_area }} m2 @else {{ $pub->property_land_area }} m2 @endif
                      </p>
                      @endif
                      @if($pub->topLevelCategory()->id == 3)
                      <h4 class="media-heading"><a href="{{ route('product',[$pub->id]) }}">{{ $pub->title }}</a></h4>
                      <p>{{ $pub->vehicleFullLocation() }}
                          
                      </p>
                      @endif
                      <div class="btn-group" role="group">
                        <button type="button" onclick="{{ ($authenticated? 'javascript:void(0))' : "javascript:window.location='".url('favorite', [$pub->id])."'") }}" class="btn btn-default btn-favorite pub-{{ $pub->id }} {{ ($pub->isFavorite() ? 'btn-favorite-marked':'') }}" data-pub-id="{{ $pub->id }}"><i class="fa fa-heart" aria-hidden="true"></i></button>
                      </div>
                      @if($pub->price == '0')
                      <h3 style="text-transform: none;font-size: 16px;">Precio a convenir</h3>
                      @else
                      <h3>S/{{ number_format($pub->price, 2, ',', '.') }}</h3>
                      @endif
                    </div>
                  </div>
                </div>
                @endforeach
              </div>

              @else
              <div class="row">
                @foreach ($pubs as $key => $pub)
                @if ($key > 0 and $key % 3 == 0) <div class="row"> @endif
                <div class="col-sm-4 col-xs-12">
                  <div class="productBox">
                    <div class="productImage clearfix">
                    @if (count($pub->images) > 0)
                    <img src="{{ url('/photo/250x250?url='.urlencode($pub->images[0]->name)) }}" alt="{{ $pub->title }}">
                    @else
                    <img src="{{ url('/photo/250x250?url=nophoto.jpg') }}">
                    @endif
                      <div class="productMasking">
                        <ul class="list-inline btn-group" role="group">
                          <li><a href="{{ ($authenticated? 'javascript:void(0))' : url('favorite', [$pub->id])) }}" class="btn btn-default btn-favorite pub-{{ $pub->id }} {{ ($pub->isFavorite() ? 'btn-favorite-marked':'') }}" data-pub-id="{{ $pub->id }}"><i class="fa fa-heart"></i></a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="productCaption clearfix">
                      @if($pub->topLevelCategory()->id == 1)
                        <a href="{{ route('product',[$pub->id]) }}">
                          <h5>{{ $pub->category->parentCategory->name }} {{ $pub->category->name }} {{ $pub->title }}</h5>
                        </a>
                       {{ $pub->vehicle_year }} / @if($pub->vehicle_kilometers) {{ number_format($pub->vehicle_kilometers,0,',','.') }} Km / @endif{{ $pub->vehicleFullLocation() }}
                      @endif
                      @if($pub->topLevelCategory()->id == 2)
                        <a href="{{ route('product',[$pub->id]) }}">
                          <h5>{{ $pub->category->parentCategory->name }} en {{ $pub->category->name }} {{ $pub->title }}</h5>
                        </a>
                      {{ $pub->category->parentCategory->name }} en {{ $pub->category->name }} / {{ $pub->vehicleFullLocation() }} /
                        @if($pub->property_construction_area){{ $pub->property_construction_area }} m2 @else {{ $pub->property_land_area }} m2 @endif
                      @endif
                      
                      @if($pub->topLevelCategory()->id == 3)
                        <a href="{{ route('product',[$pub->id]) }}">
                          <h5>{{ $pub->title }}</h5>
                        </a>
                      {{ $pub->vehicleFullLocation() }}
                      @endif
                      
                      @if($pub->topLevelCategory()->id == 4)
                        <a href="{{ route('product',[$pub->id]) }}">
                          <h5>{{ $pub->title }}</h5>
                        </a>
                      Artículo {{ $pub->condition }} / {{ $pub->getSales() }} vendidos / {{ $pub->location() }}
                      @endif
                      
                      @if($pub->price == '0')
                      <h3 style="text-transform: none;font-size: 16px;">Precio a convenir</h3>
                      @else
                      <h3>S/{{ number_format($pub->price, 2, ',', '.') }}</h3>
                      @endif
                    </div>
                  </div>
                </div>
                @if ($key !=  (count($pubs)-1) and ($key+1) % 3 == 0) </div> @endif
                @endforeach
              </div>

              @endif

            </div>

            <div class="col-md-offset-3 col-sm-offset-4 col-md-9 col-sm-8 col-xs-12">
              <div class="text-center">
                  {{ $pubs->appends($query)->links() }}
              </div>
            </div>

          </div>
        </div>
          @else
          <div class="row">
              <div class="col-xs-12">
                <div class="alert alert-warning" role="alert">
                    <strong>No hay publicaciones que coincidan con tu búsqueda</strong>
                    <ul id="list-no-items">
                        <li>Revisa la ortografía de las palabras de tu búsqueda.</li>
                        <li>Intenta utílizar otras palabras o menos palabras para tu búsqueda.</li>
                        <li>Además puedes intentar navegar a través de las categorías para encontrar lo que buscas.</li>
                    </ul>
                </div>
              </div>
          </div>
          @endif
      </section>

@endsection

@section('scripts')
	@parent
    <script>
        var getUrlParameters = function getUrlParameters() {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            var params = new Object();
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                params[sParameterName[0]] = sParameterName[1];
            }
            return params;
        };
        $(document).ready(function() {
            $('#sort-box').change(function() {
                if($(this).val() == 'relevance') {
                   $(location).attr('href', "{!! route('list', array_merge($query, ['sort' => 'relevance'])) !!}");
                }
                if($(this).val() == 'lowtohigh') {
                   $(location).attr('href', "{!! route('list', array_merge($query, ['sort' => 'lowtohigh'])) !!}");
                }
                if($(this).val() == 'hightolow') {
                   $(location).attr('href', "{!! route('list', array_merge($query, ['sort' => 'hightolow'])) !!}");
                }
            });

            $(".form-control-price").bind('keypress', function (e) {
                return !(e.which != 8 && e.which != 0 &&
                        (e.which < 48 || e.which > 57));
            });
            var urlParams = getUrlParameters();
            $('#price-filter-button').click(function() {
                var urlPath = "{!! route('list') !!}";

                priceValues = Object;
                if($("#price-min").val()!='') { priceValues.min = $("#price-min").val(); }
                if($("#price-max").val()!='') { priceValues.max = $("#price-max").val(); }
                if(parseInt(priceValues.min) > parseInt(priceValues.max)) {
                    var aux = priceValues.min;
                    priceValues.min = priceValues.max;
                    priceValues.max = aux;
                }
                delete urlParams['min'];
                delete urlParams['max'];
                for (var v in priceValues){
                    if (priceValues.hasOwnProperty(v)) {
                         urlParams[v] = priceValues[v];
                    }
                }
                //urlParams = Object.assign(urlParams, priceValues);
                window.location=urlPath+'?'+$.param(urlParams);
            });



            $(".btn-favorite").click(function() {
                var anchor = $(this);
                $.ajax({
                    method: "POST",
                    url: "{{url('/favorite')}}",
                    cache: false,
                    data: { id: anchor.attr('data-pub-id'), _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
                    if(msg.is_ok) {
                        if(msg.value) {
                            $(".pub-"+anchor.attr('data-pub-id')).addClass('btn-favorite-marked');
                        } else {
                            $(".pub-"+anchor.attr('data-pub-id')).removeClass('btn-favorite-marked');
                        }
                    }
                });
            });
        });
    </script>
@endsection

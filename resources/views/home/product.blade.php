@extends('layouts.app')

@section('title', 'Listado')

@section('content')


      <!-- LIGHT SECTION -->
      <section class="lightSection clearfix pageHeader">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <ol class="breadcrumb">
                  <li><a href="{{ route('home') }}">Inicio</a></li>
                @foreach ($breadcrumb as $bc)
                <li>
                  <a href="{{ route('list', ['category' => $bc->id]) }}">{{ $bc->name }}</a>
                </li>
                @endforeach
              </ol>
            </div>
          </div>
        </div>
      </section>

      <!-- MAIN CONTENT SECTION -->
      <section class="mainContent clearfix">
        <div class="container">
          <div class="row singleProduct">
            <div class="col-xs-12">
              <div class="media">
                <div class="media-left productSlider">
                  <div id="carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <?php $youtubeSlideThumb = 0; ?>
                        @if (count($images))
                            <?php $count = 0; ?>
                            @foreach ($images as $img)
                                @if($count == 0)
                                <div class="item active" data-thumb="{{ floor(($count)/4) }}">
                                @else
                                <div class="item" data-thumb="{{ floor(($count)/4) }}">
                                @endif
                                    <img src="{{ url('/photo/459x459?url='.urlencode($img->name)) }}">
                                </div>
                            <?php $count++; ?>
                            <?php $youtubeSlideThumb += $count; ?>
                            @endforeach
                                        @if(!is_null($youtubeUrl))
                                            <div class="item" data-thumb="{{$youtubeSlideThumb-1}}">
                                                <iframe width="459" height="459"
                                                        src="{{$youtubeUrl}}">
                                                </iframe>
                                            </div>
                                        @endif
                        @else
                        <div class="item active" data-thumb="0">
                            <img src="{{ url('/photo/459x459?url=nophoto.jpg') }}">
                        </div>
                        @endif
                    </div>
                  </div>
                  <div class="clearfix">
                    <div id="thumbcarousel" class="carousel slide" data-interval="false">
                      <div class="carousel-inner">
                        <?php $youtubeSlider = 0; ?>
                        @if (count($images))
                        <?php $count = 0; ?>
                        <div class="item active">
                        @foreach ($images as $img)
                            @if ($count > 0 and $count % 4 == 0)
                            <div class="item">
                            @endif
                            <div data-target="#carousel" data-slide-to="{{  $count }}" class="thumb">
                                <img src="{{ url('/photo/459x459?url='.urlencode($img->name)) }}">
                            </div>
                            @if (($count+1) != count($publication->images) and ($count+1) % 4 == 0)
                            </div>
                            @endif
                        <?php $count++; ?>

                        <?php $youtubeSlider += $count; ?>

                        @endforeach
                            @if(!is_null($youtubeUrl))
                                    <div data-target="#carousel" data-slide-to="{{$youtubeSlider-1}}" class="thumb">
                                        <img src="{{ asset('img/products/youtube.jpeg') }}">
                                    </div>
                            @endif
                        </div>
                        @else
                        <div data-target="#carousel" data-slide-to="0" class="thumb">
                            <img src="{{ url('/photo/459x459?url=nophoto.jpg') }}">
                        </div>
                        @endif
                      </div>
                      <a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                      </a>
                      <a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="media-body">
                  <ul class="list-inline">
                    <li><a href="{{ route('list', ['category' => $breadcrumb[count($breadcrumb)-1]->id]) }}"><i class="fa fa-reply" aria-hidden="true"></i>Volver al listado</a></li>
                  </ul>
                  <!-- si es vehiculo -->
                  @if ($toplevel->parent_id == '1')
                  <h2>
                      {{ $publication->title }} <small style="color: #999999;font-size: 14px;">#{{ $publication->id }}</small>
                  </h2>
                  <h3>S/{{ number_format($publication->price, 2, ',', '.') }}</h3>
                  <div class="col-xs-12">
                    <span class="year-km-text">{{ $publication->vehicle_year }}</span>
                    @if($publication->vehicle_kilometers != 0)|
                    <span class="year-km-text">{{ number_format($publication->vehicle_kilometers, 0, ',', '.') }} Km</span>
                    @endif
                    <br>
                    <small style="color:#999">{{ $publication->vehicleFullLocation() }}</small>
                  </div>
                  @endif
                  <!-- fin si es vehiculo -->
                  @if ($toplevel->parent_id == '2')
                  <h2>
                      {{ $publication->category->parentCategory->name }} en {{ $publication->category->name }} {{ $publication->title }} <small style="color: #999999;font-size: 14px;">#{{ $publication->id }}</small>
                  </h2>
                  <h3>S/{{ number_format($publication->price, 2, ',', '.') }}</h3>
                  <div>
                    <span style="color:#666">{{ $publication->vehicleFullLocation() }}</span>
                    @if(!empty($publication->address_street))
                    <br>
                    <span style="color:#666"><strong>Calle:</strong> {{ $publication->address_street }}</span>
                    @endif
                    @if(!empty($publication->address_number))
                    <br>
                    <span style="color:#666"><strong>Número:</strong> {{ $publication->address_number }}</span>
                    @endif
                  </div>
                  @endif
                  @if ($toplevel->parent_id == '3')
                  <h2>
                      {{ $publication->title }} <small style="color: #999999;font-size: 14px;">#{{ $publication->id }}</small>
                  </h2>                  
                    @if($publication->price == '0')
                    <h3 style="text-transform: none;font-size: 16px;">Precio a convenir</h3>
                    @else
                    <h3>S/{{ number_format($publication->price,2,',','.') }}</h3>
                    @endif
                    <div>
                    <span style="color:#666">{{ $publication->vehicleFullLocation() }}</span>
                    @if(!empty($publication->address_street))
                    <br>
                    <span style="color:#666"><strong>Calle:</strong> {{ $publication->address_street }}</span>
                    @endif
                    @if(!empty($publication->address_number))
                    <br>
                    <span style="color:#666"><strong>Número:</strong> {{ $publication->address_number }}</span>
                    @endif
                  </div>
                  @endif
                  @if ($toplevel->parent_id == '1' or $toplevel->parent_id == '2' or $toplevel->parent_id == '3')
                  @if ($publication->status == 'Active')
                  <div class="col-xs-12" style="margin-bottom: 10px">
                    <div class="ad-contact-container">
                      <div><strong>Anunciante: </strong>{{ $publication->user->getFullName() }}</div>
                      <div style="margin-top:5px"><strong><i class="fa fa-phone-square"></i> </strong>{{ $publication->contact_phone }}</div>
                      <div>
                        <div id="consultation-message"></div>
                        <textarea class="form-control ad-contact" id="message"></textarea>
                      </div>
                    </div>
                  </div>
                  @endif
                  <div class="col-xs-12">
                    <div class="btn-area">
                        @if ($publication->status == 'Active')
                        @if(Auth::check())
                        @if($publication->user_id != Auth::user()->id)
                        <a href="javascript:void(0)"  id="btn-consultation" class="btn btn-primary  btn-block">Consultar <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        @endif
                        @else
                        <a href="javascript:void(0)"  class="btn btn-primary btn-block" disabled>Consultar <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        <a href="{{ url('login') }}"  class="btn btn-primary btn-block">Iniciar Sesión</a>
                        @endif
                        @if(Auth::check() &&$publication->user_id != Auth::user()->id)
                        <a href="{{ ($authenticated? 'javascript:void(0))' : url('favorite', [$publication->id])) }}" class="btn-favorite pub-{{ $publication->id }} {{ ($publication->isFavorite() ? 'btn-favorite-marked':'') }}" data-pub-id="{{ $publication->id }}"><i class="fa fa-heart fa-product" data-toggle="tooltip" data-placement="bottom" title="Añadir a favoritos"></i></a>
                        @endif
                        @else
                        <div class="alert alert-info" role="alert">Publicación
                          @if($publication->status == 'Canceled') Cancelada @endif
                          @if($publication->status == 'Paused') Pausada @endif
                          @if($publication->status == 'Finished') Finalizada @endif
                          @if($publication->status == 'Revision') en Revisión @endif
                        </div>
                        @endif
                    </div>
                    <div>
                        <p><i class="fa fa-shield fa-product-shield"></i> Tu compra está protegida. Ver condiciones</p>
                    </div>
                  </div>
                  @endif
                  <!-- si es un producto -->
                  @if ($toplevel->parent_id == '4')
                  <h2>
                      {{ $publication->title }} <small style="color: #999999;font-size: 14px;">#{{ $publication->id }}</small>
                  </h2>
                  <h3>S/{{ number_format($publication->price, 2, ',', '.') }}</h3>
                  <div class="col-md-6">
                    <p class="product-payment-method-p"><i class="fa fa-credit-card-alt fa-credit-card-product"></i>Métodos de pagos<br>
                    Transferencia o deposito
                    </p>
                    <p>
                        <i class="fa fa-tag" style="color:#797979;font-size: 16px;"></i> {{ $publication->condition }} - <i class="fa fa-shopping-cart" style="color:#797979;font-size: 16px;"></i> {{ $publication->getSales() }} Vendidos
                    </p>
                  </div>
                  <div class="col-md-6">
                  <p>
                      <i class="fa fa-user" style="color:#797979;font-size: 16px;"></i> Vendedor
                      <br>
                      <span>
                          {!! compute_stars($reputation['value']) !!}
                      </span><br>
                      {{ $publication->location() }}<br>
                      <a href="javascript:void(0)" id="go-shipping"><i class="fa fa-truck"  style="color:#797979;font-size: 16px;"></i> Ver métodos de envío</a>
                  </p>
                  </div>
                  <div class="col-xs-12">
                    <div class="btn-area">
                        @if ($publication->status == 'Active')
                        <input type="text" id="spinner" class="form-control spinner"  value="1" onblur="javascript: if($(this).val()=='' || $(this).val() < 1) $(this).val('1');">
                        @if(Auth::check())
                        @if($publication->user_id != Auth::user()->id)
                        <a href="#" data-toggle="modal" id="buynow" data-target="#mod-confirm-modal" class="btn btn-primary btn-block">Comprar <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        @endif
                        @else
                        <a href="{{ url('login') }}"  class="btn btn-primary btn-block">Iniciar Sesión</a>
                        @endif
                        @if(Auth::check() &&$publication->user_id != Auth::user()->id)
                        <a href="{{ ($authenticated? 'javascript:void(0))' : url('favorite', [$publication->id])) }}" class="btn-favorite pub-{{ $publication->id }} {{ ($publication->isFavorite() ? 'btn-favorite-marked':'') }}" data-pub-id="{{ $publication->id }}"><i class="fa fa-heart fa-product" data-toggle="tooltip" data-placement="bottom" title="Añadir a favoritos"></i></a>
                        <a href="javascript: void(0);" id="go-questions"><i class="fa fa-comments fa-product" data-toggle="tooltip" data-placement="bottom" title="Realizar una pregunta"></i></a>
                        @endif
                        @else
                        <div class="alert alert-info" role="alert">Publicación
                          @if($publication->status == 'Canceled') Cancelada @endif
                          @if($publication->status == 'Paused') Pausada @endif
                          @if($publication->status == 'Finished') Finalizada @endif
                          @if($publication->status == 'Revision') en Revisión @endif
                        </div>
                        @endif
                    </div>
                    <div>
                        <p><i class="fa fa-shield fa-product-shield"></i> Tu compra está protegida. Ver condiciones</p>
                    </div>
                  </div>
                  @endif
                  <!-- fin si es producto -->

                </div>
              </div>
            </div>
          </div>
          @if ($toplevel->parent_id == '4')
          <div class="row">
              <div class="col-xs-12">
                  <h3 style="text-align:center">Información del Vendedor</h3>
                  @include('account.reputation_details')
                  <div style="text-align: center"><a href="{{ route('reputationseller', ['user_id' => $user->id]) }}" class="btn btn-link">Má información del vendedor</a></div>
              </div>
          </div>
            <br>
            <br>
          @endif
          @if ($toplevel->parent_id == '1')
          <div class="row properties-container">
              @if($publication->vehicle_year)
              <div class="col-md-3">
                <div class="property-name">Año</div>
                <div class="property-value">@if($publication->vehicle_year){{ $publication->vehicle_year}}@else - @endif</div>
              </div>
              @endif
              @if($publication->vehicle_kilometers)
              <div class="col-md-3">
                <div class="property-name">Kilómetros</div>
                <div class="property-value">@if($publication->vehicle_kilometers){{ $publication->vehicle_kilometers}}@else - @endif</div>
              </div>
              @endif
              @if($publication->vehicle_doors)
              <div class="col-md-3">
                <div class="property-name">Puertas</div>
                <div class="property-value">@if($publication->vehicle_doors){{ $publication->vehicle_doors}}@else - @endif</div>
              </div>
              @endif
              @if($publication->vehicle_type)
              <div class="col-md-3">
                <div class="property-name">Tipo</div>
                <div class="property-value">@if($publication->vehicle_type){{ $publication->vehicle_type}}@else - @endif</div>
              </div>
              @endif
              @if($publication->vehicle_brand)
              <div class="col-md-3">
                <div class="property-name">Marca</div>
                <div class="property-value">@if($publication->vehicle_brand){{ $publication->vehicle_brand}}@else - @endif</div>
              </div>
              @endif
              @if($publication->vehicle_model)
              <div class="col-md-3">
                <div class="property-name">Modelo</div>
                <div class="property-value">@if($publication->vehicle_model){{ $publication->vehicle_model}}@else - @endif</div>
              </div>
              @endif
              @if($publication->vehicle_version)
              <div class="col-md-3">
                <div class="property-name">Versión</div>
                <div class="property-value">@if($publication->vehicle_version){{ $publication->vehicle_version}}@else - @endif</div>
              </div>
              @endif
              @if($publication->vehicle_color)
              <div class="col-md-3">
                <div class="property-name">Color</div>
                <div class="property-value">@if($publication->vehicle_color){{ $publication->vehicle_color}}@else - @endif</div>
              </div>
              @endif
              @if($publication->vehicle_fuel)
              <div class="col-md-3">
                <div class="property-name">Combustible</div>
                <div class="property-value">@if($publication->vehicle_fuel){{ $publication->vehicle_fuel}}@else - @endif</div>
              </div>
              @endif
              @if($publication->vehicle_steering)
              <div class="col-md-3">
                <div class="property-name">Dirección</div>
                <div class="property-value">@if($publication->vehicle_steering){{ $publication->vehicle_steering}}@else - @endif</div>
              </div>
              @endif
              @if($publication->vehicle_transmission)
              <div class="col-md-3">
                <div class="property-name">Transmisión</div>
                <div class="property-value">@if($publication->vehicle_transmission){{ $publication->vehicle_transmission}}@else - @endif</div>
              </div>
              @endif
              @if($publication->vehicle_owner)
              <div class="col-md-3">
                <div class="property-name">Único dueño</div>
                <div class="property-value">@if($publication->vehicle_owner){{ $publication->vehicle_owner}}@else - @endif</div>
              </div>
              @endif
              @if($publication->vehicle_schedule)
              <div class="col-md-3">
                <div class="property-name">Horario de contacto</div>
                <div class="property-value">@if($publication->vehicle_schedule){{ $publication->vehicle_schedule}}@else - @endif</div>
              </div>
              @endif
              
              
              @foreach($parameters_title as $param)
              <div class="col-md-3">
                <div class="property-name">{{ $param->name }}</div>
                <div class="property-value">{{ $param->value }}</div>
              </div>
              @endforeach
          </div>
          
          @if(count($parameters_seguridad))
          <br>
          <h5 style="margin-bottom: 0px;">Seguridad</h5>
          <div class="row params-container">
              @foreach($parameters_seguridad as $param)
              <div class="col-md-3">
                <div class="param"><i class="fa fa-check"></i> {{ $param->name }}</div>
              </div>
              @endforeach
            </div>
          @endif
          
          @if(count($parameters_confort))
          <br>
          <h5 style="margin-bottom: 0px;">Confort</h5>
          <div class="row params-container">
              @foreach($parameters_confort as $param)
              <div class="col-md-3">
                <div class="param"><i class="fa fa-check"></i> {{ $param->name }}</div>
              </div>
              @endforeach
            </div>
          @endif
          
          @if(count($parameters_sonido))
          <br>
          <h5 style="margin-bottom: 0px;">Sonido</h5>
          <div class="row params-container">
              @foreach($parameters_sonido as $param)
              <div class="col-md-3">
                <div class="param"><i class="fa fa-check"></i> {{ $param->name }}</div>
              </div>
              @endforeach
            </div>
          @endif
          
          @if(count($parameters_exterior))
          <br>
          <h5 style="margin-bottom: 0px;">Exterior</h5>
          <div class="row params-container">
              @foreach($parameters_exterior as $param)
              <div class="col-md-3">
                <div class="param"><i class="fa fa-check"></i> {{ $param->name }}</div>
              </div>
              @endforeach
            </div>
          @endif
          
          @endif

          @if ($toplevel->parent_id == '2')
          <div class="row properties-container">
              @if($publication->vehicle_type)
              <div class="col-md-3">
                <div class="property-name">Tipo de inmueble</div>
                <div class="property-value">@if($publication->vehicle_type){{ $publication->vehicle_type}}@else - @endif</div>
              </div>
              @endif
              @if($publication->property_age)
              <div class="col-md-3">
                <div class="property-name">Antiguedad</div>
                <div class="property-value">{{ $publication->property_age}}</div>
              </div>
              @endif
              @if($publication->property_environments)
              <div class="col-md-3">
                <div class="property-name">Ambientes</div>
                <div class="property-value">{{ $publication->property_environments}}</div>
              </div>
              @endif
              @if($publication->property_bathrooms)
              <div class="col-md-3">
                <div class="property-name">Baños</div>
                <div class="property-value">{{ $publication->property_bathrooms}}</div>
              </div>
              @endif
              @if($publication->property_bedrooms)
              <div class="col-md-3">
                <div class="property-name">Habitaciones</div>
                <div class="property-value">{{ $publication->property_bedrooms}}</div>
              </div>
              @endif
              @if($publication->property_land_area)
              <div class="col-md-3">
                <div class="property-name">Area de terreno (m²)</div>
                <div class="property-value">{{ $publication->property_land_area}}</div>
              </div>
              @endif
              @if($publication->property_construction_area)
              <div class="col-md-3">
                <div class="property-name">Area de construcción (m²)</div>
                <div class="property-value">{{ $publication->property_construction_area}}</div>
              </div>
              @endif
              @if($publication->vehicle_schedule)
              <div class="col-md-3">
                <div class="property-name">Horario de contacto</div>
                <div class="property-value">@if($publication->vehicle_schedule){{ $publication->vehicle_schedule}}@else - @endif</div>
              </div>
              @endif
              @if(count($parameters_title))
              @foreach($parameters_title as $param)
              <div class="col-md-3">
                <div class="property-name">{{ $param->name }}</div>
                <div class="property-value">{{ $param->value }}</div>
              </div>
              @endforeach
              @endif
          </div>
          
          @if(count($parameters_ambientes))
          <br>
          <h5 style="margin-bottom: 0px;">Ambientes</h5>
          <div class="row params-container">
              @foreach($parameters_ambientes as $param)
              <div class="col-md-3">
                <div class="param"><i class="fa fa-check"></i> {{ $param->name }}</div>
              </div>
              @endforeach
            </div>
          @endif
          
          @if(count($parameters_comodidades))
          <br>
          <h5 style="margin-bottom: 0px;">Comodidades</h5>
          <div class="row params-container">
              @foreach($parameters_comodidades as $param)
              <div class="col-md-3">
                <div class="param"><i class="fa fa-check"></i> {{ $param->name }}</div>
              </div>
              @endforeach
            </div>
          @endif
          
          @if(count($parameters_caracteristicas))
          <br>
          <h5 style="margin-bottom: 0px;">Características Adicionales</h5>
          <div class="row params-container">
              @foreach($parameters_caracteristicas as $param)
              <div class="col-md-3">
                <div class="param"><i class="fa fa-check"></i> {{ $param->name }}</div>
              </div>
              @endforeach
            </div>
          @endif
          
          @endif
          <!--end property-->

          @if ($toplevel->parent_id == '3')
          
          @if(count($parameters_title))
          <div class="row properties-container">
              @foreach($parameters_title as $param)
              <div class="col-md-3">
                <div class="property-name">{{ $param->name }}</div>
                <div class="property-value">{{ $param->value }}</div>
              </div>
              @endforeach
          </div>
          @endif
          @if(count($parameters_zonas))
          <br>
          <h5 style="margin-bottom: 0px;">Zonas de Cobertura</h5>
          <div class="row params-container">
              @foreach($parameters_zonas as $param)
              <div class="col-md-3">
                <div class="param"><i class="fa fa-check"></i> {{ $param->name }}</div>
              </div>
              @endforeach
            </div>
          @endif
          @endif
          
          @if(!empty($publication->description_html))
          <br><br>
          <div class="row">
              <div class="col-xs-12" style="margin-bottom: 20px">
                  <h3 style="text-align:center">Descripción</h3>
              </div>
          </div>
          <div claentragass="row">
              <div class="col-sm-8 col-sm-offset-2">
                  {!! $publication->description_html !!}
              </div>
          </div>
          @endif
          
          @if ($toplevel->parent_id == '4')
            <br>
            <br>
            <div class="row" id="shipping">
                <div class="col-xs-12">
                    <h3 style="text-align:center">Métodos de Envío</h3>
                    <div style="text-align:center">
                      <img src="/img/assets/schedule-shipping.png">
                      @if ($publication->personal_pickup)
                      <img src="/img/assets/personal-pickup.png">
                      @endif
                      @if ($publication->free_shipment)
                      <img src="/img/assets/free-shipping.png">
                      @endif
                      <div class="arrow-box">
                        @if (!$publication->free_shipment)
                        <h3 style="text-align:center">Entrega a acordar con el vendedor</h3>
                        @else
                        <h3 style="text-align:center">Envío gratis a todo el país</h3>
                        @endif
                        <div style="font-size:16px; font-weight:bold;margin-bottom:8px;margin-top:15px">Ubicación</div>
                        <div style="font-size:14px"><i class="fa fa-map-marker" style="margin-right:10px;font-size:20px"></i>{{ $publication->location() }}</div>
                        @if (count($shipping_costs) and !$publication->free_shipment)
                        <div style="font-size:16px; font-weight:bold;margin-bottom:8px;margin-top:15px">Costos de envío</div>
                        <div class="table-responsive">
                          <table class="table table-bordered table-shipping-costs">
                            <tbody>
                              @foreach ($shipping_costs as $cost)
                              <tr><td style="width:80%">{{ $cost->name }}</td><td class="costs">{{ number_format($cost->cost,2,',','.') }}</td></tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                        @endif
                        <div style="font-size:16px; font-weight:bold;margin-bottom:8px;margin-top:15px">¿Cómo debo coordinar la entrega del producto?</div>
                        <div class="line-desc"><span class="label label-default" style="margin-right: 10px">1</span>Una vez realices la compra, recibirás los datos del vendedor.</div>
                        <div class="line-desc"><span class="label label-default" style="margin-right: 10px">2</span>Contáctalo para acordar la fecha y el lugar de la entrega del producto.</div>

                      </div>
                    </div>
                </div>
            </div>
            @endif
            <br>
            <br>
          @if ($toplevel->parent_id == '4')
          <div class="row">
                @if($publication->status == 'Active')
                @if(!$authenticated)
                <div class="col-xs-12" id="question">
                <h3 style="text-align:center">Preguntas</h3>
                <div class="col-xs-12 register-col">
                    <span class="register-text">¿Deseas realizar una pregunta al vendedor?</span> <a href="{{ route('login') }}"><button class="btn btn-primary">Inicia Sesión</button></a>
                </div>
                </div>
                @elseif(Auth::user()->id != $publication->user_id)
                <div class="col-xs-12" id="question">
                    <h3 style="text-align:center">Preguntas</h3>
            		<div class="form-group has-success has-feedback">
            		  <div class="input-group">
            			<span class="input-group-addon"><i class="fa fa-comment"></i></span>
            			<textarea  class="form-control" id="question-input" aria-describedby="question-input"></textarea>
            		  </div>
            		</div>
            		<button class="btn btn-primary question-controls" disabled="true" style="float:left; display:none" id="btn-question">Preguntar</button>
                        <div class="question-controls" style="display:none; float:left; margin-left:10px;"><small>No ingreses datos de contacto (correos, números telefónicos, sitios web, etc.), no uses lenguaje vulgar ni ofertes o preguntes por otro artículo.</small></div>
            		<div style="float:right; display:none" id="char-question" class="question-controls">Quedan <span id="num-chars">500</span> caracteres.</div>
                        
                </div>
                @endif  
                @endif
          </div>
          <div class="row" id="question-message" style="margin-top: 10px;">
          </div>
          <div  id="questions">
            @foreach ($questions as $question)
            <div class="row">
                <div class="col-xs-12 user-question"><i class="fa fa-comment"></i>
                  @if ($question->banned_question == 0)
                  {{ $question->question }}
                  @else
                    <span class="label label-warning lb-lg">Esta pregunta fue eliminada por infringir las normas del sitio.</span>
                  @endif
                </div>
                @if (!empty($question->answer))
                <div class="col-xs-12 user-answer">
                  <i class="fa fa-comments"></i>
                  @if ($question->banned_answer == 0)
                    {{ $question->answer }} <small style="color: #666"> - {{ date('d/m/y H:i', strtotime($question->updated_at)) }}</small>
                  @else
                    <span class="label label-warning lb-lg">Esta respuesta fue eliminada por infringir las normas del sitio.</span>
                  @endif
                </div>
                @endif
            </div>
            <input type="hidden" id="latest-question-id" value="{{ $question->id }}">
            @endforeach
          </div>
          @if(count($questions) >= 15)
          <button class="btn btn-link" id="watch-more-questions">Ver más preguntas</button>
          @endif

          @endif

          
        @if (count($suggestions) > 0)
        <div style="height:30px;"></div>
      <div class="row featuredProducts">
        <div class="col-xs-12">
          <div class="page-header">
                <h4>Recomendaciones en <span>{{ $category->parentCategory->name }} - {{ $category->name }}</span></h4>
            </div>
            </div>
            <div class="col-xs-12">
              <div class="owl-carousel featuredProductsSlider">

                @foreach ($suggestions as $pub)
                <div class="slide">
                  <div class="productImage clearfix">
                    @if (count($pub->images) > 0)
                    <img src="{{ url('/photo/120x120?url='.urlencode($pub->images[0]->name)) }}" alt="{{ $pub->title }}">
                    @else
                    <img src="{{ url('/photo/120x120?url=nophoto.jpg') }}">
                    @endif
                    <div class="productMasking">
                      <ul class="list-inline btn-group" role="group">
                        <li><a href="{{ ($authenticated? 'javascript:void(0))' : url('favorite', [$pub->id])) }}" class="btn btn-default btn-favorite pub-{{ $pub->id }} {{ ($pub->isFavorite() ? 'btn-favorite-marked':'') }}" data-pub-id="{{ $pub->id }}"><i class="fa fa-heart"></i></a></li>
                      </ul>
                        <a href="{{ route('product',[$pub->id]) }}"><p class="title">{{ $pub->title }}</p></a>
                    </div>
                  </div>
                  <div class="productCaption clearfix">
                    @if($pub->price == '0')
                    <a href="{{ route('product',[$pub->id]) }}"><h3 style="text-transform: none;font-size: 16px;">Precio a convenir</h3></a>
                    @else
                    <a href="{{ route('product',[$pub->id]) }}"><h3>S/{{ number_format($pub->price, 2, ',', '.') }}</h3></a>
                    @endif
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        @endif
        
        @if ($toplevel->parent_id == '4')
        @if (count($bestsellers) > 0)
        <div style="height:30px;"></div>
          <div class="row featuredProducts">
            <div class="col-xs-12">
              <div class="page-header">
                <h4>Los más vendidos en <span>{{ $toplevel->name }}</span></h4>
              </div>
            </div>
            <div class="col-xs-12">
              <div class="owl-carousel featuredProductsSlider">

                @foreach ($bestsellers as $pub)
                <div class="slide">
                  <div class="productImage clearfix">
                    @if (count($pub->images) > 0)
                    <img src="{{ url('/photo/120x120?url='.urlencode($pub->images[0]->name)) }}" alt="{{ $pub->title }}">
                    @else
                    <img src="{{ url('/photo/120x120?url=nophoto.jpg') }}">
                    @endif
                    <div class="productMasking">
                      <ul class="list-inline btn-group" role="group">
                        <li><a href="{{ ($authenticated? 'javascript:void(0))' : url('favorite', [$pub->id])) }}" class="btn btn-default btn-favorite pub-{{ $pub->id }} {{ ($pub->isFavorite() ? 'btn-favorite-marked':'') }}" data-pub-id="{{ $pub->id }}"><i class="fa fa-heart"></i></a></li>
                      </ul>
                        <a href="{{ route('product',[$pub->id]) }}"><p class="title">{{ $pub->title }}</p></a>
                    </div>
                  </div>
                  <div class="productCaption clearfix">
                    @if($pub->price == '0')
                    <a href="{{ route('product',[$pub->id]) }}"><h3 style="text-transform: none;font-size: 16px;">Precio a convenir</h3></a>
                    @else
                    <a href="{{ route('product',[$pub->id]) }}"><h3>S/{{ number_format($pub->price, 2, ',', '.') }}</h3></a>
                    @endif
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        @endif
        @else
        @if (count($latest) > 0)
        <div style="height:30px;"></div>
          <div class="row featuredProducts">
            <div class="col-xs-12">
              <div class="page-header">
                <h4>Últimos publicados en <span>{{ $toplevel->name }}</span></h4>
              </div>
            </div>
            <div class="col-xs-12">
              <div class="owl-carousel featuredProductsSlider">

                @foreach ($latest as $pub)
                <div class="slide">
                  <div class="productImage clearfix">
                    @if (count($pub->images) > 0)
                    <img src="{{ url('/photo/120x120?url='.urlencode($pub->images[0]->name)) }}" alt="{{ $pub->title }}">
                    @else
                    <img src="{{ url('/photo/120x120?url=nophoto.jpg') }}">
                    @endif
                    <div class="productMasking">
                      <ul class="list-inline btn-group" role="group">
                        <li><a href="{{ ($authenticated? 'javascript:void(0))' : url('favorite', [$pub->id])) }}" class="btn btn-default btn-favorite pub-{{ $pub->id }} {{ ($pub->isFavorite() ? 'btn-favorite-marked':'') }}" data-pub-id="{{ $pub->id }}"><i class="fa fa-heart"></i></a></li>
                      </ul>
                        <a href="{{ route('product',[$pub->id]) }}"><p class="title">{{ $pub->title }}</p></a>
                    </div>
                  </div>
                  <div class="productCaption clearfix">
                    @if($pub->price == '0')
                    <a href="{{ route('product',[$pub->id]) }}"><h3 style="text-transform: none;font-size: 16px;">Precio a convenir</h3></a>
                    @else
                    <a href="{{ route('product',[$pub->id]) }}"><h3>S/{{ number_format($pub->price, 2, ',', '.') }}</h3></a>
                    @endif
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        @endif
        @endif

        </div>
      </section>


@if (Auth::check())
      <div class="modal fade" id="mod-confirm-modal" tabindex="-1" role="dialog" aria-labelledby="mod-confirm-modal-label">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <form id="msg-form" method="post" role="form">
                      {{ csrf_field() }}
                      <div class="modal-header mod-modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="mod-phone-modal-label">{{ Auth::user()->first_name }}, Confirma tu compra</h4>
                      </div>
                      <div class="modal-body">
                          <div class="form-group sandbox">
                            <div class="row">
                              <div class="col-xs-12" id="message-modal"></div>
                            </div>
                            <div class="row">
                              <div class="col-xs-8">
                                <div>Cantidad: <label>@if($publication->available_quantity == 1)¡Único Disponible!@else <span id="show-quantity">1</span> @endif</label></div>
                                <div>E-mail: <span style="color:#999;font-family: sans-serif;">{{ Auth::user()->email }}</span><br>
                                    Nombre: <span style="color:#999;font-family: sans-serif;">{{ $publication->user->username }}</span><br>
                                    Número de Telefono: <span style="color:#999;font-family: sans-serif;">{{ $publication->user->phone_number }}</span><br>

                                    <a href="{{ url('logout') }}" class="btn btn-link">Soy otro usuario</a></div>
                                <br><br>

                                <div style="text-align:center" id="confirm-modal" style="font-size: 18px;"><a href="javascript: void(0)" class="btn btn-primary" id="confirm-button">Confirmar Compra</a>
                                <img src="/img/assets/ajax-loader.gif" id="spinner2"></div>
                              </div>
                              <div class="col-xs-4">
                                <div class="productImage clearfix">
                                  @if (count($publication->images) > 0)
                                  <img src="{{ url('/photo/120x120?url='.urlencode($publication->images[0]->name)) }}" alt="{{ $publication->title }}">
                                  @else
                                  <img src="{{ url('/photo/120x120?url=nophoto.jpg') }}">
                                  @endif
                                </div>
                                <div class="productCaption clearfix">
                                <h5>{{ $publication->title }}</h5>
                                <h3>S/{{ number_format($publication->price,2,',','.') }}</h3>
                                </div></div>
                            </div>
                          </div>
                      </div>
                      <div class="modal-footer">
                          <button id="cancel-msg" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                      </div>
                  </form>
              </div>
          </div>
      </div>
      @endif

@endsection

@section('scripts')
	@parent
    <script>
        $(document).ready(function() {
            
            $("#spinner2").hide();
            $(".btn-favorite").click(function() {
                var anchor = $(this);
                $.ajax({
                    method: "POST",
                    url: "{{url('/favorite')}}",
                    cache: false,
                    data: { id: anchor.attr('data-pub-id'), _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
                    if(msg.is_ok) {
                        if(msg.value) {
                            $(".pub-"+anchor.attr('data-pub-id')).addClass('btn-favorite-marked');
                        } else {
                            $(".pub-"+anchor.attr('data-pub-id')).removeClass('btn-favorite-marked');
                        }
                    }
                });
            });
            var spinner = $( "#spinner" ).spinner({ min: 1, max: '{{ $publication->available_quantity }}' });
            $('.ui-spinner-button').click(function() { $(this).siblings('input').change(); });

            $('#spinner').spinner().change(function(){
                 $("#show-quantity").text($(this).spinner('value') || '1');
            });

            $( "#disable" ).on( "click", function() {
              if ( spinner.spinner( "option", "disabled" ) ) {
                spinner.spinner( "enable" );
              } else {
                spinner.spinner( "disable" );
              }
            });
            $( "#destroy" ).on( "click", function() {
              if ( spinner.spinner( "instance" ) ) {
                spinner.spinner( "destroy" );
              } else {
                spinner.spinner();
              }
            });
            $( "#getvalue" ).on( "click", function() {
              alert( spinner.spinner( "value" ) );
            });
            $( "#setvalue" ).on( "click", function() {
              spinner.spinner( "value", 5 );
            });

            $( "button" ).button();


      			/* question input behavior */
      			$("#question-input").focus(function() {
      				$(this).animate({ height: "120px" }, 300 );
      				$(".question-controls").show()
      			});

      			$("#question-input").blur(function() {
      				$(this).animate({ height: "54px" }, 300 );
      				//$(".question-controls").hide();
      			});

      			var maxChars = 500;
      			$("#question-input").keyup(controlChars);

            function controlChars(e) {
              var rest = maxChars - $(this).val().length;
              if(rest < 0) rest = 0;
              if($(this).val().length > 0) $("#btn-question").prop('disabled', false);
              else $("#btn-question").prop('disabled', true);
              $("#num-chars").text(rest);
      				if (e.which < 0x20) {
      					// e.which < 0x20, then it's not a printable character
      					// e.which === 0 - Not a character
      					return;     // Do nothing
      				}
      				if ($(this).val().length == maxChars) {
      					e.preventDefault();
      				} else if ($(this).val().length > maxChars) {
      					// Maximum exceeded
      					$(this).val($(this).val().substring(0, maxChars));
      				}
      			}

            /* submit question */


            $("#btn-question").click(function() {
                $("#btn-question").attr('disabled', true);
                $.ajax({
                    method: "POST",
                    url: "{{url('/add_question')}}",
                    cache: false,
                    data: { id: '<?php echo $publication->id; ?>', message: $("#question-input").val(), _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
                    if(msg.is_ok) {
                        $("#question-input").val('');
                        $("#question-message").html('<div class="alert alert-success" role="alert">Tu pregunta ha sido enviada.</div>');
                        $("#questions").prepend('<div class="row"><div class="col-xs-12 user-question"><i class="fa fa-comment"></i> '+msg.message+' </div></div>');
                        $("#num-chars").text(maxChars);
                        
                    } else {
                        if(msg.value == 'blocked') {
                            $("#question-message").html('<div class="alert alert-danger" role="alert">Has sido bloqueado por el vendedor para realizar preguntas en sus publicaciones.</div>');
                        } else {
                            $("#question-message").html('<div class="alert alert-danger" role="alert">Hubo un problema al enviar tu pregunta. Recuerda no consignar datos de contacto.</div>');
                        }
                        $("#btn-question").removeAttr('disabled');
                    }
                });
            });

            $("#go-questions").click(function() {
              goToQuestions();
            });
            function goToQuestions() {
              var target = $("#question");

              $('html, body').animate({
                  'scrollTop': target.offset().top-80
              }, 500, 'swing', function () { });
            }

            $("#go-shipping").click(function() {
              goToShipping();
            });
            function goToShipping() {
              var target = $("#shipping");

              $('html, body').animate({
                  'scrollTop': target.offset().top-80
              }, 500, 'swing', function () { });
            }

            /* get more questions */


            $("#watch-more-questions").click(function() {
                $.ajax({
                    method: "POST",
                    url: "{{url('/get_questions')}}",
                    cache: false,
                    data: { id: '<?php echo $publication->id; ?>', latest: $("#latest-question-id").val(), _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
                    if(msg.is_ok) {
                      for(var i in msg.questions) {
                        var item = msg.questions[i];

                        var output = '<div class="row">';
                        output += '<div class="col-xs-12 user-question"><i class="fa fa-comment"></i>';
                        if(item['question'] !== 'XXX') {
                          output += item['question'];
                        } else {
                          output += '<span class="label label-warning lb-lg">Esta pregunta fue eliminada por infringir las normas del sitio.</span>';
                        }
                        output += '</div>';
                        if(item['answer'] !== '') {
                          output += '<div class="col-xs-12 user-answer"><i class="fa fa-comments"></i>';
                          if(item['answer'] !== 'XXX') {
                            output += item['answer'];
                            output += '<small style="color: #666"> - '+item['date']+'</small>' ;
                          } else {
                            output += '<span class="label label-warning lb-lg">Esta respuesta fue eliminada por infringir las normas del sitio.</span>';
                          }
                          output += '</div>';
                        }

                        $("#questions").append(output);
                        $("#latest-question-id").val(item['id']);
                      }

                      if(msg.questions.length < 15) $("#watch-more-questions").hide();
                    }
                });
            });


            $('#mod-confirm-modal').on('show.bs.modal', function (event) {
              var value = '1';
              if(parseInt($("#spinner").val()) > 0) {
                value = $("#spinner").val();
              }
              $("#show-quantity").text(value);
            });

            $("#confirm-button").click(function() {
                var anchor = $(this);
                anchor.attr('disabled','disabled');
                $("#spinner2").show();
                $.ajax({
                    method: "POST",
                    url: "{{url('/confirm-purchase')}}",
                    cache: false,
                    data: { id: {{ $publication->id }}, quantity: $("#spinner").val(), _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
                    if(msg.is_ok) {
                        $("#confirm-modal").html('<i class="fa fa-check-circle-o" style="color: #47bac1;text-align: center;font-size: 24px;margin-right: 10px;"></i>Compra Realizada');
                    } else {
                      $("#message-modal").html('<div class="alert alert-danger" role="alert">Lo sentimos, hubo un problema al confirmar tu compra, recarga la página e inténtalo de nuevo.</div>');
                      anchor.removeAttr('disabled');
                    }
                    
                    $("#spinner2").hide();
                });
            });

            /* submit consultation */

            $("#btn-consultation").click(function() {
                $("#btn-consultation").attr('disabled', true);
                $.ajax({
                    method: "POST",
                    url: "{{url('/send_consultation')}}",
                    cache: false,
                    data: { id: '<?php echo $publication->id; ?>', message: $("#message").val(), _token: '{{ csrf_token() }}'}
                  })
                .done(function( msg ) {
                    if(msg.is_ok) {
                        $("#message").val('');
                        $("#consultation-message").html('<div class="alert alert-success" role="alert">Tu consulta se ha enviado.</div>');
                        //$("#num-chars").text(maxChars);
                        //$("#btn-question").prop('disabled', true);
                    } else { 
                        if(msg.value == 'blocked') {
                            $("#consultation-message").html('<div class="alert alert-danger" role="alert">Has sido bloqueado por el vendedor para realizar consultas en sus publicaciones.</div>');
                        } else {
                            $("#consultation-message").html('<div class="alert alert-danger" role="alert">Hubo un problema al enviar tu consulta.</div>');
                        }
                        $("#btn-consultation").removeAttr('disabled');
                    }
                });
            });
            
            @if($go_questions)
            setTimeout(function(){ goToQuestions(); }, 1000);
            @endif
            
            @if($go_buy)
            setTimeout(function(){ $("#buynow").trigger("click"); }, 1000);
            @endif

        });
    </script>
@endsection
